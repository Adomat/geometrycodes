package de.adrianhoff.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import de.adrianhoff.services.GeometryCodesGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGeometryCodesParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_FLOAT", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'no'", "'nothing'", "'='", "'+='", "'!'", "'-'", "'+'", "'*'", "'/'", "'=='", "'!='", "'>'", "'>='", "'<'", "'<='", "'&&'", "'||'", "'void'", "'float'", "'vector'", "'triangle'", "'quad'", "'mesh'", "'import'", "'('", "')'", "'{'", "'}'", "','", "'iterator'", "'over'", "'each'", "'material'", "':'", "'return'", "'if'", "'for'", "'from'", "'to'", "'in'", "'while'", "'output'", "'.'", "'vertex'", "'empty'", "'false'", "'yes'", "'true'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int RULE_FLOAT=6;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalGeometryCodesParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGeometryCodesParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGeometryCodesParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGeometryCodes.g"; }


    	private GeometryCodesGrammarAccess grammarAccess;

    	public void setGrammarAccess(GeometryCodesGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleFile"
    // InternalGeometryCodes.g:53:1: entryRuleFile : ruleFile EOF ;
    public final void entryRuleFile() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:54:1: ( ruleFile EOF )
            // InternalGeometryCodes.g:55:1: ruleFile EOF
            {
             before(grammarAccess.getFileRule()); 
            pushFollow(FOLLOW_1);
            ruleFile();

            state._fsp--;

             after(grammarAccess.getFileRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFile"


    // $ANTLR start "ruleFile"
    // InternalGeometryCodes.g:62:1: ruleFile : ( ( rule__File__Group__0 ) ) ;
    public final void ruleFile() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:66:2: ( ( ( rule__File__Group__0 ) ) )
            // InternalGeometryCodes.g:67:2: ( ( rule__File__Group__0 ) )
            {
            // InternalGeometryCodes.g:67:2: ( ( rule__File__Group__0 ) )
            // InternalGeometryCodes.g:68:3: ( rule__File__Group__0 )
            {
             before(grammarAccess.getFileAccess().getGroup()); 
            // InternalGeometryCodes.g:69:3: ( rule__File__Group__0 )
            // InternalGeometryCodes.g:69:4: rule__File__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__File__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFileAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFile"


    // $ANTLR start "entryRuleImportStatement"
    // InternalGeometryCodes.g:78:1: entryRuleImportStatement : ruleImportStatement EOF ;
    public final void entryRuleImportStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:79:1: ( ruleImportStatement EOF )
            // InternalGeometryCodes.g:80:1: ruleImportStatement EOF
            {
             before(grammarAccess.getImportStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleImportStatement();

            state._fsp--;

             after(grammarAccess.getImportStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalGeometryCodes.g:87:1: ruleImportStatement : ( ( rule__ImportStatement__Group__0 ) ) ;
    public final void ruleImportStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:91:2: ( ( ( rule__ImportStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:92:2: ( ( rule__ImportStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:92:2: ( ( rule__ImportStatement__Group__0 ) )
            // InternalGeometryCodes.g:93:3: ( rule__ImportStatement__Group__0 )
            {
             before(grammarAccess.getImportStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:94:3: ( rule__ImportStatement__Group__0 )
            // InternalGeometryCodes.g:94:4: rule__ImportStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleFunctionDeclaration"
    // InternalGeometryCodes.g:103:1: entryRuleFunctionDeclaration : ruleFunctionDeclaration EOF ;
    public final void entryRuleFunctionDeclaration() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:104:1: ( ruleFunctionDeclaration EOF )
            // InternalGeometryCodes.g:105:1: ruleFunctionDeclaration EOF
            {
             before(grammarAccess.getFunctionDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleFunctionDeclaration();

            state._fsp--;

             after(grammarAccess.getFunctionDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunctionDeclaration"


    // $ANTLR start "ruleFunctionDeclaration"
    // InternalGeometryCodes.g:112:1: ruleFunctionDeclaration : ( ( rule__FunctionDeclaration__Group__0 ) ) ;
    public final void ruleFunctionDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:116:2: ( ( ( rule__FunctionDeclaration__Group__0 ) ) )
            // InternalGeometryCodes.g:117:2: ( ( rule__FunctionDeclaration__Group__0 ) )
            {
            // InternalGeometryCodes.g:117:2: ( ( rule__FunctionDeclaration__Group__0 ) )
            // InternalGeometryCodes.g:118:3: ( rule__FunctionDeclaration__Group__0 )
            {
             before(grammarAccess.getFunctionDeclarationAccess().getGroup()); 
            // InternalGeometryCodes.g:119:3: ( rule__FunctionDeclaration__Group__0 )
            // InternalGeometryCodes.g:119:4: rule__FunctionDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunctionDeclaration"


    // $ANTLR start "entryRuleIteratorDeclaration"
    // InternalGeometryCodes.g:128:1: entryRuleIteratorDeclaration : ruleIteratorDeclaration EOF ;
    public final void entryRuleIteratorDeclaration() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:129:1: ( ruleIteratorDeclaration EOF )
            // InternalGeometryCodes.g:130:1: ruleIteratorDeclaration EOF
            {
             before(grammarAccess.getIteratorDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleIteratorDeclaration();

            state._fsp--;

             after(grammarAccess.getIteratorDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIteratorDeclaration"


    // $ANTLR start "ruleIteratorDeclaration"
    // InternalGeometryCodes.g:137:1: ruleIteratorDeclaration : ( ( rule__IteratorDeclaration__Group__0 ) ) ;
    public final void ruleIteratorDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:141:2: ( ( ( rule__IteratorDeclaration__Group__0 ) ) )
            // InternalGeometryCodes.g:142:2: ( ( rule__IteratorDeclaration__Group__0 ) )
            {
            // InternalGeometryCodes.g:142:2: ( ( rule__IteratorDeclaration__Group__0 ) )
            // InternalGeometryCodes.g:143:3: ( rule__IteratorDeclaration__Group__0 )
            {
             before(grammarAccess.getIteratorDeclarationAccess().getGroup()); 
            // InternalGeometryCodes.g:144:3: ( rule__IteratorDeclaration__Group__0 )
            // InternalGeometryCodes.g:144:4: rule__IteratorDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIteratorDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIteratorDeclaration"


    // $ANTLR start "entryRuleParameter"
    // InternalGeometryCodes.g:153:1: entryRuleParameter : ruleParameter EOF ;
    public final void entryRuleParameter() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:154:1: ( ruleParameter EOF )
            // InternalGeometryCodes.g:155:1: ruleParameter EOF
            {
             before(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_1);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getParameterRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalGeometryCodes.g:162:1: ruleParameter : ( ( rule__Parameter__Group__0 ) ) ;
    public final void ruleParameter() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:166:2: ( ( ( rule__Parameter__Group__0 ) ) )
            // InternalGeometryCodes.g:167:2: ( ( rule__Parameter__Group__0 ) )
            {
            // InternalGeometryCodes.g:167:2: ( ( rule__Parameter__Group__0 ) )
            // InternalGeometryCodes.g:168:3: ( rule__Parameter__Group__0 )
            {
             before(grammarAccess.getParameterAccess().getGroup()); 
            // InternalGeometryCodes.g:169:3: ( rule__Parameter__Group__0 )
            // InternalGeometryCodes.g:169:4: rule__Parameter__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleMaterialDeclaration"
    // InternalGeometryCodes.g:178:1: entryRuleMaterialDeclaration : ruleMaterialDeclaration EOF ;
    public final void entryRuleMaterialDeclaration() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:179:1: ( ruleMaterialDeclaration EOF )
            // InternalGeometryCodes.g:180:1: ruleMaterialDeclaration EOF
            {
             before(grammarAccess.getMaterialDeclarationRule()); 
            pushFollow(FOLLOW_1);
            ruleMaterialDeclaration();

            state._fsp--;

             after(grammarAccess.getMaterialDeclarationRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMaterialDeclaration"


    // $ANTLR start "ruleMaterialDeclaration"
    // InternalGeometryCodes.g:187:1: ruleMaterialDeclaration : ( ( rule__MaterialDeclaration__Group__0 ) ) ;
    public final void ruleMaterialDeclaration() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:191:2: ( ( ( rule__MaterialDeclaration__Group__0 ) ) )
            // InternalGeometryCodes.g:192:2: ( ( rule__MaterialDeclaration__Group__0 ) )
            {
            // InternalGeometryCodes.g:192:2: ( ( rule__MaterialDeclaration__Group__0 ) )
            // InternalGeometryCodes.g:193:3: ( rule__MaterialDeclaration__Group__0 )
            {
             before(grammarAccess.getMaterialDeclarationAccess().getGroup()); 
            // InternalGeometryCodes.g:194:3: ( rule__MaterialDeclaration__Group__0 )
            // InternalGeometryCodes.g:194:4: rule__MaterialDeclaration__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MaterialDeclaration__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMaterialDeclarationAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMaterialDeclaration"


    // $ANTLR start "entryRuleMaterialProperty"
    // InternalGeometryCodes.g:203:1: entryRuleMaterialProperty : ruleMaterialProperty EOF ;
    public final void entryRuleMaterialProperty() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:204:1: ( ruleMaterialProperty EOF )
            // InternalGeometryCodes.g:205:1: ruleMaterialProperty EOF
            {
             before(grammarAccess.getMaterialPropertyRule()); 
            pushFollow(FOLLOW_1);
            ruleMaterialProperty();

            state._fsp--;

             after(grammarAccess.getMaterialPropertyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMaterialProperty"


    // $ANTLR start "ruleMaterialProperty"
    // InternalGeometryCodes.g:212:1: ruleMaterialProperty : ( ( rule__MaterialProperty__Alternatives ) ) ;
    public final void ruleMaterialProperty() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:216:2: ( ( ( rule__MaterialProperty__Alternatives ) ) )
            // InternalGeometryCodes.g:217:2: ( ( rule__MaterialProperty__Alternatives ) )
            {
            // InternalGeometryCodes.g:217:2: ( ( rule__MaterialProperty__Alternatives ) )
            // InternalGeometryCodes.g:218:3: ( rule__MaterialProperty__Alternatives )
            {
             before(grammarAccess.getMaterialPropertyAccess().getAlternatives()); 
            // InternalGeometryCodes.g:219:3: ( rule__MaterialProperty__Alternatives )
            // InternalGeometryCodes.g:219:4: rule__MaterialProperty__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__MaterialProperty__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getMaterialPropertyAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMaterialProperty"


    // $ANTLR start "entryRuleMaterialStringProperty"
    // InternalGeometryCodes.g:228:1: entryRuleMaterialStringProperty : ruleMaterialStringProperty EOF ;
    public final void entryRuleMaterialStringProperty() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:229:1: ( ruleMaterialStringProperty EOF )
            // InternalGeometryCodes.g:230:1: ruleMaterialStringProperty EOF
            {
             before(grammarAccess.getMaterialStringPropertyRule()); 
            pushFollow(FOLLOW_1);
            ruleMaterialStringProperty();

            state._fsp--;

             after(grammarAccess.getMaterialStringPropertyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMaterialStringProperty"


    // $ANTLR start "ruleMaterialStringProperty"
    // InternalGeometryCodes.g:237:1: ruleMaterialStringProperty : ( ( rule__MaterialStringProperty__Group__0 ) ) ;
    public final void ruleMaterialStringProperty() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:241:2: ( ( ( rule__MaterialStringProperty__Group__0 ) ) )
            // InternalGeometryCodes.g:242:2: ( ( rule__MaterialStringProperty__Group__0 ) )
            {
            // InternalGeometryCodes.g:242:2: ( ( rule__MaterialStringProperty__Group__0 ) )
            // InternalGeometryCodes.g:243:3: ( rule__MaterialStringProperty__Group__0 )
            {
             before(grammarAccess.getMaterialStringPropertyAccess().getGroup()); 
            // InternalGeometryCodes.g:244:3: ( rule__MaterialStringProperty__Group__0 )
            // InternalGeometryCodes.g:244:4: rule__MaterialStringProperty__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MaterialStringProperty__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMaterialStringPropertyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMaterialStringProperty"


    // $ANTLR start "entryRuleMaterialFloatProperty"
    // InternalGeometryCodes.g:253:1: entryRuleMaterialFloatProperty : ruleMaterialFloatProperty EOF ;
    public final void entryRuleMaterialFloatProperty() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:254:1: ( ruleMaterialFloatProperty EOF )
            // InternalGeometryCodes.g:255:1: ruleMaterialFloatProperty EOF
            {
             before(grammarAccess.getMaterialFloatPropertyRule()); 
            pushFollow(FOLLOW_1);
            ruleMaterialFloatProperty();

            state._fsp--;

             after(grammarAccess.getMaterialFloatPropertyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMaterialFloatProperty"


    // $ANTLR start "ruleMaterialFloatProperty"
    // InternalGeometryCodes.g:262:1: ruleMaterialFloatProperty : ( ( rule__MaterialFloatProperty__Group__0 ) ) ;
    public final void ruleMaterialFloatProperty() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:266:2: ( ( ( rule__MaterialFloatProperty__Group__0 ) ) )
            // InternalGeometryCodes.g:267:2: ( ( rule__MaterialFloatProperty__Group__0 ) )
            {
            // InternalGeometryCodes.g:267:2: ( ( rule__MaterialFloatProperty__Group__0 ) )
            // InternalGeometryCodes.g:268:3: ( rule__MaterialFloatProperty__Group__0 )
            {
             before(grammarAccess.getMaterialFloatPropertyAccess().getGroup()); 
            // InternalGeometryCodes.g:269:3: ( rule__MaterialFloatProperty__Group__0 )
            // InternalGeometryCodes.g:269:4: rule__MaterialFloatProperty__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MaterialFloatProperty__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMaterialFloatPropertyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMaterialFloatProperty"


    // $ANTLR start "entryRuleMaterialBooleanProperty"
    // InternalGeometryCodes.g:278:1: entryRuleMaterialBooleanProperty : ruleMaterialBooleanProperty EOF ;
    public final void entryRuleMaterialBooleanProperty() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:279:1: ( ruleMaterialBooleanProperty EOF )
            // InternalGeometryCodes.g:280:1: ruleMaterialBooleanProperty EOF
            {
             before(grammarAccess.getMaterialBooleanPropertyRule()); 
            pushFollow(FOLLOW_1);
            ruleMaterialBooleanProperty();

            state._fsp--;

             after(grammarAccess.getMaterialBooleanPropertyRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMaterialBooleanProperty"


    // $ANTLR start "ruleMaterialBooleanProperty"
    // InternalGeometryCodes.g:287:1: ruleMaterialBooleanProperty : ( ( rule__MaterialBooleanProperty__Group__0 ) ) ;
    public final void ruleMaterialBooleanProperty() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:291:2: ( ( ( rule__MaterialBooleanProperty__Group__0 ) ) )
            // InternalGeometryCodes.g:292:2: ( ( rule__MaterialBooleanProperty__Group__0 ) )
            {
            // InternalGeometryCodes.g:292:2: ( ( rule__MaterialBooleanProperty__Group__0 ) )
            // InternalGeometryCodes.g:293:3: ( rule__MaterialBooleanProperty__Group__0 )
            {
             before(grammarAccess.getMaterialBooleanPropertyAccess().getGroup()); 
            // InternalGeometryCodes.g:294:3: ( rule__MaterialBooleanProperty__Group__0 )
            // InternalGeometryCodes.g:294:4: rule__MaterialBooleanProperty__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MaterialBooleanProperty__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMaterialBooleanPropertyAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMaterialBooleanProperty"


    // $ANTLR start "entryRuleStatement"
    // InternalGeometryCodes.g:303:1: entryRuleStatement : ruleStatement EOF ;
    public final void entryRuleStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:304:1: ( ruleStatement EOF )
            // InternalGeometryCodes.g:305:1: ruleStatement EOF
            {
             before(grammarAccess.getStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // InternalGeometryCodes.g:312:1: ruleStatement : ( ( rule__Statement__Alternatives ) ) ;
    public final void ruleStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:316:2: ( ( ( rule__Statement__Alternatives ) ) )
            // InternalGeometryCodes.g:317:2: ( ( rule__Statement__Alternatives ) )
            {
            // InternalGeometryCodes.g:317:2: ( ( rule__Statement__Alternatives ) )
            // InternalGeometryCodes.g:318:3: ( rule__Statement__Alternatives )
            {
             before(grammarAccess.getStatementAccess().getAlternatives()); 
            // InternalGeometryCodes.g:319:3: ( rule__Statement__Alternatives )
            // InternalGeometryCodes.g:319:4: rule__Statement__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Statement__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getStatementAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleDeclarationStatement"
    // InternalGeometryCodes.g:328:1: entryRuleDeclarationStatement : ruleDeclarationStatement EOF ;
    public final void entryRuleDeclarationStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:329:1: ( ruleDeclarationStatement EOF )
            // InternalGeometryCodes.g:330:1: ruleDeclarationStatement EOF
            {
             before(grammarAccess.getDeclarationStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleDeclarationStatement();

            state._fsp--;

             after(grammarAccess.getDeclarationStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclarationStatement"


    // $ANTLR start "ruleDeclarationStatement"
    // InternalGeometryCodes.g:337:1: ruleDeclarationStatement : ( ( rule__DeclarationStatement__Group__0 ) ) ;
    public final void ruleDeclarationStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:341:2: ( ( ( rule__DeclarationStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:342:2: ( ( rule__DeclarationStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:342:2: ( ( rule__DeclarationStatement__Group__0 ) )
            // InternalGeometryCodes.g:343:3: ( rule__DeclarationStatement__Group__0 )
            {
             before(grammarAccess.getDeclarationStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:344:3: ( rule__DeclarationStatement__Group__0 )
            // InternalGeometryCodes.g:344:4: rule__DeclarationStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__DeclarationStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclarationStatement"


    // $ANTLR start "entryRuleAssignmentStatement"
    // InternalGeometryCodes.g:353:1: entryRuleAssignmentStatement : ruleAssignmentStatement EOF ;
    public final void entryRuleAssignmentStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:354:1: ( ruleAssignmentStatement EOF )
            // InternalGeometryCodes.g:355:1: ruleAssignmentStatement EOF
            {
             before(grammarAccess.getAssignmentStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleAssignmentStatement();

            state._fsp--;

             after(grammarAccess.getAssignmentStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAssignmentStatement"


    // $ANTLR start "ruleAssignmentStatement"
    // InternalGeometryCodes.g:362:1: ruleAssignmentStatement : ( ( rule__AssignmentStatement__Group__0 ) ) ;
    public final void ruleAssignmentStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:366:2: ( ( ( rule__AssignmentStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:367:2: ( ( rule__AssignmentStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:367:2: ( ( rule__AssignmentStatement__Group__0 ) )
            // InternalGeometryCodes.g:368:3: ( rule__AssignmentStatement__Group__0 )
            {
             before(grammarAccess.getAssignmentStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:369:3: ( rule__AssignmentStatement__Group__0 )
            // InternalGeometryCodes.g:369:4: rule__AssignmentStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AssignmentStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAssignmentStatement"


    // $ANTLR start "entryRuleReferenceStatement"
    // InternalGeometryCodes.g:378:1: entryRuleReferenceStatement : ruleReferenceStatement EOF ;
    public final void entryRuleReferenceStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:379:1: ( ruleReferenceStatement EOF )
            // InternalGeometryCodes.g:380:1: ruleReferenceStatement EOF
            {
             before(grammarAccess.getReferenceStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleReferenceStatement();

            state._fsp--;

             after(grammarAccess.getReferenceStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReferenceStatement"


    // $ANTLR start "ruleReferenceStatement"
    // InternalGeometryCodes.g:387:1: ruleReferenceStatement : ( ( rule__ReferenceStatement__ReferenceAssignment ) ) ;
    public final void ruleReferenceStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:391:2: ( ( ( rule__ReferenceStatement__ReferenceAssignment ) ) )
            // InternalGeometryCodes.g:392:2: ( ( rule__ReferenceStatement__ReferenceAssignment ) )
            {
            // InternalGeometryCodes.g:392:2: ( ( rule__ReferenceStatement__ReferenceAssignment ) )
            // InternalGeometryCodes.g:393:3: ( rule__ReferenceStatement__ReferenceAssignment )
            {
             before(grammarAccess.getReferenceStatementAccess().getReferenceAssignment()); 
            // InternalGeometryCodes.g:394:3: ( rule__ReferenceStatement__ReferenceAssignment )
            // InternalGeometryCodes.g:394:4: rule__ReferenceStatement__ReferenceAssignment
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceStatement__ReferenceAssignment();

            state._fsp--;


            }

             after(grammarAccess.getReferenceStatementAccess().getReferenceAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReferenceStatement"


    // $ANTLR start "entryRuleReturnStatement"
    // InternalGeometryCodes.g:403:1: entryRuleReturnStatement : ruleReturnStatement EOF ;
    public final void entryRuleReturnStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:404:1: ( ruleReturnStatement EOF )
            // InternalGeometryCodes.g:405:1: ruleReturnStatement EOF
            {
             before(grammarAccess.getReturnStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleReturnStatement();

            state._fsp--;

             after(grammarAccess.getReturnStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReturnStatement"


    // $ANTLR start "ruleReturnStatement"
    // InternalGeometryCodes.g:412:1: ruleReturnStatement : ( ( rule__ReturnStatement__Group__0 ) ) ;
    public final void ruleReturnStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:416:2: ( ( ( rule__ReturnStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:417:2: ( ( rule__ReturnStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:417:2: ( ( rule__ReturnStatement__Group__0 ) )
            // InternalGeometryCodes.g:418:3: ( rule__ReturnStatement__Group__0 )
            {
             before(grammarAccess.getReturnStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:419:3: ( rule__ReturnStatement__Group__0 )
            // InternalGeometryCodes.g:419:4: rule__ReturnStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ReturnStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReturnStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReturnStatement"


    // $ANTLR start "entryRuleIfStatement"
    // InternalGeometryCodes.g:428:1: entryRuleIfStatement : ruleIfStatement EOF ;
    public final void entryRuleIfStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:429:1: ( ruleIfStatement EOF )
            // InternalGeometryCodes.g:430:1: ruleIfStatement EOF
            {
             before(grammarAccess.getIfStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleIfStatement();

            state._fsp--;

             after(grammarAccess.getIfStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIfStatement"


    // $ANTLR start "ruleIfStatement"
    // InternalGeometryCodes.g:437:1: ruleIfStatement : ( ( rule__IfStatement__Group__0 ) ) ;
    public final void ruleIfStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:441:2: ( ( ( rule__IfStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:442:2: ( ( rule__IfStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:442:2: ( ( rule__IfStatement__Group__0 ) )
            // InternalGeometryCodes.g:443:3: ( rule__IfStatement__Group__0 )
            {
             before(grammarAccess.getIfStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:444:3: ( rule__IfStatement__Group__0 )
            // InternalGeometryCodes.g:444:4: rule__IfStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__IfStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIfStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIfStatement"


    // $ANTLR start "entryRuleForLoopStatement"
    // InternalGeometryCodes.g:453:1: entryRuleForLoopStatement : ruleForLoopStatement EOF ;
    public final void entryRuleForLoopStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:454:1: ( ruleForLoopStatement EOF )
            // InternalGeometryCodes.g:455:1: ruleForLoopStatement EOF
            {
             before(grammarAccess.getForLoopStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleForLoopStatement();

            state._fsp--;

             after(grammarAccess.getForLoopStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleForLoopStatement"


    // $ANTLR start "ruleForLoopStatement"
    // InternalGeometryCodes.g:462:1: ruleForLoopStatement : ( ( rule__ForLoopStatement__Group__0 ) ) ;
    public final void ruleForLoopStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:466:2: ( ( ( rule__ForLoopStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:467:2: ( ( rule__ForLoopStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:467:2: ( ( rule__ForLoopStatement__Group__0 ) )
            // InternalGeometryCodes.g:468:3: ( rule__ForLoopStatement__Group__0 )
            {
             before(grammarAccess.getForLoopStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:469:3: ( rule__ForLoopStatement__Group__0 )
            // InternalGeometryCodes.g:469:4: rule__ForLoopStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getForLoopStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleForLoopStatement"


    // $ANTLR start "entryRuleForEachLoopStatement"
    // InternalGeometryCodes.g:478:1: entryRuleForEachLoopStatement : ruleForEachLoopStatement EOF ;
    public final void entryRuleForEachLoopStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:479:1: ( ruleForEachLoopStatement EOF )
            // InternalGeometryCodes.g:480:1: ruleForEachLoopStatement EOF
            {
             before(grammarAccess.getForEachLoopStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleForEachLoopStatement();

            state._fsp--;

             after(grammarAccess.getForEachLoopStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleForEachLoopStatement"


    // $ANTLR start "ruleForEachLoopStatement"
    // InternalGeometryCodes.g:487:1: ruleForEachLoopStatement : ( ( rule__ForEachLoopStatement__Group__0 ) ) ;
    public final void ruleForEachLoopStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:491:2: ( ( ( rule__ForEachLoopStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:492:2: ( ( rule__ForEachLoopStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:492:2: ( ( rule__ForEachLoopStatement__Group__0 ) )
            // InternalGeometryCodes.g:493:3: ( rule__ForEachLoopStatement__Group__0 )
            {
             before(grammarAccess.getForEachLoopStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:494:3: ( rule__ForEachLoopStatement__Group__0 )
            // InternalGeometryCodes.g:494:4: rule__ForEachLoopStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getForEachLoopStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleForEachLoopStatement"


    // $ANTLR start "entryRuleWhileLoopStatement"
    // InternalGeometryCodes.g:503:1: entryRuleWhileLoopStatement : ruleWhileLoopStatement EOF ;
    public final void entryRuleWhileLoopStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:504:1: ( ruleWhileLoopStatement EOF )
            // InternalGeometryCodes.g:505:1: ruleWhileLoopStatement EOF
            {
             before(grammarAccess.getWhileLoopStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleWhileLoopStatement();

            state._fsp--;

             after(grammarAccess.getWhileLoopStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWhileLoopStatement"


    // $ANTLR start "ruleWhileLoopStatement"
    // InternalGeometryCodes.g:512:1: ruleWhileLoopStatement : ( ( rule__WhileLoopStatement__Group__0 ) ) ;
    public final void ruleWhileLoopStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:516:2: ( ( ( rule__WhileLoopStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:517:2: ( ( rule__WhileLoopStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:517:2: ( ( rule__WhileLoopStatement__Group__0 ) )
            // InternalGeometryCodes.g:518:3: ( rule__WhileLoopStatement__Group__0 )
            {
             before(grammarAccess.getWhileLoopStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:519:3: ( rule__WhileLoopStatement__Group__0 )
            // InternalGeometryCodes.g:519:4: rule__WhileLoopStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getWhileLoopStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWhileLoopStatement"


    // $ANTLR start "entryRuleOutputStatement"
    // InternalGeometryCodes.g:528:1: entryRuleOutputStatement : ruleOutputStatement EOF ;
    public final void entryRuleOutputStatement() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:529:1: ( ruleOutputStatement EOF )
            // InternalGeometryCodes.g:530:1: ruleOutputStatement EOF
            {
             before(grammarAccess.getOutputStatementRule()); 
            pushFollow(FOLLOW_1);
            ruleOutputStatement();

            state._fsp--;

             after(grammarAccess.getOutputStatementRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOutputStatement"


    // $ANTLR start "ruleOutputStatement"
    // InternalGeometryCodes.g:537:1: ruleOutputStatement : ( ( rule__OutputStatement__Group__0 ) ) ;
    public final void ruleOutputStatement() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:541:2: ( ( ( rule__OutputStatement__Group__0 ) ) )
            // InternalGeometryCodes.g:542:2: ( ( rule__OutputStatement__Group__0 ) )
            {
            // InternalGeometryCodes.g:542:2: ( ( rule__OutputStatement__Group__0 ) )
            // InternalGeometryCodes.g:543:3: ( rule__OutputStatement__Group__0 )
            {
             before(grammarAccess.getOutputStatementAccess().getGroup()); 
            // InternalGeometryCodes.g:544:3: ( rule__OutputStatement__Group__0 )
            // InternalGeometryCodes.g:544:4: rule__OutputStatement__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OutputStatement__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOutputStatementAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOutputStatement"


    // $ANTLR start "entryRuleExpression"
    // InternalGeometryCodes.g:553:1: entryRuleExpression : ruleExpression EOF ;
    public final void entryRuleExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:554:1: ( ruleExpression EOF )
            // InternalGeometryCodes.g:555:1: ruleExpression EOF
            {
             before(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalGeometryCodes.g:562:1: ruleExpression : ( ruleLogicalExpression ) ;
    public final void ruleExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:566:2: ( ( ruleLogicalExpression ) )
            // InternalGeometryCodes.g:567:2: ( ruleLogicalExpression )
            {
            // InternalGeometryCodes.g:567:2: ( ruleLogicalExpression )
            // InternalGeometryCodes.g:568:3: ruleLogicalExpression
            {
             before(grammarAccess.getExpressionAccess().getLogicalExpressionParserRuleCall()); 
            pushFollow(FOLLOW_2);
            ruleLogicalExpression();

            state._fsp--;

             after(grammarAccess.getExpressionAccess().getLogicalExpressionParserRuleCall()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleLogicalExpression"
    // InternalGeometryCodes.g:578:1: entryRuleLogicalExpression : ruleLogicalExpression EOF ;
    public final void entryRuleLogicalExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:579:1: ( ruleLogicalExpression EOF )
            // InternalGeometryCodes.g:580:1: ruleLogicalExpression EOF
            {
             before(grammarAccess.getLogicalExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleLogicalExpression();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLogicalExpression"


    // $ANTLR start "ruleLogicalExpression"
    // InternalGeometryCodes.g:587:1: ruleLogicalExpression : ( ( rule__LogicalExpression__Group__0 ) ) ;
    public final void ruleLogicalExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:591:2: ( ( ( rule__LogicalExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:592:2: ( ( rule__LogicalExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:592:2: ( ( rule__LogicalExpression__Group__0 ) )
            // InternalGeometryCodes.g:593:3: ( rule__LogicalExpression__Group__0 )
            {
             before(grammarAccess.getLogicalExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:594:3: ( rule__LogicalExpression__Group__0 )
            // InternalGeometryCodes.g:594:4: rule__LogicalExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLogicalExpression"


    // $ANTLR start "entryRuleConditionalExpression"
    // InternalGeometryCodes.g:603:1: entryRuleConditionalExpression : ruleConditionalExpression EOF ;
    public final void entryRuleConditionalExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:604:1: ( ruleConditionalExpression EOF )
            // InternalGeometryCodes.g:605:1: ruleConditionalExpression EOF
            {
             before(grammarAccess.getConditionalExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleConditionalExpression();

            state._fsp--;

             after(grammarAccess.getConditionalExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConditionalExpression"


    // $ANTLR start "ruleConditionalExpression"
    // InternalGeometryCodes.g:612:1: ruleConditionalExpression : ( ( rule__ConditionalExpression__Group__0 ) ) ;
    public final void ruleConditionalExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:616:2: ( ( ( rule__ConditionalExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:617:2: ( ( rule__ConditionalExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:617:2: ( ( rule__ConditionalExpression__Group__0 ) )
            // InternalGeometryCodes.g:618:3: ( rule__ConditionalExpression__Group__0 )
            {
             before(grammarAccess.getConditionalExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:619:3: ( rule__ConditionalExpression__Group__0 )
            // InternalGeometryCodes.g:619:4: rule__ConditionalExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ConditionalExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConditionalExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConditionalExpression"


    // $ANTLR start "entryRuleAdditiveExpression"
    // InternalGeometryCodes.g:628:1: entryRuleAdditiveExpression : ruleAdditiveExpression EOF ;
    public final void entryRuleAdditiveExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:629:1: ( ruleAdditiveExpression EOF )
            // InternalGeometryCodes.g:630:1: ruleAdditiveExpression EOF
            {
             before(grammarAccess.getAdditiveExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleAdditiveExpression();

            state._fsp--;

             after(grammarAccess.getAdditiveExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleAdditiveExpression"


    // $ANTLR start "ruleAdditiveExpression"
    // InternalGeometryCodes.g:637:1: ruleAdditiveExpression : ( ( rule__AdditiveExpression__Group__0 ) ) ;
    public final void ruleAdditiveExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:641:2: ( ( ( rule__AdditiveExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:642:2: ( ( rule__AdditiveExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:642:2: ( ( rule__AdditiveExpression__Group__0 ) )
            // InternalGeometryCodes.g:643:3: ( rule__AdditiveExpression__Group__0 )
            {
             before(grammarAccess.getAdditiveExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:644:3: ( rule__AdditiveExpression__Group__0 )
            // InternalGeometryCodes.g:644:4: rule__AdditiveExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdditiveExpression"


    // $ANTLR start "entryRuleMultiplicativeExpression"
    // InternalGeometryCodes.g:653:1: entryRuleMultiplicativeExpression : ruleMultiplicativeExpression EOF ;
    public final void entryRuleMultiplicativeExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:654:1: ( ruleMultiplicativeExpression EOF )
            // InternalGeometryCodes.g:655:1: ruleMultiplicativeExpression EOF
            {
             before(grammarAccess.getMultiplicativeExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleMultiplicativeExpression();

            state._fsp--;

             after(grammarAccess.getMultiplicativeExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleMultiplicativeExpression"


    // $ANTLR start "ruleMultiplicativeExpression"
    // InternalGeometryCodes.g:662:1: ruleMultiplicativeExpression : ( ( rule__MultiplicativeExpression__Group__0 ) ) ;
    public final void ruleMultiplicativeExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:666:2: ( ( ( rule__MultiplicativeExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:667:2: ( ( rule__MultiplicativeExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:667:2: ( ( rule__MultiplicativeExpression__Group__0 ) )
            // InternalGeometryCodes.g:668:3: ( rule__MultiplicativeExpression__Group__0 )
            {
             before(grammarAccess.getMultiplicativeExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:669:3: ( rule__MultiplicativeExpression__Group__0 )
            // InternalGeometryCodes.g:669:4: rule__MultiplicativeExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicativeExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicativeExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiplicativeExpression"


    // $ANTLR start "entryRuleLiteralExpression"
    // InternalGeometryCodes.g:678:1: entryRuleLiteralExpression : ruleLiteralExpression EOF ;
    public final void entryRuleLiteralExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:679:1: ( ruleLiteralExpression EOF )
            // InternalGeometryCodes.g:680:1: ruleLiteralExpression EOF
            {
             before(grammarAccess.getLiteralExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleLiteralExpression();

            state._fsp--;

             after(grammarAccess.getLiteralExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLiteralExpression"


    // $ANTLR start "ruleLiteralExpression"
    // InternalGeometryCodes.g:687:1: ruleLiteralExpression : ( ( rule__LiteralExpression__Alternatives ) ) ;
    public final void ruleLiteralExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:691:2: ( ( ( rule__LiteralExpression__Alternatives ) ) )
            // InternalGeometryCodes.g:692:2: ( ( rule__LiteralExpression__Alternatives ) )
            {
            // InternalGeometryCodes.g:692:2: ( ( rule__LiteralExpression__Alternatives ) )
            // InternalGeometryCodes.g:693:3: ( rule__LiteralExpression__Alternatives )
            {
             before(grammarAccess.getLiteralExpressionAccess().getAlternatives()); 
            // InternalGeometryCodes.g:694:3: ( rule__LiteralExpression__Alternatives )
            // InternalGeometryCodes.g:694:4: rule__LiteralExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__LiteralExpression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLiteralExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLiteralExpression"


    // $ANTLR start "entryRuleUnaryExpression"
    // InternalGeometryCodes.g:703:1: entryRuleUnaryExpression : ruleUnaryExpression EOF ;
    public final void entryRuleUnaryExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:704:1: ( ruleUnaryExpression EOF )
            // InternalGeometryCodes.g:705:1: ruleUnaryExpression EOF
            {
             before(grammarAccess.getUnaryExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleUnaryExpression();

            state._fsp--;

             after(grammarAccess.getUnaryExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUnaryExpression"


    // $ANTLR start "ruleUnaryExpression"
    // InternalGeometryCodes.g:712:1: ruleUnaryExpression : ( ( rule__UnaryExpression__Group__0 ) ) ;
    public final void ruleUnaryExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:716:2: ( ( ( rule__UnaryExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:717:2: ( ( rule__UnaryExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:717:2: ( ( rule__UnaryExpression__Group__0 ) )
            // InternalGeometryCodes.g:718:3: ( rule__UnaryExpression__Group__0 )
            {
             before(grammarAccess.getUnaryExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:719:3: ( rule__UnaryExpression__Group__0 )
            // InternalGeometryCodes.g:719:4: rule__UnaryExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__UnaryExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUnaryExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnaryExpression"


    // $ANTLR start "entryRuleReferenceExpression"
    // InternalGeometryCodes.g:728:1: entryRuleReferenceExpression : ruleReferenceExpression EOF ;
    public final void entryRuleReferenceExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:729:1: ( ruleReferenceExpression EOF )
            // InternalGeometryCodes.g:730:1: ruleReferenceExpression EOF
            {
             before(grammarAccess.getReferenceExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleReferenceExpression();

            state._fsp--;

             after(grammarAccess.getReferenceExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReferenceExpression"


    // $ANTLR start "ruleReferenceExpression"
    // InternalGeometryCodes.g:737:1: ruleReferenceExpression : ( ( rule__ReferenceExpression__Group__0 ) ) ;
    public final void ruleReferenceExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:741:2: ( ( ( rule__ReferenceExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:742:2: ( ( rule__ReferenceExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:742:2: ( ( rule__ReferenceExpression__Group__0 ) )
            // InternalGeometryCodes.g:743:3: ( rule__ReferenceExpression__Group__0 )
            {
             before(grammarAccess.getReferenceExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:744:3: ( rule__ReferenceExpression__Group__0 )
            // InternalGeometryCodes.g:744:4: rule__ReferenceExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReferenceExpression"


    // $ANTLR start "entryRuleVariableReferenceExpression"
    // InternalGeometryCodes.g:753:1: entryRuleVariableReferenceExpression : ruleVariableReferenceExpression EOF ;
    public final void entryRuleVariableReferenceExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:754:1: ( ruleVariableReferenceExpression EOF )
            // InternalGeometryCodes.g:755:1: ruleVariableReferenceExpression EOF
            {
             before(grammarAccess.getVariableReferenceExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleVariableReferenceExpression();

            state._fsp--;

             after(grammarAccess.getVariableReferenceExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVariableReferenceExpression"


    // $ANTLR start "ruleVariableReferenceExpression"
    // InternalGeometryCodes.g:762:1: ruleVariableReferenceExpression : ( ( rule__VariableReferenceExpression__TargetAssignment ) ) ;
    public final void ruleVariableReferenceExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:766:2: ( ( ( rule__VariableReferenceExpression__TargetAssignment ) ) )
            // InternalGeometryCodes.g:767:2: ( ( rule__VariableReferenceExpression__TargetAssignment ) )
            {
            // InternalGeometryCodes.g:767:2: ( ( rule__VariableReferenceExpression__TargetAssignment ) )
            // InternalGeometryCodes.g:768:3: ( rule__VariableReferenceExpression__TargetAssignment )
            {
             before(grammarAccess.getVariableReferenceExpressionAccess().getTargetAssignment()); 
            // InternalGeometryCodes.g:769:3: ( rule__VariableReferenceExpression__TargetAssignment )
            // InternalGeometryCodes.g:769:4: rule__VariableReferenceExpression__TargetAssignment
            {
            pushFollow(FOLLOW_2);
            rule__VariableReferenceExpression__TargetAssignment();

            state._fsp--;


            }

             after(grammarAccess.getVariableReferenceExpressionAccess().getTargetAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVariableReferenceExpression"


    // $ANTLR start "entryRuleFunctionCallExpression"
    // InternalGeometryCodes.g:778:1: entryRuleFunctionCallExpression : ruleFunctionCallExpression EOF ;
    public final void entryRuleFunctionCallExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:779:1: ( ruleFunctionCallExpression EOF )
            // InternalGeometryCodes.g:780:1: ruleFunctionCallExpression EOF
            {
             before(grammarAccess.getFunctionCallExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleFunctionCallExpression();

            state._fsp--;

             after(grammarAccess.getFunctionCallExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFunctionCallExpression"


    // $ANTLR start "ruleFunctionCallExpression"
    // InternalGeometryCodes.g:787:1: ruleFunctionCallExpression : ( ( rule__FunctionCallExpression__Group__0 ) ) ;
    public final void ruleFunctionCallExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:791:2: ( ( ( rule__FunctionCallExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:792:2: ( ( rule__FunctionCallExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:792:2: ( ( rule__FunctionCallExpression__Group__0 ) )
            // InternalGeometryCodes.g:793:3: ( rule__FunctionCallExpression__Group__0 )
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:794:3: ( rule__FunctionCallExpression__Group__0 )
            // InternalGeometryCodes.g:794:4: rule__FunctionCallExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionCallExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFunctionCallExpression"


    // $ANTLR start "entryRuleDeclarationExpression"
    // InternalGeometryCodes.g:803:1: entryRuleDeclarationExpression : ruleDeclarationExpression EOF ;
    public final void entryRuleDeclarationExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:804:1: ( ruleDeclarationExpression EOF )
            // InternalGeometryCodes.g:805:1: ruleDeclarationExpression EOF
            {
             before(grammarAccess.getDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleDeclarationExpression();

            state._fsp--;

             after(grammarAccess.getDeclarationExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleDeclarationExpression"


    // $ANTLR start "ruleDeclarationExpression"
    // InternalGeometryCodes.g:812:1: ruleDeclarationExpression : ( ( rule__DeclarationExpression__Alternatives ) ) ;
    public final void ruleDeclarationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:816:2: ( ( ( rule__DeclarationExpression__Alternatives ) ) )
            // InternalGeometryCodes.g:817:2: ( ( rule__DeclarationExpression__Alternatives ) )
            {
            // InternalGeometryCodes.g:817:2: ( ( rule__DeclarationExpression__Alternatives ) )
            // InternalGeometryCodes.g:818:3: ( rule__DeclarationExpression__Alternatives )
            {
             before(grammarAccess.getDeclarationExpressionAccess().getAlternatives()); 
            // InternalGeometryCodes.g:819:3: ( rule__DeclarationExpression__Alternatives )
            // InternalGeometryCodes.g:819:4: rule__DeclarationExpression__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__DeclarationExpression__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationExpressionAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleDeclarationExpression"


    // $ANTLR start "entryRuleVectorDeclarationExpression"
    // InternalGeometryCodes.g:828:1: entryRuleVectorDeclarationExpression : ruleVectorDeclarationExpression EOF ;
    public final void entryRuleVectorDeclarationExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:829:1: ( ruleVectorDeclarationExpression EOF )
            // InternalGeometryCodes.g:830:1: ruleVectorDeclarationExpression EOF
            {
             before(grammarAccess.getVectorDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleVectorDeclarationExpression();

            state._fsp--;

             after(grammarAccess.getVectorDeclarationExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVectorDeclarationExpression"


    // $ANTLR start "ruleVectorDeclarationExpression"
    // InternalGeometryCodes.g:837:1: ruleVectorDeclarationExpression : ( ( rule__VectorDeclarationExpression__Group__0 ) ) ;
    public final void ruleVectorDeclarationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:841:2: ( ( ( rule__VectorDeclarationExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:842:2: ( ( rule__VectorDeclarationExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:842:2: ( ( rule__VectorDeclarationExpression__Group__0 ) )
            // InternalGeometryCodes.g:843:3: ( rule__VectorDeclarationExpression__Group__0 )
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:844:3: ( rule__VectorDeclarationExpression__Group__0 )
            // InternalGeometryCodes.g:844:4: rule__VectorDeclarationExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVectorDeclarationExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVectorDeclarationExpression"


    // $ANTLR start "entryRuleVertexDeclarationExpression"
    // InternalGeometryCodes.g:853:1: entryRuleVertexDeclarationExpression : ruleVertexDeclarationExpression EOF ;
    public final void entryRuleVertexDeclarationExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:854:1: ( ruleVertexDeclarationExpression EOF )
            // InternalGeometryCodes.g:855:1: ruleVertexDeclarationExpression EOF
            {
             before(grammarAccess.getVertexDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleVertexDeclarationExpression();

            state._fsp--;

             after(grammarAccess.getVertexDeclarationExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVertexDeclarationExpression"


    // $ANTLR start "ruleVertexDeclarationExpression"
    // InternalGeometryCodes.g:862:1: ruleVertexDeclarationExpression : ( ( rule__VertexDeclarationExpression__Group__0 ) ) ;
    public final void ruleVertexDeclarationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:866:2: ( ( ( rule__VertexDeclarationExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:867:2: ( ( rule__VertexDeclarationExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:867:2: ( ( rule__VertexDeclarationExpression__Group__0 ) )
            // InternalGeometryCodes.g:868:3: ( rule__VertexDeclarationExpression__Group__0 )
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:869:3: ( rule__VertexDeclarationExpression__Group__0 )
            // InternalGeometryCodes.g:869:4: rule__VertexDeclarationExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVertexDeclarationExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVertexDeclarationExpression"


    // $ANTLR start "entryRuleTriangleDeclarationExpression"
    // InternalGeometryCodes.g:878:1: entryRuleTriangleDeclarationExpression : ruleTriangleDeclarationExpression EOF ;
    public final void entryRuleTriangleDeclarationExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:879:1: ( ruleTriangleDeclarationExpression EOF )
            // InternalGeometryCodes.g:880:1: ruleTriangleDeclarationExpression EOF
            {
             before(grammarAccess.getTriangleDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleTriangleDeclarationExpression();

            state._fsp--;

             after(grammarAccess.getTriangleDeclarationExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTriangleDeclarationExpression"


    // $ANTLR start "ruleTriangleDeclarationExpression"
    // InternalGeometryCodes.g:887:1: ruleTriangleDeclarationExpression : ( ( rule__TriangleDeclarationExpression__Group__0 ) ) ;
    public final void ruleTriangleDeclarationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:891:2: ( ( ( rule__TriangleDeclarationExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:892:2: ( ( rule__TriangleDeclarationExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:892:2: ( ( rule__TriangleDeclarationExpression__Group__0 ) )
            // InternalGeometryCodes.g:893:3: ( rule__TriangleDeclarationExpression__Group__0 )
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:894:3: ( rule__TriangleDeclarationExpression__Group__0 )
            // InternalGeometryCodes.g:894:4: rule__TriangleDeclarationExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTriangleDeclarationExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTriangleDeclarationExpression"


    // $ANTLR start "entryRuleQuadDeclarationExpression"
    // InternalGeometryCodes.g:903:1: entryRuleQuadDeclarationExpression : ruleQuadDeclarationExpression EOF ;
    public final void entryRuleQuadDeclarationExpression() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:904:1: ( ruleQuadDeclarationExpression EOF )
            // InternalGeometryCodes.g:905:1: ruleQuadDeclarationExpression EOF
            {
             before(grammarAccess.getQuadDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            ruleQuadDeclarationExpression();

            state._fsp--;

             after(grammarAccess.getQuadDeclarationExpressionRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuadDeclarationExpression"


    // $ANTLR start "ruleQuadDeclarationExpression"
    // InternalGeometryCodes.g:912:1: ruleQuadDeclarationExpression : ( ( rule__QuadDeclarationExpression__Group__0 ) ) ;
    public final void ruleQuadDeclarationExpression() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:916:2: ( ( ( rule__QuadDeclarationExpression__Group__0 ) ) )
            // InternalGeometryCodes.g:917:2: ( ( rule__QuadDeclarationExpression__Group__0 ) )
            {
            // InternalGeometryCodes.g:917:2: ( ( rule__QuadDeclarationExpression__Group__0 ) )
            // InternalGeometryCodes.g:918:3: ( rule__QuadDeclarationExpression__Group__0 )
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getGroup()); 
            // InternalGeometryCodes.g:919:3: ( rule__QuadDeclarationExpression__Group__0 )
            // InternalGeometryCodes.g:919:4: rule__QuadDeclarationExpression__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuadDeclarationExpressionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuadDeclarationExpression"


    // $ANTLR start "entryRuleLiteral"
    // InternalGeometryCodes.g:928:1: entryRuleLiteral : ruleLiteral EOF ;
    public final void entryRuleLiteral() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:929:1: ( ruleLiteral EOF )
            // InternalGeometryCodes.g:930:1: ruleLiteral EOF
            {
             before(grammarAccess.getLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleLiteral();

            state._fsp--;

             after(grammarAccess.getLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // InternalGeometryCodes.g:937:1: ruleLiteral : ( ( rule__Literal__Alternatives ) ) ;
    public final void ruleLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:941:2: ( ( ( rule__Literal__Alternatives ) ) )
            // InternalGeometryCodes.g:942:2: ( ( rule__Literal__Alternatives ) )
            {
            // InternalGeometryCodes.g:942:2: ( ( rule__Literal__Alternatives ) )
            // InternalGeometryCodes.g:943:3: ( rule__Literal__Alternatives )
            {
             before(grammarAccess.getLiteralAccess().getAlternatives()); 
            // InternalGeometryCodes.g:944:3: ( rule__Literal__Alternatives )
            // InternalGeometryCodes.g:944:4: rule__Literal__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Literal__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "entryRuleFloatLiteral"
    // InternalGeometryCodes.g:953:1: entryRuleFloatLiteral : ruleFloatLiteral EOF ;
    public final void entryRuleFloatLiteral() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:954:1: ( ruleFloatLiteral EOF )
            // InternalGeometryCodes.g:955:1: ruleFloatLiteral EOF
            {
             before(grammarAccess.getFloatLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleFloatLiteral();

            state._fsp--;

             after(grammarAccess.getFloatLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFloatLiteral"


    // $ANTLR start "ruleFloatLiteral"
    // InternalGeometryCodes.g:962:1: ruleFloatLiteral : ( ( rule__FloatLiteral__ValueAssignment ) ) ;
    public final void ruleFloatLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:966:2: ( ( ( rule__FloatLiteral__ValueAssignment ) ) )
            // InternalGeometryCodes.g:967:2: ( ( rule__FloatLiteral__ValueAssignment ) )
            {
            // InternalGeometryCodes.g:967:2: ( ( rule__FloatLiteral__ValueAssignment ) )
            // InternalGeometryCodes.g:968:3: ( rule__FloatLiteral__ValueAssignment )
            {
             before(grammarAccess.getFloatLiteralAccess().getValueAssignment()); 
            // InternalGeometryCodes.g:969:3: ( rule__FloatLiteral__ValueAssignment )
            // InternalGeometryCodes.g:969:4: rule__FloatLiteral__ValueAssignment
            {
            pushFollow(FOLLOW_2);
            rule__FloatLiteral__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getFloatLiteralAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFloatLiteral"


    // $ANTLR start "entryRuleStringLiteral"
    // InternalGeometryCodes.g:978:1: entryRuleStringLiteral : ruleStringLiteral EOF ;
    public final void entryRuleStringLiteral() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:979:1: ( ruleStringLiteral EOF )
            // InternalGeometryCodes.g:980:1: ruleStringLiteral EOF
            {
             before(grammarAccess.getStringLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleStringLiteral();

            state._fsp--;

             after(grammarAccess.getStringLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleStringLiteral"


    // $ANTLR start "ruleStringLiteral"
    // InternalGeometryCodes.g:987:1: ruleStringLiteral : ( ( rule__StringLiteral__ValueAssignment ) ) ;
    public final void ruleStringLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:991:2: ( ( ( rule__StringLiteral__ValueAssignment ) ) )
            // InternalGeometryCodes.g:992:2: ( ( rule__StringLiteral__ValueAssignment ) )
            {
            // InternalGeometryCodes.g:992:2: ( ( rule__StringLiteral__ValueAssignment ) )
            // InternalGeometryCodes.g:993:3: ( rule__StringLiteral__ValueAssignment )
            {
             before(grammarAccess.getStringLiteralAccess().getValueAssignment()); 
            // InternalGeometryCodes.g:994:3: ( rule__StringLiteral__ValueAssignment )
            // InternalGeometryCodes.g:994:4: rule__StringLiteral__ValueAssignment
            {
            pushFollow(FOLLOW_2);
            rule__StringLiteral__ValueAssignment();

            state._fsp--;


            }

             after(grammarAccess.getStringLiteralAccess().getValueAssignment()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleStringLiteral"


    // $ANTLR start "entryRuleBooleanLiteral"
    // InternalGeometryCodes.g:1003:1: entryRuleBooleanLiteral : ruleBooleanLiteral EOF ;
    public final void entryRuleBooleanLiteral() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:1004:1: ( ruleBooleanLiteral EOF )
            // InternalGeometryCodes.g:1005:1: ruleBooleanLiteral EOF
            {
             before(grammarAccess.getBooleanLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleBooleanLiteral();

            state._fsp--;

             after(grammarAccess.getBooleanLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleBooleanLiteral"


    // $ANTLR start "ruleBooleanLiteral"
    // InternalGeometryCodes.g:1012:1: ruleBooleanLiteral : ( ( rule__BooleanLiteral__Alternatives ) ) ;
    public final void ruleBooleanLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1016:2: ( ( ( rule__BooleanLiteral__Alternatives ) ) )
            // InternalGeometryCodes.g:1017:2: ( ( rule__BooleanLiteral__Alternatives ) )
            {
            // InternalGeometryCodes.g:1017:2: ( ( rule__BooleanLiteral__Alternatives ) )
            // InternalGeometryCodes.g:1018:3: ( rule__BooleanLiteral__Alternatives )
            {
             before(grammarAccess.getBooleanLiteralAccess().getAlternatives()); 
            // InternalGeometryCodes.g:1019:3: ( rule__BooleanLiteral__Alternatives )
            // InternalGeometryCodes.g:1019:4: rule__BooleanLiteral__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getBooleanLiteralAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleBooleanLiteral"


    // $ANTLR start "ruleAssignmentOperator"
    // InternalGeometryCodes.g:1028:1: ruleAssignmentOperator : ( ( rule__AssignmentOperator__Alternatives ) ) ;
    public final void ruleAssignmentOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1032:1: ( ( ( rule__AssignmentOperator__Alternatives ) ) )
            // InternalGeometryCodes.g:1033:2: ( ( rule__AssignmentOperator__Alternatives ) )
            {
            // InternalGeometryCodes.g:1033:2: ( ( rule__AssignmentOperator__Alternatives ) )
            // InternalGeometryCodes.g:1034:3: ( rule__AssignmentOperator__Alternatives )
            {
             before(grammarAccess.getAssignmentOperatorAccess().getAlternatives()); 
            // InternalGeometryCodes.g:1035:3: ( rule__AssignmentOperator__Alternatives )
            // InternalGeometryCodes.g:1035:4: rule__AssignmentOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AssignmentOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAssignmentOperator"


    // $ANTLR start "ruleUnaryOperator"
    // InternalGeometryCodes.g:1044:1: ruleUnaryOperator : ( ( rule__UnaryOperator__Alternatives ) ) ;
    public final void ruleUnaryOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1048:1: ( ( ( rule__UnaryOperator__Alternatives ) ) )
            // InternalGeometryCodes.g:1049:2: ( ( rule__UnaryOperator__Alternatives ) )
            {
            // InternalGeometryCodes.g:1049:2: ( ( rule__UnaryOperator__Alternatives ) )
            // InternalGeometryCodes.g:1050:3: ( rule__UnaryOperator__Alternatives )
            {
             before(grammarAccess.getUnaryOperatorAccess().getAlternatives()); 
            // InternalGeometryCodes.g:1051:3: ( rule__UnaryOperator__Alternatives )
            // InternalGeometryCodes.g:1051:4: rule__UnaryOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__UnaryOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getUnaryOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUnaryOperator"


    // $ANTLR start "ruleAdditiveOperator"
    // InternalGeometryCodes.g:1060:1: ruleAdditiveOperator : ( ( rule__AdditiveOperator__Alternatives ) ) ;
    public final void ruleAdditiveOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1064:1: ( ( ( rule__AdditiveOperator__Alternatives ) ) )
            // InternalGeometryCodes.g:1065:2: ( ( rule__AdditiveOperator__Alternatives ) )
            {
            // InternalGeometryCodes.g:1065:2: ( ( rule__AdditiveOperator__Alternatives ) )
            // InternalGeometryCodes.g:1066:3: ( rule__AdditiveOperator__Alternatives )
            {
             before(grammarAccess.getAdditiveOperatorAccess().getAlternatives()); 
            // InternalGeometryCodes.g:1067:3: ( rule__AdditiveOperator__Alternatives )
            // InternalGeometryCodes.g:1067:4: rule__AdditiveOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleAdditiveOperator"


    // $ANTLR start "ruleMultiplicativeOperator"
    // InternalGeometryCodes.g:1076:1: ruleMultiplicativeOperator : ( ( rule__MultiplicativeOperator__Alternatives ) ) ;
    public final void ruleMultiplicativeOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1080:1: ( ( ( rule__MultiplicativeOperator__Alternatives ) ) )
            // InternalGeometryCodes.g:1081:2: ( ( rule__MultiplicativeOperator__Alternatives ) )
            {
            // InternalGeometryCodes.g:1081:2: ( ( rule__MultiplicativeOperator__Alternatives ) )
            // InternalGeometryCodes.g:1082:3: ( rule__MultiplicativeOperator__Alternatives )
            {
             before(grammarAccess.getMultiplicativeOperatorAccess().getAlternatives()); 
            // InternalGeometryCodes.g:1083:3: ( rule__MultiplicativeOperator__Alternatives )
            // InternalGeometryCodes.g:1083:4: rule__MultiplicativeOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicativeOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicativeOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleMultiplicativeOperator"


    // $ANTLR start "ruleComparisonOperator"
    // InternalGeometryCodes.g:1092:1: ruleComparisonOperator : ( ( rule__ComparisonOperator__Alternatives ) ) ;
    public final void ruleComparisonOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1096:1: ( ( ( rule__ComparisonOperator__Alternatives ) ) )
            // InternalGeometryCodes.g:1097:2: ( ( rule__ComparisonOperator__Alternatives ) )
            {
            // InternalGeometryCodes.g:1097:2: ( ( rule__ComparisonOperator__Alternatives ) )
            // InternalGeometryCodes.g:1098:3: ( rule__ComparisonOperator__Alternatives )
            {
             before(grammarAccess.getComparisonOperatorAccess().getAlternatives()); 
            // InternalGeometryCodes.g:1099:3: ( rule__ComparisonOperator__Alternatives )
            // InternalGeometryCodes.g:1099:4: rule__ComparisonOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__ComparisonOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getComparisonOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleComparisonOperator"


    // $ANTLR start "ruleLogicalOperator"
    // InternalGeometryCodes.g:1108:1: ruleLogicalOperator : ( ( rule__LogicalOperator__Alternatives ) ) ;
    public final void ruleLogicalOperator() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1112:1: ( ( ( rule__LogicalOperator__Alternatives ) ) )
            // InternalGeometryCodes.g:1113:2: ( ( rule__LogicalOperator__Alternatives ) )
            {
            // InternalGeometryCodes.g:1113:2: ( ( rule__LogicalOperator__Alternatives ) )
            // InternalGeometryCodes.g:1114:3: ( rule__LogicalOperator__Alternatives )
            {
             before(grammarAccess.getLogicalOperatorAccess().getAlternatives()); 
            // InternalGeometryCodes.g:1115:3: ( rule__LogicalOperator__Alternatives )
            // InternalGeometryCodes.g:1115:4: rule__LogicalOperator__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__LogicalOperator__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getLogicalOperatorAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLogicalOperator"


    // $ANTLR start "ruleType"
    // InternalGeometryCodes.g:1124:1: ruleType : ( ( rule__Type__Alternatives ) ) ;
    public final void ruleType() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1128:1: ( ( ( rule__Type__Alternatives ) ) )
            // InternalGeometryCodes.g:1129:2: ( ( rule__Type__Alternatives ) )
            {
            // InternalGeometryCodes.g:1129:2: ( ( rule__Type__Alternatives ) )
            // InternalGeometryCodes.g:1130:3: ( rule__Type__Alternatives )
            {
             before(grammarAccess.getTypeAccess().getAlternatives()); 
            // InternalGeometryCodes.g:1131:3: ( rule__Type__Alternatives )
            // InternalGeometryCodes.g:1131:4: rule__Type__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Type__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getTypeAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleType"


    // $ANTLR start "rule__File__Alternatives_1"
    // InternalGeometryCodes.g:1139:1: rule__File__Alternatives_1 : ( ( ( rule__File__FunctionsAssignment_1_0 ) ) | ( ( rule__File__IteratorsAssignment_1_1 ) ) | ( ( rule__File__MaterialsAssignment_1_2 ) ) | ( ( rule__File__StatementsAssignment_1_3 ) ) );
    public final void rule__File__Alternatives_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1143:1: ( ( ( rule__File__FunctionsAssignment_1_0 ) ) | ( ( rule__File__IteratorsAssignment_1_1 ) ) | ( ( rule__File__MaterialsAssignment_1_2 ) ) | ( ( rule__File__StatementsAssignment_1_3 ) ) )
            int alt1=4;
            alt1 = dfa1.predict(input);
            switch (alt1) {
                case 1 :
                    // InternalGeometryCodes.g:1144:2: ( ( rule__File__FunctionsAssignment_1_0 ) )
                    {
                    // InternalGeometryCodes.g:1144:2: ( ( rule__File__FunctionsAssignment_1_0 ) )
                    // InternalGeometryCodes.g:1145:3: ( rule__File__FunctionsAssignment_1_0 )
                    {
                     before(grammarAccess.getFileAccess().getFunctionsAssignment_1_0()); 
                    // InternalGeometryCodes.g:1146:3: ( rule__File__FunctionsAssignment_1_0 )
                    // InternalGeometryCodes.g:1146:4: rule__File__FunctionsAssignment_1_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__File__FunctionsAssignment_1_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getFileAccess().getFunctionsAssignment_1_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1150:2: ( ( rule__File__IteratorsAssignment_1_1 ) )
                    {
                    // InternalGeometryCodes.g:1150:2: ( ( rule__File__IteratorsAssignment_1_1 ) )
                    // InternalGeometryCodes.g:1151:3: ( rule__File__IteratorsAssignment_1_1 )
                    {
                     before(grammarAccess.getFileAccess().getIteratorsAssignment_1_1()); 
                    // InternalGeometryCodes.g:1152:3: ( rule__File__IteratorsAssignment_1_1 )
                    // InternalGeometryCodes.g:1152:4: rule__File__IteratorsAssignment_1_1
                    {
                    pushFollow(FOLLOW_2);
                    rule__File__IteratorsAssignment_1_1();

                    state._fsp--;


                    }

                     after(grammarAccess.getFileAccess().getIteratorsAssignment_1_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:1156:2: ( ( rule__File__MaterialsAssignment_1_2 ) )
                    {
                    // InternalGeometryCodes.g:1156:2: ( ( rule__File__MaterialsAssignment_1_2 ) )
                    // InternalGeometryCodes.g:1157:3: ( rule__File__MaterialsAssignment_1_2 )
                    {
                     before(grammarAccess.getFileAccess().getMaterialsAssignment_1_2()); 
                    // InternalGeometryCodes.g:1158:3: ( rule__File__MaterialsAssignment_1_2 )
                    // InternalGeometryCodes.g:1158:4: rule__File__MaterialsAssignment_1_2
                    {
                    pushFollow(FOLLOW_2);
                    rule__File__MaterialsAssignment_1_2();

                    state._fsp--;


                    }

                     after(grammarAccess.getFileAccess().getMaterialsAssignment_1_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:1162:2: ( ( rule__File__StatementsAssignment_1_3 ) )
                    {
                    // InternalGeometryCodes.g:1162:2: ( ( rule__File__StatementsAssignment_1_3 ) )
                    // InternalGeometryCodes.g:1163:3: ( rule__File__StatementsAssignment_1_3 )
                    {
                     before(grammarAccess.getFileAccess().getStatementsAssignment_1_3()); 
                    // InternalGeometryCodes.g:1164:3: ( rule__File__StatementsAssignment_1_3 )
                    // InternalGeometryCodes.g:1164:4: rule__File__StatementsAssignment_1_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__File__StatementsAssignment_1_3();

                    state._fsp--;


                    }

                     after(grammarAccess.getFileAccess().getStatementsAssignment_1_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Alternatives_1"


    // $ANTLR start "rule__MaterialProperty__Alternatives"
    // InternalGeometryCodes.g:1172:1: rule__MaterialProperty__Alternatives : ( ( ruleMaterialStringProperty ) | ( ruleMaterialFloatProperty ) | ( ruleMaterialBooleanProperty ) );
    public final void rule__MaterialProperty__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1176:1: ( ( ruleMaterialStringProperty ) | ( ruleMaterialFloatProperty ) | ( ruleMaterialBooleanProperty ) )
            int alt2=3;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1==45) ) {
                    switch ( input.LA(3) ) {
                    case RULE_FLOAT:
                        {
                        alt2=2;
                        }
                        break;
                    case RULE_STRING:
                        {
                        alt2=1;
                        }
                        break;
                    case 12:
                    case 58:
                        {
                        alt2=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 2, 2, input);

                        throw nvae;
                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 2, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // InternalGeometryCodes.g:1177:2: ( ruleMaterialStringProperty )
                    {
                    // InternalGeometryCodes.g:1177:2: ( ruleMaterialStringProperty )
                    // InternalGeometryCodes.g:1178:3: ruleMaterialStringProperty
                    {
                     before(grammarAccess.getMaterialPropertyAccess().getMaterialStringPropertyParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleMaterialStringProperty();

                    state._fsp--;

                     after(grammarAccess.getMaterialPropertyAccess().getMaterialStringPropertyParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1183:2: ( ruleMaterialFloatProperty )
                    {
                    // InternalGeometryCodes.g:1183:2: ( ruleMaterialFloatProperty )
                    // InternalGeometryCodes.g:1184:3: ruleMaterialFloatProperty
                    {
                     before(grammarAccess.getMaterialPropertyAccess().getMaterialFloatPropertyParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleMaterialFloatProperty();

                    state._fsp--;

                     after(grammarAccess.getMaterialPropertyAccess().getMaterialFloatPropertyParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:1189:2: ( ruleMaterialBooleanProperty )
                    {
                    // InternalGeometryCodes.g:1189:2: ( ruleMaterialBooleanProperty )
                    // InternalGeometryCodes.g:1190:3: ruleMaterialBooleanProperty
                    {
                     before(grammarAccess.getMaterialPropertyAccess().getMaterialBooleanPropertyParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleMaterialBooleanProperty();

                    state._fsp--;

                     after(grammarAccess.getMaterialPropertyAccess().getMaterialBooleanPropertyParserRuleCall_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialProperty__Alternatives"


    // $ANTLR start "rule__MaterialBooleanProperty__Alternatives_2"
    // InternalGeometryCodes.g:1199:1: rule__MaterialBooleanProperty__Alternatives_2 : ( ( ( rule__MaterialBooleanProperty__ValueAssignment_2_0 ) ) | ( 'no' ) );
    public final void rule__MaterialBooleanProperty__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1203:1: ( ( ( rule__MaterialBooleanProperty__ValueAssignment_2_0 ) ) | ( 'no' ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==58) ) {
                alt3=1;
            }
            else if ( (LA3_0==12) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalGeometryCodes.g:1204:2: ( ( rule__MaterialBooleanProperty__ValueAssignment_2_0 ) )
                    {
                    // InternalGeometryCodes.g:1204:2: ( ( rule__MaterialBooleanProperty__ValueAssignment_2_0 ) )
                    // InternalGeometryCodes.g:1205:3: ( rule__MaterialBooleanProperty__ValueAssignment_2_0 )
                    {
                     before(grammarAccess.getMaterialBooleanPropertyAccess().getValueAssignment_2_0()); 
                    // InternalGeometryCodes.g:1206:3: ( rule__MaterialBooleanProperty__ValueAssignment_2_0 )
                    // InternalGeometryCodes.g:1206:4: rule__MaterialBooleanProperty__ValueAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__MaterialBooleanProperty__ValueAssignment_2_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getMaterialBooleanPropertyAccess().getValueAssignment_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1210:2: ( 'no' )
                    {
                    // InternalGeometryCodes.g:1210:2: ( 'no' )
                    // InternalGeometryCodes.g:1211:3: 'no'
                    {
                     before(grammarAccess.getMaterialBooleanPropertyAccess().getNoKeyword_2_1()); 
                    match(input,12,FOLLOW_2); 
                     after(grammarAccess.getMaterialBooleanPropertyAccess().getNoKeyword_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__Alternatives_2"


    // $ANTLR start "rule__Statement__Alternatives"
    // InternalGeometryCodes.g:1220:1: rule__Statement__Alternatives : ( ( ruleDeclarationStatement ) | ( ruleAssignmentStatement ) | ( ruleReferenceStatement ) | ( ruleReturnStatement ) | ( ruleIfStatement ) | ( ruleForLoopStatement ) | ( ruleForEachLoopStatement ) | ( ruleWhileLoopStatement ) | ( ruleOutputStatement ) );
    public final void rule__Statement__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1224:1: ( ( ruleDeclarationStatement ) | ( ruleAssignmentStatement ) | ( ruleReferenceStatement ) | ( ruleReturnStatement ) | ( ruleIfStatement ) | ( ruleForLoopStatement ) | ( ruleForEachLoopStatement ) | ( ruleWhileLoopStatement ) | ( ruleOutputStatement ) )
            int alt4=9;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // InternalGeometryCodes.g:1225:2: ( ruleDeclarationStatement )
                    {
                    // InternalGeometryCodes.g:1225:2: ( ruleDeclarationStatement )
                    // InternalGeometryCodes.g:1226:3: ruleDeclarationStatement
                    {
                     before(grammarAccess.getStatementAccess().getDeclarationStatementParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleDeclarationStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getDeclarationStatementParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1231:2: ( ruleAssignmentStatement )
                    {
                    // InternalGeometryCodes.g:1231:2: ( ruleAssignmentStatement )
                    // InternalGeometryCodes.g:1232:3: ruleAssignmentStatement
                    {
                     before(grammarAccess.getStatementAccess().getAssignmentStatementParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleAssignmentStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getAssignmentStatementParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:1237:2: ( ruleReferenceStatement )
                    {
                    // InternalGeometryCodes.g:1237:2: ( ruleReferenceStatement )
                    // InternalGeometryCodes.g:1238:3: ruleReferenceStatement
                    {
                     before(grammarAccess.getStatementAccess().getReferenceStatementParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleReferenceStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getReferenceStatementParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:1243:2: ( ruleReturnStatement )
                    {
                    // InternalGeometryCodes.g:1243:2: ( ruleReturnStatement )
                    // InternalGeometryCodes.g:1244:3: ruleReturnStatement
                    {
                     before(grammarAccess.getStatementAccess().getReturnStatementParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleReturnStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getReturnStatementParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGeometryCodes.g:1249:2: ( ruleIfStatement )
                    {
                    // InternalGeometryCodes.g:1249:2: ( ruleIfStatement )
                    // InternalGeometryCodes.g:1250:3: ruleIfStatement
                    {
                     before(grammarAccess.getStatementAccess().getIfStatementParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleIfStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getIfStatementParserRuleCall_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalGeometryCodes.g:1255:2: ( ruleForLoopStatement )
                    {
                    // InternalGeometryCodes.g:1255:2: ( ruleForLoopStatement )
                    // InternalGeometryCodes.g:1256:3: ruleForLoopStatement
                    {
                     before(grammarAccess.getStatementAccess().getForLoopStatementParserRuleCall_5()); 
                    pushFollow(FOLLOW_2);
                    ruleForLoopStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getForLoopStatementParserRuleCall_5()); 

                    }


                    }
                    break;
                case 7 :
                    // InternalGeometryCodes.g:1261:2: ( ruleForEachLoopStatement )
                    {
                    // InternalGeometryCodes.g:1261:2: ( ruleForEachLoopStatement )
                    // InternalGeometryCodes.g:1262:3: ruleForEachLoopStatement
                    {
                     before(grammarAccess.getStatementAccess().getForEachLoopStatementParserRuleCall_6()); 
                    pushFollow(FOLLOW_2);
                    ruleForEachLoopStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getForEachLoopStatementParserRuleCall_6()); 

                    }


                    }
                    break;
                case 8 :
                    // InternalGeometryCodes.g:1267:2: ( ruleWhileLoopStatement )
                    {
                    // InternalGeometryCodes.g:1267:2: ( ruleWhileLoopStatement )
                    // InternalGeometryCodes.g:1268:3: ruleWhileLoopStatement
                    {
                     before(grammarAccess.getStatementAccess().getWhileLoopStatementParserRuleCall_7()); 
                    pushFollow(FOLLOW_2);
                    ruleWhileLoopStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getWhileLoopStatementParserRuleCall_7()); 

                    }


                    }
                    break;
                case 9 :
                    // InternalGeometryCodes.g:1273:2: ( ruleOutputStatement )
                    {
                    // InternalGeometryCodes.g:1273:2: ( ruleOutputStatement )
                    // InternalGeometryCodes.g:1274:3: ruleOutputStatement
                    {
                     before(grammarAccess.getStatementAccess().getOutputStatementParserRuleCall_8()); 
                    pushFollow(FOLLOW_2);
                    ruleOutputStatement();

                    state._fsp--;

                     after(grammarAccess.getStatementAccess().getOutputStatementParserRuleCall_8()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Statement__Alternatives"


    // $ANTLR start "rule__ReturnStatement__Alternatives_2"
    // InternalGeometryCodes.g:1283:1: rule__ReturnStatement__Alternatives_2 : ( ( ( rule__ReturnStatement__ValueAssignment_2_0 ) ) | ( 'nothing' ) );
    public final void rule__ReturnStatement__Alternatives_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1287:1: ( ( ( rule__ReturnStatement__ValueAssignment_2_0 ) ) | ( 'nothing' ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( ((LA5_0>=RULE_STRING && LA5_0<=RULE_FLOAT)||(LA5_0>=16 && LA5_0<=17)||(LA5_0>=31 && LA5_0<=33)||LA5_0==36||(LA5_0>=55 && LA5_0<=57)||LA5_0==59) ) {
                alt5=1;
            }
            else if ( (LA5_0==13) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalGeometryCodes.g:1288:2: ( ( rule__ReturnStatement__ValueAssignment_2_0 ) )
                    {
                    // InternalGeometryCodes.g:1288:2: ( ( rule__ReturnStatement__ValueAssignment_2_0 ) )
                    // InternalGeometryCodes.g:1289:3: ( rule__ReturnStatement__ValueAssignment_2_0 )
                    {
                     before(grammarAccess.getReturnStatementAccess().getValueAssignment_2_0()); 
                    // InternalGeometryCodes.g:1290:3: ( rule__ReturnStatement__ValueAssignment_2_0 )
                    // InternalGeometryCodes.g:1290:4: rule__ReturnStatement__ValueAssignment_2_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ReturnStatement__ValueAssignment_2_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getReturnStatementAccess().getValueAssignment_2_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1294:2: ( 'nothing' )
                    {
                    // InternalGeometryCodes.g:1294:2: ( 'nothing' )
                    // InternalGeometryCodes.g:1295:3: 'nothing'
                    {
                     before(grammarAccess.getReturnStatementAccess().getNothingKeyword_2_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getReturnStatementAccess().getNothingKeyword_2_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnStatement__Alternatives_2"


    // $ANTLR start "rule__LiteralExpression__Alternatives"
    // InternalGeometryCodes.g:1304:1: rule__LiteralExpression__Alternatives : ( ( ruleLiteral ) | ( ( rule__LiteralExpression__Group_1__0 ) ) | ( ruleUnaryExpression ) | ( ruleReferenceExpression ) | ( ruleDeclarationExpression ) );
    public final void rule__LiteralExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1308:1: ( ( ruleLiteral ) | ( ( rule__LiteralExpression__Group_1__0 ) ) | ( ruleUnaryExpression ) | ( ruleReferenceExpression ) | ( ruleDeclarationExpression ) )
            int alt6=5;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_FLOAT:
            case 56:
            case 57:
            case 59:
                {
                alt6=1;
                }
                break;
            case 36:
                {
                alt6=2;
                }
                break;
            case 16:
            case 17:
                {
                alt6=3;
                }
                break;
            case RULE_ID:
                {
                alt6=4;
                }
                break;
            case 31:
            case 32:
            case 33:
            case 55:
                {
                alt6=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }

            switch (alt6) {
                case 1 :
                    // InternalGeometryCodes.g:1309:2: ( ruleLiteral )
                    {
                    // InternalGeometryCodes.g:1309:2: ( ruleLiteral )
                    // InternalGeometryCodes.g:1310:3: ruleLiteral
                    {
                     before(grammarAccess.getLiteralExpressionAccess().getLiteralParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleLiteral();

                    state._fsp--;

                     after(grammarAccess.getLiteralExpressionAccess().getLiteralParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1315:2: ( ( rule__LiteralExpression__Group_1__0 ) )
                    {
                    // InternalGeometryCodes.g:1315:2: ( ( rule__LiteralExpression__Group_1__0 ) )
                    // InternalGeometryCodes.g:1316:3: ( rule__LiteralExpression__Group_1__0 )
                    {
                     before(grammarAccess.getLiteralExpressionAccess().getGroup_1()); 
                    // InternalGeometryCodes.g:1317:3: ( rule__LiteralExpression__Group_1__0 )
                    // InternalGeometryCodes.g:1317:4: rule__LiteralExpression__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__LiteralExpression__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLiteralExpressionAccess().getGroup_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:1321:2: ( ruleUnaryExpression )
                    {
                    // InternalGeometryCodes.g:1321:2: ( ruleUnaryExpression )
                    // InternalGeometryCodes.g:1322:3: ruleUnaryExpression
                    {
                     before(grammarAccess.getLiteralExpressionAccess().getUnaryExpressionParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleUnaryExpression();

                    state._fsp--;

                     after(grammarAccess.getLiteralExpressionAccess().getUnaryExpressionParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:1327:2: ( ruleReferenceExpression )
                    {
                    // InternalGeometryCodes.g:1327:2: ( ruleReferenceExpression )
                    // InternalGeometryCodes.g:1328:3: ruleReferenceExpression
                    {
                     before(grammarAccess.getLiteralExpressionAccess().getReferenceExpressionParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleReferenceExpression();

                    state._fsp--;

                     after(grammarAccess.getLiteralExpressionAccess().getReferenceExpressionParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGeometryCodes.g:1333:2: ( ruleDeclarationExpression )
                    {
                    // InternalGeometryCodes.g:1333:2: ( ruleDeclarationExpression )
                    // InternalGeometryCodes.g:1334:3: ruleDeclarationExpression
                    {
                     before(grammarAccess.getLiteralExpressionAccess().getDeclarationExpressionParserRuleCall_4()); 
                    pushFollow(FOLLOW_2);
                    ruleDeclarationExpression();

                    state._fsp--;

                     after(grammarAccess.getLiteralExpressionAccess().getDeclarationExpressionParserRuleCall_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralExpression__Alternatives"


    // $ANTLR start "rule__ReferenceExpression__Alternatives_0"
    // InternalGeometryCodes.g:1343:1: rule__ReferenceExpression__Alternatives_0 : ( ( ruleVariableReferenceExpression ) | ( ruleFunctionCallExpression ) );
    public final void rule__ReferenceExpression__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1347:1: ( ( ruleVariableReferenceExpression ) | ( ruleFunctionCallExpression ) )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==RULE_ID) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==36) ) {
                    alt7=2;
                }
                else if ( (LA7_1==EOF||LA7_1==RULE_ID||(LA7_1>=17 && LA7_1<=34)||LA7_1==37||(LA7_1>=39 && LA7_1<=41)||LA7_1==44||(LA7_1>=46 && LA7_1<=48)||LA7_1==50||(LA7_1>=52 && LA7_1<=54)) ) {
                    alt7=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 7, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalGeometryCodes.g:1348:2: ( ruleVariableReferenceExpression )
                    {
                    // InternalGeometryCodes.g:1348:2: ( ruleVariableReferenceExpression )
                    // InternalGeometryCodes.g:1349:3: ruleVariableReferenceExpression
                    {
                     before(grammarAccess.getReferenceExpressionAccess().getVariableReferenceExpressionParserRuleCall_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleVariableReferenceExpression();

                    state._fsp--;

                     after(grammarAccess.getReferenceExpressionAccess().getVariableReferenceExpressionParserRuleCall_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1354:2: ( ruleFunctionCallExpression )
                    {
                    // InternalGeometryCodes.g:1354:2: ( ruleFunctionCallExpression )
                    // InternalGeometryCodes.g:1355:3: ruleFunctionCallExpression
                    {
                     before(grammarAccess.getReferenceExpressionAccess().getFunctionCallExpressionParserRuleCall_0_1()); 
                    pushFollow(FOLLOW_2);
                    ruleFunctionCallExpression();

                    state._fsp--;

                     after(grammarAccess.getReferenceExpressionAccess().getFunctionCallExpressionParserRuleCall_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Alternatives_0"


    // $ANTLR start "rule__DeclarationExpression__Alternatives"
    // InternalGeometryCodes.g:1364:1: rule__DeclarationExpression__Alternatives : ( ( ruleVectorDeclarationExpression ) | ( ruleVertexDeclarationExpression ) | ( ruleTriangleDeclarationExpression ) | ( ruleQuadDeclarationExpression ) );
    public final void rule__DeclarationExpression__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1368:1: ( ( ruleVectorDeclarationExpression ) | ( ruleVertexDeclarationExpression ) | ( ruleTriangleDeclarationExpression ) | ( ruleQuadDeclarationExpression ) )
            int alt8=4;
            switch ( input.LA(1) ) {
            case 31:
                {
                alt8=1;
                }
                break;
            case 55:
                {
                alt8=2;
                }
                break;
            case 32:
                {
                alt8=3;
                }
                break;
            case 33:
                {
                alt8=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }

            switch (alt8) {
                case 1 :
                    // InternalGeometryCodes.g:1369:2: ( ruleVectorDeclarationExpression )
                    {
                    // InternalGeometryCodes.g:1369:2: ( ruleVectorDeclarationExpression )
                    // InternalGeometryCodes.g:1370:3: ruleVectorDeclarationExpression
                    {
                     before(grammarAccess.getDeclarationExpressionAccess().getVectorDeclarationExpressionParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleVectorDeclarationExpression();

                    state._fsp--;

                     after(grammarAccess.getDeclarationExpressionAccess().getVectorDeclarationExpressionParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1375:2: ( ruleVertexDeclarationExpression )
                    {
                    // InternalGeometryCodes.g:1375:2: ( ruleVertexDeclarationExpression )
                    // InternalGeometryCodes.g:1376:3: ruleVertexDeclarationExpression
                    {
                     before(grammarAccess.getDeclarationExpressionAccess().getVertexDeclarationExpressionParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleVertexDeclarationExpression();

                    state._fsp--;

                     after(grammarAccess.getDeclarationExpressionAccess().getVertexDeclarationExpressionParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:1381:2: ( ruleTriangleDeclarationExpression )
                    {
                    // InternalGeometryCodes.g:1381:2: ( ruleTriangleDeclarationExpression )
                    // InternalGeometryCodes.g:1382:3: ruleTriangleDeclarationExpression
                    {
                     before(grammarAccess.getDeclarationExpressionAccess().getTriangleDeclarationExpressionParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleTriangleDeclarationExpression();

                    state._fsp--;

                     after(grammarAccess.getDeclarationExpressionAccess().getTriangleDeclarationExpressionParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:1387:2: ( ruleQuadDeclarationExpression )
                    {
                    // InternalGeometryCodes.g:1387:2: ( ruleQuadDeclarationExpression )
                    // InternalGeometryCodes.g:1388:3: ruleQuadDeclarationExpression
                    {
                     before(grammarAccess.getDeclarationExpressionAccess().getQuadDeclarationExpressionParserRuleCall_3()); 
                    pushFollow(FOLLOW_2);
                    ruleQuadDeclarationExpression();

                    state._fsp--;

                     after(grammarAccess.getDeclarationExpressionAccess().getQuadDeclarationExpressionParserRuleCall_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationExpression__Alternatives"


    // $ANTLR start "rule__Literal__Alternatives"
    // InternalGeometryCodes.g:1397:1: rule__Literal__Alternatives : ( ( ruleFloatLiteral ) | ( ruleStringLiteral ) | ( ruleBooleanLiteral ) | ( ( rule__Literal__Group_3__0 ) ) );
    public final void rule__Literal__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1401:1: ( ( ruleFloatLiteral ) | ( ruleStringLiteral ) | ( ruleBooleanLiteral ) | ( ( rule__Literal__Group_3__0 ) ) )
            int alt9=4;
            switch ( input.LA(1) ) {
            case RULE_FLOAT:
                {
                alt9=1;
                }
                break;
            case RULE_STRING:
                {
                alt9=2;
                }
                break;
            case 57:
            case 59:
                {
                alt9=3;
                }
                break;
            case 56:
                {
                alt9=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 9, 0, input);

                throw nvae;
            }

            switch (alt9) {
                case 1 :
                    // InternalGeometryCodes.g:1402:2: ( ruleFloatLiteral )
                    {
                    // InternalGeometryCodes.g:1402:2: ( ruleFloatLiteral )
                    // InternalGeometryCodes.g:1403:3: ruleFloatLiteral
                    {
                     before(grammarAccess.getLiteralAccess().getFloatLiteralParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleFloatLiteral();

                    state._fsp--;

                     after(grammarAccess.getLiteralAccess().getFloatLiteralParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1408:2: ( ruleStringLiteral )
                    {
                    // InternalGeometryCodes.g:1408:2: ( ruleStringLiteral )
                    // InternalGeometryCodes.g:1409:3: ruleStringLiteral
                    {
                     before(grammarAccess.getLiteralAccess().getStringLiteralParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleStringLiteral();

                    state._fsp--;

                     after(grammarAccess.getLiteralAccess().getStringLiteralParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:1414:2: ( ruleBooleanLiteral )
                    {
                    // InternalGeometryCodes.g:1414:2: ( ruleBooleanLiteral )
                    // InternalGeometryCodes.g:1415:3: ruleBooleanLiteral
                    {
                     before(grammarAccess.getLiteralAccess().getBooleanLiteralParserRuleCall_2()); 
                    pushFollow(FOLLOW_2);
                    ruleBooleanLiteral();

                    state._fsp--;

                     after(grammarAccess.getLiteralAccess().getBooleanLiteralParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:1420:2: ( ( rule__Literal__Group_3__0 ) )
                    {
                    // InternalGeometryCodes.g:1420:2: ( ( rule__Literal__Group_3__0 ) )
                    // InternalGeometryCodes.g:1421:3: ( rule__Literal__Group_3__0 )
                    {
                     before(grammarAccess.getLiteralAccess().getGroup_3()); 
                    // InternalGeometryCodes.g:1422:3: ( rule__Literal__Group_3__0 )
                    // InternalGeometryCodes.g:1422:4: rule__Literal__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Literal__Group_3__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getLiteralAccess().getGroup_3()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Alternatives"


    // $ANTLR start "rule__BooleanLiteral__Alternatives"
    // InternalGeometryCodes.g:1430:1: rule__BooleanLiteral__Alternatives : ( ( ( rule__BooleanLiteral__ValueAssignment_0 ) ) | ( ( rule__BooleanLiteral__Group_1__0 ) ) );
    public final void rule__BooleanLiteral__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1434:1: ( ( ( rule__BooleanLiteral__ValueAssignment_0 ) ) | ( ( rule__BooleanLiteral__Group_1__0 ) ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==59) ) {
                alt10=1;
            }
            else if ( (LA10_0==57) ) {
                alt10=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalGeometryCodes.g:1435:2: ( ( rule__BooleanLiteral__ValueAssignment_0 ) )
                    {
                    // InternalGeometryCodes.g:1435:2: ( ( rule__BooleanLiteral__ValueAssignment_0 ) )
                    // InternalGeometryCodes.g:1436:3: ( rule__BooleanLiteral__ValueAssignment_0 )
                    {
                     before(grammarAccess.getBooleanLiteralAccess().getValueAssignment_0()); 
                    // InternalGeometryCodes.g:1437:3: ( rule__BooleanLiteral__ValueAssignment_0 )
                    // InternalGeometryCodes.g:1437:4: rule__BooleanLiteral__ValueAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BooleanLiteral__ValueAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanLiteralAccess().getValueAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1441:2: ( ( rule__BooleanLiteral__Group_1__0 ) )
                    {
                    // InternalGeometryCodes.g:1441:2: ( ( rule__BooleanLiteral__Group_1__0 ) )
                    // InternalGeometryCodes.g:1442:3: ( rule__BooleanLiteral__Group_1__0 )
                    {
                     before(grammarAccess.getBooleanLiteralAccess().getGroup_1()); 
                    // InternalGeometryCodes.g:1443:3: ( rule__BooleanLiteral__Group_1__0 )
                    // InternalGeometryCodes.g:1443:4: rule__BooleanLiteral__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__BooleanLiteral__Group_1__0();

                    state._fsp--;


                    }

                     after(grammarAccess.getBooleanLiteralAccess().getGroup_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Alternatives"


    // $ANTLR start "rule__AssignmentOperator__Alternatives"
    // InternalGeometryCodes.g:1451:1: rule__AssignmentOperator__Alternatives : ( ( ( '=' ) ) | ( ( '+=' ) ) );
    public final void rule__AssignmentOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1455:1: ( ( ( '=' ) ) | ( ( '+=' ) ) )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==14) ) {
                alt11=1;
            }
            else if ( (LA11_0==15) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalGeometryCodes.g:1456:2: ( ( '=' ) )
                    {
                    // InternalGeometryCodes.g:1456:2: ( ( '=' ) )
                    // InternalGeometryCodes.g:1457:3: ( '=' )
                    {
                     before(grammarAccess.getAssignmentOperatorAccess().getASSIGNEnumLiteralDeclaration_0()); 
                    // InternalGeometryCodes.g:1458:3: ( '=' )
                    // InternalGeometryCodes.g:1458:4: '='
                    {
                    match(input,14,FOLLOW_2); 

                    }

                     after(grammarAccess.getAssignmentOperatorAccess().getASSIGNEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1462:2: ( ( '+=' ) )
                    {
                    // InternalGeometryCodes.g:1462:2: ( ( '+=' ) )
                    // InternalGeometryCodes.g:1463:3: ( '+=' )
                    {
                     before(grammarAccess.getAssignmentOperatorAccess().getAPPENDEnumLiteralDeclaration_1()); 
                    // InternalGeometryCodes.g:1464:3: ( '+=' )
                    // InternalGeometryCodes.g:1464:4: '+='
                    {
                    match(input,15,FOLLOW_2); 

                    }

                     after(grammarAccess.getAssignmentOperatorAccess().getAPPENDEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentOperator__Alternatives"


    // $ANTLR start "rule__UnaryOperator__Alternatives"
    // InternalGeometryCodes.g:1472:1: rule__UnaryOperator__Alternatives : ( ( ( '!' ) ) | ( ( '-' ) ) );
    public final void rule__UnaryOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1476:1: ( ( ( '!' ) ) | ( ( '-' ) ) )
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==16) ) {
                alt12=1;
            }
            else if ( (LA12_0==17) ) {
                alt12=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 12, 0, input);

                throw nvae;
            }
            switch (alt12) {
                case 1 :
                    // InternalGeometryCodes.g:1477:2: ( ( '!' ) )
                    {
                    // InternalGeometryCodes.g:1477:2: ( ( '!' ) )
                    // InternalGeometryCodes.g:1478:3: ( '!' )
                    {
                     before(grammarAccess.getUnaryOperatorAccess().getNEGATIONEnumLiteralDeclaration_0()); 
                    // InternalGeometryCodes.g:1479:3: ( '!' )
                    // InternalGeometryCodes.g:1479:4: '!'
                    {
                    match(input,16,FOLLOW_2); 

                    }

                     after(grammarAccess.getUnaryOperatorAccess().getNEGATIONEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1483:2: ( ( '-' ) )
                    {
                    // InternalGeometryCodes.g:1483:2: ( ( '-' ) )
                    // InternalGeometryCodes.g:1484:3: ( '-' )
                    {
                     before(grammarAccess.getUnaryOperatorAccess().getNEGATIONEnumLiteralDeclaration_1()); 
                    // InternalGeometryCodes.g:1485:3: ( '-' )
                    // InternalGeometryCodes.g:1485:4: '-'
                    {
                    match(input,17,FOLLOW_2); 

                    }

                     after(grammarAccess.getUnaryOperatorAccess().getNEGATIONEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryOperator__Alternatives"


    // $ANTLR start "rule__AdditiveOperator__Alternatives"
    // InternalGeometryCodes.g:1493:1: rule__AdditiveOperator__Alternatives : ( ( ( '+' ) ) | ( ( '-' ) ) );
    public final void rule__AdditiveOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1497:1: ( ( ( '+' ) ) | ( ( '-' ) ) )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==18) ) {
                alt13=1;
            }
            else if ( (LA13_0==17) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalGeometryCodes.g:1498:2: ( ( '+' ) )
                    {
                    // InternalGeometryCodes.g:1498:2: ( ( '+' ) )
                    // InternalGeometryCodes.g:1499:3: ( '+' )
                    {
                     before(grammarAccess.getAdditiveOperatorAccess().getADDITIONEnumLiteralDeclaration_0()); 
                    // InternalGeometryCodes.g:1500:3: ( '+' )
                    // InternalGeometryCodes.g:1500:4: '+'
                    {
                    match(input,18,FOLLOW_2); 

                    }

                     after(grammarAccess.getAdditiveOperatorAccess().getADDITIONEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1504:2: ( ( '-' ) )
                    {
                    // InternalGeometryCodes.g:1504:2: ( ( '-' ) )
                    // InternalGeometryCodes.g:1505:3: ( '-' )
                    {
                     before(grammarAccess.getAdditiveOperatorAccess().getSUBTRACTIONEnumLiteralDeclaration_1()); 
                    // InternalGeometryCodes.g:1506:3: ( '-' )
                    // InternalGeometryCodes.g:1506:4: '-'
                    {
                    match(input,17,FOLLOW_2); 

                    }

                     after(grammarAccess.getAdditiveOperatorAccess().getSUBTRACTIONEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveOperator__Alternatives"


    // $ANTLR start "rule__MultiplicativeOperator__Alternatives"
    // InternalGeometryCodes.g:1514:1: rule__MultiplicativeOperator__Alternatives : ( ( ( '*' ) ) | ( ( '/' ) ) );
    public final void rule__MultiplicativeOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1518:1: ( ( ( '*' ) ) | ( ( '/' ) ) )
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==19) ) {
                alt14=1;
            }
            else if ( (LA14_0==20) ) {
                alt14=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 14, 0, input);

                throw nvae;
            }
            switch (alt14) {
                case 1 :
                    // InternalGeometryCodes.g:1519:2: ( ( '*' ) )
                    {
                    // InternalGeometryCodes.g:1519:2: ( ( '*' ) )
                    // InternalGeometryCodes.g:1520:3: ( '*' )
                    {
                     before(grammarAccess.getMultiplicativeOperatorAccess().getMULTIPLICATIONEnumLiteralDeclaration_0()); 
                    // InternalGeometryCodes.g:1521:3: ( '*' )
                    // InternalGeometryCodes.g:1521:4: '*'
                    {
                    match(input,19,FOLLOW_2); 

                    }

                     after(grammarAccess.getMultiplicativeOperatorAccess().getMULTIPLICATIONEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1525:2: ( ( '/' ) )
                    {
                    // InternalGeometryCodes.g:1525:2: ( ( '/' ) )
                    // InternalGeometryCodes.g:1526:3: ( '/' )
                    {
                     before(grammarAccess.getMultiplicativeOperatorAccess().getDIVISIONEnumLiteralDeclaration_1()); 
                    // InternalGeometryCodes.g:1527:3: ( '/' )
                    // InternalGeometryCodes.g:1527:4: '/'
                    {
                    match(input,20,FOLLOW_2); 

                    }

                     after(grammarAccess.getMultiplicativeOperatorAccess().getDIVISIONEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeOperator__Alternatives"


    // $ANTLR start "rule__ComparisonOperator__Alternatives"
    // InternalGeometryCodes.g:1535:1: rule__ComparisonOperator__Alternatives : ( ( ( '==' ) ) | ( ( '!=' ) ) | ( ( '>' ) ) | ( ( '>=' ) ) | ( ( '<' ) ) | ( ( '<=' ) ) );
    public final void rule__ComparisonOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1539:1: ( ( ( '==' ) ) | ( ( '!=' ) ) | ( ( '>' ) ) | ( ( '>=' ) ) | ( ( '<' ) ) | ( ( '<=' ) ) )
            int alt15=6;
            switch ( input.LA(1) ) {
            case 21:
                {
                alt15=1;
                }
                break;
            case 22:
                {
                alt15=2;
                }
                break;
            case 23:
                {
                alt15=3;
                }
                break;
            case 24:
                {
                alt15=4;
                }
                break;
            case 25:
                {
                alt15=5;
                }
                break;
            case 26:
                {
                alt15=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }

            switch (alt15) {
                case 1 :
                    // InternalGeometryCodes.g:1540:2: ( ( '==' ) )
                    {
                    // InternalGeometryCodes.g:1540:2: ( ( '==' ) )
                    // InternalGeometryCodes.g:1541:3: ( '==' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getEQUALEnumLiteralDeclaration_0()); 
                    // InternalGeometryCodes.g:1542:3: ( '==' )
                    // InternalGeometryCodes.g:1542:4: '=='
                    {
                    match(input,21,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getEQUALEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1546:2: ( ( '!=' ) )
                    {
                    // InternalGeometryCodes.g:1546:2: ( ( '!=' ) )
                    // InternalGeometryCodes.g:1547:3: ( '!=' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getNOTEQUALEnumLiteralDeclaration_1()); 
                    // InternalGeometryCodes.g:1548:3: ( '!=' )
                    // InternalGeometryCodes.g:1548:4: '!='
                    {
                    match(input,22,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getNOTEQUALEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:1552:2: ( ( '>' ) )
                    {
                    // InternalGeometryCodes.g:1552:2: ( ( '>' ) )
                    // InternalGeometryCodes.g:1553:3: ( '>' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getGREATEREnumLiteralDeclaration_2()); 
                    // InternalGeometryCodes.g:1554:3: ( '>' )
                    // InternalGeometryCodes.g:1554:4: '>'
                    {
                    match(input,23,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getGREATEREnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:1558:2: ( ( '>=' ) )
                    {
                    // InternalGeometryCodes.g:1558:2: ( ( '>=' ) )
                    // InternalGeometryCodes.g:1559:3: ( '>=' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getGREATEREQUALEnumLiteralDeclaration_3()); 
                    // InternalGeometryCodes.g:1560:3: ( '>=' )
                    // InternalGeometryCodes.g:1560:4: '>='
                    {
                    match(input,24,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getGREATEREQUALEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGeometryCodes.g:1564:2: ( ( '<' ) )
                    {
                    // InternalGeometryCodes.g:1564:2: ( ( '<' ) )
                    // InternalGeometryCodes.g:1565:3: ( '<' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getLESSEnumLiteralDeclaration_4()); 
                    // InternalGeometryCodes.g:1566:3: ( '<' )
                    // InternalGeometryCodes.g:1566:4: '<'
                    {
                    match(input,25,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getLESSEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalGeometryCodes.g:1570:2: ( ( '<=' ) )
                    {
                    // InternalGeometryCodes.g:1570:2: ( ( '<=' ) )
                    // InternalGeometryCodes.g:1571:3: ( '<=' )
                    {
                     before(grammarAccess.getComparisonOperatorAccess().getLESSEQUALEnumLiteralDeclaration_5()); 
                    // InternalGeometryCodes.g:1572:3: ( '<=' )
                    // InternalGeometryCodes.g:1572:4: '<='
                    {
                    match(input,26,FOLLOW_2); 

                    }

                     after(grammarAccess.getComparisonOperatorAccess().getLESSEQUALEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ComparisonOperator__Alternatives"


    // $ANTLR start "rule__LogicalOperator__Alternatives"
    // InternalGeometryCodes.g:1580:1: rule__LogicalOperator__Alternatives : ( ( ( '&&' ) ) | ( ( '||' ) ) );
    public final void rule__LogicalOperator__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1584:1: ( ( ( '&&' ) ) | ( ( '||' ) ) )
            int alt16=2;
            int LA16_0 = input.LA(1);

            if ( (LA16_0==27) ) {
                alt16=1;
            }
            else if ( (LA16_0==28) ) {
                alt16=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 16, 0, input);

                throw nvae;
            }
            switch (alt16) {
                case 1 :
                    // InternalGeometryCodes.g:1585:2: ( ( '&&' ) )
                    {
                    // InternalGeometryCodes.g:1585:2: ( ( '&&' ) )
                    // InternalGeometryCodes.g:1586:3: ( '&&' )
                    {
                     before(grammarAccess.getLogicalOperatorAccess().getANDEnumLiteralDeclaration_0()); 
                    // InternalGeometryCodes.g:1587:3: ( '&&' )
                    // InternalGeometryCodes.g:1587:4: '&&'
                    {
                    match(input,27,FOLLOW_2); 

                    }

                     after(grammarAccess.getLogicalOperatorAccess().getANDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1591:2: ( ( '||' ) )
                    {
                    // InternalGeometryCodes.g:1591:2: ( ( '||' ) )
                    // InternalGeometryCodes.g:1592:3: ( '||' )
                    {
                     before(grammarAccess.getLogicalOperatorAccess().getOREnumLiteralDeclaration_1()); 
                    // InternalGeometryCodes.g:1593:3: ( '||' )
                    // InternalGeometryCodes.g:1593:4: '||'
                    {
                    match(input,28,FOLLOW_2); 

                    }

                     after(grammarAccess.getLogicalOperatorAccess().getOREnumLiteralDeclaration_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalOperator__Alternatives"


    // $ANTLR start "rule__Type__Alternatives"
    // InternalGeometryCodes.g:1601:1: rule__Type__Alternatives : ( ( ( 'void' ) ) | ( ( 'float' ) ) | ( ( 'vector' ) ) | ( ( 'triangle' ) ) | ( ( 'quad' ) ) | ( ( 'mesh' ) ) );
    public final void rule__Type__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1605:1: ( ( ( 'void' ) ) | ( ( 'float' ) ) | ( ( 'vector' ) ) | ( ( 'triangle' ) ) | ( ( 'quad' ) ) | ( ( 'mesh' ) ) )
            int alt17=6;
            switch ( input.LA(1) ) {
            case 29:
                {
                alt17=1;
                }
                break;
            case 30:
                {
                alt17=2;
                }
                break;
            case 31:
                {
                alt17=3;
                }
                break;
            case 32:
                {
                alt17=4;
                }
                break;
            case 33:
                {
                alt17=5;
                }
                break;
            case 34:
                {
                alt17=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalGeometryCodes.g:1606:2: ( ( 'void' ) )
                    {
                    // InternalGeometryCodes.g:1606:2: ( ( 'void' ) )
                    // InternalGeometryCodes.g:1607:3: ( 'void' )
                    {
                     before(grammarAccess.getTypeAccess().getVOIDEnumLiteralDeclaration_0()); 
                    // InternalGeometryCodes.g:1608:3: ( 'void' )
                    // InternalGeometryCodes.g:1608:4: 'void'
                    {
                    match(input,29,FOLLOW_2); 

                    }

                     after(grammarAccess.getTypeAccess().getVOIDEnumLiteralDeclaration_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1612:2: ( ( 'float' ) )
                    {
                    // InternalGeometryCodes.g:1612:2: ( ( 'float' ) )
                    // InternalGeometryCodes.g:1613:3: ( 'float' )
                    {
                     before(grammarAccess.getTypeAccess().getFLOATEnumLiteralDeclaration_1()); 
                    // InternalGeometryCodes.g:1614:3: ( 'float' )
                    // InternalGeometryCodes.g:1614:4: 'float'
                    {
                    match(input,30,FOLLOW_2); 

                    }

                     after(grammarAccess.getTypeAccess().getFLOATEnumLiteralDeclaration_1()); 

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:1618:2: ( ( 'vector' ) )
                    {
                    // InternalGeometryCodes.g:1618:2: ( ( 'vector' ) )
                    // InternalGeometryCodes.g:1619:3: ( 'vector' )
                    {
                     before(grammarAccess.getTypeAccess().getVECTOREnumLiteralDeclaration_2()); 
                    // InternalGeometryCodes.g:1620:3: ( 'vector' )
                    // InternalGeometryCodes.g:1620:4: 'vector'
                    {
                    match(input,31,FOLLOW_2); 

                    }

                     after(grammarAccess.getTypeAccess().getVECTOREnumLiteralDeclaration_2()); 

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:1624:2: ( ( 'triangle' ) )
                    {
                    // InternalGeometryCodes.g:1624:2: ( ( 'triangle' ) )
                    // InternalGeometryCodes.g:1625:3: ( 'triangle' )
                    {
                     before(grammarAccess.getTypeAccess().getTRIANGLEEnumLiteralDeclaration_3()); 
                    // InternalGeometryCodes.g:1626:3: ( 'triangle' )
                    // InternalGeometryCodes.g:1626:4: 'triangle'
                    {
                    match(input,32,FOLLOW_2); 

                    }

                     after(grammarAccess.getTypeAccess().getTRIANGLEEnumLiteralDeclaration_3()); 

                    }


                    }
                    break;
                case 5 :
                    // InternalGeometryCodes.g:1630:2: ( ( 'quad' ) )
                    {
                    // InternalGeometryCodes.g:1630:2: ( ( 'quad' ) )
                    // InternalGeometryCodes.g:1631:3: ( 'quad' )
                    {
                     before(grammarAccess.getTypeAccess().getQUADEnumLiteralDeclaration_4()); 
                    // InternalGeometryCodes.g:1632:3: ( 'quad' )
                    // InternalGeometryCodes.g:1632:4: 'quad'
                    {
                    match(input,33,FOLLOW_2); 

                    }

                     after(grammarAccess.getTypeAccess().getQUADEnumLiteralDeclaration_4()); 

                    }


                    }
                    break;
                case 6 :
                    // InternalGeometryCodes.g:1636:2: ( ( 'mesh' ) )
                    {
                    // InternalGeometryCodes.g:1636:2: ( ( 'mesh' ) )
                    // InternalGeometryCodes.g:1637:3: ( 'mesh' )
                    {
                     before(grammarAccess.getTypeAccess().getMESHEnumLiteralDeclaration_5()); 
                    // InternalGeometryCodes.g:1638:3: ( 'mesh' )
                    // InternalGeometryCodes.g:1638:4: 'mesh'
                    {
                    match(input,34,FOLLOW_2); 

                    }

                     after(grammarAccess.getTypeAccess().getMESHEnumLiteralDeclaration_5()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Type__Alternatives"


    // $ANTLR start "rule__File__Group__0"
    // InternalGeometryCodes.g:1646:1: rule__File__Group__0 : rule__File__Group__0__Impl rule__File__Group__1 ;
    public final void rule__File__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1650:1: ( rule__File__Group__0__Impl rule__File__Group__1 )
            // InternalGeometryCodes.g:1651:2: rule__File__Group__0__Impl rule__File__Group__1
            {
            pushFollow(FOLLOW_3);
            rule__File__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__File__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__0"


    // $ANTLR start "rule__File__Group__0__Impl"
    // InternalGeometryCodes.g:1658:1: rule__File__Group__0__Impl : ( ( rule__File__ImportsAssignment_0 )* ) ;
    public final void rule__File__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1662:1: ( ( ( rule__File__ImportsAssignment_0 )* ) )
            // InternalGeometryCodes.g:1663:1: ( ( rule__File__ImportsAssignment_0 )* )
            {
            // InternalGeometryCodes.g:1663:1: ( ( rule__File__ImportsAssignment_0 )* )
            // InternalGeometryCodes.g:1664:2: ( rule__File__ImportsAssignment_0 )*
            {
             before(grammarAccess.getFileAccess().getImportsAssignment_0()); 
            // InternalGeometryCodes.g:1665:2: ( rule__File__ImportsAssignment_0 )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( (LA18_0==35) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalGeometryCodes.g:1665:3: rule__File__ImportsAssignment_0
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__File__ImportsAssignment_0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);

             after(grammarAccess.getFileAccess().getImportsAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__0__Impl"


    // $ANTLR start "rule__File__Group__1"
    // InternalGeometryCodes.g:1673:1: rule__File__Group__1 : rule__File__Group__1__Impl ;
    public final void rule__File__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1677:1: ( rule__File__Group__1__Impl )
            // InternalGeometryCodes.g:1678:2: rule__File__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__File__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__1"


    // $ANTLR start "rule__File__Group__1__Impl"
    // InternalGeometryCodes.g:1684:1: rule__File__Group__1__Impl : ( ( rule__File__Alternatives_1 )* ) ;
    public final void rule__File__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1688:1: ( ( ( rule__File__Alternatives_1 )* ) )
            // InternalGeometryCodes.g:1689:1: ( ( rule__File__Alternatives_1 )* )
            {
            // InternalGeometryCodes.g:1689:1: ( ( rule__File__Alternatives_1 )* )
            // InternalGeometryCodes.g:1690:2: ( rule__File__Alternatives_1 )*
            {
             before(grammarAccess.getFileAccess().getAlternatives_1()); 
            // InternalGeometryCodes.g:1691:2: ( rule__File__Alternatives_1 )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( (LA19_0==RULE_ID||(LA19_0>=29 && LA19_0<=34)||LA19_0==41||LA19_0==44||(LA19_0>=46 && LA19_0<=48)||(LA19_0>=52 && LA19_0<=53)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalGeometryCodes.g:1691:3: rule__File__Alternatives_1
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__File__Alternatives_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);

             after(grammarAccess.getFileAccess().getAlternatives_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__Group__1__Impl"


    // $ANTLR start "rule__ImportStatement__Group__0"
    // InternalGeometryCodes.g:1700:1: rule__ImportStatement__Group__0 : rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1 ;
    public final void rule__ImportStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1704:1: ( rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1 )
            // InternalGeometryCodes.g:1705:2: rule__ImportStatement__Group__0__Impl rule__ImportStatement__Group__1
            {
            pushFollow(FOLLOW_6);
            rule__ImportStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__0"


    // $ANTLR start "rule__ImportStatement__Group__0__Impl"
    // InternalGeometryCodes.g:1712:1: rule__ImportStatement__Group__0__Impl : ( 'import' ) ;
    public final void rule__ImportStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1716:1: ( ( 'import' ) )
            // InternalGeometryCodes.g:1717:1: ( 'import' )
            {
            // InternalGeometryCodes.g:1717:1: ( 'import' )
            // InternalGeometryCodes.g:1718:2: 'import'
            {
             before(grammarAccess.getImportStatementAccess().getImportKeyword_0()); 
            match(input,35,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getImportKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__0__Impl"


    // $ANTLR start "rule__ImportStatement__Group__1"
    // InternalGeometryCodes.g:1727:1: rule__ImportStatement__Group__1 : rule__ImportStatement__Group__1__Impl ;
    public final void rule__ImportStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1731:1: ( rule__ImportStatement__Group__1__Impl )
            // InternalGeometryCodes.g:1732:2: rule__ImportStatement__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__1"


    // $ANTLR start "rule__ImportStatement__Group__1__Impl"
    // InternalGeometryCodes.g:1738:1: rule__ImportStatement__Group__1__Impl : ( ( rule__ImportStatement__ImportURIAssignment_1 ) ) ;
    public final void rule__ImportStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1742:1: ( ( ( rule__ImportStatement__ImportURIAssignment_1 ) ) )
            // InternalGeometryCodes.g:1743:1: ( ( rule__ImportStatement__ImportURIAssignment_1 ) )
            {
            // InternalGeometryCodes.g:1743:1: ( ( rule__ImportStatement__ImportURIAssignment_1 ) )
            // InternalGeometryCodes.g:1744:2: ( rule__ImportStatement__ImportURIAssignment_1 )
            {
             before(grammarAccess.getImportStatementAccess().getImportURIAssignment_1()); 
            // InternalGeometryCodes.g:1745:2: ( rule__ImportStatement__ImportURIAssignment_1 )
            // InternalGeometryCodes.g:1745:3: rule__ImportStatement__ImportURIAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__ImportStatement__ImportURIAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImportStatementAccess().getImportURIAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__Group__1__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group__0"
    // InternalGeometryCodes.g:1754:1: rule__FunctionDeclaration__Group__0 : rule__FunctionDeclaration__Group__0__Impl rule__FunctionDeclaration__Group__1 ;
    public final void rule__FunctionDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1758:1: ( rule__FunctionDeclaration__Group__0__Impl rule__FunctionDeclaration__Group__1 )
            // InternalGeometryCodes.g:1759:2: rule__FunctionDeclaration__Group__0__Impl rule__FunctionDeclaration__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__FunctionDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__0"


    // $ANTLR start "rule__FunctionDeclaration__Group__0__Impl"
    // InternalGeometryCodes.g:1766:1: rule__FunctionDeclaration__Group__0__Impl : ( ( rule__FunctionDeclaration__TypeAssignment_0 ) ) ;
    public final void rule__FunctionDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1770:1: ( ( ( rule__FunctionDeclaration__TypeAssignment_0 ) ) )
            // InternalGeometryCodes.g:1771:1: ( ( rule__FunctionDeclaration__TypeAssignment_0 ) )
            {
            // InternalGeometryCodes.g:1771:1: ( ( rule__FunctionDeclaration__TypeAssignment_0 ) )
            // InternalGeometryCodes.g:1772:2: ( rule__FunctionDeclaration__TypeAssignment_0 )
            {
             before(grammarAccess.getFunctionDeclarationAccess().getTypeAssignment_0()); 
            // InternalGeometryCodes.g:1773:2: ( rule__FunctionDeclaration__TypeAssignment_0 )
            // InternalGeometryCodes.g:1773:3: rule__FunctionDeclaration__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionDeclarationAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__0__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group__1"
    // InternalGeometryCodes.g:1781:1: rule__FunctionDeclaration__Group__1 : rule__FunctionDeclaration__Group__1__Impl rule__FunctionDeclaration__Group__2 ;
    public final void rule__FunctionDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1785:1: ( rule__FunctionDeclaration__Group__1__Impl rule__FunctionDeclaration__Group__2 )
            // InternalGeometryCodes.g:1786:2: rule__FunctionDeclaration__Group__1__Impl rule__FunctionDeclaration__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__FunctionDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__1"


    // $ANTLR start "rule__FunctionDeclaration__Group__1__Impl"
    // InternalGeometryCodes.g:1793:1: rule__FunctionDeclaration__Group__1__Impl : ( ( rule__FunctionDeclaration__NameAssignment_1 ) ) ;
    public final void rule__FunctionDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1797:1: ( ( ( rule__FunctionDeclaration__NameAssignment_1 ) ) )
            // InternalGeometryCodes.g:1798:1: ( ( rule__FunctionDeclaration__NameAssignment_1 ) )
            {
            // InternalGeometryCodes.g:1798:1: ( ( rule__FunctionDeclaration__NameAssignment_1 ) )
            // InternalGeometryCodes.g:1799:2: ( rule__FunctionDeclaration__NameAssignment_1 )
            {
             before(grammarAccess.getFunctionDeclarationAccess().getNameAssignment_1()); 
            // InternalGeometryCodes.g:1800:2: ( rule__FunctionDeclaration__NameAssignment_1 )
            // InternalGeometryCodes.g:1800:3: rule__FunctionDeclaration__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionDeclarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__1__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group__2"
    // InternalGeometryCodes.g:1808:1: rule__FunctionDeclaration__Group__2 : rule__FunctionDeclaration__Group__2__Impl rule__FunctionDeclaration__Group__3 ;
    public final void rule__FunctionDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1812:1: ( rule__FunctionDeclaration__Group__2__Impl rule__FunctionDeclaration__Group__3 )
            // InternalGeometryCodes.g:1813:2: rule__FunctionDeclaration__Group__2__Impl rule__FunctionDeclaration__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__FunctionDeclaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__2"


    // $ANTLR start "rule__FunctionDeclaration__Group__2__Impl"
    // InternalGeometryCodes.g:1820:1: rule__FunctionDeclaration__Group__2__Impl : ( '(' ) ;
    public final void rule__FunctionDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1824:1: ( ( '(' ) )
            // InternalGeometryCodes.g:1825:1: ( '(' )
            {
            // InternalGeometryCodes.g:1825:1: ( '(' )
            // InternalGeometryCodes.g:1826:2: '('
            {
             before(grammarAccess.getFunctionDeclarationAccess().getLeftParenthesisKeyword_2()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getFunctionDeclarationAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__2__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group__3"
    // InternalGeometryCodes.g:1835:1: rule__FunctionDeclaration__Group__3 : rule__FunctionDeclaration__Group__3__Impl rule__FunctionDeclaration__Group__4 ;
    public final void rule__FunctionDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1839:1: ( rule__FunctionDeclaration__Group__3__Impl rule__FunctionDeclaration__Group__4 )
            // InternalGeometryCodes.g:1840:2: rule__FunctionDeclaration__Group__3__Impl rule__FunctionDeclaration__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__FunctionDeclaration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__3"


    // $ANTLR start "rule__FunctionDeclaration__Group__3__Impl"
    // InternalGeometryCodes.g:1847:1: rule__FunctionDeclaration__Group__3__Impl : ( ( rule__FunctionDeclaration__Group_3__0 )? ) ;
    public final void rule__FunctionDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1851:1: ( ( ( rule__FunctionDeclaration__Group_3__0 )? ) )
            // InternalGeometryCodes.g:1852:1: ( ( rule__FunctionDeclaration__Group_3__0 )? )
            {
            // InternalGeometryCodes.g:1852:1: ( ( rule__FunctionDeclaration__Group_3__0 )? )
            // InternalGeometryCodes.g:1853:2: ( rule__FunctionDeclaration__Group_3__0 )?
            {
             before(grammarAccess.getFunctionDeclarationAccess().getGroup_3()); 
            // InternalGeometryCodes.g:1854:2: ( rule__FunctionDeclaration__Group_3__0 )?
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( ((LA20_0>=29 && LA20_0<=34)) ) {
                alt20=1;
            }
            switch (alt20) {
                case 1 :
                    // InternalGeometryCodes.g:1854:3: rule__FunctionDeclaration__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FunctionDeclaration__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunctionDeclarationAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__3__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group__4"
    // InternalGeometryCodes.g:1862:1: rule__FunctionDeclaration__Group__4 : rule__FunctionDeclaration__Group__4__Impl rule__FunctionDeclaration__Group__5 ;
    public final void rule__FunctionDeclaration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1866:1: ( rule__FunctionDeclaration__Group__4__Impl rule__FunctionDeclaration__Group__5 )
            // InternalGeometryCodes.g:1867:2: rule__FunctionDeclaration__Group__4__Impl rule__FunctionDeclaration__Group__5
            {
            pushFollow(FOLLOW_10);
            rule__FunctionDeclaration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__4"


    // $ANTLR start "rule__FunctionDeclaration__Group__4__Impl"
    // InternalGeometryCodes.g:1874:1: rule__FunctionDeclaration__Group__4__Impl : ( ')' ) ;
    public final void rule__FunctionDeclaration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1878:1: ( ( ')' ) )
            // InternalGeometryCodes.g:1879:1: ( ')' )
            {
            // InternalGeometryCodes.g:1879:1: ( ')' )
            // InternalGeometryCodes.g:1880:2: ')'
            {
             before(grammarAccess.getFunctionDeclarationAccess().getRightParenthesisKeyword_4()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getFunctionDeclarationAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__4__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group__5"
    // InternalGeometryCodes.g:1889:1: rule__FunctionDeclaration__Group__5 : rule__FunctionDeclaration__Group__5__Impl rule__FunctionDeclaration__Group__6 ;
    public final void rule__FunctionDeclaration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1893:1: ( rule__FunctionDeclaration__Group__5__Impl rule__FunctionDeclaration__Group__6 )
            // InternalGeometryCodes.g:1894:2: rule__FunctionDeclaration__Group__5__Impl rule__FunctionDeclaration__Group__6
            {
            pushFollow(FOLLOW_11);
            rule__FunctionDeclaration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__5"


    // $ANTLR start "rule__FunctionDeclaration__Group__5__Impl"
    // InternalGeometryCodes.g:1901:1: rule__FunctionDeclaration__Group__5__Impl : ( '{' ) ;
    public final void rule__FunctionDeclaration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1905:1: ( ( '{' ) )
            // InternalGeometryCodes.g:1906:1: ( '{' )
            {
            // InternalGeometryCodes.g:1906:1: ( '{' )
            // InternalGeometryCodes.g:1907:2: '{'
            {
             before(grammarAccess.getFunctionDeclarationAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getFunctionDeclarationAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__5__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group__6"
    // InternalGeometryCodes.g:1916:1: rule__FunctionDeclaration__Group__6 : rule__FunctionDeclaration__Group__6__Impl rule__FunctionDeclaration__Group__7 ;
    public final void rule__FunctionDeclaration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1920:1: ( rule__FunctionDeclaration__Group__6__Impl rule__FunctionDeclaration__Group__7 )
            // InternalGeometryCodes.g:1921:2: rule__FunctionDeclaration__Group__6__Impl rule__FunctionDeclaration__Group__7
            {
            pushFollow(FOLLOW_11);
            rule__FunctionDeclaration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__6"


    // $ANTLR start "rule__FunctionDeclaration__Group__6__Impl"
    // InternalGeometryCodes.g:1928:1: rule__FunctionDeclaration__Group__6__Impl : ( ( rule__FunctionDeclaration__StatementsAssignment_6 )* ) ;
    public final void rule__FunctionDeclaration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1932:1: ( ( ( rule__FunctionDeclaration__StatementsAssignment_6 )* ) )
            // InternalGeometryCodes.g:1933:1: ( ( rule__FunctionDeclaration__StatementsAssignment_6 )* )
            {
            // InternalGeometryCodes.g:1933:1: ( ( rule__FunctionDeclaration__StatementsAssignment_6 )* )
            // InternalGeometryCodes.g:1934:2: ( rule__FunctionDeclaration__StatementsAssignment_6 )*
            {
             before(grammarAccess.getFunctionDeclarationAccess().getStatementsAssignment_6()); 
            // InternalGeometryCodes.g:1935:2: ( rule__FunctionDeclaration__StatementsAssignment_6 )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( (LA21_0==RULE_ID||(LA21_0>=29 && LA21_0<=34)||(LA21_0>=46 && LA21_0<=48)||(LA21_0>=52 && LA21_0<=53)) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalGeometryCodes.g:1935:3: rule__FunctionDeclaration__StatementsAssignment_6
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__FunctionDeclaration__StatementsAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);

             after(grammarAccess.getFunctionDeclarationAccess().getStatementsAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__6__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group__7"
    // InternalGeometryCodes.g:1943:1: rule__FunctionDeclaration__Group__7 : rule__FunctionDeclaration__Group__7__Impl ;
    public final void rule__FunctionDeclaration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1947:1: ( rule__FunctionDeclaration__Group__7__Impl )
            // InternalGeometryCodes.g:1948:2: rule__FunctionDeclaration__Group__7__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group__7__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__7"


    // $ANTLR start "rule__FunctionDeclaration__Group__7__Impl"
    // InternalGeometryCodes.g:1954:1: rule__FunctionDeclaration__Group__7__Impl : ( '}' ) ;
    public final void rule__FunctionDeclaration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1958:1: ( ( '}' ) )
            // InternalGeometryCodes.g:1959:1: ( '}' )
            {
            // InternalGeometryCodes.g:1959:1: ( '}' )
            // InternalGeometryCodes.g:1960:2: '}'
            {
             before(grammarAccess.getFunctionDeclarationAccess().getRightCurlyBracketKeyword_7()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getFunctionDeclarationAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group__7__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group_3__0"
    // InternalGeometryCodes.g:1970:1: rule__FunctionDeclaration__Group_3__0 : rule__FunctionDeclaration__Group_3__0__Impl rule__FunctionDeclaration__Group_3__1 ;
    public final void rule__FunctionDeclaration__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1974:1: ( rule__FunctionDeclaration__Group_3__0__Impl rule__FunctionDeclaration__Group_3__1 )
            // InternalGeometryCodes.g:1975:2: rule__FunctionDeclaration__Group_3__0__Impl rule__FunctionDeclaration__Group_3__1
            {
            pushFollow(FOLLOW_12);
            rule__FunctionDeclaration__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group_3__0"


    // $ANTLR start "rule__FunctionDeclaration__Group_3__0__Impl"
    // InternalGeometryCodes.g:1982:1: rule__FunctionDeclaration__Group_3__0__Impl : ( ( rule__FunctionDeclaration__ParametersAssignment_3_0 ) ) ;
    public final void rule__FunctionDeclaration__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:1986:1: ( ( ( rule__FunctionDeclaration__ParametersAssignment_3_0 ) ) )
            // InternalGeometryCodes.g:1987:1: ( ( rule__FunctionDeclaration__ParametersAssignment_3_0 ) )
            {
            // InternalGeometryCodes.g:1987:1: ( ( rule__FunctionDeclaration__ParametersAssignment_3_0 ) )
            // InternalGeometryCodes.g:1988:2: ( rule__FunctionDeclaration__ParametersAssignment_3_0 )
            {
             before(grammarAccess.getFunctionDeclarationAccess().getParametersAssignment_3_0()); 
            // InternalGeometryCodes.g:1989:2: ( rule__FunctionDeclaration__ParametersAssignment_3_0 )
            // InternalGeometryCodes.g:1989:3: rule__FunctionDeclaration__ParametersAssignment_3_0
            {
            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__ParametersAssignment_3_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionDeclarationAccess().getParametersAssignment_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group_3__0__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group_3__1"
    // InternalGeometryCodes.g:1997:1: rule__FunctionDeclaration__Group_3__1 : rule__FunctionDeclaration__Group_3__1__Impl ;
    public final void rule__FunctionDeclaration__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2001:1: ( rule__FunctionDeclaration__Group_3__1__Impl )
            // InternalGeometryCodes.g:2002:2: rule__FunctionDeclaration__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group_3__1"


    // $ANTLR start "rule__FunctionDeclaration__Group_3__1__Impl"
    // InternalGeometryCodes.g:2008:1: rule__FunctionDeclaration__Group_3__1__Impl : ( ( rule__FunctionDeclaration__Group_3_1__0 )* ) ;
    public final void rule__FunctionDeclaration__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2012:1: ( ( ( rule__FunctionDeclaration__Group_3_1__0 )* ) )
            // InternalGeometryCodes.g:2013:1: ( ( rule__FunctionDeclaration__Group_3_1__0 )* )
            {
            // InternalGeometryCodes.g:2013:1: ( ( rule__FunctionDeclaration__Group_3_1__0 )* )
            // InternalGeometryCodes.g:2014:2: ( rule__FunctionDeclaration__Group_3_1__0 )*
            {
             before(grammarAccess.getFunctionDeclarationAccess().getGroup_3_1()); 
            // InternalGeometryCodes.g:2015:2: ( rule__FunctionDeclaration__Group_3_1__0 )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==40) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalGeometryCodes.g:2015:3: rule__FunctionDeclaration__Group_3_1__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__FunctionDeclaration__Group_3_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);

             after(grammarAccess.getFunctionDeclarationAccess().getGroup_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group_3__1__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group_3_1__0"
    // InternalGeometryCodes.g:2024:1: rule__FunctionDeclaration__Group_3_1__0 : rule__FunctionDeclaration__Group_3_1__0__Impl rule__FunctionDeclaration__Group_3_1__1 ;
    public final void rule__FunctionDeclaration__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2028:1: ( rule__FunctionDeclaration__Group_3_1__0__Impl rule__FunctionDeclaration__Group_3_1__1 )
            // InternalGeometryCodes.g:2029:2: rule__FunctionDeclaration__Group_3_1__0__Impl rule__FunctionDeclaration__Group_3_1__1
            {
            pushFollow(FOLLOW_14);
            rule__FunctionDeclaration__Group_3_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group_3_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group_3_1__0"


    // $ANTLR start "rule__FunctionDeclaration__Group_3_1__0__Impl"
    // InternalGeometryCodes.g:2036:1: rule__FunctionDeclaration__Group_3_1__0__Impl : ( ',' ) ;
    public final void rule__FunctionDeclaration__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2040:1: ( ( ',' ) )
            // InternalGeometryCodes.g:2041:1: ( ',' )
            {
            // InternalGeometryCodes.g:2041:1: ( ',' )
            // InternalGeometryCodes.g:2042:2: ','
            {
             before(grammarAccess.getFunctionDeclarationAccess().getCommaKeyword_3_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getFunctionDeclarationAccess().getCommaKeyword_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group_3_1__0__Impl"


    // $ANTLR start "rule__FunctionDeclaration__Group_3_1__1"
    // InternalGeometryCodes.g:2051:1: rule__FunctionDeclaration__Group_3_1__1 : rule__FunctionDeclaration__Group_3_1__1__Impl ;
    public final void rule__FunctionDeclaration__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2055:1: ( rule__FunctionDeclaration__Group_3_1__1__Impl )
            // InternalGeometryCodes.g:2056:2: rule__FunctionDeclaration__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__Group_3_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group_3_1__1"


    // $ANTLR start "rule__FunctionDeclaration__Group_3_1__1__Impl"
    // InternalGeometryCodes.g:2062:1: rule__FunctionDeclaration__Group_3_1__1__Impl : ( ( rule__FunctionDeclaration__ParametersAssignment_3_1_1 ) ) ;
    public final void rule__FunctionDeclaration__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2066:1: ( ( ( rule__FunctionDeclaration__ParametersAssignment_3_1_1 ) ) )
            // InternalGeometryCodes.g:2067:1: ( ( rule__FunctionDeclaration__ParametersAssignment_3_1_1 ) )
            {
            // InternalGeometryCodes.g:2067:1: ( ( rule__FunctionDeclaration__ParametersAssignment_3_1_1 ) )
            // InternalGeometryCodes.g:2068:2: ( rule__FunctionDeclaration__ParametersAssignment_3_1_1 )
            {
             before(grammarAccess.getFunctionDeclarationAccess().getParametersAssignment_3_1_1()); 
            // InternalGeometryCodes.g:2069:2: ( rule__FunctionDeclaration__ParametersAssignment_3_1_1 )
            // InternalGeometryCodes.g:2069:3: rule__FunctionDeclaration__ParametersAssignment_3_1_1
            {
            pushFollow(FOLLOW_2);
            rule__FunctionDeclaration__ParametersAssignment_3_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionDeclarationAccess().getParametersAssignment_3_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__Group_3_1__1__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__0"
    // InternalGeometryCodes.g:2078:1: rule__IteratorDeclaration__Group__0 : rule__IteratorDeclaration__Group__0__Impl rule__IteratorDeclaration__Group__1 ;
    public final void rule__IteratorDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2082:1: ( rule__IteratorDeclaration__Group__0__Impl rule__IteratorDeclaration__Group__1 )
            // InternalGeometryCodes.g:2083:2: rule__IteratorDeclaration__Group__0__Impl rule__IteratorDeclaration__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__IteratorDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__0"


    // $ANTLR start "rule__IteratorDeclaration__Group__0__Impl"
    // InternalGeometryCodes.g:2090:1: rule__IteratorDeclaration__Group__0__Impl : ( 'iterator' ) ;
    public final void rule__IteratorDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2094:1: ( ( 'iterator' ) )
            // InternalGeometryCodes.g:2095:1: ( 'iterator' )
            {
            // InternalGeometryCodes.g:2095:1: ( 'iterator' )
            // InternalGeometryCodes.g:2096:2: 'iterator'
            {
             before(grammarAccess.getIteratorDeclarationAccess().getIteratorKeyword_0()); 
            match(input,41,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getIteratorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__0__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__1"
    // InternalGeometryCodes.g:2105:1: rule__IteratorDeclaration__Group__1 : rule__IteratorDeclaration__Group__1__Impl rule__IteratorDeclaration__Group__2 ;
    public final void rule__IteratorDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2109:1: ( rule__IteratorDeclaration__Group__1__Impl rule__IteratorDeclaration__Group__2 )
            // InternalGeometryCodes.g:2110:2: rule__IteratorDeclaration__Group__1__Impl rule__IteratorDeclaration__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__IteratorDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__1"


    // $ANTLR start "rule__IteratorDeclaration__Group__1__Impl"
    // InternalGeometryCodes.g:2117:1: rule__IteratorDeclaration__Group__1__Impl : ( ( rule__IteratorDeclaration__NameAssignment_1 ) ) ;
    public final void rule__IteratorDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2121:1: ( ( ( rule__IteratorDeclaration__NameAssignment_1 ) ) )
            // InternalGeometryCodes.g:2122:1: ( ( rule__IteratorDeclaration__NameAssignment_1 ) )
            {
            // InternalGeometryCodes.g:2122:1: ( ( rule__IteratorDeclaration__NameAssignment_1 ) )
            // InternalGeometryCodes.g:2123:2: ( rule__IteratorDeclaration__NameAssignment_1 )
            {
             before(grammarAccess.getIteratorDeclarationAccess().getNameAssignment_1()); 
            // InternalGeometryCodes.g:2124:2: ( rule__IteratorDeclaration__NameAssignment_1 )
            // InternalGeometryCodes.g:2124:3: rule__IteratorDeclaration__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIteratorDeclarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__1__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__2"
    // InternalGeometryCodes.g:2132:1: rule__IteratorDeclaration__Group__2 : rule__IteratorDeclaration__Group__2__Impl rule__IteratorDeclaration__Group__3 ;
    public final void rule__IteratorDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2136:1: ( rule__IteratorDeclaration__Group__2__Impl rule__IteratorDeclaration__Group__3 )
            // InternalGeometryCodes.g:2137:2: rule__IteratorDeclaration__Group__2__Impl rule__IteratorDeclaration__Group__3
            {
            pushFollow(FOLLOW_9);
            rule__IteratorDeclaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__2"


    // $ANTLR start "rule__IteratorDeclaration__Group__2__Impl"
    // InternalGeometryCodes.g:2144:1: rule__IteratorDeclaration__Group__2__Impl : ( '(' ) ;
    public final void rule__IteratorDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2148:1: ( ( '(' ) )
            // InternalGeometryCodes.g:2149:1: ( '(' )
            {
            // InternalGeometryCodes.g:2149:1: ( '(' )
            // InternalGeometryCodes.g:2150:2: '('
            {
             before(grammarAccess.getIteratorDeclarationAccess().getLeftParenthesisKeyword_2()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__2__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__3"
    // InternalGeometryCodes.g:2159:1: rule__IteratorDeclaration__Group__3 : rule__IteratorDeclaration__Group__3__Impl rule__IteratorDeclaration__Group__4 ;
    public final void rule__IteratorDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2163:1: ( rule__IteratorDeclaration__Group__3__Impl rule__IteratorDeclaration__Group__4 )
            // InternalGeometryCodes.g:2164:2: rule__IteratorDeclaration__Group__3__Impl rule__IteratorDeclaration__Group__4
            {
            pushFollow(FOLLOW_9);
            rule__IteratorDeclaration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__3"


    // $ANTLR start "rule__IteratorDeclaration__Group__3__Impl"
    // InternalGeometryCodes.g:2171:1: rule__IteratorDeclaration__Group__3__Impl : ( ( rule__IteratorDeclaration__Group_3__0 )? ) ;
    public final void rule__IteratorDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2175:1: ( ( ( rule__IteratorDeclaration__Group_3__0 )? ) )
            // InternalGeometryCodes.g:2176:1: ( ( rule__IteratorDeclaration__Group_3__0 )? )
            {
            // InternalGeometryCodes.g:2176:1: ( ( rule__IteratorDeclaration__Group_3__0 )? )
            // InternalGeometryCodes.g:2177:2: ( rule__IteratorDeclaration__Group_3__0 )?
            {
             before(grammarAccess.getIteratorDeclarationAccess().getGroup_3()); 
            // InternalGeometryCodes.g:2178:2: ( rule__IteratorDeclaration__Group_3__0 )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( ((LA23_0>=29 && LA23_0<=34)) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalGeometryCodes.g:2178:3: rule__IteratorDeclaration__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__IteratorDeclaration__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getIteratorDeclarationAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__3__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__4"
    // InternalGeometryCodes.g:2186:1: rule__IteratorDeclaration__Group__4 : rule__IteratorDeclaration__Group__4__Impl rule__IteratorDeclaration__Group__5 ;
    public final void rule__IteratorDeclaration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2190:1: ( rule__IteratorDeclaration__Group__4__Impl rule__IteratorDeclaration__Group__5 )
            // InternalGeometryCodes.g:2191:2: rule__IteratorDeclaration__Group__4__Impl rule__IteratorDeclaration__Group__5
            {
            pushFollow(FOLLOW_15);
            rule__IteratorDeclaration__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__4"


    // $ANTLR start "rule__IteratorDeclaration__Group__4__Impl"
    // InternalGeometryCodes.g:2198:1: rule__IteratorDeclaration__Group__4__Impl : ( ')' ) ;
    public final void rule__IteratorDeclaration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2202:1: ( ( ')' ) )
            // InternalGeometryCodes.g:2203:1: ( ')' )
            {
            // InternalGeometryCodes.g:2203:1: ( ')' )
            // InternalGeometryCodes.g:2204:2: ')'
            {
             before(grammarAccess.getIteratorDeclarationAccess().getRightParenthesisKeyword_4()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__4__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__5"
    // InternalGeometryCodes.g:2213:1: rule__IteratorDeclaration__Group__5 : rule__IteratorDeclaration__Group__5__Impl rule__IteratorDeclaration__Group__6 ;
    public final void rule__IteratorDeclaration__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2217:1: ( rule__IteratorDeclaration__Group__5__Impl rule__IteratorDeclaration__Group__6 )
            // InternalGeometryCodes.g:2218:2: rule__IteratorDeclaration__Group__5__Impl rule__IteratorDeclaration__Group__6
            {
            pushFollow(FOLLOW_16);
            rule__IteratorDeclaration__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__5"


    // $ANTLR start "rule__IteratorDeclaration__Group__5__Impl"
    // InternalGeometryCodes.g:2225:1: rule__IteratorDeclaration__Group__5__Impl : ( 'over' ) ;
    public final void rule__IteratorDeclaration__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2229:1: ( ( 'over' ) )
            // InternalGeometryCodes.g:2230:1: ( 'over' )
            {
            // InternalGeometryCodes.g:2230:1: ( 'over' )
            // InternalGeometryCodes.g:2231:2: 'over'
            {
             before(grammarAccess.getIteratorDeclarationAccess().getOverKeyword_5()); 
            match(input,42,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getOverKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__5__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__6"
    // InternalGeometryCodes.g:2240:1: rule__IteratorDeclaration__Group__6 : rule__IteratorDeclaration__Group__6__Impl rule__IteratorDeclaration__Group__7 ;
    public final void rule__IteratorDeclaration__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2244:1: ( rule__IteratorDeclaration__Group__6__Impl rule__IteratorDeclaration__Group__7 )
            // InternalGeometryCodes.g:2245:2: rule__IteratorDeclaration__Group__6__Impl rule__IteratorDeclaration__Group__7
            {
            pushFollow(FOLLOW_14);
            rule__IteratorDeclaration__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__6"


    // $ANTLR start "rule__IteratorDeclaration__Group__6__Impl"
    // InternalGeometryCodes.g:2252:1: rule__IteratorDeclaration__Group__6__Impl : ( 'each' ) ;
    public final void rule__IteratorDeclaration__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2256:1: ( ( 'each' ) )
            // InternalGeometryCodes.g:2257:1: ( 'each' )
            {
            // InternalGeometryCodes.g:2257:1: ( 'each' )
            // InternalGeometryCodes.g:2258:2: 'each'
            {
             before(grammarAccess.getIteratorDeclarationAccess().getEachKeyword_6()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getEachKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__6__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__7"
    // InternalGeometryCodes.g:2267:1: rule__IteratorDeclaration__Group__7 : rule__IteratorDeclaration__Group__7__Impl rule__IteratorDeclaration__Group__8 ;
    public final void rule__IteratorDeclaration__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2271:1: ( rule__IteratorDeclaration__Group__7__Impl rule__IteratorDeclaration__Group__8 )
            // InternalGeometryCodes.g:2272:2: rule__IteratorDeclaration__Group__7__Impl rule__IteratorDeclaration__Group__8
            {
            pushFollow(FOLLOW_10);
            rule__IteratorDeclaration__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__7"


    // $ANTLR start "rule__IteratorDeclaration__Group__7__Impl"
    // InternalGeometryCodes.g:2279:1: rule__IteratorDeclaration__Group__7__Impl : ( ( rule__IteratorDeclaration__IteratedParameterAssignment_7 ) ) ;
    public final void rule__IteratorDeclaration__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2283:1: ( ( ( rule__IteratorDeclaration__IteratedParameterAssignment_7 ) ) )
            // InternalGeometryCodes.g:2284:1: ( ( rule__IteratorDeclaration__IteratedParameterAssignment_7 ) )
            {
            // InternalGeometryCodes.g:2284:1: ( ( rule__IteratorDeclaration__IteratedParameterAssignment_7 ) )
            // InternalGeometryCodes.g:2285:2: ( rule__IteratorDeclaration__IteratedParameterAssignment_7 )
            {
             before(grammarAccess.getIteratorDeclarationAccess().getIteratedParameterAssignment_7()); 
            // InternalGeometryCodes.g:2286:2: ( rule__IteratorDeclaration__IteratedParameterAssignment_7 )
            // InternalGeometryCodes.g:2286:3: rule__IteratorDeclaration__IteratedParameterAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__IteratedParameterAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getIteratorDeclarationAccess().getIteratedParameterAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__7__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__8"
    // InternalGeometryCodes.g:2294:1: rule__IteratorDeclaration__Group__8 : rule__IteratorDeclaration__Group__8__Impl rule__IteratorDeclaration__Group__9 ;
    public final void rule__IteratorDeclaration__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2298:1: ( rule__IteratorDeclaration__Group__8__Impl rule__IteratorDeclaration__Group__9 )
            // InternalGeometryCodes.g:2299:2: rule__IteratorDeclaration__Group__8__Impl rule__IteratorDeclaration__Group__9
            {
            pushFollow(FOLLOW_11);
            rule__IteratorDeclaration__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__8"


    // $ANTLR start "rule__IteratorDeclaration__Group__8__Impl"
    // InternalGeometryCodes.g:2306:1: rule__IteratorDeclaration__Group__8__Impl : ( '{' ) ;
    public final void rule__IteratorDeclaration__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2310:1: ( ( '{' ) )
            // InternalGeometryCodes.g:2311:1: ( '{' )
            {
            // InternalGeometryCodes.g:2311:1: ( '{' )
            // InternalGeometryCodes.g:2312:2: '{'
            {
             before(grammarAccess.getIteratorDeclarationAccess().getLeftCurlyBracketKeyword_8()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getLeftCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__8__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__9"
    // InternalGeometryCodes.g:2321:1: rule__IteratorDeclaration__Group__9 : rule__IteratorDeclaration__Group__9__Impl rule__IteratorDeclaration__Group__10 ;
    public final void rule__IteratorDeclaration__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2325:1: ( rule__IteratorDeclaration__Group__9__Impl rule__IteratorDeclaration__Group__10 )
            // InternalGeometryCodes.g:2326:2: rule__IteratorDeclaration__Group__9__Impl rule__IteratorDeclaration__Group__10
            {
            pushFollow(FOLLOW_11);
            rule__IteratorDeclaration__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__9"


    // $ANTLR start "rule__IteratorDeclaration__Group__9__Impl"
    // InternalGeometryCodes.g:2333:1: rule__IteratorDeclaration__Group__9__Impl : ( ( rule__IteratorDeclaration__StatementsAssignment_9 )* ) ;
    public final void rule__IteratorDeclaration__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2337:1: ( ( ( rule__IteratorDeclaration__StatementsAssignment_9 )* ) )
            // InternalGeometryCodes.g:2338:1: ( ( rule__IteratorDeclaration__StatementsAssignment_9 )* )
            {
            // InternalGeometryCodes.g:2338:1: ( ( rule__IteratorDeclaration__StatementsAssignment_9 )* )
            // InternalGeometryCodes.g:2339:2: ( rule__IteratorDeclaration__StatementsAssignment_9 )*
            {
             before(grammarAccess.getIteratorDeclarationAccess().getStatementsAssignment_9()); 
            // InternalGeometryCodes.g:2340:2: ( rule__IteratorDeclaration__StatementsAssignment_9 )*
            loop24:
            do {
                int alt24=2;
                int LA24_0 = input.LA(1);

                if ( (LA24_0==RULE_ID||(LA24_0>=29 && LA24_0<=34)||(LA24_0>=46 && LA24_0<=48)||(LA24_0>=52 && LA24_0<=53)) ) {
                    alt24=1;
                }


                switch (alt24) {
            	case 1 :
            	    // InternalGeometryCodes.g:2340:3: rule__IteratorDeclaration__StatementsAssignment_9
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__IteratorDeclaration__StatementsAssignment_9();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop24;
                }
            } while (true);

             after(grammarAccess.getIteratorDeclarationAccess().getStatementsAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__9__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group__10"
    // InternalGeometryCodes.g:2348:1: rule__IteratorDeclaration__Group__10 : rule__IteratorDeclaration__Group__10__Impl ;
    public final void rule__IteratorDeclaration__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2352:1: ( rule__IteratorDeclaration__Group__10__Impl )
            // InternalGeometryCodes.g:2353:2: rule__IteratorDeclaration__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__10"


    // $ANTLR start "rule__IteratorDeclaration__Group__10__Impl"
    // InternalGeometryCodes.g:2359:1: rule__IteratorDeclaration__Group__10__Impl : ( '}' ) ;
    public final void rule__IteratorDeclaration__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2363:1: ( ( '}' ) )
            // InternalGeometryCodes.g:2364:1: ( '}' )
            {
            // InternalGeometryCodes.g:2364:1: ( '}' )
            // InternalGeometryCodes.g:2365:2: '}'
            {
             before(grammarAccess.getIteratorDeclarationAccess().getRightCurlyBracketKeyword_10()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group__10__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group_3__0"
    // InternalGeometryCodes.g:2375:1: rule__IteratorDeclaration__Group_3__0 : rule__IteratorDeclaration__Group_3__0__Impl rule__IteratorDeclaration__Group_3__1 ;
    public final void rule__IteratorDeclaration__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2379:1: ( rule__IteratorDeclaration__Group_3__0__Impl rule__IteratorDeclaration__Group_3__1 )
            // InternalGeometryCodes.g:2380:2: rule__IteratorDeclaration__Group_3__0__Impl rule__IteratorDeclaration__Group_3__1
            {
            pushFollow(FOLLOW_12);
            rule__IteratorDeclaration__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group_3__0"


    // $ANTLR start "rule__IteratorDeclaration__Group_3__0__Impl"
    // InternalGeometryCodes.g:2387:1: rule__IteratorDeclaration__Group_3__0__Impl : ( ( rule__IteratorDeclaration__ParametersAssignment_3_0 ) ) ;
    public final void rule__IteratorDeclaration__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2391:1: ( ( ( rule__IteratorDeclaration__ParametersAssignment_3_0 ) ) )
            // InternalGeometryCodes.g:2392:1: ( ( rule__IteratorDeclaration__ParametersAssignment_3_0 ) )
            {
            // InternalGeometryCodes.g:2392:1: ( ( rule__IteratorDeclaration__ParametersAssignment_3_0 ) )
            // InternalGeometryCodes.g:2393:2: ( rule__IteratorDeclaration__ParametersAssignment_3_0 )
            {
             before(grammarAccess.getIteratorDeclarationAccess().getParametersAssignment_3_0()); 
            // InternalGeometryCodes.g:2394:2: ( rule__IteratorDeclaration__ParametersAssignment_3_0 )
            // InternalGeometryCodes.g:2394:3: rule__IteratorDeclaration__ParametersAssignment_3_0
            {
            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__ParametersAssignment_3_0();

            state._fsp--;


            }

             after(grammarAccess.getIteratorDeclarationAccess().getParametersAssignment_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group_3__0__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group_3__1"
    // InternalGeometryCodes.g:2402:1: rule__IteratorDeclaration__Group_3__1 : rule__IteratorDeclaration__Group_3__1__Impl ;
    public final void rule__IteratorDeclaration__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2406:1: ( rule__IteratorDeclaration__Group_3__1__Impl )
            // InternalGeometryCodes.g:2407:2: rule__IteratorDeclaration__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group_3__1"


    // $ANTLR start "rule__IteratorDeclaration__Group_3__1__Impl"
    // InternalGeometryCodes.g:2413:1: rule__IteratorDeclaration__Group_3__1__Impl : ( ( rule__IteratorDeclaration__Group_3_1__0 )* ) ;
    public final void rule__IteratorDeclaration__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2417:1: ( ( ( rule__IteratorDeclaration__Group_3_1__0 )* ) )
            // InternalGeometryCodes.g:2418:1: ( ( rule__IteratorDeclaration__Group_3_1__0 )* )
            {
            // InternalGeometryCodes.g:2418:1: ( ( rule__IteratorDeclaration__Group_3_1__0 )* )
            // InternalGeometryCodes.g:2419:2: ( rule__IteratorDeclaration__Group_3_1__0 )*
            {
             before(grammarAccess.getIteratorDeclarationAccess().getGroup_3_1()); 
            // InternalGeometryCodes.g:2420:2: ( rule__IteratorDeclaration__Group_3_1__0 )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==40) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalGeometryCodes.g:2420:3: rule__IteratorDeclaration__Group_3_1__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__IteratorDeclaration__Group_3_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

             after(grammarAccess.getIteratorDeclarationAccess().getGroup_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group_3__1__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group_3_1__0"
    // InternalGeometryCodes.g:2429:1: rule__IteratorDeclaration__Group_3_1__0 : rule__IteratorDeclaration__Group_3_1__0__Impl rule__IteratorDeclaration__Group_3_1__1 ;
    public final void rule__IteratorDeclaration__Group_3_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2433:1: ( rule__IteratorDeclaration__Group_3_1__0__Impl rule__IteratorDeclaration__Group_3_1__1 )
            // InternalGeometryCodes.g:2434:2: rule__IteratorDeclaration__Group_3_1__0__Impl rule__IteratorDeclaration__Group_3_1__1
            {
            pushFollow(FOLLOW_14);
            rule__IteratorDeclaration__Group_3_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group_3_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group_3_1__0"


    // $ANTLR start "rule__IteratorDeclaration__Group_3_1__0__Impl"
    // InternalGeometryCodes.g:2441:1: rule__IteratorDeclaration__Group_3_1__0__Impl : ( ',' ) ;
    public final void rule__IteratorDeclaration__Group_3_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2445:1: ( ( ',' ) )
            // InternalGeometryCodes.g:2446:1: ( ',' )
            {
            // InternalGeometryCodes.g:2446:1: ( ',' )
            // InternalGeometryCodes.g:2447:2: ','
            {
             before(grammarAccess.getIteratorDeclarationAccess().getCommaKeyword_3_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getCommaKeyword_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group_3_1__0__Impl"


    // $ANTLR start "rule__IteratorDeclaration__Group_3_1__1"
    // InternalGeometryCodes.g:2456:1: rule__IteratorDeclaration__Group_3_1__1 : rule__IteratorDeclaration__Group_3_1__1__Impl ;
    public final void rule__IteratorDeclaration__Group_3_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2460:1: ( rule__IteratorDeclaration__Group_3_1__1__Impl )
            // InternalGeometryCodes.g:2461:2: rule__IteratorDeclaration__Group_3_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__Group_3_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group_3_1__1"


    // $ANTLR start "rule__IteratorDeclaration__Group_3_1__1__Impl"
    // InternalGeometryCodes.g:2467:1: rule__IteratorDeclaration__Group_3_1__1__Impl : ( ( rule__IteratorDeclaration__ParametersAssignment_3_1_1 ) ) ;
    public final void rule__IteratorDeclaration__Group_3_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2471:1: ( ( ( rule__IteratorDeclaration__ParametersAssignment_3_1_1 ) ) )
            // InternalGeometryCodes.g:2472:1: ( ( rule__IteratorDeclaration__ParametersAssignment_3_1_1 ) )
            {
            // InternalGeometryCodes.g:2472:1: ( ( rule__IteratorDeclaration__ParametersAssignment_3_1_1 ) )
            // InternalGeometryCodes.g:2473:2: ( rule__IteratorDeclaration__ParametersAssignment_3_1_1 )
            {
             before(grammarAccess.getIteratorDeclarationAccess().getParametersAssignment_3_1_1()); 
            // InternalGeometryCodes.g:2474:2: ( rule__IteratorDeclaration__ParametersAssignment_3_1_1 )
            // InternalGeometryCodes.g:2474:3: rule__IteratorDeclaration__ParametersAssignment_3_1_1
            {
            pushFollow(FOLLOW_2);
            rule__IteratorDeclaration__ParametersAssignment_3_1_1();

            state._fsp--;


            }

             after(grammarAccess.getIteratorDeclarationAccess().getParametersAssignment_3_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__Group_3_1__1__Impl"


    // $ANTLR start "rule__Parameter__Group__0"
    // InternalGeometryCodes.g:2483:1: rule__Parameter__Group__0 : rule__Parameter__Group__0__Impl rule__Parameter__Group__1 ;
    public final void rule__Parameter__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2487:1: ( rule__Parameter__Group__0__Impl rule__Parameter__Group__1 )
            // InternalGeometryCodes.g:2488:2: rule__Parameter__Group__0__Impl rule__Parameter__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__Parameter__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Parameter__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0"


    // $ANTLR start "rule__Parameter__Group__0__Impl"
    // InternalGeometryCodes.g:2495:1: rule__Parameter__Group__0__Impl : ( ( rule__Parameter__TypeAssignment_0 ) ) ;
    public final void rule__Parameter__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2499:1: ( ( ( rule__Parameter__TypeAssignment_0 ) ) )
            // InternalGeometryCodes.g:2500:1: ( ( rule__Parameter__TypeAssignment_0 ) )
            {
            // InternalGeometryCodes.g:2500:1: ( ( rule__Parameter__TypeAssignment_0 ) )
            // InternalGeometryCodes.g:2501:2: ( rule__Parameter__TypeAssignment_0 )
            {
             before(grammarAccess.getParameterAccess().getTypeAssignment_0()); 
            // InternalGeometryCodes.g:2502:2: ( rule__Parameter__TypeAssignment_0 )
            // InternalGeometryCodes.g:2502:3: rule__Parameter__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__0__Impl"


    // $ANTLR start "rule__Parameter__Group__1"
    // InternalGeometryCodes.g:2510:1: rule__Parameter__Group__1 : rule__Parameter__Group__1__Impl ;
    public final void rule__Parameter__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2514:1: ( rule__Parameter__Group__1__Impl )
            // InternalGeometryCodes.g:2515:2: rule__Parameter__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1"


    // $ANTLR start "rule__Parameter__Group__1__Impl"
    // InternalGeometryCodes.g:2521:1: rule__Parameter__Group__1__Impl : ( ( rule__Parameter__NameAssignment_1 ) ) ;
    public final void rule__Parameter__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2525:1: ( ( ( rule__Parameter__NameAssignment_1 ) ) )
            // InternalGeometryCodes.g:2526:1: ( ( rule__Parameter__NameAssignment_1 ) )
            {
            // InternalGeometryCodes.g:2526:1: ( ( rule__Parameter__NameAssignment_1 ) )
            // InternalGeometryCodes.g:2527:2: ( rule__Parameter__NameAssignment_1 )
            {
             before(grammarAccess.getParameterAccess().getNameAssignment_1()); 
            // InternalGeometryCodes.g:2528:2: ( rule__Parameter__NameAssignment_1 )
            // InternalGeometryCodes.g:2528:3: rule__Parameter__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Parameter__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getParameterAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__Group__1__Impl"


    // $ANTLR start "rule__MaterialDeclaration__Group__0"
    // InternalGeometryCodes.g:2537:1: rule__MaterialDeclaration__Group__0 : rule__MaterialDeclaration__Group__0__Impl rule__MaterialDeclaration__Group__1 ;
    public final void rule__MaterialDeclaration__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2541:1: ( rule__MaterialDeclaration__Group__0__Impl rule__MaterialDeclaration__Group__1 )
            // InternalGeometryCodes.g:2542:2: rule__MaterialDeclaration__Group__0__Impl rule__MaterialDeclaration__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__MaterialDeclaration__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialDeclaration__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__0"


    // $ANTLR start "rule__MaterialDeclaration__Group__0__Impl"
    // InternalGeometryCodes.g:2549:1: rule__MaterialDeclaration__Group__0__Impl : ( 'material' ) ;
    public final void rule__MaterialDeclaration__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2553:1: ( ( 'material' ) )
            // InternalGeometryCodes.g:2554:1: ( 'material' )
            {
            // InternalGeometryCodes.g:2554:1: ( 'material' )
            // InternalGeometryCodes.g:2555:2: 'material'
            {
             before(grammarAccess.getMaterialDeclarationAccess().getMaterialKeyword_0()); 
            match(input,44,FOLLOW_2); 
             after(grammarAccess.getMaterialDeclarationAccess().getMaterialKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__0__Impl"


    // $ANTLR start "rule__MaterialDeclaration__Group__1"
    // InternalGeometryCodes.g:2564:1: rule__MaterialDeclaration__Group__1 : rule__MaterialDeclaration__Group__1__Impl rule__MaterialDeclaration__Group__2 ;
    public final void rule__MaterialDeclaration__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2568:1: ( rule__MaterialDeclaration__Group__1__Impl rule__MaterialDeclaration__Group__2 )
            // InternalGeometryCodes.g:2569:2: rule__MaterialDeclaration__Group__1__Impl rule__MaterialDeclaration__Group__2
            {
            pushFollow(FOLLOW_10);
            rule__MaterialDeclaration__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialDeclaration__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__1"


    // $ANTLR start "rule__MaterialDeclaration__Group__1__Impl"
    // InternalGeometryCodes.g:2576:1: rule__MaterialDeclaration__Group__1__Impl : ( ( rule__MaterialDeclaration__NameAssignment_1 ) ) ;
    public final void rule__MaterialDeclaration__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2580:1: ( ( ( rule__MaterialDeclaration__NameAssignment_1 ) ) )
            // InternalGeometryCodes.g:2581:1: ( ( rule__MaterialDeclaration__NameAssignment_1 ) )
            {
            // InternalGeometryCodes.g:2581:1: ( ( rule__MaterialDeclaration__NameAssignment_1 ) )
            // InternalGeometryCodes.g:2582:2: ( rule__MaterialDeclaration__NameAssignment_1 )
            {
             before(grammarAccess.getMaterialDeclarationAccess().getNameAssignment_1()); 
            // InternalGeometryCodes.g:2583:2: ( rule__MaterialDeclaration__NameAssignment_1 )
            // InternalGeometryCodes.g:2583:3: rule__MaterialDeclaration__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__MaterialDeclaration__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getMaterialDeclarationAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__1__Impl"


    // $ANTLR start "rule__MaterialDeclaration__Group__2"
    // InternalGeometryCodes.g:2591:1: rule__MaterialDeclaration__Group__2 : rule__MaterialDeclaration__Group__2__Impl rule__MaterialDeclaration__Group__3 ;
    public final void rule__MaterialDeclaration__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2595:1: ( rule__MaterialDeclaration__Group__2__Impl rule__MaterialDeclaration__Group__3 )
            // InternalGeometryCodes.g:2596:2: rule__MaterialDeclaration__Group__2__Impl rule__MaterialDeclaration__Group__3
            {
            pushFollow(FOLLOW_17);
            rule__MaterialDeclaration__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialDeclaration__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__2"


    // $ANTLR start "rule__MaterialDeclaration__Group__2__Impl"
    // InternalGeometryCodes.g:2603:1: rule__MaterialDeclaration__Group__2__Impl : ( '{' ) ;
    public final void rule__MaterialDeclaration__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2607:1: ( ( '{' ) )
            // InternalGeometryCodes.g:2608:1: ( '{' )
            {
            // InternalGeometryCodes.g:2608:1: ( '{' )
            // InternalGeometryCodes.g:2609:2: '{'
            {
             before(grammarAccess.getMaterialDeclarationAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getMaterialDeclarationAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__2__Impl"


    // $ANTLR start "rule__MaterialDeclaration__Group__3"
    // InternalGeometryCodes.g:2618:1: rule__MaterialDeclaration__Group__3 : rule__MaterialDeclaration__Group__3__Impl rule__MaterialDeclaration__Group__4 ;
    public final void rule__MaterialDeclaration__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2622:1: ( rule__MaterialDeclaration__Group__3__Impl rule__MaterialDeclaration__Group__4 )
            // InternalGeometryCodes.g:2623:2: rule__MaterialDeclaration__Group__3__Impl rule__MaterialDeclaration__Group__4
            {
            pushFollow(FOLLOW_17);
            rule__MaterialDeclaration__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialDeclaration__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__3"


    // $ANTLR start "rule__MaterialDeclaration__Group__3__Impl"
    // InternalGeometryCodes.g:2630:1: rule__MaterialDeclaration__Group__3__Impl : ( ( rule__MaterialDeclaration__PropertiesAssignment_3 )* ) ;
    public final void rule__MaterialDeclaration__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2634:1: ( ( ( rule__MaterialDeclaration__PropertiesAssignment_3 )* ) )
            // InternalGeometryCodes.g:2635:1: ( ( rule__MaterialDeclaration__PropertiesAssignment_3 )* )
            {
            // InternalGeometryCodes.g:2635:1: ( ( rule__MaterialDeclaration__PropertiesAssignment_3 )* )
            // InternalGeometryCodes.g:2636:2: ( rule__MaterialDeclaration__PropertiesAssignment_3 )*
            {
             before(grammarAccess.getMaterialDeclarationAccess().getPropertiesAssignment_3()); 
            // InternalGeometryCodes.g:2637:2: ( rule__MaterialDeclaration__PropertiesAssignment_3 )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==RULE_ID) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalGeometryCodes.g:2637:3: rule__MaterialDeclaration__PropertiesAssignment_3
            	    {
            	    pushFollow(FOLLOW_18);
            	    rule__MaterialDeclaration__PropertiesAssignment_3();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);

             after(grammarAccess.getMaterialDeclarationAccess().getPropertiesAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__3__Impl"


    // $ANTLR start "rule__MaterialDeclaration__Group__4"
    // InternalGeometryCodes.g:2645:1: rule__MaterialDeclaration__Group__4 : rule__MaterialDeclaration__Group__4__Impl ;
    public final void rule__MaterialDeclaration__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2649:1: ( rule__MaterialDeclaration__Group__4__Impl )
            // InternalGeometryCodes.g:2650:2: rule__MaterialDeclaration__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MaterialDeclaration__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__4"


    // $ANTLR start "rule__MaterialDeclaration__Group__4__Impl"
    // InternalGeometryCodes.g:2656:1: rule__MaterialDeclaration__Group__4__Impl : ( '}' ) ;
    public final void rule__MaterialDeclaration__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2660:1: ( ( '}' ) )
            // InternalGeometryCodes.g:2661:1: ( '}' )
            {
            // InternalGeometryCodes.g:2661:1: ( '}' )
            // InternalGeometryCodes.g:2662:2: '}'
            {
             before(grammarAccess.getMaterialDeclarationAccess().getRightCurlyBracketKeyword_4()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getMaterialDeclarationAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__Group__4__Impl"


    // $ANTLR start "rule__MaterialStringProperty__Group__0"
    // InternalGeometryCodes.g:2672:1: rule__MaterialStringProperty__Group__0 : rule__MaterialStringProperty__Group__0__Impl rule__MaterialStringProperty__Group__1 ;
    public final void rule__MaterialStringProperty__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2676:1: ( rule__MaterialStringProperty__Group__0__Impl rule__MaterialStringProperty__Group__1 )
            // InternalGeometryCodes.g:2677:2: rule__MaterialStringProperty__Group__0__Impl rule__MaterialStringProperty__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__MaterialStringProperty__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialStringProperty__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialStringProperty__Group__0"


    // $ANTLR start "rule__MaterialStringProperty__Group__0__Impl"
    // InternalGeometryCodes.g:2684:1: rule__MaterialStringProperty__Group__0__Impl : ( ( rule__MaterialStringProperty__NameAssignment_0 ) ) ;
    public final void rule__MaterialStringProperty__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2688:1: ( ( ( rule__MaterialStringProperty__NameAssignment_0 ) ) )
            // InternalGeometryCodes.g:2689:1: ( ( rule__MaterialStringProperty__NameAssignment_0 ) )
            {
            // InternalGeometryCodes.g:2689:1: ( ( rule__MaterialStringProperty__NameAssignment_0 ) )
            // InternalGeometryCodes.g:2690:2: ( rule__MaterialStringProperty__NameAssignment_0 )
            {
             before(grammarAccess.getMaterialStringPropertyAccess().getNameAssignment_0()); 
            // InternalGeometryCodes.g:2691:2: ( rule__MaterialStringProperty__NameAssignment_0 )
            // InternalGeometryCodes.g:2691:3: rule__MaterialStringProperty__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__MaterialStringProperty__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getMaterialStringPropertyAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialStringProperty__Group__0__Impl"


    // $ANTLR start "rule__MaterialStringProperty__Group__1"
    // InternalGeometryCodes.g:2699:1: rule__MaterialStringProperty__Group__1 : rule__MaterialStringProperty__Group__1__Impl rule__MaterialStringProperty__Group__2 ;
    public final void rule__MaterialStringProperty__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2703:1: ( rule__MaterialStringProperty__Group__1__Impl rule__MaterialStringProperty__Group__2 )
            // InternalGeometryCodes.g:2704:2: rule__MaterialStringProperty__Group__1__Impl rule__MaterialStringProperty__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__MaterialStringProperty__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialStringProperty__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialStringProperty__Group__1"


    // $ANTLR start "rule__MaterialStringProperty__Group__1__Impl"
    // InternalGeometryCodes.g:2711:1: rule__MaterialStringProperty__Group__1__Impl : ( ':' ) ;
    public final void rule__MaterialStringProperty__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2715:1: ( ( ':' ) )
            // InternalGeometryCodes.g:2716:1: ( ':' )
            {
            // InternalGeometryCodes.g:2716:1: ( ':' )
            // InternalGeometryCodes.g:2717:2: ':'
            {
             before(grammarAccess.getMaterialStringPropertyAccess().getColonKeyword_1()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getMaterialStringPropertyAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialStringProperty__Group__1__Impl"


    // $ANTLR start "rule__MaterialStringProperty__Group__2"
    // InternalGeometryCodes.g:2726:1: rule__MaterialStringProperty__Group__2 : rule__MaterialStringProperty__Group__2__Impl ;
    public final void rule__MaterialStringProperty__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2730:1: ( rule__MaterialStringProperty__Group__2__Impl )
            // InternalGeometryCodes.g:2731:2: rule__MaterialStringProperty__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MaterialStringProperty__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialStringProperty__Group__2"


    // $ANTLR start "rule__MaterialStringProperty__Group__2__Impl"
    // InternalGeometryCodes.g:2737:1: rule__MaterialStringProperty__Group__2__Impl : ( ( rule__MaterialStringProperty__ValueAssignment_2 ) ) ;
    public final void rule__MaterialStringProperty__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2741:1: ( ( ( rule__MaterialStringProperty__ValueAssignment_2 ) ) )
            // InternalGeometryCodes.g:2742:1: ( ( rule__MaterialStringProperty__ValueAssignment_2 ) )
            {
            // InternalGeometryCodes.g:2742:1: ( ( rule__MaterialStringProperty__ValueAssignment_2 ) )
            // InternalGeometryCodes.g:2743:2: ( rule__MaterialStringProperty__ValueAssignment_2 )
            {
             before(grammarAccess.getMaterialStringPropertyAccess().getValueAssignment_2()); 
            // InternalGeometryCodes.g:2744:2: ( rule__MaterialStringProperty__ValueAssignment_2 )
            // InternalGeometryCodes.g:2744:3: rule__MaterialStringProperty__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__MaterialStringProperty__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMaterialStringPropertyAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialStringProperty__Group__2__Impl"


    // $ANTLR start "rule__MaterialFloatProperty__Group__0"
    // InternalGeometryCodes.g:2753:1: rule__MaterialFloatProperty__Group__0 : rule__MaterialFloatProperty__Group__0__Impl rule__MaterialFloatProperty__Group__1 ;
    public final void rule__MaterialFloatProperty__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2757:1: ( rule__MaterialFloatProperty__Group__0__Impl rule__MaterialFloatProperty__Group__1 )
            // InternalGeometryCodes.g:2758:2: rule__MaterialFloatProperty__Group__0__Impl rule__MaterialFloatProperty__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__MaterialFloatProperty__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialFloatProperty__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialFloatProperty__Group__0"


    // $ANTLR start "rule__MaterialFloatProperty__Group__0__Impl"
    // InternalGeometryCodes.g:2765:1: rule__MaterialFloatProperty__Group__0__Impl : ( ( rule__MaterialFloatProperty__NameAssignment_0 ) ) ;
    public final void rule__MaterialFloatProperty__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2769:1: ( ( ( rule__MaterialFloatProperty__NameAssignment_0 ) ) )
            // InternalGeometryCodes.g:2770:1: ( ( rule__MaterialFloatProperty__NameAssignment_0 ) )
            {
            // InternalGeometryCodes.g:2770:1: ( ( rule__MaterialFloatProperty__NameAssignment_0 ) )
            // InternalGeometryCodes.g:2771:2: ( rule__MaterialFloatProperty__NameAssignment_0 )
            {
             before(grammarAccess.getMaterialFloatPropertyAccess().getNameAssignment_0()); 
            // InternalGeometryCodes.g:2772:2: ( rule__MaterialFloatProperty__NameAssignment_0 )
            // InternalGeometryCodes.g:2772:3: rule__MaterialFloatProperty__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__MaterialFloatProperty__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getMaterialFloatPropertyAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialFloatProperty__Group__0__Impl"


    // $ANTLR start "rule__MaterialFloatProperty__Group__1"
    // InternalGeometryCodes.g:2780:1: rule__MaterialFloatProperty__Group__1 : rule__MaterialFloatProperty__Group__1__Impl rule__MaterialFloatProperty__Group__2 ;
    public final void rule__MaterialFloatProperty__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2784:1: ( rule__MaterialFloatProperty__Group__1__Impl rule__MaterialFloatProperty__Group__2 )
            // InternalGeometryCodes.g:2785:2: rule__MaterialFloatProperty__Group__1__Impl rule__MaterialFloatProperty__Group__2
            {
            pushFollow(FOLLOW_20);
            rule__MaterialFloatProperty__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialFloatProperty__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialFloatProperty__Group__1"


    // $ANTLR start "rule__MaterialFloatProperty__Group__1__Impl"
    // InternalGeometryCodes.g:2792:1: rule__MaterialFloatProperty__Group__1__Impl : ( ':' ) ;
    public final void rule__MaterialFloatProperty__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2796:1: ( ( ':' ) )
            // InternalGeometryCodes.g:2797:1: ( ':' )
            {
            // InternalGeometryCodes.g:2797:1: ( ':' )
            // InternalGeometryCodes.g:2798:2: ':'
            {
             before(grammarAccess.getMaterialFloatPropertyAccess().getColonKeyword_1()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getMaterialFloatPropertyAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialFloatProperty__Group__1__Impl"


    // $ANTLR start "rule__MaterialFloatProperty__Group__2"
    // InternalGeometryCodes.g:2807:1: rule__MaterialFloatProperty__Group__2 : rule__MaterialFloatProperty__Group__2__Impl ;
    public final void rule__MaterialFloatProperty__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2811:1: ( rule__MaterialFloatProperty__Group__2__Impl )
            // InternalGeometryCodes.g:2812:2: rule__MaterialFloatProperty__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MaterialFloatProperty__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialFloatProperty__Group__2"


    // $ANTLR start "rule__MaterialFloatProperty__Group__2__Impl"
    // InternalGeometryCodes.g:2818:1: rule__MaterialFloatProperty__Group__2__Impl : ( ( rule__MaterialFloatProperty__ValueAssignment_2 ) ) ;
    public final void rule__MaterialFloatProperty__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2822:1: ( ( ( rule__MaterialFloatProperty__ValueAssignment_2 ) ) )
            // InternalGeometryCodes.g:2823:1: ( ( rule__MaterialFloatProperty__ValueAssignment_2 ) )
            {
            // InternalGeometryCodes.g:2823:1: ( ( rule__MaterialFloatProperty__ValueAssignment_2 ) )
            // InternalGeometryCodes.g:2824:2: ( rule__MaterialFloatProperty__ValueAssignment_2 )
            {
             before(grammarAccess.getMaterialFloatPropertyAccess().getValueAssignment_2()); 
            // InternalGeometryCodes.g:2825:2: ( rule__MaterialFloatProperty__ValueAssignment_2 )
            // InternalGeometryCodes.g:2825:3: rule__MaterialFloatProperty__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__MaterialFloatProperty__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getMaterialFloatPropertyAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialFloatProperty__Group__2__Impl"


    // $ANTLR start "rule__MaterialBooleanProperty__Group__0"
    // InternalGeometryCodes.g:2834:1: rule__MaterialBooleanProperty__Group__0 : rule__MaterialBooleanProperty__Group__0__Impl rule__MaterialBooleanProperty__Group__1 ;
    public final void rule__MaterialBooleanProperty__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2838:1: ( rule__MaterialBooleanProperty__Group__0__Impl rule__MaterialBooleanProperty__Group__1 )
            // InternalGeometryCodes.g:2839:2: rule__MaterialBooleanProperty__Group__0__Impl rule__MaterialBooleanProperty__Group__1
            {
            pushFollow(FOLLOW_19);
            rule__MaterialBooleanProperty__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialBooleanProperty__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__Group__0"


    // $ANTLR start "rule__MaterialBooleanProperty__Group__0__Impl"
    // InternalGeometryCodes.g:2846:1: rule__MaterialBooleanProperty__Group__0__Impl : ( ( rule__MaterialBooleanProperty__NameAssignment_0 ) ) ;
    public final void rule__MaterialBooleanProperty__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2850:1: ( ( ( rule__MaterialBooleanProperty__NameAssignment_0 ) ) )
            // InternalGeometryCodes.g:2851:1: ( ( rule__MaterialBooleanProperty__NameAssignment_0 ) )
            {
            // InternalGeometryCodes.g:2851:1: ( ( rule__MaterialBooleanProperty__NameAssignment_0 ) )
            // InternalGeometryCodes.g:2852:2: ( rule__MaterialBooleanProperty__NameAssignment_0 )
            {
             before(grammarAccess.getMaterialBooleanPropertyAccess().getNameAssignment_0()); 
            // InternalGeometryCodes.g:2853:2: ( rule__MaterialBooleanProperty__NameAssignment_0 )
            // InternalGeometryCodes.g:2853:3: rule__MaterialBooleanProperty__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__MaterialBooleanProperty__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getMaterialBooleanPropertyAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__Group__0__Impl"


    // $ANTLR start "rule__MaterialBooleanProperty__Group__1"
    // InternalGeometryCodes.g:2861:1: rule__MaterialBooleanProperty__Group__1 : rule__MaterialBooleanProperty__Group__1__Impl rule__MaterialBooleanProperty__Group__2 ;
    public final void rule__MaterialBooleanProperty__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2865:1: ( rule__MaterialBooleanProperty__Group__1__Impl rule__MaterialBooleanProperty__Group__2 )
            // InternalGeometryCodes.g:2866:2: rule__MaterialBooleanProperty__Group__1__Impl rule__MaterialBooleanProperty__Group__2
            {
            pushFollow(FOLLOW_21);
            rule__MaterialBooleanProperty__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MaterialBooleanProperty__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__Group__1"


    // $ANTLR start "rule__MaterialBooleanProperty__Group__1__Impl"
    // InternalGeometryCodes.g:2873:1: rule__MaterialBooleanProperty__Group__1__Impl : ( ':' ) ;
    public final void rule__MaterialBooleanProperty__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2877:1: ( ( ':' ) )
            // InternalGeometryCodes.g:2878:1: ( ':' )
            {
            // InternalGeometryCodes.g:2878:1: ( ':' )
            // InternalGeometryCodes.g:2879:2: ':'
            {
             before(grammarAccess.getMaterialBooleanPropertyAccess().getColonKeyword_1()); 
            match(input,45,FOLLOW_2); 
             after(grammarAccess.getMaterialBooleanPropertyAccess().getColonKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__Group__1__Impl"


    // $ANTLR start "rule__MaterialBooleanProperty__Group__2"
    // InternalGeometryCodes.g:2888:1: rule__MaterialBooleanProperty__Group__2 : rule__MaterialBooleanProperty__Group__2__Impl ;
    public final void rule__MaterialBooleanProperty__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2892:1: ( rule__MaterialBooleanProperty__Group__2__Impl )
            // InternalGeometryCodes.g:2893:2: rule__MaterialBooleanProperty__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MaterialBooleanProperty__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__Group__2"


    // $ANTLR start "rule__MaterialBooleanProperty__Group__2__Impl"
    // InternalGeometryCodes.g:2899:1: rule__MaterialBooleanProperty__Group__2__Impl : ( ( rule__MaterialBooleanProperty__Alternatives_2 ) ) ;
    public final void rule__MaterialBooleanProperty__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2903:1: ( ( ( rule__MaterialBooleanProperty__Alternatives_2 ) ) )
            // InternalGeometryCodes.g:2904:1: ( ( rule__MaterialBooleanProperty__Alternatives_2 ) )
            {
            // InternalGeometryCodes.g:2904:1: ( ( rule__MaterialBooleanProperty__Alternatives_2 ) )
            // InternalGeometryCodes.g:2905:2: ( rule__MaterialBooleanProperty__Alternatives_2 )
            {
             before(grammarAccess.getMaterialBooleanPropertyAccess().getAlternatives_2()); 
            // InternalGeometryCodes.g:2906:2: ( rule__MaterialBooleanProperty__Alternatives_2 )
            // InternalGeometryCodes.g:2906:3: rule__MaterialBooleanProperty__Alternatives_2
            {
            pushFollow(FOLLOW_2);
            rule__MaterialBooleanProperty__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getMaterialBooleanPropertyAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__Group__2__Impl"


    // $ANTLR start "rule__DeclarationStatement__Group__0"
    // InternalGeometryCodes.g:2915:1: rule__DeclarationStatement__Group__0 : rule__DeclarationStatement__Group__0__Impl rule__DeclarationStatement__Group__1 ;
    public final void rule__DeclarationStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2919:1: ( rule__DeclarationStatement__Group__0__Impl rule__DeclarationStatement__Group__1 )
            // InternalGeometryCodes.g:2920:2: rule__DeclarationStatement__Group__0__Impl rule__DeclarationStatement__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__DeclarationStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeclarationStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__Group__0"


    // $ANTLR start "rule__DeclarationStatement__Group__0__Impl"
    // InternalGeometryCodes.g:2927:1: rule__DeclarationStatement__Group__0__Impl : ( ( rule__DeclarationStatement__TypeAssignment_0 ) ) ;
    public final void rule__DeclarationStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2931:1: ( ( ( rule__DeclarationStatement__TypeAssignment_0 ) ) )
            // InternalGeometryCodes.g:2932:1: ( ( rule__DeclarationStatement__TypeAssignment_0 ) )
            {
            // InternalGeometryCodes.g:2932:1: ( ( rule__DeclarationStatement__TypeAssignment_0 ) )
            // InternalGeometryCodes.g:2933:2: ( rule__DeclarationStatement__TypeAssignment_0 )
            {
             before(grammarAccess.getDeclarationStatementAccess().getTypeAssignment_0()); 
            // InternalGeometryCodes.g:2934:2: ( rule__DeclarationStatement__TypeAssignment_0 )
            // InternalGeometryCodes.g:2934:3: rule__DeclarationStatement__TypeAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__DeclarationStatement__TypeAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationStatementAccess().getTypeAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__Group__0__Impl"


    // $ANTLR start "rule__DeclarationStatement__Group__1"
    // InternalGeometryCodes.g:2942:1: rule__DeclarationStatement__Group__1 : rule__DeclarationStatement__Group__1__Impl rule__DeclarationStatement__Group__2 ;
    public final void rule__DeclarationStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2946:1: ( rule__DeclarationStatement__Group__1__Impl rule__DeclarationStatement__Group__2 )
            // InternalGeometryCodes.g:2947:2: rule__DeclarationStatement__Group__1__Impl rule__DeclarationStatement__Group__2
            {
            pushFollow(FOLLOW_22);
            rule__DeclarationStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeclarationStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__Group__1"


    // $ANTLR start "rule__DeclarationStatement__Group__1__Impl"
    // InternalGeometryCodes.g:2954:1: rule__DeclarationStatement__Group__1__Impl : ( ( rule__DeclarationStatement__NameAssignment_1 ) ) ;
    public final void rule__DeclarationStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2958:1: ( ( ( rule__DeclarationStatement__NameAssignment_1 ) ) )
            // InternalGeometryCodes.g:2959:1: ( ( rule__DeclarationStatement__NameAssignment_1 ) )
            {
            // InternalGeometryCodes.g:2959:1: ( ( rule__DeclarationStatement__NameAssignment_1 ) )
            // InternalGeometryCodes.g:2960:2: ( rule__DeclarationStatement__NameAssignment_1 )
            {
             before(grammarAccess.getDeclarationStatementAccess().getNameAssignment_1()); 
            // InternalGeometryCodes.g:2961:2: ( rule__DeclarationStatement__NameAssignment_1 )
            // InternalGeometryCodes.g:2961:3: rule__DeclarationStatement__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__DeclarationStatement__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationStatementAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__Group__1__Impl"


    // $ANTLR start "rule__DeclarationStatement__Group__2"
    // InternalGeometryCodes.g:2969:1: rule__DeclarationStatement__Group__2 : rule__DeclarationStatement__Group__2__Impl rule__DeclarationStatement__Group__3 ;
    public final void rule__DeclarationStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2973:1: ( rule__DeclarationStatement__Group__2__Impl rule__DeclarationStatement__Group__3 )
            // InternalGeometryCodes.g:2974:2: rule__DeclarationStatement__Group__2__Impl rule__DeclarationStatement__Group__3
            {
            pushFollow(FOLLOW_23);
            rule__DeclarationStatement__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__DeclarationStatement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__Group__2"


    // $ANTLR start "rule__DeclarationStatement__Group__2__Impl"
    // InternalGeometryCodes.g:2981:1: rule__DeclarationStatement__Group__2__Impl : ( '=' ) ;
    public final void rule__DeclarationStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:2985:1: ( ( '=' ) )
            // InternalGeometryCodes.g:2986:1: ( '=' )
            {
            // InternalGeometryCodes.g:2986:1: ( '=' )
            // InternalGeometryCodes.g:2987:2: '='
            {
             before(grammarAccess.getDeclarationStatementAccess().getEqualsSignKeyword_2()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getDeclarationStatementAccess().getEqualsSignKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__Group__2__Impl"


    // $ANTLR start "rule__DeclarationStatement__Group__3"
    // InternalGeometryCodes.g:2996:1: rule__DeclarationStatement__Group__3 : rule__DeclarationStatement__Group__3__Impl ;
    public final void rule__DeclarationStatement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3000:1: ( rule__DeclarationStatement__Group__3__Impl )
            // InternalGeometryCodes.g:3001:2: rule__DeclarationStatement__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__DeclarationStatement__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__Group__3"


    // $ANTLR start "rule__DeclarationStatement__Group__3__Impl"
    // InternalGeometryCodes.g:3007:1: rule__DeclarationStatement__Group__3__Impl : ( ( rule__DeclarationStatement__ValueAssignment_3 ) ) ;
    public final void rule__DeclarationStatement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3011:1: ( ( ( rule__DeclarationStatement__ValueAssignment_3 ) ) )
            // InternalGeometryCodes.g:3012:1: ( ( rule__DeclarationStatement__ValueAssignment_3 ) )
            {
            // InternalGeometryCodes.g:3012:1: ( ( rule__DeclarationStatement__ValueAssignment_3 ) )
            // InternalGeometryCodes.g:3013:2: ( rule__DeclarationStatement__ValueAssignment_3 )
            {
             before(grammarAccess.getDeclarationStatementAccess().getValueAssignment_3()); 
            // InternalGeometryCodes.g:3014:2: ( rule__DeclarationStatement__ValueAssignment_3 )
            // InternalGeometryCodes.g:3014:3: rule__DeclarationStatement__ValueAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__DeclarationStatement__ValueAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getDeclarationStatementAccess().getValueAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__Group__3__Impl"


    // $ANTLR start "rule__AssignmentStatement__Group__0"
    // InternalGeometryCodes.g:3023:1: rule__AssignmentStatement__Group__0 : rule__AssignmentStatement__Group__0__Impl rule__AssignmentStatement__Group__1 ;
    public final void rule__AssignmentStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3027:1: ( rule__AssignmentStatement__Group__0__Impl rule__AssignmentStatement__Group__1 )
            // InternalGeometryCodes.g:3028:2: rule__AssignmentStatement__Group__0__Impl rule__AssignmentStatement__Group__1
            {
            pushFollow(FOLLOW_24);
            rule__AssignmentStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AssignmentStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__Group__0"


    // $ANTLR start "rule__AssignmentStatement__Group__0__Impl"
    // InternalGeometryCodes.g:3035:1: rule__AssignmentStatement__Group__0__Impl : ( ( rule__AssignmentStatement__TargetAssignment_0 ) ) ;
    public final void rule__AssignmentStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3039:1: ( ( ( rule__AssignmentStatement__TargetAssignment_0 ) ) )
            // InternalGeometryCodes.g:3040:1: ( ( rule__AssignmentStatement__TargetAssignment_0 ) )
            {
            // InternalGeometryCodes.g:3040:1: ( ( rule__AssignmentStatement__TargetAssignment_0 ) )
            // InternalGeometryCodes.g:3041:2: ( rule__AssignmentStatement__TargetAssignment_0 )
            {
             before(grammarAccess.getAssignmentStatementAccess().getTargetAssignment_0()); 
            // InternalGeometryCodes.g:3042:2: ( rule__AssignmentStatement__TargetAssignment_0 )
            // InternalGeometryCodes.g:3042:3: rule__AssignmentStatement__TargetAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__AssignmentStatement__TargetAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentStatementAccess().getTargetAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__Group__0__Impl"


    // $ANTLR start "rule__AssignmentStatement__Group__1"
    // InternalGeometryCodes.g:3050:1: rule__AssignmentStatement__Group__1 : rule__AssignmentStatement__Group__1__Impl rule__AssignmentStatement__Group__2 ;
    public final void rule__AssignmentStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3054:1: ( rule__AssignmentStatement__Group__1__Impl rule__AssignmentStatement__Group__2 )
            // InternalGeometryCodes.g:3055:2: rule__AssignmentStatement__Group__1__Impl rule__AssignmentStatement__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__AssignmentStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AssignmentStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__Group__1"


    // $ANTLR start "rule__AssignmentStatement__Group__1__Impl"
    // InternalGeometryCodes.g:3062:1: rule__AssignmentStatement__Group__1__Impl : ( ( rule__AssignmentStatement__OperatorAssignment_1 ) ) ;
    public final void rule__AssignmentStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3066:1: ( ( ( rule__AssignmentStatement__OperatorAssignment_1 ) ) )
            // InternalGeometryCodes.g:3067:1: ( ( rule__AssignmentStatement__OperatorAssignment_1 ) )
            {
            // InternalGeometryCodes.g:3067:1: ( ( rule__AssignmentStatement__OperatorAssignment_1 ) )
            // InternalGeometryCodes.g:3068:2: ( rule__AssignmentStatement__OperatorAssignment_1 )
            {
             before(grammarAccess.getAssignmentStatementAccess().getOperatorAssignment_1()); 
            // InternalGeometryCodes.g:3069:2: ( rule__AssignmentStatement__OperatorAssignment_1 )
            // InternalGeometryCodes.g:3069:3: rule__AssignmentStatement__OperatorAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__AssignmentStatement__OperatorAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentStatementAccess().getOperatorAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__Group__1__Impl"


    // $ANTLR start "rule__AssignmentStatement__Group__2"
    // InternalGeometryCodes.g:3077:1: rule__AssignmentStatement__Group__2 : rule__AssignmentStatement__Group__2__Impl ;
    public final void rule__AssignmentStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3081:1: ( rule__AssignmentStatement__Group__2__Impl )
            // InternalGeometryCodes.g:3082:2: rule__AssignmentStatement__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AssignmentStatement__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__Group__2"


    // $ANTLR start "rule__AssignmentStatement__Group__2__Impl"
    // InternalGeometryCodes.g:3088:1: rule__AssignmentStatement__Group__2__Impl : ( ( rule__AssignmentStatement__ValueAssignment_2 ) ) ;
    public final void rule__AssignmentStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3092:1: ( ( ( rule__AssignmentStatement__ValueAssignment_2 ) ) )
            // InternalGeometryCodes.g:3093:1: ( ( rule__AssignmentStatement__ValueAssignment_2 ) )
            {
            // InternalGeometryCodes.g:3093:1: ( ( rule__AssignmentStatement__ValueAssignment_2 ) )
            // InternalGeometryCodes.g:3094:2: ( rule__AssignmentStatement__ValueAssignment_2 )
            {
             before(grammarAccess.getAssignmentStatementAccess().getValueAssignment_2()); 
            // InternalGeometryCodes.g:3095:2: ( rule__AssignmentStatement__ValueAssignment_2 )
            // InternalGeometryCodes.g:3095:3: rule__AssignmentStatement__ValueAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__AssignmentStatement__ValueAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getAssignmentStatementAccess().getValueAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__Group__2__Impl"


    // $ANTLR start "rule__ReturnStatement__Group__0"
    // InternalGeometryCodes.g:3104:1: rule__ReturnStatement__Group__0 : rule__ReturnStatement__Group__0__Impl rule__ReturnStatement__Group__1 ;
    public final void rule__ReturnStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3108:1: ( rule__ReturnStatement__Group__0__Impl rule__ReturnStatement__Group__1 )
            // InternalGeometryCodes.g:3109:2: rule__ReturnStatement__Group__0__Impl rule__ReturnStatement__Group__1
            {
            pushFollow(FOLLOW_25);
            rule__ReturnStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReturnStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnStatement__Group__0"


    // $ANTLR start "rule__ReturnStatement__Group__0__Impl"
    // InternalGeometryCodes.g:3116:1: rule__ReturnStatement__Group__0__Impl : ( () ) ;
    public final void rule__ReturnStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3120:1: ( ( () ) )
            // InternalGeometryCodes.g:3121:1: ( () )
            {
            // InternalGeometryCodes.g:3121:1: ( () )
            // InternalGeometryCodes.g:3122:2: ()
            {
             before(grammarAccess.getReturnStatementAccess().getReturnStatementAction_0()); 
            // InternalGeometryCodes.g:3123:2: ()
            // InternalGeometryCodes.g:3123:3: 
            {
            }

             after(grammarAccess.getReturnStatementAccess().getReturnStatementAction_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnStatement__Group__0__Impl"


    // $ANTLR start "rule__ReturnStatement__Group__1"
    // InternalGeometryCodes.g:3131:1: rule__ReturnStatement__Group__1 : rule__ReturnStatement__Group__1__Impl rule__ReturnStatement__Group__2 ;
    public final void rule__ReturnStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3135:1: ( rule__ReturnStatement__Group__1__Impl rule__ReturnStatement__Group__2 )
            // InternalGeometryCodes.g:3136:2: rule__ReturnStatement__Group__1__Impl rule__ReturnStatement__Group__2
            {
            pushFollow(FOLLOW_26);
            rule__ReturnStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReturnStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnStatement__Group__1"


    // $ANTLR start "rule__ReturnStatement__Group__1__Impl"
    // InternalGeometryCodes.g:3143:1: rule__ReturnStatement__Group__1__Impl : ( 'return' ) ;
    public final void rule__ReturnStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3147:1: ( ( 'return' ) )
            // InternalGeometryCodes.g:3148:1: ( 'return' )
            {
            // InternalGeometryCodes.g:3148:1: ( 'return' )
            // InternalGeometryCodes.g:3149:2: 'return'
            {
             before(grammarAccess.getReturnStatementAccess().getReturnKeyword_1()); 
            match(input,46,FOLLOW_2); 
             after(grammarAccess.getReturnStatementAccess().getReturnKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnStatement__Group__1__Impl"


    // $ANTLR start "rule__ReturnStatement__Group__2"
    // InternalGeometryCodes.g:3158:1: rule__ReturnStatement__Group__2 : rule__ReturnStatement__Group__2__Impl ;
    public final void rule__ReturnStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3162:1: ( rule__ReturnStatement__Group__2__Impl )
            // InternalGeometryCodes.g:3163:2: rule__ReturnStatement__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReturnStatement__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnStatement__Group__2"


    // $ANTLR start "rule__ReturnStatement__Group__2__Impl"
    // InternalGeometryCodes.g:3169:1: rule__ReturnStatement__Group__2__Impl : ( ( rule__ReturnStatement__Alternatives_2 ) ) ;
    public final void rule__ReturnStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3173:1: ( ( ( rule__ReturnStatement__Alternatives_2 ) ) )
            // InternalGeometryCodes.g:3174:1: ( ( rule__ReturnStatement__Alternatives_2 ) )
            {
            // InternalGeometryCodes.g:3174:1: ( ( rule__ReturnStatement__Alternatives_2 ) )
            // InternalGeometryCodes.g:3175:2: ( rule__ReturnStatement__Alternatives_2 )
            {
             before(grammarAccess.getReturnStatementAccess().getAlternatives_2()); 
            // InternalGeometryCodes.g:3176:2: ( rule__ReturnStatement__Alternatives_2 )
            // InternalGeometryCodes.g:3176:3: rule__ReturnStatement__Alternatives_2
            {
            pushFollow(FOLLOW_2);
            rule__ReturnStatement__Alternatives_2();

            state._fsp--;


            }

             after(grammarAccess.getReturnStatementAccess().getAlternatives_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnStatement__Group__2__Impl"


    // $ANTLR start "rule__IfStatement__Group__0"
    // InternalGeometryCodes.g:3185:1: rule__IfStatement__Group__0 : rule__IfStatement__Group__0__Impl rule__IfStatement__Group__1 ;
    public final void rule__IfStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3189:1: ( rule__IfStatement__Group__0__Impl rule__IfStatement__Group__1 )
            // InternalGeometryCodes.g:3190:2: rule__IfStatement__Group__0__Impl rule__IfStatement__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__IfStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__0"


    // $ANTLR start "rule__IfStatement__Group__0__Impl"
    // InternalGeometryCodes.g:3197:1: rule__IfStatement__Group__0__Impl : ( 'if' ) ;
    public final void rule__IfStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3201:1: ( ( 'if' ) )
            // InternalGeometryCodes.g:3202:1: ( 'if' )
            {
            // InternalGeometryCodes.g:3202:1: ( 'if' )
            // InternalGeometryCodes.g:3203:2: 'if'
            {
             before(grammarAccess.getIfStatementAccess().getIfKeyword_0()); 
            match(input,47,FOLLOW_2); 
             after(grammarAccess.getIfStatementAccess().getIfKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__0__Impl"


    // $ANTLR start "rule__IfStatement__Group__1"
    // InternalGeometryCodes.g:3212:1: rule__IfStatement__Group__1 : rule__IfStatement__Group__1__Impl rule__IfStatement__Group__2 ;
    public final void rule__IfStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3216:1: ( rule__IfStatement__Group__1__Impl rule__IfStatement__Group__2 )
            // InternalGeometryCodes.g:3217:2: rule__IfStatement__Group__1__Impl rule__IfStatement__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__IfStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__1"


    // $ANTLR start "rule__IfStatement__Group__1__Impl"
    // InternalGeometryCodes.g:3224:1: rule__IfStatement__Group__1__Impl : ( '(' ) ;
    public final void rule__IfStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3228:1: ( ( '(' ) )
            // InternalGeometryCodes.g:3229:1: ( '(' )
            {
            // InternalGeometryCodes.g:3229:1: ( '(' )
            // InternalGeometryCodes.g:3230:2: '('
            {
             before(grammarAccess.getIfStatementAccess().getLeftParenthesisKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getIfStatementAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__1__Impl"


    // $ANTLR start "rule__IfStatement__Group__2"
    // InternalGeometryCodes.g:3239:1: rule__IfStatement__Group__2 : rule__IfStatement__Group__2__Impl rule__IfStatement__Group__3 ;
    public final void rule__IfStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3243:1: ( rule__IfStatement__Group__2__Impl rule__IfStatement__Group__3 )
            // InternalGeometryCodes.g:3244:2: rule__IfStatement__Group__2__Impl rule__IfStatement__Group__3
            {
            pushFollow(FOLLOW_27);
            rule__IfStatement__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfStatement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__2"


    // $ANTLR start "rule__IfStatement__Group__2__Impl"
    // InternalGeometryCodes.g:3251:1: rule__IfStatement__Group__2__Impl : ( ( rule__IfStatement__ConditionAssignment_2 ) ) ;
    public final void rule__IfStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3255:1: ( ( ( rule__IfStatement__ConditionAssignment_2 ) ) )
            // InternalGeometryCodes.g:3256:1: ( ( rule__IfStatement__ConditionAssignment_2 ) )
            {
            // InternalGeometryCodes.g:3256:1: ( ( rule__IfStatement__ConditionAssignment_2 ) )
            // InternalGeometryCodes.g:3257:2: ( rule__IfStatement__ConditionAssignment_2 )
            {
             before(grammarAccess.getIfStatementAccess().getConditionAssignment_2()); 
            // InternalGeometryCodes.g:3258:2: ( rule__IfStatement__ConditionAssignment_2 )
            // InternalGeometryCodes.g:3258:3: rule__IfStatement__ConditionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__IfStatement__ConditionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getIfStatementAccess().getConditionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__2__Impl"


    // $ANTLR start "rule__IfStatement__Group__3"
    // InternalGeometryCodes.g:3266:1: rule__IfStatement__Group__3 : rule__IfStatement__Group__3__Impl rule__IfStatement__Group__4 ;
    public final void rule__IfStatement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3270:1: ( rule__IfStatement__Group__3__Impl rule__IfStatement__Group__4 )
            // InternalGeometryCodes.g:3271:2: rule__IfStatement__Group__3__Impl rule__IfStatement__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__IfStatement__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfStatement__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__3"


    // $ANTLR start "rule__IfStatement__Group__3__Impl"
    // InternalGeometryCodes.g:3278:1: rule__IfStatement__Group__3__Impl : ( ')' ) ;
    public final void rule__IfStatement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3282:1: ( ( ')' ) )
            // InternalGeometryCodes.g:3283:1: ( ')' )
            {
            // InternalGeometryCodes.g:3283:1: ( ')' )
            // InternalGeometryCodes.g:3284:2: ')'
            {
             before(grammarAccess.getIfStatementAccess().getRightParenthesisKeyword_3()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getIfStatementAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__3__Impl"


    // $ANTLR start "rule__IfStatement__Group__4"
    // InternalGeometryCodes.g:3293:1: rule__IfStatement__Group__4 : rule__IfStatement__Group__4__Impl rule__IfStatement__Group__5 ;
    public final void rule__IfStatement__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3297:1: ( rule__IfStatement__Group__4__Impl rule__IfStatement__Group__5 )
            // InternalGeometryCodes.g:3298:2: rule__IfStatement__Group__4__Impl rule__IfStatement__Group__5
            {
            pushFollow(FOLLOW_11);
            rule__IfStatement__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfStatement__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__4"


    // $ANTLR start "rule__IfStatement__Group__4__Impl"
    // InternalGeometryCodes.g:3305:1: rule__IfStatement__Group__4__Impl : ( '{' ) ;
    public final void rule__IfStatement__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3309:1: ( ( '{' ) )
            // InternalGeometryCodes.g:3310:1: ( '{' )
            {
            // InternalGeometryCodes.g:3310:1: ( '{' )
            // InternalGeometryCodes.g:3311:2: '{'
            {
             before(grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__4__Impl"


    // $ANTLR start "rule__IfStatement__Group__5"
    // InternalGeometryCodes.g:3320:1: rule__IfStatement__Group__5 : rule__IfStatement__Group__5__Impl rule__IfStatement__Group__6 ;
    public final void rule__IfStatement__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3324:1: ( rule__IfStatement__Group__5__Impl rule__IfStatement__Group__6 )
            // InternalGeometryCodes.g:3325:2: rule__IfStatement__Group__5__Impl rule__IfStatement__Group__6
            {
            pushFollow(FOLLOW_11);
            rule__IfStatement__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__IfStatement__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__5"


    // $ANTLR start "rule__IfStatement__Group__5__Impl"
    // InternalGeometryCodes.g:3332:1: rule__IfStatement__Group__5__Impl : ( ( rule__IfStatement__StatementsAssignment_5 )* ) ;
    public final void rule__IfStatement__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3336:1: ( ( ( rule__IfStatement__StatementsAssignment_5 )* ) )
            // InternalGeometryCodes.g:3337:1: ( ( rule__IfStatement__StatementsAssignment_5 )* )
            {
            // InternalGeometryCodes.g:3337:1: ( ( rule__IfStatement__StatementsAssignment_5 )* )
            // InternalGeometryCodes.g:3338:2: ( rule__IfStatement__StatementsAssignment_5 )*
            {
             before(grammarAccess.getIfStatementAccess().getStatementsAssignment_5()); 
            // InternalGeometryCodes.g:3339:2: ( rule__IfStatement__StatementsAssignment_5 )*
            loop27:
            do {
                int alt27=2;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==RULE_ID||(LA27_0>=29 && LA27_0<=34)||(LA27_0>=46 && LA27_0<=48)||(LA27_0>=52 && LA27_0<=53)) ) {
                    alt27=1;
                }


                switch (alt27) {
            	case 1 :
            	    // InternalGeometryCodes.g:3339:3: rule__IfStatement__StatementsAssignment_5
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__IfStatement__StatementsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

             after(grammarAccess.getIfStatementAccess().getStatementsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__5__Impl"


    // $ANTLR start "rule__IfStatement__Group__6"
    // InternalGeometryCodes.g:3347:1: rule__IfStatement__Group__6 : rule__IfStatement__Group__6__Impl ;
    public final void rule__IfStatement__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3351:1: ( rule__IfStatement__Group__6__Impl )
            // InternalGeometryCodes.g:3352:2: rule__IfStatement__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__IfStatement__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__6"


    // $ANTLR start "rule__IfStatement__Group__6__Impl"
    // InternalGeometryCodes.g:3358:1: rule__IfStatement__Group__6__Impl : ( '}' ) ;
    public final void rule__IfStatement__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3362:1: ( ( '}' ) )
            // InternalGeometryCodes.g:3363:1: ( '}' )
            {
            // InternalGeometryCodes.g:3363:1: ( '}' )
            // InternalGeometryCodes.g:3364:2: '}'
            {
             before(grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_6()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__Group__6__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__0"
    // InternalGeometryCodes.g:3374:1: rule__ForLoopStatement__Group__0 : rule__ForLoopStatement__Group__0__Impl rule__ForLoopStatement__Group__1 ;
    public final void rule__ForLoopStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3378:1: ( rule__ForLoopStatement__Group__0__Impl rule__ForLoopStatement__Group__1 )
            // InternalGeometryCodes.g:3379:2: rule__ForLoopStatement__Group__0__Impl rule__ForLoopStatement__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__ForLoopStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__0"


    // $ANTLR start "rule__ForLoopStatement__Group__0__Impl"
    // InternalGeometryCodes.g:3386:1: rule__ForLoopStatement__Group__0__Impl : ( 'for' ) ;
    public final void rule__ForLoopStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3390:1: ( ( 'for' ) )
            // InternalGeometryCodes.g:3391:1: ( 'for' )
            {
            // InternalGeometryCodes.g:3391:1: ( 'for' )
            // InternalGeometryCodes.g:3392:2: 'for'
            {
             before(grammarAccess.getForLoopStatementAccess().getForKeyword_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getForLoopStatementAccess().getForKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__0__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__1"
    // InternalGeometryCodes.g:3401:1: rule__ForLoopStatement__Group__1 : rule__ForLoopStatement__Group__1__Impl rule__ForLoopStatement__Group__2 ;
    public final void rule__ForLoopStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3405:1: ( rule__ForLoopStatement__Group__1__Impl rule__ForLoopStatement__Group__2 )
            // InternalGeometryCodes.g:3406:2: rule__ForLoopStatement__Group__1__Impl rule__ForLoopStatement__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__ForLoopStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__1"


    // $ANTLR start "rule__ForLoopStatement__Group__1__Impl"
    // InternalGeometryCodes.g:3413:1: rule__ForLoopStatement__Group__1__Impl : ( '(' ) ;
    public final void rule__ForLoopStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3417:1: ( ( '(' ) )
            // InternalGeometryCodes.g:3418:1: ( '(' )
            {
            // InternalGeometryCodes.g:3418:1: ( '(' )
            // InternalGeometryCodes.g:3419:2: '('
            {
             before(grammarAccess.getForLoopStatementAccess().getLeftParenthesisKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getForLoopStatementAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__1__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__2"
    // InternalGeometryCodes.g:3428:1: rule__ForLoopStatement__Group__2 : rule__ForLoopStatement__Group__2__Impl rule__ForLoopStatement__Group__3 ;
    public final void rule__ForLoopStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3432:1: ( rule__ForLoopStatement__Group__2__Impl rule__ForLoopStatement__Group__3 )
            // InternalGeometryCodes.g:3433:2: rule__ForLoopStatement__Group__2__Impl rule__ForLoopStatement__Group__3
            {
            pushFollow(FOLLOW_7);
            rule__ForLoopStatement__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__2"


    // $ANTLR start "rule__ForLoopStatement__Group__2__Impl"
    // InternalGeometryCodes.g:3440:1: rule__ForLoopStatement__Group__2__Impl : ( ( rule__ForLoopStatement__TypeAssignment_2 ) ) ;
    public final void rule__ForLoopStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3444:1: ( ( ( rule__ForLoopStatement__TypeAssignment_2 ) ) )
            // InternalGeometryCodes.g:3445:1: ( ( rule__ForLoopStatement__TypeAssignment_2 ) )
            {
            // InternalGeometryCodes.g:3445:1: ( ( rule__ForLoopStatement__TypeAssignment_2 ) )
            // InternalGeometryCodes.g:3446:2: ( rule__ForLoopStatement__TypeAssignment_2 )
            {
             before(grammarAccess.getForLoopStatementAccess().getTypeAssignment_2()); 
            // InternalGeometryCodes.g:3447:2: ( rule__ForLoopStatement__TypeAssignment_2 )
            // InternalGeometryCodes.g:3447:3: rule__ForLoopStatement__TypeAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__TypeAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getForLoopStatementAccess().getTypeAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__2__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__3"
    // InternalGeometryCodes.g:3455:1: rule__ForLoopStatement__Group__3 : rule__ForLoopStatement__Group__3__Impl rule__ForLoopStatement__Group__4 ;
    public final void rule__ForLoopStatement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3459:1: ( rule__ForLoopStatement__Group__3__Impl rule__ForLoopStatement__Group__4 )
            // InternalGeometryCodes.g:3460:2: rule__ForLoopStatement__Group__3__Impl rule__ForLoopStatement__Group__4
            {
            pushFollow(FOLLOW_28);
            rule__ForLoopStatement__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__3"


    // $ANTLR start "rule__ForLoopStatement__Group__3__Impl"
    // InternalGeometryCodes.g:3467:1: rule__ForLoopStatement__Group__3__Impl : ( ( rule__ForLoopStatement__NameAssignment_3 ) ) ;
    public final void rule__ForLoopStatement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3471:1: ( ( ( rule__ForLoopStatement__NameAssignment_3 ) ) )
            // InternalGeometryCodes.g:3472:1: ( ( rule__ForLoopStatement__NameAssignment_3 ) )
            {
            // InternalGeometryCodes.g:3472:1: ( ( rule__ForLoopStatement__NameAssignment_3 ) )
            // InternalGeometryCodes.g:3473:2: ( rule__ForLoopStatement__NameAssignment_3 )
            {
             before(grammarAccess.getForLoopStatementAccess().getNameAssignment_3()); 
            // InternalGeometryCodes.g:3474:2: ( rule__ForLoopStatement__NameAssignment_3 )
            // InternalGeometryCodes.g:3474:3: rule__ForLoopStatement__NameAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__NameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getForLoopStatementAccess().getNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__3__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__4"
    // InternalGeometryCodes.g:3482:1: rule__ForLoopStatement__Group__4 : rule__ForLoopStatement__Group__4__Impl rule__ForLoopStatement__Group__5 ;
    public final void rule__ForLoopStatement__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3486:1: ( rule__ForLoopStatement__Group__4__Impl rule__ForLoopStatement__Group__5 )
            // InternalGeometryCodes.g:3487:2: rule__ForLoopStatement__Group__4__Impl rule__ForLoopStatement__Group__5
            {
            pushFollow(FOLLOW_23);
            rule__ForLoopStatement__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__4"


    // $ANTLR start "rule__ForLoopStatement__Group__4__Impl"
    // InternalGeometryCodes.g:3494:1: rule__ForLoopStatement__Group__4__Impl : ( 'from' ) ;
    public final void rule__ForLoopStatement__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3498:1: ( ( 'from' ) )
            // InternalGeometryCodes.g:3499:1: ( 'from' )
            {
            // InternalGeometryCodes.g:3499:1: ( 'from' )
            // InternalGeometryCodes.g:3500:2: 'from'
            {
             before(grammarAccess.getForLoopStatementAccess().getFromKeyword_4()); 
            match(input,49,FOLLOW_2); 
             after(grammarAccess.getForLoopStatementAccess().getFromKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__4__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__5"
    // InternalGeometryCodes.g:3509:1: rule__ForLoopStatement__Group__5 : rule__ForLoopStatement__Group__5__Impl rule__ForLoopStatement__Group__6 ;
    public final void rule__ForLoopStatement__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3513:1: ( rule__ForLoopStatement__Group__5__Impl rule__ForLoopStatement__Group__6 )
            // InternalGeometryCodes.g:3514:2: rule__ForLoopStatement__Group__5__Impl rule__ForLoopStatement__Group__6
            {
            pushFollow(FOLLOW_29);
            rule__ForLoopStatement__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__5"


    // $ANTLR start "rule__ForLoopStatement__Group__5__Impl"
    // InternalGeometryCodes.g:3521:1: rule__ForLoopStatement__Group__5__Impl : ( ( rule__ForLoopStatement__FromValueAssignment_5 ) ) ;
    public final void rule__ForLoopStatement__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3525:1: ( ( ( rule__ForLoopStatement__FromValueAssignment_5 ) ) )
            // InternalGeometryCodes.g:3526:1: ( ( rule__ForLoopStatement__FromValueAssignment_5 ) )
            {
            // InternalGeometryCodes.g:3526:1: ( ( rule__ForLoopStatement__FromValueAssignment_5 ) )
            // InternalGeometryCodes.g:3527:2: ( rule__ForLoopStatement__FromValueAssignment_5 )
            {
             before(grammarAccess.getForLoopStatementAccess().getFromValueAssignment_5()); 
            // InternalGeometryCodes.g:3528:2: ( rule__ForLoopStatement__FromValueAssignment_5 )
            // InternalGeometryCodes.g:3528:3: rule__ForLoopStatement__FromValueAssignment_5
            {
            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__FromValueAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getForLoopStatementAccess().getFromValueAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__5__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__6"
    // InternalGeometryCodes.g:3536:1: rule__ForLoopStatement__Group__6 : rule__ForLoopStatement__Group__6__Impl rule__ForLoopStatement__Group__7 ;
    public final void rule__ForLoopStatement__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3540:1: ( rule__ForLoopStatement__Group__6__Impl rule__ForLoopStatement__Group__7 )
            // InternalGeometryCodes.g:3541:2: rule__ForLoopStatement__Group__6__Impl rule__ForLoopStatement__Group__7
            {
            pushFollow(FOLLOW_23);
            rule__ForLoopStatement__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__6"


    // $ANTLR start "rule__ForLoopStatement__Group__6__Impl"
    // InternalGeometryCodes.g:3548:1: rule__ForLoopStatement__Group__6__Impl : ( 'to' ) ;
    public final void rule__ForLoopStatement__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3552:1: ( ( 'to' ) )
            // InternalGeometryCodes.g:3553:1: ( 'to' )
            {
            // InternalGeometryCodes.g:3553:1: ( 'to' )
            // InternalGeometryCodes.g:3554:2: 'to'
            {
             before(grammarAccess.getForLoopStatementAccess().getToKeyword_6()); 
            match(input,50,FOLLOW_2); 
             after(grammarAccess.getForLoopStatementAccess().getToKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__6__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__7"
    // InternalGeometryCodes.g:3563:1: rule__ForLoopStatement__Group__7 : rule__ForLoopStatement__Group__7__Impl rule__ForLoopStatement__Group__8 ;
    public final void rule__ForLoopStatement__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3567:1: ( rule__ForLoopStatement__Group__7__Impl rule__ForLoopStatement__Group__8 )
            // InternalGeometryCodes.g:3568:2: rule__ForLoopStatement__Group__7__Impl rule__ForLoopStatement__Group__8
            {
            pushFollow(FOLLOW_27);
            rule__ForLoopStatement__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__7"


    // $ANTLR start "rule__ForLoopStatement__Group__7__Impl"
    // InternalGeometryCodes.g:3575:1: rule__ForLoopStatement__Group__7__Impl : ( ( rule__ForLoopStatement__ToValueAssignment_7 ) ) ;
    public final void rule__ForLoopStatement__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3579:1: ( ( ( rule__ForLoopStatement__ToValueAssignment_7 ) ) )
            // InternalGeometryCodes.g:3580:1: ( ( rule__ForLoopStatement__ToValueAssignment_7 ) )
            {
            // InternalGeometryCodes.g:3580:1: ( ( rule__ForLoopStatement__ToValueAssignment_7 ) )
            // InternalGeometryCodes.g:3581:2: ( rule__ForLoopStatement__ToValueAssignment_7 )
            {
             before(grammarAccess.getForLoopStatementAccess().getToValueAssignment_7()); 
            // InternalGeometryCodes.g:3582:2: ( rule__ForLoopStatement__ToValueAssignment_7 )
            // InternalGeometryCodes.g:3582:3: rule__ForLoopStatement__ToValueAssignment_7
            {
            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__ToValueAssignment_7();

            state._fsp--;


            }

             after(grammarAccess.getForLoopStatementAccess().getToValueAssignment_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__7__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__8"
    // InternalGeometryCodes.g:3590:1: rule__ForLoopStatement__Group__8 : rule__ForLoopStatement__Group__8__Impl rule__ForLoopStatement__Group__9 ;
    public final void rule__ForLoopStatement__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3594:1: ( rule__ForLoopStatement__Group__8__Impl rule__ForLoopStatement__Group__9 )
            // InternalGeometryCodes.g:3595:2: rule__ForLoopStatement__Group__8__Impl rule__ForLoopStatement__Group__9
            {
            pushFollow(FOLLOW_10);
            rule__ForLoopStatement__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__8"


    // $ANTLR start "rule__ForLoopStatement__Group__8__Impl"
    // InternalGeometryCodes.g:3602:1: rule__ForLoopStatement__Group__8__Impl : ( ')' ) ;
    public final void rule__ForLoopStatement__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3606:1: ( ( ')' ) )
            // InternalGeometryCodes.g:3607:1: ( ')' )
            {
            // InternalGeometryCodes.g:3607:1: ( ')' )
            // InternalGeometryCodes.g:3608:2: ')'
            {
             before(grammarAccess.getForLoopStatementAccess().getRightParenthesisKeyword_8()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getForLoopStatementAccess().getRightParenthesisKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__8__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__9"
    // InternalGeometryCodes.g:3617:1: rule__ForLoopStatement__Group__9 : rule__ForLoopStatement__Group__9__Impl rule__ForLoopStatement__Group__10 ;
    public final void rule__ForLoopStatement__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3621:1: ( rule__ForLoopStatement__Group__9__Impl rule__ForLoopStatement__Group__10 )
            // InternalGeometryCodes.g:3622:2: rule__ForLoopStatement__Group__9__Impl rule__ForLoopStatement__Group__10
            {
            pushFollow(FOLLOW_11);
            rule__ForLoopStatement__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__9"


    // $ANTLR start "rule__ForLoopStatement__Group__9__Impl"
    // InternalGeometryCodes.g:3629:1: rule__ForLoopStatement__Group__9__Impl : ( '{' ) ;
    public final void rule__ForLoopStatement__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3633:1: ( ( '{' ) )
            // InternalGeometryCodes.g:3634:1: ( '{' )
            {
            // InternalGeometryCodes.g:3634:1: ( '{' )
            // InternalGeometryCodes.g:3635:2: '{'
            {
             before(grammarAccess.getForLoopStatementAccess().getLeftCurlyBracketKeyword_9()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getForLoopStatementAccess().getLeftCurlyBracketKeyword_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__9__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__10"
    // InternalGeometryCodes.g:3644:1: rule__ForLoopStatement__Group__10 : rule__ForLoopStatement__Group__10__Impl rule__ForLoopStatement__Group__11 ;
    public final void rule__ForLoopStatement__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3648:1: ( rule__ForLoopStatement__Group__10__Impl rule__ForLoopStatement__Group__11 )
            // InternalGeometryCodes.g:3649:2: rule__ForLoopStatement__Group__10__Impl rule__ForLoopStatement__Group__11
            {
            pushFollow(FOLLOW_11);
            rule__ForLoopStatement__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__10"


    // $ANTLR start "rule__ForLoopStatement__Group__10__Impl"
    // InternalGeometryCodes.g:3656:1: rule__ForLoopStatement__Group__10__Impl : ( ( rule__ForLoopStatement__StatementsAssignment_10 )* ) ;
    public final void rule__ForLoopStatement__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3660:1: ( ( ( rule__ForLoopStatement__StatementsAssignment_10 )* ) )
            // InternalGeometryCodes.g:3661:1: ( ( rule__ForLoopStatement__StatementsAssignment_10 )* )
            {
            // InternalGeometryCodes.g:3661:1: ( ( rule__ForLoopStatement__StatementsAssignment_10 )* )
            // InternalGeometryCodes.g:3662:2: ( rule__ForLoopStatement__StatementsAssignment_10 )*
            {
             before(grammarAccess.getForLoopStatementAccess().getStatementsAssignment_10()); 
            // InternalGeometryCodes.g:3663:2: ( rule__ForLoopStatement__StatementsAssignment_10 )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==RULE_ID||(LA28_0>=29 && LA28_0<=34)||(LA28_0>=46 && LA28_0<=48)||(LA28_0>=52 && LA28_0<=53)) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalGeometryCodes.g:3663:3: rule__ForLoopStatement__StatementsAssignment_10
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__ForLoopStatement__StatementsAssignment_10();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

             after(grammarAccess.getForLoopStatementAccess().getStatementsAssignment_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__10__Impl"


    // $ANTLR start "rule__ForLoopStatement__Group__11"
    // InternalGeometryCodes.g:3671:1: rule__ForLoopStatement__Group__11 : rule__ForLoopStatement__Group__11__Impl ;
    public final void rule__ForLoopStatement__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3675:1: ( rule__ForLoopStatement__Group__11__Impl )
            // InternalGeometryCodes.g:3676:2: rule__ForLoopStatement__Group__11__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ForLoopStatement__Group__11__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__11"


    // $ANTLR start "rule__ForLoopStatement__Group__11__Impl"
    // InternalGeometryCodes.g:3682:1: rule__ForLoopStatement__Group__11__Impl : ( '}' ) ;
    public final void rule__ForLoopStatement__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3686:1: ( ( '}' ) )
            // InternalGeometryCodes.g:3687:1: ( '}' )
            {
            // InternalGeometryCodes.g:3687:1: ( '}' )
            // InternalGeometryCodes.g:3688:2: '}'
            {
             before(grammarAccess.getForLoopStatementAccess().getRightCurlyBracketKeyword_11()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getForLoopStatementAccess().getRightCurlyBracketKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__Group__11__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__0"
    // InternalGeometryCodes.g:3698:1: rule__ForEachLoopStatement__Group__0 : rule__ForEachLoopStatement__Group__0__Impl rule__ForEachLoopStatement__Group__1 ;
    public final void rule__ForEachLoopStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3702:1: ( rule__ForEachLoopStatement__Group__0__Impl rule__ForEachLoopStatement__Group__1 )
            // InternalGeometryCodes.g:3703:2: rule__ForEachLoopStatement__Group__0__Impl rule__ForEachLoopStatement__Group__1
            {
            pushFollow(FOLLOW_16);
            rule__ForEachLoopStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__0"


    // $ANTLR start "rule__ForEachLoopStatement__Group__0__Impl"
    // InternalGeometryCodes.g:3710:1: rule__ForEachLoopStatement__Group__0__Impl : ( 'for' ) ;
    public final void rule__ForEachLoopStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3714:1: ( ( 'for' ) )
            // InternalGeometryCodes.g:3715:1: ( 'for' )
            {
            // InternalGeometryCodes.g:3715:1: ( 'for' )
            // InternalGeometryCodes.g:3716:2: 'for'
            {
             before(grammarAccess.getForEachLoopStatementAccess().getForKeyword_0()); 
            match(input,48,FOLLOW_2); 
             after(grammarAccess.getForEachLoopStatementAccess().getForKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__0__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__1"
    // InternalGeometryCodes.g:3725:1: rule__ForEachLoopStatement__Group__1 : rule__ForEachLoopStatement__Group__1__Impl rule__ForEachLoopStatement__Group__2 ;
    public final void rule__ForEachLoopStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3729:1: ( rule__ForEachLoopStatement__Group__1__Impl rule__ForEachLoopStatement__Group__2 )
            // InternalGeometryCodes.g:3730:2: rule__ForEachLoopStatement__Group__1__Impl rule__ForEachLoopStatement__Group__2
            {
            pushFollow(FOLLOW_8);
            rule__ForEachLoopStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__1"


    // $ANTLR start "rule__ForEachLoopStatement__Group__1__Impl"
    // InternalGeometryCodes.g:3737:1: rule__ForEachLoopStatement__Group__1__Impl : ( 'each' ) ;
    public final void rule__ForEachLoopStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3741:1: ( ( 'each' ) )
            // InternalGeometryCodes.g:3742:1: ( 'each' )
            {
            // InternalGeometryCodes.g:3742:1: ( 'each' )
            // InternalGeometryCodes.g:3743:2: 'each'
            {
             before(grammarAccess.getForEachLoopStatementAccess().getEachKeyword_1()); 
            match(input,43,FOLLOW_2); 
             after(grammarAccess.getForEachLoopStatementAccess().getEachKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__1__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__2"
    // InternalGeometryCodes.g:3752:1: rule__ForEachLoopStatement__Group__2 : rule__ForEachLoopStatement__Group__2__Impl rule__ForEachLoopStatement__Group__3 ;
    public final void rule__ForEachLoopStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3756:1: ( rule__ForEachLoopStatement__Group__2__Impl rule__ForEachLoopStatement__Group__3 )
            // InternalGeometryCodes.g:3757:2: rule__ForEachLoopStatement__Group__2__Impl rule__ForEachLoopStatement__Group__3
            {
            pushFollow(FOLLOW_14);
            rule__ForEachLoopStatement__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__2"


    // $ANTLR start "rule__ForEachLoopStatement__Group__2__Impl"
    // InternalGeometryCodes.g:3764:1: rule__ForEachLoopStatement__Group__2__Impl : ( '(' ) ;
    public final void rule__ForEachLoopStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3768:1: ( ( '(' ) )
            // InternalGeometryCodes.g:3769:1: ( '(' )
            {
            // InternalGeometryCodes.g:3769:1: ( '(' )
            // InternalGeometryCodes.g:3770:2: '('
            {
             before(grammarAccess.getForEachLoopStatementAccess().getLeftParenthesisKeyword_2()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getForEachLoopStatementAccess().getLeftParenthesisKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__2__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__3"
    // InternalGeometryCodes.g:3779:1: rule__ForEachLoopStatement__Group__3 : rule__ForEachLoopStatement__Group__3__Impl rule__ForEachLoopStatement__Group__4 ;
    public final void rule__ForEachLoopStatement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3783:1: ( rule__ForEachLoopStatement__Group__3__Impl rule__ForEachLoopStatement__Group__4 )
            // InternalGeometryCodes.g:3784:2: rule__ForEachLoopStatement__Group__3__Impl rule__ForEachLoopStatement__Group__4
            {
            pushFollow(FOLLOW_7);
            rule__ForEachLoopStatement__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__3"


    // $ANTLR start "rule__ForEachLoopStatement__Group__3__Impl"
    // InternalGeometryCodes.g:3791:1: rule__ForEachLoopStatement__Group__3__Impl : ( ( rule__ForEachLoopStatement__TypeAssignment_3 ) ) ;
    public final void rule__ForEachLoopStatement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3795:1: ( ( ( rule__ForEachLoopStatement__TypeAssignment_3 ) ) )
            // InternalGeometryCodes.g:3796:1: ( ( rule__ForEachLoopStatement__TypeAssignment_3 ) )
            {
            // InternalGeometryCodes.g:3796:1: ( ( rule__ForEachLoopStatement__TypeAssignment_3 ) )
            // InternalGeometryCodes.g:3797:2: ( rule__ForEachLoopStatement__TypeAssignment_3 )
            {
             before(grammarAccess.getForEachLoopStatementAccess().getTypeAssignment_3()); 
            // InternalGeometryCodes.g:3798:2: ( rule__ForEachLoopStatement__TypeAssignment_3 )
            // InternalGeometryCodes.g:3798:3: rule__ForEachLoopStatement__TypeAssignment_3
            {
            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__TypeAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getForEachLoopStatementAccess().getTypeAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__3__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__4"
    // InternalGeometryCodes.g:3806:1: rule__ForEachLoopStatement__Group__4 : rule__ForEachLoopStatement__Group__4__Impl rule__ForEachLoopStatement__Group__5 ;
    public final void rule__ForEachLoopStatement__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3810:1: ( rule__ForEachLoopStatement__Group__4__Impl rule__ForEachLoopStatement__Group__5 )
            // InternalGeometryCodes.g:3811:2: rule__ForEachLoopStatement__Group__4__Impl rule__ForEachLoopStatement__Group__5
            {
            pushFollow(FOLLOW_30);
            rule__ForEachLoopStatement__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__4"


    // $ANTLR start "rule__ForEachLoopStatement__Group__4__Impl"
    // InternalGeometryCodes.g:3818:1: rule__ForEachLoopStatement__Group__4__Impl : ( ( rule__ForEachLoopStatement__NameAssignment_4 ) ) ;
    public final void rule__ForEachLoopStatement__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3822:1: ( ( ( rule__ForEachLoopStatement__NameAssignment_4 ) ) )
            // InternalGeometryCodes.g:3823:1: ( ( rule__ForEachLoopStatement__NameAssignment_4 ) )
            {
            // InternalGeometryCodes.g:3823:1: ( ( rule__ForEachLoopStatement__NameAssignment_4 ) )
            // InternalGeometryCodes.g:3824:2: ( rule__ForEachLoopStatement__NameAssignment_4 )
            {
             before(grammarAccess.getForEachLoopStatementAccess().getNameAssignment_4()); 
            // InternalGeometryCodes.g:3825:2: ( rule__ForEachLoopStatement__NameAssignment_4 )
            // InternalGeometryCodes.g:3825:3: rule__ForEachLoopStatement__NameAssignment_4
            {
            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__NameAssignment_4();

            state._fsp--;


            }

             after(grammarAccess.getForEachLoopStatementAccess().getNameAssignment_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__4__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__5"
    // InternalGeometryCodes.g:3833:1: rule__ForEachLoopStatement__Group__5 : rule__ForEachLoopStatement__Group__5__Impl rule__ForEachLoopStatement__Group__6 ;
    public final void rule__ForEachLoopStatement__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3837:1: ( rule__ForEachLoopStatement__Group__5__Impl rule__ForEachLoopStatement__Group__6 )
            // InternalGeometryCodes.g:3838:2: rule__ForEachLoopStatement__Group__5__Impl rule__ForEachLoopStatement__Group__6
            {
            pushFollow(FOLLOW_23);
            rule__ForEachLoopStatement__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__5"


    // $ANTLR start "rule__ForEachLoopStatement__Group__5__Impl"
    // InternalGeometryCodes.g:3845:1: rule__ForEachLoopStatement__Group__5__Impl : ( 'in' ) ;
    public final void rule__ForEachLoopStatement__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3849:1: ( ( 'in' ) )
            // InternalGeometryCodes.g:3850:1: ( 'in' )
            {
            // InternalGeometryCodes.g:3850:1: ( 'in' )
            // InternalGeometryCodes.g:3851:2: 'in'
            {
             before(grammarAccess.getForEachLoopStatementAccess().getInKeyword_5()); 
            match(input,51,FOLLOW_2); 
             after(grammarAccess.getForEachLoopStatementAccess().getInKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__5__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__6"
    // InternalGeometryCodes.g:3860:1: rule__ForEachLoopStatement__Group__6 : rule__ForEachLoopStatement__Group__6__Impl rule__ForEachLoopStatement__Group__7 ;
    public final void rule__ForEachLoopStatement__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3864:1: ( rule__ForEachLoopStatement__Group__6__Impl rule__ForEachLoopStatement__Group__7 )
            // InternalGeometryCodes.g:3865:2: rule__ForEachLoopStatement__Group__6__Impl rule__ForEachLoopStatement__Group__7
            {
            pushFollow(FOLLOW_27);
            rule__ForEachLoopStatement__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__6"


    // $ANTLR start "rule__ForEachLoopStatement__Group__6__Impl"
    // InternalGeometryCodes.g:3872:1: rule__ForEachLoopStatement__Group__6__Impl : ( ( rule__ForEachLoopStatement__ValueAssignment_6 ) ) ;
    public final void rule__ForEachLoopStatement__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3876:1: ( ( ( rule__ForEachLoopStatement__ValueAssignment_6 ) ) )
            // InternalGeometryCodes.g:3877:1: ( ( rule__ForEachLoopStatement__ValueAssignment_6 ) )
            {
            // InternalGeometryCodes.g:3877:1: ( ( rule__ForEachLoopStatement__ValueAssignment_6 ) )
            // InternalGeometryCodes.g:3878:2: ( rule__ForEachLoopStatement__ValueAssignment_6 )
            {
             before(grammarAccess.getForEachLoopStatementAccess().getValueAssignment_6()); 
            // InternalGeometryCodes.g:3879:2: ( rule__ForEachLoopStatement__ValueAssignment_6 )
            // InternalGeometryCodes.g:3879:3: rule__ForEachLoopStatement__ValueAssignment_6
            {
            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__ValueAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getForEachLoopStatementAccess().getValueAssignment_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__6__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__7"
    // InternalGeometryCodes.g:3887:1: rule__ForEachLoopStatement__Group__7 : rule__ForEachLoopStatement__Group__7__Impl rule__ForEachLoopStatement__Group__8 ;
    public final void rule__ForEachLoopStatement__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3891:1: ( rule__ForEachLoopStatement__Group__7__Impl rule__ForEachLoopStatement__Group__8 )
            // InternalGeometryCodes.g:3892:2: rule__ForEachLoopStatement__Group__7__Impl rule__ForEachLoopStatement__Group__8
            {
            pushFollow(FOLLOW_10);
            rule__ForEachLoopStatement__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__7"


    // $ANTLR start "rule__ForEachLoopStatement__Group__7__Impl"
    // InternalGeometryCodes.g:3899:1: rule__ForEachLoopStatement__Group__7__Impl : ( ')' ) ;
    public final void rule__ForEachLoopStatement__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3903:1: ( ( ')' ) )
            // InternalGeometryCodes.g:3904:1: ( ')' )
            {
            // InternalGeometryCodes.g:3904:1: ( ')' )
            // InternalGeometryCodes.g:3905:2: ')'
            {
             before(grammarAccess.getForEachLoopStatementAccess().getRightParenthesisKeyword_7()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getForEachLoopStatementAccess().getRightParenthesisKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__7__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__8"
    // InternalGeometryCodes.g:3914:1: rule__ForEachLoopStatement__Group__8 : rule__ForEachLoopStatement__Group__8__Impl rule__ForEachLoopStatement__Group__9 ;
    public final void rule__ForEachLoopStatement__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3918:1: ( rule__ForEachLoopStatement__Group__8__Impl rule__ForEachLoopStatement__Group__9 )
            // InternalGeometryCodes.g:3919:2: rule__ForEachLoopStatement__Group__8__Impl rule__ForEachLoopStatement__Group__9
            {
            pushFollow(FOLLOW_11);
            rule__ForEachLoopStatement__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__8"


    // $ANTLR start "rule__ForEachLoopStatement__Group__8__Impl"
    // InternalGeometryCodes.g:3926:1: rule__ForEachLoopStatement__Group__8__Impl : ( '{' ) ;
    public final void rule__ForEachLoopStatement__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3930:1: ( ( '{' ) )
            // InternalGeometryCodes.g:3931:1: ( '{' )
            {
            // InternalGeometryCodes.g:3931:1: ( '{' )
            // InternalGeometryCodes.g:3932:2: '{'
            {
             before(grammarAccess.getForEachLoopStatementAccess().getLeftCurlyBracketKeyword_8()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getForEachLoopStatementAccess().getLeftCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__8__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__9"
    // InternalGeometryCodes.g:3941:1: rule__ForEachLoopStatement__Group__9 : rule__ForEachLoopStatement__Group__9__Impl rule__ForEachLoopStatement__Group__10 ;
    public final void rule__ForEachLoopStatement__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3945:1: ( rule__ForEachLoopStatement__Group__9__Impl rule__ForEachLoopStatement__Group__10 )
            // InternalGeometryCodes.g:3946:2: rule__ForEachLoopStatement__Group__9__Impl rule__ForEachLoopStatement__Group__10
            {
            pushFollow(FOLLOW_11);
            rule__ForEachLoopStatement__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__9"


    // $ANTLR start "rule__ForEachLoopStatement__Group__9__Impl"
    // InternalGeometryCodes.g:3953:1: rule__ForEachLoopStatement__Group__9__Impl : ( ( rule__ForEachLoopStatement__StatementsAssignment_9 )* ) ;
    public final void rule__ForEachLoopStatement__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3957:1: ( ( ( rule__ForEachLoopStatement__StatementsAssignment_9 )* ) )
            // InternalGeometryCodes.g:3958:1: ( ( rule__ForEachLoopStatement__StatementsAssignment_9 )* )
            {
            // InternalGeometryCodes.g:3958:1: ( ( rule__ForEachLoopStatement__StatementsAssignment_9 )* )
            // InternalGeometryCodes.g:3959:2: ( rule__ForEachLoopStatement__StatementsAssignment_9 )*
            {
             before(grammarAccess.getForEachLoopStatementAccess().getStatementsAssignment_9()); 
            // InternalGeometryCodes.g:3960:2: ( rule__ForEachLoopStatement__StatementsAssignment_9 )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==RULE_ID||(LA29_0>=29 && LA29_0<=34)||(LA29_0>=46 && LA29_0<=48)||(LA29_0>=52 && LA29_0<=53)) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalGeometryCodes.g:3960:3: rule__ForEachLoopStatement__StatementsAssignment_9
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__ForEachLoopStatement__StatementsAssignment_9();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

             after(grammarAccess.getForEachLoopStatementAccess().getStatementsAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__9__Impl"


    // $ANTLR start "rule__ForEachLoopStatement__Group__10"
    // InternalGeometryCodes.g:3968:1: rule__ForEachLoopStatement__Group__10 : rule__ForEachLoopStatement__Group__10__Impl ;
    public final void rule__ForEachLoopStatement__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3972:1: ( rule__ForEachLoopStatement__Group__10__Impl )
            // InternalGeometryCodes.g:3973:2: rule__ForEachLoopStatement__Group__10__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ForEachLoopStatement__Group__10__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__10"


    // $ANTLR start "rule__ForEachLoopStatement__Group__10__Impl"
    // InternalGeometryCodes.g:3979:1: rule__ForEachLoopStatement__Group__10__Impl : ( '}' ) ;
    public final void rule__ForEachLoopStatement__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3983:1: ( ( '}' ) )
            // InternalGeometryCodes.g:3984:1: ( '}' )
            {
            // InternalGeometryCodes.g:3984:1: ( '}' )
            // InternalGeometryCodes.g:3985:2: '}'
            {
             before(grammarAccess.getForEachLoopStatementAccess().getRightCurlyBracketKeyword_10()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getForEachLoopStatementAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__Group__10__Impl"


    // $ANTLR start "rule__WhileLoopStatement__Group__0"
    // InternalGeometryCodes.g:3995:1: rule__WhileLoopStatement__Group__0 : rule__WhileLoopStatement__Group__0__Impl rule__WhileLoopStatement__Group__1 ;
    public final void rule__WhileLoopStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:3999:1: ( rule__WhileLoopStatement__Group__0__Impl rule__WhileLoopStatement__Group__1 )
            // InternalGeometryCodes.g:4000:2: rule__WhileLoopStatement__Group__0__Impl rule__WhileLoopStatement__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__WhileLoopStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__0"


    // $ANTLR start "rule__WhileLoopStatement__Group__0__Impl"
    // InternalGeometryCodes.g:4007:1: rule__WhileLoopStatement__Group__0__Impl : ( 'while' ) ;
    public final void rule__WhileLoopStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4011:1: ( ( 'while' ) )
            // InternalGeometryCodes.g:4012:1: ( 'while' )
            {
            // InternalGeometryCodes.g:4012:1: ( 'while' )
            // InternalGeometryCodes.g:4013:2: 'while'
            {
             before(grammarAccess.getWhileLoopStatementAccess().getWhileKeyword_0()); 
            match(input,52,FOLLOW_2); 
             after(grammarAccess.getWhileLoopStatementAccess().getWhileKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__0__Impl"


    // $ANTLR start "rule__WhileLoopStatement__Group__1"
    // InternalGeometryCodes.g:4022:1: rule__WhileLoopStatement__Group__1 : rule__WhileLoopStatement__Group__1__Impl rule__WhileLoopStatement__Group__2 ;
    public final void rule__WhileLoopStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4026:1: ( rule__WhileLoopStatement__Group__1__Impl rule__WhileLoopStatement__Group__2 )
            // InternalGeometryCodes.g:4027:2: rule__WhileLoopStatement__Group__1__Impl rule__WhileLoopStatement__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__WhileLoopStatement__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__1"


    // $ANTLR start "rule__WhileLoopStatement__Group__1__Impl"
    // InternalGeometryCodes.g:4034:1: rule__WhileLoopStatement__Group__1__Impl : ( '(' ) ;
    public final void rule__WhileLoopStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4038:1: ( ( '(' ) )
            // InternalGeometryCodes.g:4039:1: ( '(' )
            {
            // InternalGeometryCodes.g:4039:1: ( '(' )
            // InternalGeometryCodes.g:4040:2: '('
            {
             before(grammarAccess.getWhileLoopStatementAccess().getLeftParenthesisKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getWhileLoopStatementAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__1__Impl"


    // $ANTLR start "rule__WhileLoopStatement__Group__2"
    // InternalGeometryCodes.g:4049:1: rule__WhileLoopStatement__Group__2 : rule__WhileLoopStatement__Group__2__Impl rule__WhileLoopStatement__Group__3 ;
    public final void rule__WhileLoopStatement__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4053:1: ( rule__WhileLoopStatement__Group__2__Impl rule__WhileLoopStatement__Group__3 )
            // InternalGeometryCodes.g:4054:2: rule__WhileLoopStatement__Group__2__Impl rule__WhileLoopStatement__Group__3
            {
            pushFollow(FOLLOW_27);
            rule__WhileLoopStatement__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__2"


    // $ANTLR start "rule__WhileLoopStatement__Group__2__Impl"
    // InternalGeometryCodes.g:4061:1: rule__WhileLoopStatement__Group__2__Impl : ( ( rule__WhileLoopStatement__ConditionAssignment_2 ) ) ;
    public final void rule__WhileLoopStatement__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4065:1: ( ( ( rule__WhileLoopStatement__ConditionAssignment_2 ) ) )
            // InternalGeometryCodes.g:4066:1: ( ( rule__WhileLoopStatement__ConditionAssignment_2 ) )
            {
            // InternalGeometryCodes.g:4066:1: ( ( rule__WhileLoopStatement__ConditionAssignment_2 ) )
            // InternalGeometryCodes.g:4067:2: ( rule__WhileLoopStatement__ConditionAssignment_2 )
            {
             before(grammarAccess.getWhileLoopStatementAccess().getConditionAssignment_2()); 
            // InternalGeometryCodes.g:4068:2: ( rule__WhileLoopStatement__ConditionAssignment_2 )
            // InternalGeometryCodes.g:4068:3: rule__WhileLoopStatement__ConditionAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__ConditionAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getWhileLoopStatementAccess().getConditionAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__2__Impl"


    // $ANTLR start "rule__WhileLoopStatement__Group__3"
    // InternalGeometryCodes.g:4076:1: rule__WhileLoopStatement__Group__3 : rule__WhileLoopStatement__Group__3__Impl rule__WhileLoopStatement__Group__4 ;
    public final void rule__WhileLoopStatement__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4080:1: ( rule__WhileLoopStatement__Group__3__Impl rule__WhileLoopStatement__Group__4 )
            // InternalGeometryCodes.g:4081:2: rule__WhileLoopStatement__Group__3__Impl rule__WhileLoopStatement__Group__4
            {
            pushFollow(FOLLOW_10);
            rule__WhileLoopStatement__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__3"


    // $ANTLR start "rule__WhileLoopStatement__Group__3__Impl"
    // InternalGeometryCodes.g:4088:1: rule__WhileLoopStatement__Group__3__Impl : ( ')' ) ;
    public final void rule__WhileLoopStatement__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4092:1: ( ( ')' ) )
            // InternalGeometryCodes.g:4093:1: ( ')' )
            {
            // InternalGeometryCodes.g:4093:1: ( ')' )
            // InternalGeometryCodes.g:4094:2: ')'
            {
             before(grammarAccess.getWhileLoopStatementAccess().getRightParenthesisKeyword_3()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getWhileLoopStatementAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__3__Impl"


    // $ANTLR start "rule__WhileLoopStatement__Group__4"
    // InternalGeometryCodes.g:4103:1: rule__WhileLoopStatement__Group__4 : rule__WhileLoopStatement__Group__4__Impl rule__WhileLoopStatement__Group__5 ;
    public final void rule__WhileLoopStatement__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4107:1: ( rule__WhileLoopStatement__Group__4__Impl rule__WhileLoopStatement__Group__5 )
            // InternalGeometryCodes.g:4108:2: rule__WhileLoopStatement__Group__4__Impl rule__WhileLoopStatement__Group__5
            {
            pushFollow(FOLLOW_11);
            rule__WhileLoopStatement__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__4"


    // $ANTLR start "rule__WhileLoopStatement__Group__4__Impl"
    // InternalGeometryCodes.g:4115:1: rule__WhileLoopStatement__Group__4__Impl : ( '{' ) ;
    public final void rule__WhileLoopStatement__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4119:1: ( ( '{' ) )
            // InternalGeometryCodes.g:4120:1: ( '{' )
            {
            // InternalGeometryCodes.g:4120:1: ( '{' )
            // InternalGeometryCodes.g:4121:2: '{'
            {
             before(grammarAccess.getWhileLoopStatementAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,38,FOLLOW_2); 
             after(grammarAccess.getWhileLoopStatementAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__4__Impl"


    // $ANTLR start "rule__WhileLoopStatement__Group__5"
    // InternalGeometryCodes.g:4130:1: rule__WhileLoopStatement__Group__5 : rule__WhileLoopStatement__Group__5__Impl rule__WhileLoopStatement__Group__6 ;
    public final void rule__WhileLoopStatement__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4134:1: ( rule__WhileLoopStatement__Group__5__Impl rule__WhileLoopStatement__Group__6 )
            // InternalGeometryCodes.g:4135:2: rule__WhileLoopStatement__Group__5__Impl rule__WhileLoopStatement__Group__6
            {
            pushFollow(FOLLOW_11);
            rule__WhileLoopStatement__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__5"


    // $ANTLR start "rule__WhileLoopStatement__Group__5__Impl"
    // InternalGeometryCodes.g:4142:1: rule__WhileLoopStatement__Group__5__Impl : ( ( rule__WhileLoopStatement__StatementsAssignment_5 )* ) ;
    public final void rule__WhileLoopStatement__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4146:1: ( ( ( rule__WhileLoopStatement__StatementsAssignment_5 )* ) )
            // InternalGeometryCodes.g:4147:1: ( ( rule__WhileLoopStatement__StatementsAssignment_5 )* )
            {
            // InternalGeometryCodes.g:4147:1: ( ( rule__WhileLoopStatement__StatementsAssignment_5 )* )
            // InternalGeometryCodes.g:4148:2: ( rule__WhileLoopStatement__StatementsAssignment_5 )*
            {
             before(grammarAccess.getWhileLoopStatementAccess().getStatementsAssignment_5()); 
            // InternalGeometryCodes.g:4149:2: ( rule__WhileLoopStatement__StatementsAssignment_5 )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==RULE_ID||(LA30_0>=29 && LA30_0<=34)||(LA30_0>=46 && LA30_0<=48)||(LA30_0>=52 && LA30_0<=53)) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalGeometryCodes.g:4149:3: rule__WhileLoopStatement__StatementsAssignment_5
            	    {
            	    pushFollow(FOLLOW_5);
            	    rule__WhileLoopStatement__StatementsAssignment_5();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

             after(grammarAccess.getWhileLoopStatementAccess().getStatementsAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__5__Impl"


    // $ANTLR start "rule__WhileLoopStatement__Group__6"
    // InternalGeometryCodes.g:4157:1: rule__WhileLoopStatement__Group__6 : rule__WhileLoopStatement__Group__6__Impl ;
    public final void rule__WhileLoopStatement__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4161:1: ( rule__WhileLoopStatement__Group__6__Impl )
            // InternalGeometryCodes.g:4162:2: rule__WhileLoopStatement__Group__6__Impl
            {
            pushFollow(FOLLOW_2);
            rule__WhileLoopStatement__Group__6__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__6"


    // $ANTLR start "rule__WhileLoopStatement__Group__6__Impl"
    // InternalGeometryCodes.g:4168:1: rule__WhileLoopStatement__Group__6__Impl : ( '}' ) ;
    public final void rule__WhileLoopStatement__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4172:1: ( ( '}' ) )
            // InternalGeometryCodes.g:4173:1: ( '}' )
            {
            // InternalGeometryCodes.g:4173:1: ( '}' )
            // InternalGeometryCodes.g:4174:2: '}'
            {
             before(grammarAccess.getWhileLoopStatementAccess().getRightCurlyBracketKeyword_6()); 
            match(input,39,FOLLOW_2); 
             after(grammarAccess.getWhileLoopStatementAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__Group__6__Impl"


    // $ANTLR start "rule__OutputStatement__Group__0"
    // InternalGeometryCodes.g:4184:1: rule__OutputStatement__Group__0 : rule__OutputStatement__Group__0__Impl rule__OutputStatement__Group__1 ;
    public final void rule__OutputStatement__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4188:1: ( rule__OutputStatement__Group__0__Impl rule__OutputStatement__Group__1 )
            // InternalGeometryCodes.g:4189:2: rule__OutputStatement__Group__0__Impl rule__OutputStatement__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__OutputStatement__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OutputStatement__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputStatement__Group__0"


    // $ANTLR start "rule__OutputStatement__Group__0__Impl"
    // InternalGeometryCodes.g:4196:1: rule__OutputStatement__Group__0__Impl : ( 'output' ) ;
    public final void rule__OutputStatement__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4200:1: ( ( 'output' ) )
            // InternalGeometryCodes.g:4201:1: ( 'output' )
            {
            // InternalGeometryCodes.g:4201:1: ( 'output' )
            // InternalGeometryCodes.g:4202:2: 'output'
            {
             before(grammarAccess.getOutputStatementAccess().getOutputKeyword_0()); 
            match(input,53,FOLLOW_2); 
             after(grammarAccess.getOutputStatementAccess().getOutputKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputStatement__Group__0__Impl"


    // $ANTLR start "rule__OutputStatement__Group__1"
    // InternalGeometryCodes.g:4211:1: rule__OutputStatement__Group__1 : rule__OutputStatement__Group__1__Impl ;
    public final void rule__OutputStatement__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4215:1: ( rule__OutputStatement__Group__1__Impl )
            // InternalGeometryCodes.g:4216:2: rule__OutputStatement__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OutputStatement__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputStatement__Group__1"


    // $ANTLR start "rule__OutputStatement__Group__1__Impl"
    // InternalGeometryCodes.g:4222:1: rule__OutputStatement__Group__1__Impl : ( ( rule__OutputStatement__ValueAssignment_1 ) ) ;
    public final void rule__OutputStatement__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4226:1: ( ( ( rule__OutputStatement__ValueAssignment_1 ) ) )
            // InternalGeometryCodes.g:4227:1: ( ( rule__OutputStatement__ValueAssignment_1 ) )
            {
            // InternalGeometryCodes.g:4227:1: ( ( rule__OutputStatement__ValueAssignment_1 ) )
            // InternalGeometryCodes.g:4228:2: ( rule__OutputStatement__ValueAssignment_1 )
            {
             before(grammarAccess.getOutputStatementAccess().getValueAssignment_1()); 
            // InternalGeometryCodes.g:4229:2: ( rule__OutputStatement__ValueAssignment_1 )
            // InternalGeometryCodes.g:4229:3: rule__OutputStatement__ValueAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__OutputStatement__ValueAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOutputStatementAccess().getValueAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputStatement__Group__1__Impl"


    // $ANTLR start "rule__LogicalExpression__Group__0"
    // InternalGeometryCodes.g:4238:1: rule__LogicalExpression__Group__0 : rule__LogicalExpression__Group__0__Impl rule__LogicalExpression__Group__1 ;
    public final void rule__LogicalExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4242:1: ( rule__LogicalExpression__Group__0__Impl rule__LogicalExpression__Group__1 )
            // InternalGeometryCodes.g:4243:2: rule__LogicalExpression__Group__0__Impl rule__LogicalExpression__Group__1
            {
            pushFollow(FOLLOW_31);
            rule__LogicalExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group__0"


    // $ANTLR start "rule__LogicalExpression__Group__0__Impl"
    // InternalGeometryCodes.g:4250:1: rule__LogicalExpression__Group__0__Impl : ( ruleConditionalExpression ) ;
    public final void rule__LogicalExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4254:1: ( ( ruleConditionalExpression ) )
            // InternalGeometryCodes.g:4255:1: ( ruleConditionalExpression )
            {
            // InternalGeometryCodes.g:4255:1: ( ruleConditionalExpression )
            // InternalGeometryCodes.g:4256:2: ruleConditionalExpression
            {
             before(grammarAccess.getLogicalExpressionAccess().getConditionalExpressionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleConditionalExpression();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionAccess().getConditionalExpressionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group__0__Impl"


    // $ANTLR start "rule__LogicalExpression__Group__1"
    // InternalGeometryCodes.g:4265:1: rule__LogicalExpression__Group__1 : rule__LogicalExpression__Group__1__Impl ;
    public final void rule__LogicalExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4269:1: ( rule__LogicalExpression__Group__1__Impl )
            // InternalGeometryCodes.g:4270:2: rule__LogicalExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group__1"


    // $ANTLR start "rule__LogicalExpression__Group__1__Impl"
    // InternalGeometryCodes.g:4276:1: rule__LogicalExpression__Group__1__Impl : ( ( rule__LogicalExpression__Group_1__0 )* ) ;
    public final void rule__LogicalExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4280:1: ( ( ( rule__LogicalExpression__Group_1__0 )* ) )
            // InternalGeometryCodes.g:4281:1: ( ( rule__LogicalExpression__Group_1__0 )* )
            {
            // InternalGeometryCodes.g:4281:1: ( ( rule__LogicalExpression__Group_1__0 )* )
            // InternalGeometryCodes.g:4282:2: ( rule__LogicalExpression__Group_1__0 )*
            {
             before(grammarAccess.getLogicalExpressionAccess().getGroup_1()); 
            // InternalGeometryCodes.g:4283:2: ( rule__LogicalExpression__Group_1__0 )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( ((LA31_0>=27 && LA31_0<=28)) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalGeometryCodes.g:4283:3: rule__LogicalExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_32);
            	    rule__LogicalExpression__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

             after(grammarAccess.getLogicalExpressionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group__1__Impl"


    // $ANTLR start "rule__LogicalExpression__Group_1__0"
    // InternalGeometryCodes.g:4292:1: rule__LogicalExpression__Group_1__0 : rule__LogicalExpression__Group_1__0__Impl rule__LogicalExpression__Group_1__1 ;
    public final void rule__LogicalExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4296:1: ( rule__LogicalExpression__Group_1__0__Impl rule__LogicalExpression__Group_1__1 )
            // InternalGeometryCodes.g:4297:2: rule__LogicalExpression__Group_1__0__Impl rule__LogicalExpression__Group_1__1
            {
            pushFollow(FOLLOW_31);
            rule__LogicalExpression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group_1__0"


    // $ANTLR start "rule__LogicalExpression__Group_1__0__Impl"
    // InternalGeometryCodes.g:4304:1: rule__LogicalExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__LogicalExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4308:1: ( ( () ) )
            // InternalGeometryCodes.g:4309:1: ( () )
            {
            // InternalGeometryCodes.g:4309:1: ( () )
            // InternalGeometryCodes.g:4310:2: ()
            {
             before(grammarAccess.getLogicalExpressionAccess().getLogicalExpressionLeftChildAction_1_0()); 
            // InternalGeometryCodes.g:4311:2: ()
            // InternalGeometryCodes.g:4311:3: 
            {
            }

             after(grammarAccess.getLogicalExpressionAccess().getLogicalExpressionLeftChildAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group_1__0__Impl"


    // $ANTLR start "rule__LogicalExpression__Group_1__1"
    // InternalGeometryCodes.g:4319:1: rule__LogicalExpression__Group_1__1 : rule__LogicalExpression__Group_1__1__Impl rule__LogicalExpression__Group_1__2 ;
    public final void rule__LogicalExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4323:1: ( rule__LogicalExpression__Group_1__1__Impl rule__LogicalExpression__Group_1__2 )
            // InternalGeometryCodes.g:4324:2: rule__LogicalExpression__Group_1__1__Impl rule__LogicalExpression__Group_1__2
            {
            pushFollow(FOLLOW_23);
            rule__LogicalExpression__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LogicalExpression__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group_1__1"


    // $ANTLR start "rule__LogicalExpression__Group_1__1__Impl"
    // InternalGeometryCodes.g:4331:1: rule__LogicalExpression__Group_1__1__Impl : ( ( rule__LogicalExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__LogicalExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4335:1: ( ( ( rule__LogicalExpression__OperatorAssignment_1_1 ) ) )
            // InternalGeometryCodes.g:4336:1: ( ( rule__LogicalExpression__OperatorAssignment_1_1 ) )
            {
            // InternalGeometryCodes.g:4336:1: ( ( rule__LogicalExpression__OperatorAssignment_1_1 ) )
            // InternalGeometryCodes.g:4337:2: ( rule__LogicalExpression__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getLogicalExpressionAccess().getOperatorAssignment_1_1()); 
            // InternalGeometryCodes.g:4338:2: ( rule__LogicalExpression__OperatorAssignment_1_1 )
            // InternalGeometryCodes.g:4338:3: rule__LogicalExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpression__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group_1__1__Impl"


    // $ANTLR start "rule__LogicalExpression__Group_1__2"
    // InternalGeometryCodes.g:4346:1: rule__LogicalExpression__Group_1__2 : rule__LogicalExpression__Group_1__2__Impl ;
    public final void rule__LogicalExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4350:1: ( rule__LogicalExpression__Group_1__2__Impl )
            // InternalGeometryCodes.g:4351:2: rule__LogicalExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpression__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group_1__2"


    // $ANTLR start "rule__LogicalExpression__Group_1__2__Impl"
    // InternalGeometryCodes.g:4357:1: rule__LogicalExpression__Group_1__2__Impl : ( ( rule__LogicalExpression__RightChildAssignment_1_2 ) ) ;
    public final void rule__LogicalExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4361:1: ( ( ( rule__LogicalExpression__RightChildAssignment_1_2 ) ) )
            // InternalGeometryCodes.g:4362:1: ( ( rule__LogicalExpression__RightChildAssignment_1_2 ) )
            {
            // InternalGeometryCodes.g:4362:1: ( ( rule__LogicalExpression__RightChildAssignment_1_2 ) )
            // InternalGeometryCodes.g:4363:2: ( rule__LogicalExpression__RightChildAssignment_1_2 )
            {
             before(grammarAccess.getLogicalExpressionAccess().getRightChildAssignment_1_2()); 
            // InternalGeometryCodes.g:4364:2: ( rule__LogicalExpression__RightChildAssignment_1_2 )
            // InternalGeometryCodes.g:4364:3: rule__LogicalExpression__RightChildAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__LogicalExpression__RightChildAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getLogicalExpressionAccess().getRightChildAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__Group_1__2__Impl"


    // $ANTLR start "rule__ConditionalExpression__Group__0"
    // InternalGeometryCodes.g:4373:1: rule__ConditionalExpression__Group__0 : rule__ConditionalExpression__Group__0__Impl rule__ConditionalExpression__Group__1 ;
    public final void rule__ConditionalExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4377:1: ( rule__ConditionalExpression__Group__0__Impl rule__ConditionalExpression__Group__1 )
            // InternalGeometryCodes.g:4378:2: rule__ConditionalExpression__Group__0__Impl rule__ConditionalExpression__Group__1
            {
            pushFollow(FOLLOW_33);
            rule__ConditionalExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConditionalExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group__0"


    // $ANTLR start "rule__ConditionalExpression__Group__0__Impl"
    // InternalGeometryCodes.g:4385:1: rule__ConditionalExpression__Group__0__Impl : ( ruleAdditiveExpression ) ;
    public final void rule__ConditionalExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4389:1: ( ( ruleAdditiveExpression ) )
            // InternalGeometryCodes.g:4390:1: ( ruleAdditiveExpression )
            {
            // InternalGeometryCodes.g:4390:1: ( ruleAdditiveExpression )
            // InternalGeometryCodes.g:4391:2: ruleAdditiveExpression
            {
             before(grammarAccess.getConditionalExpressionAccess().getAdditiveExpressionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleAdditiveExpression();

            state._fsp--;

             after(grammarAccess.getConditionalExpressionAccess().getAdditiveExpressionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group__0__Impl"


    // $ANTLR start "rule__ConditionalExpression__Group__1"
    // InternalGeometryCodes.g:4400:1: rule__ConditionalExpression__Group__1 : rule__ConditionalExpression__Group__1__Impl ;
    public final void rule__ConditionalExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4404:1: ( rule__ConditionalExpression__Group__1__Impl )
            // InternalGeometryCodes.g:4405:2: rule__ConditionalExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConditionalExpression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group__1"


    // $ANTLR start "rule__ConditionalExpression__Group__1__Impl"
    // InternalGeometryCodes.g:4411:1: rule__ConditionalExpression__Group__1__Impl : ( ( rule__ConditionalExpression__Group_1__0 )* ) ;
    public final void rule__ConditionalExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4415:1: ( ( ( rule__ConditionalExpression__Group_1__0 )* ) )
            // InternalGeometryCodes.g:4416:1: ( ( rule__ConditionalExpression__Group_1__0 )* )
            {
            // InternalGeometryCodes.g:4416:1: ( ( rule__ConditionalExpression__Group_1__0 )* )
            // InternalGeometryCodes.g:4417:2: ( rule__ConditionalExpression__Group_1__0 )*
            {
             before(grammarAccess.getConditionalExpressionAccess().getGroup_1()); 
            // InternalGeometryCodes.g:4418:2: ( rule__ConditionalExpression__Group_1__0 )*
            loop32:
            do {
                int alt32=2;
                int LA32_0 = input.LA(1);

                if ( ((LA32_0>=21 && LA32_0<=26)) ) {
                    alt32=1;
                }


                switch (alt32) {
            	case 1 :
            	    // InternalGeometryCodes.g:4418:3: rule__ConditionalExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_34);
            	    rule__ConditionalExpression__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop32;
                }
            } while (true);

             after(grammarAccess.getConditionalExpressionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group__1__Impl"


    // $ANTLR start "rule__ConditionalExpression__Group_1__0"
    // InternalGeometryCodes.g:4427:1: rule__ConditionalExpression__Group_1__0 : rule__ConditionalExpression__Group_1__0__Impl rule__ConditionalExpression__Group_1__1 ;
    public final void rule__ConditionalExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4431:1: ( rule__ConditionalExpression__Group_1__0__Impl rule__ConditionalExpression__Group_1__1 )
            // InternalGeometryCodes.g:4432:2: rule__ConditionalExpression__Group_1__0__Impl rule__ConditionalExpression__Group_1__1
            {
            pushFollow(FOLLOW_33);
            rule__ConditionalExpression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConditionalExpression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group_1__0"


    // $ANTLR start "rule__ConditionalExpression__Group_1__0__Impl"
    // InternalGeometryCodes.g:4439:1: rule__ConditionalExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__ConditionalExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4443:1: ( ( () ) )
            // InternalGeometryCodes.g:4444:1: ( () )
            {
            // InternalGeometryCodes.g:4444:1: ( () )
            // InternalGeometryCodes.g:4445:2: ()
            {
             before(grammarAccess.getConditionalExpressionAccess().getConditionalExpressionLeftChildAction_1_0()); 
            // InternalGeometryCodes.g:4446:2: ()
            // InternalGeometryCodes.g:4446:3: 
            {
            }

             after(grammarAccess.getConditionalExpressionAccess().getConditionalExpressionLeftChildAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group_1__0__Impl"


    // $ANTLR start "rule__ConditionalExpression__Group_1__1"
    // InternalGeometryCodes.g:4454:1: rule__ConditionalExpression__Group_1__1 : rule__ConditionalExpression__Group_1__1__Impl rule__ConditionalExpression__Group_1__2 ;
    public final void rule__ConditionalExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4458:1: ( rule__ConditionalExpression__Group_1__1__Impl rule__ConditionalExpression__Group_1__2 )
            // InternalGeometryCodes.g:4459:2: rule__ConditionalExpression__Group_1__1__Impl rule__ConditionalExpression__Group_1__2
            {
            pushFollow(FOLLOW_23);
            rule__ConditionalExpression__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ConditionalExpression__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group_1__1"


    // $ANTLR start "rule__ConditionalExpression__Group_1__1__Impl"
    // InternalGeometryCodes.g:4466:1: rule__ConditionalExpression__Group_1__1__Impl : ( ( rule__ConditionalExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__ConditionalExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4470:1: ( ( ( rule__ConditionalExpression__OperatorAssignment_1_1 ) ) )
            // InternalGeometryCodes.g:4471:1: ( ( rule__ConditionalExpression__OperatorAssignment_1_1 ) )
            {
            // InternalGeometryCodes.g:4471:1: ( ( rule__ConditionalExpression__OperatorAssignment_1_1 ) )
            // InternalGeometryCodes.g:4472:2: ( rule__ConditionalExpression__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getConditionalExpressionAccess().getOperatorAssignment_1_1()); 
            // InternalGeometryCodes.g:4473:2: ( rule__ConditionalExpression__OperatorAssignment_1_1 )
            // InternalGeometryCodes.g:4473:3: rule__ConditionalExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ConditionalExpression__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getConditionalExpressionAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group_1__1__Impl"


    // $ANTLR start "rule__ConditionalExpression__Group_1__2"
    // InternalGeometryCodes.g:4481:1: rule__ConditionalExpression__Group_1__2 : rule__ConditionalExpression__Group_1__2__Impl ;
    public final void rule__ConditionalExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4485:1: ( rule__ConditionalExpression__Group_1__2__Impl )
            // InternalGeometryCodes.g:4486:2: rule__ConditionalExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ConditionalExpression__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group_1__2"


    // $ANTLR start "rule__ConditionalExpression__Group_1__2__Impl"
    // InternalGeometryCodes.g:4492:1: rule__ConditionalExpression__Group_1__2__Impl : ( ( rule__ConditionalExpression__RightChildAssignment_1_2 ) ) ;
    public final void rule__ConditionalExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4496:1: ( ( ( rule__ConditionalExpression__RightChildAssignment_1_2 ) ) )
            // InternalGeometryCodes.g:4497:1: ( ( rule__ConditionalExpression__RightChildAssignment_1_2 ) )
            {
            // InternalGeometryCodes.g:4497:1: ( ( rule__ConditionalExpression__RightChildAssignment_1_2 ) )
            // InternalGeometryCodes.g:4498:2: ( rule__ConditionalExpression__RightChildAssignment_1_2 )
            {
             before(grammarAccess.getConditionalExpressionAccess().getRightChildAssignment_1_2()); 
            // InternalGeometryCodes.g:4499:2: ( rule__ConditionalExpression__RightChildAssignment_1_2 )
            // InternalGeometryCodes.g:4499:3: rule__ConditionalExpression__RightChildAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__ConditionalExpression__RightChildAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getConditionalExpressionAccess().getRightChildAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__Group_1__2__Impl"


    // $ANTLR start "rule__AdditiveExpression__Group__0"
    // InternalGeometryCodes.g:4508:1: rule__AdditiveExpression__Group__0 : rule__AdditiveExpression__Group__0__Impl rule__AdditiveExpression__Group__1 ;
    public final void rule__AdditiveExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4512:1: ( rule__AdditiveExpression__Group__0__Impl rule__AdditiveExpression__Group__1 )
            // InternalGeometryCodes.g:4513:2: rule__AdditiveExpression__Group__0__Impl rule__AdditiveExpression__Group__1
            {
            pushFollow(FOLLOW_35);
            rule__AdditiveExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AdditiveExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group__0"


    // $ANTLR start "rule__AdditiveExpression__Group__0__Impl"
    // InternalGeometryCodes.g:4520:1: rule__AdditiveExpression__Group__0__Impl : ( ruleMultiplicativeExpression ) ;
    public final void rule__AdditiveExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4524:1: ( ( ruleMultiplicativeExpression ) )
            // InternalGeometryCodes.g:4525:1: ( ruleMultiplicativeExpression )
            {
            // InternalGeometryCodes.g:4525:1: ( ruleMultiplicativeExpression )
            // InternalGeometryCodes.g:4526:2: ruleMultiplicativeExpression
            {
             before(grammarAccess.getAdditiveExpressionAccess().getMultiplicativeExpressionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleMultiplicativeExpression();

            state._fsp--;

             after(grammarAccess.getAdditiveExpressionAccess().getMultiplicativeExpressionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group__0__Impl"


    // $ANTLR start "rule__AdditiveExpression__Group__1"
    // InternalGeometryCodes.g:4535:1: rule__AdditiveExpression__Group__1 : rule__AdditiveExpression__Group__1__Impl ;
    public final void rule__AdditiveExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4539:1: ( rule__AdditiveExpression__Group__1__Impl )
            // InternalGeometryCodes.g:4540:2: rule__AdditiveExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group__1"


    // $ANTLR start "rule__AdditiveExpression__Group__1__Impl"
    // InternalGeometryCodes.g:4546:1: rule__AdditiveExpression__Group__1__Impl : ( ( rule__AdditiveExpression__Group_1__0 )* ) ;
    public final void rule__AdditiveExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4550:1: ( ( ( rule__AdditiveExpression__Group_1__0 )* ) )
            // InternalGeometryCodes.g:4551:1: ( ( rule__AdditiveExpression__Group_1__0 )* )
            {
            // InternalGeometryCodes.g:4551:1: ( ( rule__AdditiveExpression__Group_1__0 )* )
            // InternalGeometryCodes.g:4552:2: ( rule__AdditiveExpression__Group_1__0 )*
            {
             before(grammarAccess.getAdditiveExpressionAccess().getGroup_1()); 
            // InternalGeometryCodes.g:4553:2: ( rule__AdditiveExpression__Group_1__0 )*
            loop33:
            do {
                int alt33=2;
                int LA33_0 = input.LA(1);

                if ( ((LA33_0>=17 && LA33_0<=18)) ) {
                    alt33=1;
                }


                switch (alt33) {
            	case 1 :
            	    // InternalGeometryCodes.g:4553:3: rule__AdditiveExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_36);
            	    rule__AdditiveExpression__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop33;
                }
            } while (true);

             after(grammarAccess.getAdditiveExpressionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group__1__Impl"


    // $ANTLR start "rule__AdditiveExpression__Group_1__0"
    // InternalGeometryCodes.g:4562:1: rule__AdditiveExpression__Group_1__0 : rule__AdditiveExpression__Group_1__0__Impl rule__AdditiveExpression__Group_1__1 ;
    public final void rule__AdditiveExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4566:1: ( rule__AdditiveExpression__Group_1__0__Impl rule__AdditiveExpression__Group_1__1 )
            // InternalGeometryCodes.g:4567:2: rule__AdditiveExpression__Group_1__0__Impl rule__AdditiveExpression__Group_1__1
            {
            pushFollow(FOLLOW_35);
            rule__AdditiveExpression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AdditiveExpression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group_1__0"


    // $ANTLR start "rule__AdditiveExpression__Group_1__0__Impl"
    // InternalGeometryCodes.g:4574:1: rule__AdditiveExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__AdditiveExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4578:1: ( ( () ) )
            // InternalGeometryCodes.g:4579:1: ( () )
            {
            // InternalGeometryCodes.g:4579:1: ( () )
            // InternalGeometryCodes.g:4580:2: ()
            {
             before(grammarAccess.getAdditiveExpressionAccess().getAdditiveExpressionLeftChildAction_1_0()); 
            // InternalGeometryCodes.g:4581:2: ()
            // InternalGeometryCodes.g:4581:3: 
            {
            }

             after(grammarAccess.getAdditiveExpressionAccess().getAdditiveExpressionLeftChildAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group_1__0__Impl"


    // $ANTLR start "rule__AdditiveExpression__Group_1__1"
    // InternalGeometryCodes.g:4589:1: rule__AdditiveExpression__Group_1__1 : rule__AdditiveExpression__Group_1__1__Impl rule__AdditiveExpression__Group_1__2 ;
    public final void rule__AdditiveExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4593:1: ( rule__AdditiveExpression__Group_1__1__Impl rule__AdditiveExpression__Group_1__2 )
            // InternalGeometryCodes.g:4594:2: rule__AdditiveExpression__Group_1__1__Impl rule__AdditiveExpression__Group_1__2
            {
            pushFollow(FOLLOW_23);
            rule__AdditiveExpression__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__AdditiveExpression__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group_1__1"


    // $ANTLR start "rule__AdditiveExpression__Group_1__1__Impl"
    // InternalGeometryCodes.g:4601:1: rule__AdditiveExpression__Group_1__1__Impl : ( ( rule__AdditiveExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__AdditiveExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4605:1: ( ( ( rule__AdditiveExpression__OperatorAssignment_1_1 ) ) )
            // InternalGeometryCodes.g:4606:1: ( ( rule__AdditiveExpression__OperatorAssignment_1_1 ) )
            {
            // InternalGeometryCodes.g:4606:1: ( ( rule__AdditiveExpression__OperatorAssignment_1_1 ) )
            // InternalGeometryCodes.g:4607:2: ( rule__AdditiveExpression__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getAdditiveExpressionAccess().getOperatorAssignment_1_1()); 
            // InternalGeometryCodes.g:4608:2: ( rule__AdditiveExpression__OperatorAssignment_1_1 )
            // InternalGeometryCodes.g:4608:3: rule__AdditiveExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpression__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveExpressionAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group_1__1__Impl"


    // $ANTLR start "rule__AdditiveExpression__Group_1__2"
    // InternalGeometryCodes.g:4616:1: rule__AdditiveExpression__Group_1__2 : rule__AdditiveExpression__Group_1__2__Impl ;
    public final void rule__AdditiveExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4620:1: ( rule__AdditiveExpression__Group_1__2__Impl )
            // InternalGeometryCodes.g:4621:2: rule__AdditiveExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpression__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group_1__2"


    // $ANTLR start "rule__AdditiveExpression__Group_1__2__Impl"
    // InternalGeometryCodes.g:4627:1: rule__AdditiveExpression__Group_1__2__Impl : ( ( rule__AdditiveExpression__RightChildAssignment_1_2 ) ) ;
    public final void rule__AdditiveExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4631:1: ( ( ( rule__AdditiveExpression__RightChildAssignment_1_2 ) ) )
            // InternalGeometryCodes.g:4632:1: ( ( rule__AdditiveExpression__RightChildAssignment_1_2 ) )
            {
            // InternalGeometryCodes.g:4632:1: ( ( rule__AdditiveExpression__RightChildAssignment_1_2 ) )
            // InternalGeometryCodes.g:4633:2: ( rule__AdditiveExpression__RightChildAssignment_1_2 )
            {
             before(grammarAccess.getAdditiveExpressionAccess().getRightChildAssignment_1_2()); 
            // InternalGeometryCodes.g:4634:2: ( rule__AdditiveExpression__RightChildAssignment_1_2 )
            // InternalGeometryCodes.g:4634:3: rule__AdditiveExpression__RightChildAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__AdditiveExpression__RightChildAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getAdditiveExpressionAccess().getRightChildAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__Group_1__2__Impl"


    // $ANTLR start "rule__MultiplicativeExpression__Group__0"
    // InternalGeometryCodes.g:4643:1: rule__MultiplicativeExpression__Group__0 : rule__MultiplicativeExpression__Group__0__Impl rule__MultiplicativeExpression__Group__1 ;
    public final void rule__MultiplicativeExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4647:1: ( rule__MultiplicativeExpression__Group__0__Impl rule__MultiplicativeExpression__Group__1 )
            // InternalGeometryCodes.g:4648:2: rule__MultiplicativeExpression__Group__0__Impl rule__MultiplicativeExpression__Group__1
            {
            pushFollow(FOLLOW_37);
            rule__MultiplicativeExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiplicativeExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group__0"


    // $ANTLR start "rule__MultiplicativeExpression__Group__0__Impl"
    // InternalGeometryCodes.g:4655:1: rule__MultiplicativeExpression__Group__0__Impl : ( ruleLiteralExpression ) ;
    public final void rule__MultiplicativeExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4659:1: ( ( ruleLiteralExpression ) )
            // InternalGeometryCodes.g:4660:1: ( ruleLiteralExpression )
            {
            // InternalGeometryCodes.g:4660:1: ( ruleLiteralExpression )
            // InternalGeometryCodes.g:4661:2: ruleLiteralExpression
            {
             before(grammarAccess.getMultiplicativeExpressionAccess().getLiteralExpressionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleLiteralExpression();

            state._fsp--;

             after(grammarAccess.getMultiplicativeExpressionAccess().getLiteralExpressionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group__0__Impl"


    // $ANTLR start "rule__MultiplicativeExpression__Group__1"
    // InternalGeometryCodes.g:4670:1: rule__MultiplicativeExpression__Group__1 : rule__MultiplicativeExpression__Group__1__Impl ;
    public final void rule__MultiplicativeExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4674:1: ( rule__MultiplicativeExpression__Group__1__Impl )
            // InternalGeometryCodes.g:4675:2: rule__MultiplicativeExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicativeExpression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group__1"


    // $ANTLR start "rule__MultiplicativeExpression__Group__1__Impl"
    // InternalGeometryCodes.g:4681:1: rule__MultiplicativeExpression__Group__1__Impl : ( ( rule__MultiplicativeExpression__Group_1__0 )* ) ;
    public final void rule__MultiplicativeExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4685:1: ( ( ( rule__MultiplicativeExpression__Group_1__0 )* ) )
            // InternalGeometryCodes.g:4686:1: ( ( rule__MultiplicativeExpression__Group_1__0 )* )
            {
            // InternalGeometryCodes.g:4686:1: ( ( rule__MultiplicativeExpression__Group_1__0 )* )
            // InternalGeometryCodes.g:4687:2: ( rule__MultiplicativeExpression__Group_1__0 )*
            {
             before(grammarAccess.getMultiplicativeExpressionAccess().getGroup_1()); 
            // InternalGeometryCodes.g:4688:2: ( rule__MultiplicativeExpression__Group_1__0 )*
            loop34:
            do {
                int alt34=2;
                int LA34_0 = input.LA(1);

                if ( ((LA34_0>=19 && LA34_0<=20)) ) {
                    alt34=1;
                }


                switch (alt34) {
            	case 1 :
            	    // InternalGeometryCodes.g:4688:3: rule__MultiplicativeExpression__Group_1__0
            	    {
            	    pushFollow(FOLLOW_38);
            	    rule__MultiplicativeExpression__Group_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop34;
                }
            } while (true);

             after(grammarAccess.getMultiplicativeExpressionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group__1__Impl"


    // $ANTLR start "rule__MultiplicativeExpression__Group_1__0"
    // InternalGeometryCodes.g:4697:1: rule__MultiplicativeExpression__Group_1__0 : rule__MultiplicativeExpression__Group_1__0__Impl rule__MultiplicativeExpression__Group_1__1 ;
    public final void rule__MultiplicativeExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4701:1: ( rule__MultiplicativeExpression__Group_1__0__Impl rule__MultiplicativeExpression__Group_1__1 )
            // InternalGeometryCodes.g:4702:2: rule__MultiplicativeExpression__Group_1__0__Impl rule__MultiplicativeExpression__Group_1__1
            {
            pushFollow(FOLLOW_37);
            rule__MultiplicativeExpression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiplicativeExpression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group_1__0"


    // $ANTLR start "rule__MultiplicativeExpression__Group_1__0__Impl"
    // InternalGeometryCodes.g:4709:1: rule__MultiplicativeExpression__Group_1__0__Impl : ( () ) ;
    public final void rule__MultiplicativeExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4713:1: ( ( () ) )
            // InternalGeometryCodes.g:4714:1: ( () )
            {
            // InternalGeometryCodes.g:4714:1: ( () )
            // InternalGeometryCodes.g:4715:2: ()
            {
             before(grammarAccess.getMultiplicativeExpressionAccess().getMultiplicativeExpressionLeftChildAction_1_0()); 
            // InternalGeometryCodes.g:4716:2: ()
            // InternalGeometryCodes.g:4716:3: 
            {
            }

             after(grammarAccess.getMultiplicativeExpressionAccess().getMultiplicativeExpressionLeftChildAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group_1__0__Impl"


    // $ANTLR start "rule__MultiplicativeExpression__Group_1__1"
    // InternalGeometryCodes.g:4724:1: rule__MultiplicativeExpression__Group_1__1 : rule__MultiplicativeExpression__Group_1__1__Impl rule__MultiplicativeExpression__Group_1__2 ;
    public final void rule__MultiplicativeExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4728:1: ( rule__MultiplicativeExpression__Group_1__1__Impl rule__MultiplicativeExpression__Group_1__2 )
            // InternalGeometryCodes.g:4729:2: rule__MultiplicativeExpression__Group_1__1__Impl rule__MultiplicativeExpression__Group_1__2
            {
            pushFollow(FOLLOW_23);
            rule__MultiplicativeExpression__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__MultiplicativeExpression__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group_1__1"


    // $ANTLR start "rule__MultiplicativeExpression__Group_1__1__Impl"
    // InternalGeometryCodes.g:4736:1: rule__MultiplicativeExpression__Group_1__1__Impl : ( ( rule__MultiplicativeExpression__OperatorAssignment_1_1 ) ) ;
    public final void rule__MultiplicativeExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4740:1: ( ( ( rule__MultiplicativeExpression__OperatorAssignment_1_1 ) ) )
            // InternalGeometryCodes.g:4741:1: ( ( rule__MultiplicativeExpression__OperatorAssignment_1_1 ) )
            {
            // InternalGeometryCodes.g:4741:1: ( ( rule__MultiplicativeExpression__OperatorAssignment_1_1 ) )
            // InternalGeometryCodes.g:4742:2: ( rule__MultiplicativeExpression__OperatorAssignment_1_1 )
            {
             before(grammarAccess.getMultiplicativeExpressionAccess().getOperatorAssignment_1_1()); 
            // InternalGeometryCodes.g:4743:2: ( rule__MultiplicativeExpression__OperatorAssignment_1_1 )
            // InternalGeometryCodes.g:4743:3: rule__MultiplicativeExpression__OperatorAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicativeExpression__OperatorAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicativeExpressionAccess().getOperatorAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group_1__1__Impl"


    // $ANTLR start "rule__MultiplicativeExpression__Group_1__2"
    // InternalGeometryCodes.g:4751:1: rule__MultiplicativeExpression__Group_1__2 : rule__MultiplicativeExpression__Group_1__2__Impl ;
    public final void rule__MultiplicativeExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4755:1: ( rule__MultiplicativeExpression__Group_1__2__Impl )
            // InternalGeometryCodes.g:4756:2: rule__MultiplicativeExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicativeExpression__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group_1__2"


    // $ANTLR start "rule__MultiplicativeExpression__Group_1__2__Impl"
    // InternalGeometryCodes.g:4762:1: rule__MultiplicativeExpression__Group_1__2__Impl : ( ( rule__MultiplicativeExpression__RightChildAssignment_1_2 ) ) ;
    public final void rule__MultiplicativeExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4766:1: ( ( ( rule__MultiplicativeExpression__RightChildAssignment_1_2 ) ) )
            // InternalGeometryCodes.g:4767:1: ( ( rule__MultiplicativeExpression__RightChildAssignment_1_2 ) )
            {
            // InternalGeometryCodes.g:4767:1: ( ( rule__MultiplicativeExpression__RightChildAssignment_1_2 ) )
            // InternalGeometryCodes.g:4768:2: ( rule__MultiplicativeExpression__RightChildAssignment_1_2 )
            {
             before(grammarAccess.getMultiplicativeExpressionAccess().getRightChildAssignment_1_2()); 
            // InternalGeometryCodes.g:4769:2: ( rule__MultiplicativeExpression__RightChildAssignment_1_2 )
            // InternalGeometryCodes.g:4769:3: rule__MultiplicativeExpression__RightChildAssignment_1_2
            {
            pushFollow(FOLLOW_2);
            rule__MultiplicativeExpression__RightChildAssignment_1_2();

            state._fsp--;


            }

             after(grammarAccess.getMultiplicativeExpressionAccess().getRightChildAssignment_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__Group_1__2__Impl"


    // $ANTLR start "rule__LiteralExpression__Group_1__0"
    // InternalGeometryCodes.g:4778:1: rule__LiteralExpression__Group_1__0 : rule__LiteralExpression__Group_1__0__Impl rule__LiteralExpression__Group_1__1 ;
    public final void rule__LiteralExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4782:1: ( rule__LiteralExpression__Group_1__0__Impl rule__LiteralExpression__Group_1__1 )
            // InternalGeometryCodes.g:4783:2: rule__LiteralExpression__Group_1__0__Impl rule__LiteralExpression__Group_1__1
            {
            pushFollow(FOLLOW_23);
            rule__LiteralExpression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LiteralExpression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralExpression__Group_1__0"


    // $ANTLR start "rule__LiteralExpression__Group_1__0__Impl"
    // InternalGeometryCodes.g:4790:1: rule__LiteralExpression__Group_1__0__Impl : ( '(' ) ;
    public final void rule__LiteralExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4794:1: ( ( '(' ) )
            // InternalGeometryCodes.g:4795:1: ( '(' )
            {
            // InternalGeometryCodes.g:4795:1: ( '(' )
            // InternalGeometryCodes.g:4796:2: '('
            {
             before(grammarAccess.getLiteralExpressionAccess().getLeftParenthesisKeyword_1_0()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getLiteralExpressionAccess().getLeftParenthesisKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralExpression__Group_1__0__Impl"


    // $ANTLR start "rule__LiteralExpression__Group_1__1"
    // InternalGeometryCodes.g:4805:1: rule__LiteralExpression__Group_1__1 : rule__LiteralExpression__Group_1__1__Impl rule__LiteralExpression__Group_1__2 ;
    public final void rule__LiteralExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4809:1: ( rule__LiteralExpression__Group_1__1__Impl rule__LiteralExpression__Group_1__2 )
            // InternalGeometryCodes.g:4810:2: rule__LiteralExpression__Group_1__1__Impl rule__LiteralExpression__Group_1__2
            {
            pushFollow(FOLLOW_27);
            rule__LiteralExpression__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__LiteralExpression__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralExpression__Group_1__1"


    // $ANTLR start "rule__LiteralExpression__Group_1__1__Impl"
    // InternalGeometryCodes.g:4817:1: rule__LiteralExpression__Group_1__1__Impl : ( ruleExpression ) ;
    public final void rule__LiteralExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4821:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:4822:1: ( ruleExpression )
            {
            // InternalGeometryCodes.g:4822:1: ( ruleExpression )
            // InternalGeometryCodes.g:4823:2: ruleExpression
            {
             before(grammarAccess.getLiteralExpressionAccess().getExpressionParserRuleCall_1_1()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getLiteralExpressionAccess().getExpressionParserRuleCall_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralExpression__Group_1__1__Impl"


    // $ANTLR start "rule__LiteralExpression__Group_1__2"
    // InternalGeometryCodes.g:4832:1: rule__LiteralExpression__Group_1__2 : rule__LiteralExpression__Group_1__2__Impl ;
    public final void rule__LiteralExpression__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4836:1: ( rule__LiteralExpression__Group_1__2__Impl )
            // InternalGeometryCodes.g:4837:2: rule__LiteralExpression__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__LiteralExpression__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralExpression__Group_1__2"


    // $ANTLR start "rule__LiteralExpression__Group_1__2__Impl"
    // InternalGeometryCodes.g:4843:1: rule__LiteralExpression__Group_1__2__Impl : ( ')' ) ;
    public final void rule__LiteralExpression__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4847:1: ( ( ')' ) )
            // InternalGeometryCodes.g:4848:1: ( ')' )
            {
            // InternalGeometryCodes.g:4848:1: ( ')' )
            // InternalGeometryCodes.g:4849:2: ')'
            {
             before(grammarAccess.getLiteralExpressionAccess().getRightParenthesisKeyword_1_2()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getLiteralExpressionAccess().getRightParenthesisKeyword_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LiteralExpression__Group_1__2__Impl"


    // $ANTLR start "rule__UnaryExpression__Group__0"
    // InternalGeometryCodes.g:4859:1: rule__UnaryExpression__Group__0 : rule__UnaryExpression__Group__0__Impl rule__UnaryExpression__Group__1 ;
    public final void rule__UnaryExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4863:1: ( rule__UnaryExpression__Group__0__Impl rule__UnaryExpression__Group__1 )
            // InternalGeometryCodes.g:4864:2: rule__UnaryExpression__Group__0__Impl rule__UnaryExpression__Group__1
            {
            pushFollow(FOLLOW_23);
            rule__UnaryExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__UnaryExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryExpression__Group__0"


    // $ANTLR start "rule__UnaryExpression__Group__0__Impl"
    // InternalGeometryCodes.g:4871:1: rule__UnaryExpression__Group__0__Impl : ( ( rule__UnaryExpression__OperatorAssignment_0 ) ) ;
    public final void rule__UnaryExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4875:1: ( ( ( rule__UnaryExpression__OperatorAssignment_0 ) ) )
            // InternalGeometryCodes.g:4876:1: ( ( rule__UnaryExpression__OperatorAssignment_0 ) )
            {
            // InternalGeometryCodes.g:4876:1: ( ( rule__UnaryExpression__OperatorAssignment_0 ) )
            // InternalGeometryCodes.g:4877:2: ( rule__UnaryExpression__OperatorAssignment_0 )
            {
             before(grammarAccess.getUnaryExpressionAccess().getOperatorAssignment_0()); 
            // InternalGeometryCodes.g:4878:2: ( rule__UnaryExpression__OperatorAssignment_0 )
            // InternalGeometryCodes.g:4878:3: rule__UnaryExpression__OperatorAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__UnaryExpression__OperatorAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getUnaryExpressionAccess().getOperatorAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryExpression__Group__0__Impl"


    // $ANTLR start "rule__UnaryExpression__Group__1"
    // InternalGeometryCodes.g:4886:1: rule__UnaryExpression__Group__1 : rule__UnaryExpression__Group__1__Impl ;
    public final void rule__UnaryExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4890:1: ( rule__UnaryExpression__Group__1__Impl )
            // InternalGeometryCodes.g:4891:2: rule__UnaryExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__UnaryExpression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryExpression__Group__1"


    // $ANTLR start "rule__UnaryExpression__Group__1__Impl"
    // InternalGeometryCodes.g:4897:1: rule__UnaryExpression__Group__1__Impl : ( ( rule__UnaryExpression__ChildAssignment_1 ) ) ;
    public final void rule__UnaryExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4901:1: ( ( ( rule__UnaryExpression__ChildAssignment_1 ) ) )
            // InternalGeometryCodes.g:4902:1: ( ( rule__UnaryExpression__ChildAssignment_1 ) )
            {
            // InternalGeometryCodes.g:4902:1: ( ( rule__UnaryExpression__ChildAssignment_1 ) )
            // InternalGeometryCodes.g:4903:2: ( rule__UnaryExpression__ChildAssignment_1 )
            {
             before(grammarAccess.getUnaryExpressionAccess().getChildAssignment_1()); 
            // InternalGeometryCodes.g:4904:2: ( rule__UnaryExpression__ChildAssignment_1 )
            // InternalGeometryCodes.g:4904:3: rule__UnaryExpression__ChildAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__UnaryExpression__ChildAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUnaryExpressionAccess().getChildAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryExpression__Group__1__Impl"


    // $ANTLR start "rule__ReferenceExpression__Group__0"
    // InternalGeometryCodes.g:4913:1: rule__ReferenceExpression__Group__0 : rule__ReferenceExpression__Group__0__Impl rule__ReferenceExpression__Group__1 ;
    public final void rule__ReferenceExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4917:1: ( rule__ReferenceExpression__Group__0__Impl rule__ReferenceExpression__Group__1 )
            // InternalGeometryCodes.g:4918:2: rule__ReferenceExpression__Group__0__Impl rule__ReferenceExpression__Group__1
            {
            pushFollow(FOLLOW_39);
            rule__ReferenceExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReferenceExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Group__0"


    // $ANTLR start "rule__ReferenceExpression__Group__0__Impl"
    // InternalGeometryCodes.g:4925:1: rule__ReferenceExpression__Group__0__Impl : ( ( rule__ReferenceExpression__Alternatives_0 ) ) ;
    public final void rule__ReferenceExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4929:1: ( ( ( rule__ReferenceExpression__Alternatives_0 ) ) )
            // InternalGeometryCodes.g:4930:1: ( ( rule__ReferenceExpression__Alternatives_0 ) )
            {
            // InternalGeometryCodes.g:4930:1: ( ( rule__ReferenceExpression__Alternatives_0 ) )
            // InternalGeometryCodes.g:4931:2: ( rule__ReferenceExpression__Alternatives_0 )
            {
             before(grammarAccess.getReferenceExpressionAccess().getAlternatives_0()); 
            // InternalGeometryCodes.g:4932:2: ( rule__ReferenceExpression__Alternatives_0 )
            // InternalGeometryCodes.g:4932:3: rule__ReferenceExpression__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceExpression__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getReferenceExpressionAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Group__0__Impl"


    // $ANTLR start "rule__ReferenceExpression__Group__1"
    // InternalGeometryCodes.g:4940:1: rule__ReferenceExpression__Group__1 : rule__ReferenceExpression__Group__1__Impl ;
    public final void rule__ReferenceExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4944:1: ( rule__ReferenceExpression__Group__1__Impl )
            // InternalGeometryCodes.g:4945:2: rule__ReferenceExpression__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceExpression__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Group__1"


    // $ANTLR start "rule__ReferenceExpression__Group__1__Impl"
    // InternalGeometryCodes.g:4951:1: rule__ReferenceExpression__Group__1__Impl : ( ( rule__ReferenceExpression__Group_1__0 )? ) ;
    public final void rule__ReferenceExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4955:1: ( ( ( rule__ReferenceExpression__Group_1__0 )? ) )
            // InternalGeometryCodes.g:4956:1: ( ( rule__ReferenceExpression__Group_1__0 )? )
            {
            // InternalGeometryCodes.g:4956:1: ( ( rule__ReferenceExpression__Group_1__0 )? )
            // InternalGeometryCodes.g:4957:2: ( rule__ReferenceExpression__Group_1__0 )?
            {
             before(grammarAccess.getReferenceExpressionAccess().getGroup_1()); 
            // InternalGeometryCodes.g:4958:2: ( rule__ReferenceExpression__Group_1__0 )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==54) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalGeometryCodes.g:4958:3: rule__ReferenceExpression__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__ReferenceExpression__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReferenceExpressionAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Group__1__Impl"


    // $ANTLR start "rule__ReferenceExpression__Group_1__0"
    // InternalGeometryCodes.g:4967:1: rule__ReferenceExpression__Group_1__0 : rule__ReferenceExpression__Group_1__0__Impl rule__ReferenceExpression__Group_1__1 ;
    public final void rule__ReferenceExpression__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4971:1: ( rule__ReferenceExpression__Group_1__0__Impl rule__ReferenceExpression__Group_1__1 )
            // InternalGeometryCodes.g:4972:2: rule__ReferenceExpression__Group_1__0__Impl rule__ReferenceExpression__Group_1__1
            {
            pushFollow(FOLLOW_7);
            rule__ReferenceExpression__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__ReferenceExpression__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Group_1__0"


    // $ANTLR start "rule__ReferenceExpression__Group_1__0__Impl"
    // InternalGeometryCodes.g:4979:1: rule__ReferenceExpression__Group_1__0__Impl : ( '.' ) ;
    public final void rule__ReferenceExpression__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4983:1: ( ( '.' ) )
            // InternalGeometryCodes.g:4984:1: ( '.' )
            {
            // InternalGeometryCodes.g:4984:1: ( '.' )
            // InternalGeometryCodes.g:4985:2: '.'
            {
             before(grammarAccess.getReferenceExpressionAccess().getFullStopKeyword_1_0()); 
            match(input,54,FOLLOW_2); 
             after(grammarAccess.getReferenceExpressionAccess().getFullStopKeyword_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Group_1__0__Impl"


    // $ANTLR start "rule__ReferenceExpression__Group_1__1"
    // InternalGeometryCodes.g:4994:1: rule__ReferenceExpression__Group_1__1 : rule__ReferenceExpression__Group_1__1__Impl ;
    public final void rule__ReferenceExpression__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:4998:1: ( rule__ReferenceExpression__Group_1__1__Impl )
            // InternalGeometryCodes.g:4999:2: rule__ReferenceExpression__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceExpression__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Group_1__1"


    // $ANTLR start "rule__ReferenceExpression__Group_1__1__Impl"
    // InternalGeometryCodes.g:5005:1: rule__ReferenceExpression__Group_1__1__Impl : ( ( rule__ReferenceExpression__NextAssignment_1_1 ) ) ;
    public final void rule__ReferenceExpression__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5009:1: ( ( ( rule__ReferenceExpression__NextAssignment_1_1 ) ) )
            // InternalGeometryCodes.g:5010:1: ( ( rule__ReferenceExpression__NextAssignment_1_1 ) )
            {
            // InternalGeometryCodes.g:5010:1: ( ( rule__ReferenceExpression__NextAssignment_1_1 ) )
            // InternalGeometryCodes.g:5011:2: ( rule__ReferenceExpression__NextAssignment_1_1 )
            {
             before(grammarAccess.getReferenceExpressionAccess().getNextAssignment_1_1()); 
            // InternalGeometryCodes.g:5012:2: ( rule__ReferenceExpression__NextAssignment_1_1 )
            // InternalGeometryCodes.g:5012:3: rule__ReferenceExpression__NextAssignment_1_1
            {
            pushFollow(FOLLOW_2);
            rule__ReferenceExpression__NextAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getReferenceExpressionAccess().getNextAssignment_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__Group_1__1__Impl"


    // $ANTLR start "rule__FunctionCallExpression__Group__0"
    // InternalGeometryCodes.g:5021:1: rule__FunctionCallExpression__Group__0 : rule__FunctionCallExpression__Group__0__Impl rule__FunctionCallExpression__Group__1 ;
    public final void rule__FunctionCallExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5025:1: ( rule__FunctionCallExpression__Group__0__Impl rule__FunctionCallExpression__Group__1 )
            // InternalGeometryCodes.g:5026:2: rule__FunctionCallExpression__Group__0__Impl rule__FunctionCallExpression__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__FunctionCallExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group__0"


    // $ANTLR start "rule__FunctionCallExpression__Group__0__Impl"
    // InternalGeometryCodes.g:5033:1: rule__FunctionCallExpression__Group__0__Impl : ( ( rule__FunctionCallExpression__TargetAssignment_0 ) ) ;
    public final void rule__FunctionCallExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5037:1: ( ( ( rule__FunctionCallExpression__TargetAssignment_0 ) ) )
            // InternalGeometryCodes.g:5038:1: ( ( rule__FunctionCallExpression__TargetAssignment_0 ) )
            {
            // InternalGeometryCodes.g:5038:1: ( ( rule__FunctionCallExpression__TargetAssignment_0 ) )
            // InternalGeometryCodes.g:5039:2: ( rule__FunctionCallExpression__TargetAssignment_0 )
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getTargetAssignment_0()); 
            // InternalGeometryCodes.g:5040:2: ( rule__FunctionCallExpression__TargetAssignment_0 )
            // InternalGeometryCodes.g:5040:3: rule__FunctionCallExpression__TargetAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__TargetAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionCallExpressionAccess().getTargetAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group__0__Impl"


    // $ANTLR start "rule__FunctionCallExpression__Group__1"
    // InternalGeometryCodes.g:5048:1: rule__FunctionCallExpression__Group__1 : rule__FunctionCallExpression__Group__1__Impl rule__FunctionCallExpression__Group__2 ;
    public final void rule__FunctionCallExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5052:1: ( rule__FunctionCallExpression__Group__1__Impl rule__FunctionCallExpression__Group__2 )
            // InternalGeometryCodes.g:5053:2: rule__FunctionCallExpression__Group__1__Impl rule__FunctionCallExpression__Group__2
            {
            pushFollow(FOLLOW_40);
            rule__FunctionCallExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group__1"


    // $ANTLR start "rule__FunctionCallExpression__Group__1__Impl"
    // InternalGeometryCodes.g:5060:1: rule__FunctionCallExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__FunctionCallExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5064:1: ( ( '(' ) )
            // InternalGeometryCodes.g:5065:1: ( '(' )
            {
            // InternalGeometryCodes.g:5065:1: ( '(' )
            // InternalGeometryCodes.g:5066:2: '('
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getLeftParenthesisKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getFunctionCallExpressionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group__1__Impl"


    // $ANTLR start "rule__FunctionCallExpression__Group__2"
    // InternalGeometryCodes.g:5075:1: rule__FunctionCallExpression__Group__2 : rule__FunctionCallExpression__Group__2__Impl rule__FunctionCallExpression__Group__3 ;
    public final void rule__FunctionCallExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5079:1: ( rule__FunctionCallExpression__Group__2__Impl rule__FunctionCallExpression__Group__3 )
            // InternalGeometryCodes.g:5080:2: rule__FunctionCallExpression__Group__2__Impl rule__FunctionCallExpression__Group__3
            {
            pushFollow(FOLLOW_40);
            rule__FunctionCallExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group__2"


    // $ANTLR start "rule__FunctionCallExpression__Group__2__Impl"
    // InternalGeometryCodes.g:5087:1: rule__FunctionCallExpression__Group__2__Impl : ( ( rule__FunctionCallExpression__Group_2__0 )? ) ;
    public final void rule__FunctionCallExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5091:1: ( ( ( rule__FunctionCallExpression__Group_2__0 )? ) )
            // InternalGeometryCodes.g:5092:1: ( ( rule__FunctionCallExpression__Group_2__0 )? )
            {
            // InternalGeometryCodes.g:5092:1: ( ( rule__FunctionCallExpression__Group_2__0 )? )
            // InternalGeometryCodes.g:5093:2: ( rule__FunctionCallExpression__Group_2__0 )?
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getGroup_2()); 
            // InternalGeometryCodes.g:5094:2: ( rule__FunctionCallExpression__Group_2__0 )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( ((LA36_0>=RULE_STRING && LA36_0<=RULE_FLOAT)||(LA36_0>=16 && LA36_0<=17)||(LA36_0>=31 && LA36_0<=33)||LA36_0==36||(LA36_0>=55 && LA36_0<=57)||LA36_0==59) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalGeometryCodes.g:5094:3: rule__FunctionCallExpression__Group_2__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FunctionCallExpression__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFunctionCallExpressionAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group__2__Impl"


    // $ANTLR start "rule__FunctionCallExpression__Group__3"
    // InternalGeometryCodes.g:5102:1: rule__FunctionCallExpression__Group__3 : rule__FunctionCallExpression__Group__3__Impl ;
    public final void rule__FunctionCallExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5106:1: ( rule__FunctionCallExpression__Group__3__Impl )
            // InternalGeometryCodes.g:5107:2: rule__FunctionCallExpression__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group__3"


    // $ANTLR start "rule__FunctionCallExpression__Group__3__Impl"
    // InternalGeometryCodes.g:5113:1: rule__FunctionCallExpression__Group__3__Impl : ( ')' ) ;
    public final void rule__FunctionCallExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5117:1: ( ( ')' ) )
            // InternalGeometryCodes.g:5118:1: ( ')' )
            {
            // InternalGeometryCodes.g:5118:1: ( ')' )
            // InternalGeometryCodes.g:5119:2: ')'
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getRightParenthesisKeyword_3()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getFunctionCallExpressionAccess().getRightParenthesisKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group__3__Impl"


    // $ANTLR start "rule__FunctionCallExpression__Group_2__0"
    // InternalGeometryCodes.g:5129:1: rule__FunctionCallExpression__Group_2__0 : rule__FunctionCallExpression__Group_2__0__Impl rule__FunctionCallExpression__Group_2__1 ;
    public final void rule__FunctionCallExpression__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5133:1: ( rule__FunctionCallExpression__Group_2__0__Impl rule__FunctionCallExpression__Group_2__1 )
            // InternalGeometryCodes.g:5134:2: rule__FunctionCallExpression__Group_2__0__Impl rule__FunctionCallExpression__Group_2__1
            {
            pushFollow(FOLLOW_12);
            rule__FunctionCallExpression__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group_2__0"


    // $ANTLR start "rule__FunctionCallExpression__Group_2__0__Impl"
    // InternalGeometryCodes.g:5141:1: rule__FunctionCallExpression__Group_2__0__Impl : ( ( rule__FunctionCallExpression__ArgumentsAssignment_2_0 ) ) ;
    public final void rule__FunctionCallExpression__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5145:1: ( ( ( rule__FunctionCallExpression__ArgumentsAssignment_2_0 ) ) )
            // InternalGeometryCodes.g:5146:1: ( ( rule__FunctionCallExpression__ArgumentsAssignment_2_0 ) )
            {
            // InternalGeometryCodes.g:5146:1: ( ( rule__FunctionCallExpression__ArgumentsAssignment_2_0 ) )
            // InternalGeometryCodes.g:5147:2: ( rule__FunctionCallExpression__ArgumentsAssignment_2_0 )
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getArgumentsAssignment_2_0()); 
            // InternalGeometryCodes.g:5148:2: ( rule__FunctionCallExpression__ArgumentsAssignment_2_0 )
            // InternalGeometryCodes.g:5148:3: rule__FunctionCallExpression__ArgumentsAssignment_2_0
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__ArgumentsAssignment_2_0();

            state._fsp--;


            }

             after(grammarAccess.getFunctionCallExpressionAccess().getArgumentsAssignment_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group_2__0__Impl"


    // $ANTLR start "rule__FunctionCallExpression__Group_2__1"
    // InternalGeometryCodes.g:5156:1: rule__FunctionCallExpression__Group_2__1 : rule__FunctionCallExpression__Group_2__1__Impl ;
    public final void rule__FunctionCallExpression__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5160:1: ( rule__FunctionCallExpression__Group_2__1__Impl )
            // InternalGeometryCodes.g:5161:2: rule__FunctionCallExpression__Group_2__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group_2__1"


    // $ANTLR start "rule__FunctionCallExpression__Group_2__1__Impl"
    // InternalGeometryCodes.g:5167:1: rule__FunctionCallExpression__Group_2__1__Impl : ( ( rule__FunctionCallExpression__Group_2_1__0 )* ) ;
    public final void rule__FunctionCallExpression__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5171:1: ( ( ( rule__FunctionCallExpression__Group_2_1__0 )* ) )
            // InternalGeometryCodes.g:5172:1: ( ( rule__FunctionCallExpression__Group_2_1__0 )* )
            {
            // InternalGeometryCodes.g:5172:1: ( ( rule__FunctionCallExpression__Group_2_1__0 )* )
            // InternalGeometryCodes.g:5173:2: ( rule__FunctionCallExpression__Group_2_1__0 )*
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getGroup_2_1()); 
            // InternalGeometryCodes.g:5174:2: ( rule__FunctionCallExpression__Group_2_1__0 )*
            loop37:
            do {
                int alt37=2;
                int LA37_0 = input.LA(1);

                if ( (LA37_0==40) ) {
                    alt37=1;
                }


                switch (alt37) {
            	case 1 :
            	    // InternalGeometryCodes.g:5174:3: rule__FunctionCallExpression__Group_2_1__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__FunctionCallExpression__Group_2_1__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop37;
                }
            } while (true);

             after(grammarAccess.getFunctionCallExpressionAccess().getGroup_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group_2__1__Impl"


    // $ANTLR start "rule__FunctionCallExpression__Group_2_1__0"
    // InternalGeometryCodes.g:5183:1: rule__FunctionCallExpression__Group_2_1__0 : rule__FunctionCallExpression__Group_2_1__0__Impl rule__FunctionCallExpression__Group_2_1__1 ;
    public final void rule__FunctionCallExpression__Group_2_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5187:1: ( rule__FunctionCallExpression__Group_2_1__0__Impl rule__FunctionCallExpression__Group_2_1__1 )
            // InternalGeometryCodes.g:5188:2: rule__FunctionCallExpression__Group_2_1__0__Impl rule__FunctionCallExpression__Group_2_1__1
            {
            pushFollow(FOLLOW_23);
            rule__FunctionCallExpression__Group_2_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group_2_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group_2_1__0"


    // $ANTLR start "rule__FunctionCallExpression__Group_2_1__0__Impl"
    // InternalGeometryCodes.g:5195:1: rule__FunctionCallExpression__Group_2_1__0__Impl : ( ',' ) ;
    public final void rule__FunctionCallExpression__Group_2_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5199:1: ( ( ',' ) )
            // InternalGeometryCodes.g:5200:1: ( ',' )
            {
            // InternalGeometryCodes.g:5200:1: ( ',' )
            // InternalGeometryCodes.g:5201:2: ','
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getCommaKeyword_2_1_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getFunctionCallExpressionAccess().getCommaKeyword_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group_2_1__0__Impl"


    // $ANTLR start "rule__FunctionCallExpression__Group_2_1__1"
    // InternalGeometryCodes.g:5210:1: rule__FunctionCallExpression__Group_2_1__1 : rule__FunctionCallExpression__Group_2_1__1__Impl ;
    public final void rule__FunctionCallExpression__Group_2_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5214:1: ( rule__FunctionCallExpression__Group_2_1__1__Impl )
            // InternalGeometryCodes.g:5215:2: rule__FunctionCallExpression__Group_2_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__Group_2_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group_2_1__1"


    // $ANTLR start "rule__FunctionCallExpression__Group_2_1__1__Impl"
    // InternalGeometryCodes.g:5221:1: rule__FunctionCallExpression__Group_2_1__1__Impl : ( ( rule__FunctionCallExpression__ArgumentsAssignment_2_1_1 ) ) ;
    public final void rule__FunctionCallExpression__Group_2_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5225:1: ( ( ( rule__FunctionCallExpression__ArgumentsAssignment_2_1_1 ) ) )
            // InternalGeometryCodes.g:5226:1: ( ( rule__FunctionCallExpression__ArgumentsAssignment_2_1_1 ) )
            {
            // InternalGeometryCodes.g:5226:1: ( ( rule__FunctionCallExpression__ArgumentsAssignment_2_1_1 ) )
            // InternalGeometryCodes.g:5227:2: ( rule__FunctionCallExpression__ArgumentsAssignment_2_1_1 )
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getArgumentsAssignment_2_1_1()); 
            // InternalGeometryCodes.g:5228:2: ( rule__FunctionCallExpression__ArgumentsAssignment_2_1_1 )
            // InternalGeometryCodes.g:5228:3: rule__FunctionCallExpression__ArgumentsAssignment_2_1_1
            {
            pushFollow(FOLLOW_2);
            rule__FunctionCallExpression__ArgumentsAssignment_2_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFunctionCallExpressionAccess().getArgumentsAssignment_2_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__Group_2_1__1__Impl"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__0"
    // InternalGeometryCodes.g:5237:1: rule__VectorDeclarationExpression__Group__0 : rule__VectorDeclarationExpression__Group__0__Impl rule__VectorDeclarationExpression__Group__1 ;
    public final void rule__VectorDeclarationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5241:1: ( rule__VectorDeclarationExpression__Group__0__Impl rule__VectorDeclarationExpression__Group__1 )
            // InternalGeometryCodes.g:5242:2: rule__VectorDeclarationExpression__Group__0__Impl rule__VectorDeclarationExpression__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__VectorDeclarationExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__0"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__0__Impl"
    // InternalGeometryCodes.g:5249:1: rule__VectorDeclarationExpression__Group__0__Impl : ( 'vector' ) ;
    public final void rule__VectorDeclarationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5253:1: ( ( 'vector' ) )
            // InternalGeometryCodes.g:5254:1: ( 'vector' )
            {
            // InternalGeometryCodes.g:5254:1: ( 'vector' )
            // InternalGeometryCodes.g:5255:2: 'vector'
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getVectorKeyword_0()); 
            match(input,31,FOLLOW_2); 
             after(grammarAccess.getVectorDeclarationExpressionAccess().getVectorKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__0__Impl"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__1"
    // InternalGeometryCodes.g:5264:1: rule__VectorDeclarationExpression__Group__1 : rule__VectorDeclarationExpression__Group__1__Impl rule__VectorDeclarationExpression__Group__2 ;
    public final void rule__VectorDeclarationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5268:1: ( rule__VectorDeclarationExpression__Group__1__Impl rule__VectorDeclarationExpression__Group__2 )
            // InternalGeometryCodes.g:5269:2: rule__VectorDeclarationExpression__Group__1__Impl rule__VectorDeclarationExpression__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__VectorDeclarationExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__1"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__1__Impl"
    // InternalGeometryCodes.g:5276:1: rule__VectorDeclarationExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__VectorDeclarationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5280:1: ( ( '(' ) )
            // InternalGeometryCodes.g:5281:1: ( '(' )
            {
            // InternalGeometryCodes.g:5281:1: ( '(' )
            // InternalGeometryCodes.g:5282:2: '('
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getLeftParenthesisKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getVectorDeclarationExpressionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__1__Impl"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__2"
    // InternalGeometryCodes.g:5291:1: rule__VectorDeclarationExpression__Group__2 : rule__VectorDeclarationExpression__Group__2__Impl rule__VectorDeclarationExpression__Group__3 ;
    public final void rule__VectorDeclarationExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5295:1: ( rule__VectorDeclarationExpression__Group__2__Impl rule__VectorDeclarationExpression__Group__3 )
            // InternalGeometryCodes.g:5296:2: rule__VectorDeclarationExpression__Group__2__Impl rule__VectorDeclarationExpression__Group__3
            {
            pushFollow(FOLLOW_41);
            rule__VectorDeclarationExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__2"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__2__Impl"
    // InternalGeometryCodes.g:5303:1: rule__VectorDeclarationExpression__Group__2__Impl : ( ( rule__VectorDeclarationExpression__ArgumentsAssignment_2 ) ) ;
    public final void rule__VectorDeclarationExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5307:1: ( ( ( rule__VectorDeclarationExpression__ArgumentsAssignment_2 ) ) )
            // InternalGeometryCodes.g:5308:1: ( ( rule__VectorDeclarationExpression__ArgumentsAssignment_2 ) )
            {
            // InternalGeometryCodes.g:5308:1: ( ( rule__VectorDeclarationExpression__ArgumentsAssignment_2 ) )
            // InternalGeometryCodes.g:5309:2: ( rule__VectorDeclarationExpression__ArgumentsAssignment_2 )
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsAssignment_2()); 
            // InternalGeometryCodes.g:5310:2: ( rule__VectorDeclarationExpression__ArgumentsAssignment_2 )
            // InternalGeometryCodes.g:5310:3: rule__VectorDeclarationExpression__ArgumentsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__ArgumentsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__2__Impl"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__3"
    // InternalGeometryCodes.g:5318:1: rule__VectorDeclarationExpression__Group__3 : rule__VectorDeclarationExpression__Group__3__Impl rule__VectorDeclarationExpression__Group__4 ;
    public final void rule__VectorDeclarationExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5322:1: ( rule__VectorDeclarationExpression__Group__3__Impl rule__VectorDeclarationExpression__Group__4 )
            // InternalGeometryCodes.g:5323:2: rule__VectorDeclarationExpression__Group__3__Impl rule__VectorDeclarationExpression__Group__4
            {
            pushFollow(FOLLOW_41);
            rule__VectorDeclarationExpression__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__3"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__3__Impl"
    // InternalGeometryCodes.g:5330:1: rule__VectorDeclarationExpression__Group__3__Impl : ( ( rule__VectorDeclarationExpression__Group_3__0 )* ) ;
    public final void rule__VectorDeclarationExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5334:1: ( ( ( rule__VectorDeclarationExpression__Group_3__0 )* ) )
            // InternalGeometryCodes.g:5335:1: ( ( rule__VectorDeclarationExpression__Group_3__0 )* )
            {
            // InternalGeometryCodes.g:5335:1: ( ( rule__VectorDeclarationExpression__Group_3__0 )* )
            // InternalGeometryCodes.g:5336:2: ( rule__VectorDeclarationExpression__Group_3__0 )*
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getGroup_3()); 
            // InternalGeometryCodes.g:5337:2: ( rule__VectorDeclarationExpression__Group_3__0 )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==40) ) {
                    alt38=1;
                }


                switch (alt38) {
            	case 1 :
            	    // InternalGeometryCodes.g:5337:3: rule__VectorDeclarationExpression__Group_3__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__VectorDeclarationExpression__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);

             after(grammarAccess.getVectorDeclarationExpressionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__3__Impl"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__4"
    // InternalGeometryCodes.g:5345:1: rule__VectorDeclarationExpression__Group__4 : rule__VectorDeclarationExpression__Group__4__Impl ;
    public final void rule__VectorDeclarationExpression__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5349:1: ( rule__VectorDeclarationExpression__Group__4__Impl )
            // InternalGeometryCodes.g:5350:2: rule__VectorDeclarationExpression__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__4"


    // $ANTLR start "rule__VectorDeclarationExpression__Group__4__Impl"
    // InternalGeometryCodes.g:5356:1: rule__VectorDeclarationExpression__Group__4__Impl : ( ')' ) ;
    public final void rule__VectorDeclarationExpression__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5360:1: ( ( ')' ) )
            // InternalGeometryCodes.g:5361:1: ( ')' )
            {
            // InternalGeometryCodes.g:5361:1: ( ')' )
            // InternalGeometryCodes.g:5362:2: ')'
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getRightParenthesisKeyword_4()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getVectorDeclarationExpressionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group__4__Impl"


    // $ANTLR start "rule__VectorDeclarationExpression__Group_3__0"
    // InternalGeometryCodes.g:5372:1: rule__VectorDeclarationExpression__Group_3__0 : rule__VectorDeclarationExpression__Group_3__0__Impl rule__VectorDeclarationExpression__Group_3__1 ;
    public final void rule__VectorDeclarationExpression__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5376:1: ( rule__VectorDeclarationExpression__Group_3__0__Impl rule__VectorDeclarationExpression__Group_3__1 )
            // InternalGeometryCodes.g:5377:2: rule__VectorDeclarationExpression__Group_3__0__Impl rule__VectorDeclarationExpression__Group_3__1
            {
            pushFollow(FOLLOW_23);
            rule__VectorDeclarationExpression__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group_3__0"


    // $ANTLR start "rule__VectorDeclarationExpression__Group_3__0__Impl"
    // InternalGeometryCodes.g:5384:1: rule__VectorDeclarationExpression__Group_3__0__Impl : ( ',' ) ;
    public final void rule__VectorDeclarationExpression__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5388:1: ( ( ',' ) )
            // InternalGeometryCodes.g:5389:1: ( ',' )
            {
            // InternalGeometryCodes.g:5389:1: ( ',' )
            // InternalGeometryCodes.g:5390:2: ','
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getCommaKeyword_3_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getVectorDeclarationExpressionAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group_3__0__Impl"


    // $ANTLR start "rule__VectorDeclarationExpression__Group_3__1"
    // InternalGeometryCodes.g:5399:1: rule__VectorDeclarationExpression__Group_3__1 : rule__VectorDeclarationExpression__Group_3__1__Impl ;
    public final void rule__VectorDeclarationExpression__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5403:1: ( rule__VectorDeclarationExpression__Group_3__1__Impl )
            // InternalGeometryCodes.g:5404:2: rule__VectorDeclarationExpression__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group_3__1"


    // $ANTLR start "rule__VectorDeclarationExpression__Group_3__1__Impl"
    // InternalGeometryCodes.g:5410:1: rule__VectorDeclarationExpression__Group_3__1__Impl : ( ( rule__VectorDeclarationExpression__ArgumentsAssignment_3_1 ) ) ;
    public final void rule__VectorDeclarationExpression__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5414:1: ( ( ( rule__VectorDeclarationExpression__ArgumentsAssignment_3_1 ) ) )
            // InternalGeometryCodes.g:5415:1: ( ( rule__VectorDeclarationExpression__ArgumentsAssignment_3_1 ) )
            {
            // InternalGeometryCodes.g:5415:1: ( ( rule__VectorDeclarationExpression__ArgumentsAssignment_3_1 ) )
            // InternalGeometryCodes.g:5416:2: ( rule__VectorDeclarationExpression__ArgumentsAssignment_3_1 )
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsAssignment_3_1()); 
            // InternalGeometryCodes.g:5417:2: ( rule__VectorDeclarationExpression__ArgumentsAssignment_3_1 )
            // InternalGeometryCodes.g:5417:3: rule__VectorDeclarationExpression__ArgumentsAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__VectorDeclarationExpression__ArgumentsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__Group_3__1__Impl"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__0"
    // InternalGeometryCodes.g:5426:1: rule__VertexDeclarationExpression__Group__0 : rule__VertexDeclarationExpression__Group__0__Impl rule__VertexDeclarationExpression__Group__1 ;
    public final void rule__VertexDeclarationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5430:1: ( rule__VertexDeclarationExpression__Group__0__Impl rule__VertexDeclarationExpression__Group__1 )
            // InternalGeometryCodes.g:5431:2: rule__VertexDeclarationExpression__Group__0__Impl rule__VertexDeclarationExpression__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__VertexDeclarationExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__0"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__0__Impl"
    // InternalGeometryCodes.g:5438:1: rule__VertexDeclarationExpression__Group__0__Impl : ( 'vertex' ) ;
    public final void rule__VertexDeclarationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5442:1: ( ( 'vertex' ) )
            // InternalGeometryCodes.g:5443:1: ( 'vertex' )
            {
            // InternalGeometryCodes.g:5443:1: ( 'vertex' )
            // InternalGeometryCodes.g:5444:2: 'vertex'
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getVertexKeyword_0()); 
            match(input,55,FOLLOW_2); 
             after(grammarAccess.getVertexDeclarationExpressionAccess().getVertexKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__0__Impl"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__1"
    // InternalGeometryCodes.g:5453:1: rule__VertexDeclarationExpression__Group__1 : rule__VertexDeclarationExpression__Group__1__Impl rule__VertexDeclarationExpression__Group__2 ;
    public final void rule__VertexDeclarationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5457:1: ( rule__VertexDeclarationExpression__Group__1__Impl rule__VertexDeclarationExpression__Group__2 )
            // InternalGeometryCodes.g:5458:2: rule__VertexDeclarationExpression__Group__1__Impl rule__VertexDeclarationExpression__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__VertexDeclarationExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__1"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__1__Impl"
    // InternalGeometryCodes.g:5465:1: rule__VertexDeclarationExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__VertexDeclarationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5469:1: ( ( '(' ) )
            // InternalGeometryCodes.g:5470:1: ( '(' )
            {
            // InternalGeometryCodes.g:5470:1: ( '(' )
            // InternalGeometryCodes.g:5471:2: '('
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getLeftParenthesisKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getVertexDeclarationExpressionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__1__Impl"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__2"
    // InternalGeometryCodes.g:5480:1: rule__VertexDeclarationExpression__Group__2 : rule__VertexDeclarationExpression__Group__2__Impl rule__VertexDeclarationExpression__Group__3 ;
    public final void rule__VertexDeclarationExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5484:1: ( rule__VertexDeclarationExpression__Group__2__Impl rule__VertexDeclarationExpression__Group__3 )
            // InternalGeometryCodes.g:5485:2: rule__VertexDeclarationExpression__Group__2__Impl rule__VertexDeclarationExpression__Group__3
            {
            pushFollow(FOLLOW_41);
            rule__VertexDeclarationExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__2"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__2__Impl"
    // InternalGeometryCodes.g:5492:1: rule__VertexDeclarationExpression__Group__2__Impl : ( ( rule__VertexDeclarationExpression__ArgumentsAssignment_2 ) ) ;
    public final void rule__VertexDeclarationExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5496:1: ( ( ( rule__VertexDeclarationExpression__ArgumentsAssignment_2 ) ) )
            // InternalGeometryCodes.g:5497:1: ( ( rule__VertexDeclarationExpression__ArgumentsAssignment_2 ) )
            {
            // InternalGeometryCodes.g:5497:1: ( ( rule__VertexDeclarationExpression__ArgumentsAssignment_2 ) )
            // InternalGeometryCodes.g:5498:2: ( rule__VertexDeclarationExpression__ArgumentsAssignment_2 )
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsAssignment_2()); 
            // InternalGeometryCodes.g:5499:2: ( rule__VertexDeclarationExpression__ArgumentsAssignment_2 )
            // InternalGeometryCodes.g:5499:3: rule__VertexDeclarationExpression__ArgumentsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__ArgumentsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__2__Impl"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__3"
    // InternalGeometryCodes.g:5507:1: rule__VertexDeclarationExpression__Group__3 : rule__VertexDeclarationExpression__Group__3__Impl rule__VertexDeclarationExpression__Group__4 ;
    public final void rule__VertexDeclarationExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5511:1: ( rule__VertexDeclarationExpression__Group__3__Impl rule__VertexDeclarationExpression__Group__4 )
            // InternalGeometryCodes.g:5512:2: rule__VertexDeclarationExpression__Group__3__Impl rule__VertexDeclarationExpression__Group__4
            {
            pushFollow(FOLLOW_41);
            rule__VertexDeclarationExpression__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__3"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__3__Impl"
    // InternalGeometryCodes.g:5519:1: rule__VertexDeclarationExpression__Group__3__Impl : ( ( rule__VertexDeclarationExpression__Group_3__0 )* ) ;
    public final void rule__VertexDeclarationExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5523:1: ( ( ( rule__VertexDeclarationExpression__Group_3__0 )* ) )
            // InternalGeometryCodes.g:5524:1: ( ( rule__VertexDeclarationExpression__Group_3__0 )* )
            {
            // InternalGeometryCodes.g:5524:1: ( ( rule__VertexDeclarationExpression__Group_3__0 )* )
            // InternalGeometryCodes.g:5525:2: ( rule__VertexDeclarationExpression__Group_3__0 )*
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getGroup_3()); 
            // InternalGeometryCodes.g:5526:2: ( rule__VertexDeclarationExpression__Group_3__0 )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==40) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // InternalGeometryCodes.g:5526:3: rule__VertexDeclarationExpression__Group_3__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__VertexDeclarationExpression__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);

             after(grammarAccess.getVertexDeclarationExpressionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__3__Impl"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__4"
    // InternalGeometryCodes.g:5534:1: rule__VertexDeclarationExpression__Group__4 : rule__VertexDeclarationExpression__Group__4__Impl ;
    public final void rule__VertexDeclarationExpression__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5538:1: ( rule__VertexDeclarationExpression__Group__4__Impl )
            // InternalGeometryCodes.g:5539:2: rule__VertexDeclarationExpression__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__4"


    // $ANTLR start "rule__VertexDeclarationExpression__Group__4__Impl"
    // InternalGeometryCodes.g:5545:1: rule__VertexDeclarationExpression__Group__4__Impl : ( ')' ) ;
    public final void rule__VertexDeclarationExpression__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5549:1: ( ( ')' ) )
            // InternalGeometryCodes.g:5550:1: ( ')' )
            {
            // InternalGeometryCodes.g:5550:1: ( ')' )
            // InternalGeometryCodes.g:5551:2: ')'
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getRightParenthesisKeyword_4()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getVertexDeclarationExpressionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group__4__Impl"


    // $ANTLR start "rule__VertexDeclarationExpression__Group_3__0"
    // InternalGeometryCodes.g:5561:1: rule__VertexDeclarationExpression__Group_3__0 : rule__VertexDeclarationExpression__Group_3__0__Impl rule__VertexDeclarationExpression__Group_3__1 ;
    public final void rule__VertexDeclarationExpression__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5565:1: ( rule__VertexDeclarationExpression__Group_3__0__Impl rule__VertexDeclarationExpression__Group_3__1 )
            // InternalGeometryCodes.g:5566:2: rule__VertexDeclarationExpression__Group_3__0__Impl rule__VertexDeclarationExpression__Group_3__1
            {
            pushFollow(FOLLOW_23);
            rule__VertexDeclarationExpression__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group_3__0"


    // $ANTLR start "rule__VertexDeclarationExpression__Group_3__0__Impl"
    // InternalGeometryCodes.g:5573:1: rule__VertexDeclarationExpression__Group_3__0__Impl : ( ',' ) ;
    public final void rule__VertexDeclarationExpression__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5577:1: ( ( ',' ) )
            // InternalGeometryCodes.g:5578:1: ( ',' )
            {
            // InternalGeometryCodes.g:5578:1: ( ',' )
            // InternalGeometryCodes.g:5579:2: ','
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getCommaKeyword_3_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getVertexDeclarationExpressionAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group_3__0__Impl"


    // $ANTLR start "rule__VertexDeclarationExpression__Group_3__1"
    // InternalGeometryCodes.g:5588:1: rule__VertexDeclarationExpression__Group_3__1 : rule__VertexDeclarationExpression__Group_3__1__Impl ;
    public final void rule__VertexDeclarationExpression__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5592:1: ( rule__VertexDeclarationExpression__Group_3__1__Impl )
            // InternalGeometryCodes.g:5593:2: rule__VertexDeclarationExpression__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group_3__1"


    // $ANTLR start "rule__VertexDeclarationExpression__Group_3__1__Impl"
    // InternalGeometryCodes.g:5599:1: rule__VertexDeclarationExpression__Group_3__1__Impl : ( ( rule__VertexDeclarationExpression__ArgumentsAssignment_3_1 ) ) ;
    public final void rule__VertexDeclarationExpression__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5603:1: ( ( ( rule__VertexDeclarationExpression__ArgumentsAssignment_3_1 ) ) )
            // InternalGeometryCodes.g:5604:1: ( ( rule__VertexDeclarationExpression__ArgumentsAssignment_3_1 ) )
            {
            // InternalGeometryCodes.g:5604:1: ( ( rule__VertexDeclarationExpression__ArgumentsAssignment_3_1 ) )
            // InternalGeometryCodes.g:5605:2: ( rule__VertexDeclarationExpression__ArgumentsAssignment_3_1 )
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsAssignment_3_1()); 
            // InternalGeometryCodes.g:5606:2: ( rule__VertexDeclarationExpression__ArgumentsAssignment_3_1 )
            // InternalGeometryCodes.g:5606:3: rule__VertexDeclarationExpression__ArgumentsAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__VertexDeclarationExpression__ArgumentsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__Group_3__1__Impl"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__0"
    // InternalGeometryCodes.g:5615:1: rule__TriangleDeclarationExpression__Group__0 : rule__TriangleDeclarationExpression__Group__0__Impl rule__TriangleDeclarationExpression__Group__1 ;
    public final void rule__TriangleDeclarationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5619:1: ( rule__TriangleDeclarationExpression__Group__0__Impl rule__TriangleDeclarationExpression__Group__1 )
            // InternalGeometryCodes.g:5620:2: rule__TriangleDeclarationExpression__Group__0__Impl rule__TriangleDeclarationExpression__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__TriangleDeclarationExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__0"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__0__Impl"
    // InternalGeometryCodes.g:5627:1: rule__TriangleDeclarationExpression__Group__0__Impl : ( 'triangle' ) ;
    public final void rule__TriangleDeclarationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5631:1: ( ( 'triangle' ) )
            // InternalGeometryCodes.g:5632:1: ( 'triangle' )
            {
            // InternalGeometryCodes.g:5632:1: ( 'triangle' )
            // InternalGeometryCodes.g:5633:2: 'triangle'
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getTriangleKeyword_0()); 
            match(input,32,FOLLOW_2); 
             after(grammarAccess.getTriangleDeclarationExpressionAccess().getTriangleKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__0__Impl"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__1"
    // InternalGeometryCodes.g:5642:1: rule__TriangleDeclarationExpression__Group__1 : rule__TriangleDeclarationExpression__Group__1__Impl rule__TriangleDeclarationExpression__Group__2 ;
    public final void rule__TriangleDeclarationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5646:1: ( rule__TriangleDeclarationExpression__Group__1__Impl rule__TriangleDeclarationExpression__Group__2 )
            // InternalGeometryCodes.g:5647:2: rule__TriangleDeclarationExpression__Group__1__Impl rule__TriangleDeclarationExpression__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__TriangleDeclarationExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__1"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__1__Impl"
    // InternalGeometryCodes.g:5654:1: rule__TriangleDeclarationExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__TriangleDeclarationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5658:1: ( ( '(' ) )
            // InternalGeometryCodes.g:5659:1: ( '(' )
            {
            // InternalGeometryCodes.g:5659:1: ( '(' )
            // InternalGeometryCodes.g:5660:2: '('
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getLeftParenthesisKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getTriangleDeclarationExpressionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__1__Impl"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__2"
    // InternalGeometryCodes.g:5669:1: rule__TriangleDeclarationExpression__Group__2 : rule__TriangleDeclarationExpression__Group__2__Impl rule__TriangleDeclarationExpression__Group__3 ;
    public final void rule__TriangleDeclarationExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5673:1: ( rule__TriangleDeclarationExpression__Group__2__Impl rule__TriangleDeclarationExpression__Group__3 )
            // InternalGeometryCodes.g:5674:2: rule__TriangleDeclarationExpression__Group__2__Impl rule__TriangleDeclarationExpression__Group__3
            {
            pushFollow(FOLLOW_41);
            rule__TriangleDeclarationExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__2"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__2__Impl"
    // InternalGeometryCodes.g:5681:1: rule__TriangleDeclarationExpression__Group__2__Impl : ( ( rule__TriangleDeclarationExpression__ArgumentsAssignment_2 ) ) ;
    public final void rule__TriangleDeclarationExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5685:1: ( ( ( rule__TriangleDeclarationExpression__ArgumentsAssignment_2 ) ) )
            // InternalGeometryCodes.g:5686:1: ( ( rule__TriangleDeclarationExpression__ArgumentsAssignment_2 ) )
            {
            // InternalGeometryCodes.g:5686:1: ( ( rule__TriangleDeclarationExpression__ArgumentsAssignment_2 ) )
            // InternalGeometryCodes.g:5687:2: ( rule__TriangleDeclarationExpression__ArgumentsAssignment_2 )
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsAssignment_2()); 
            // InternalGeometryCodes.g:5688:2: ( rule__TriangleDeclarationExpression__ArgumentsAssignment_2 )
            // InternalGeometryCodes.g:5688:3: rule__TriangleDeclarationExpression__ArgumentsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__ArgumentsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__2__Impl"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__3"
    // InternalGeometryCodes.g:5696:1: rule__TriangleDeclarationExpression__Group__3 : rule__TriangleDeclarationExpression__Group__3__Impl rule__TriangleDeclarationExpression__Group__4 ;
    public final void rule__TriangleDeclarationExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5700:1: ( rule__TriangleDeclarationExpression__Group__3__Impl rule__TriangleDeclarationExpression__Group__4 )
            // InternalGeometryCodes.g:5701:2: rule__TriangleDeclarationExpression__Group__3__Impl rule__TriangleDeclarationExpression__Group__4
            {
            pushFollow(FOLLOW_41);
            rule__TriangleDeclarationExpression__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__3"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__3__Impl"
    // InternalGeometryCodes.g:5708:1: rule__TriangleDeclarationExpression__Group__3__Impl : ( ( rule__TriangleDeclarationExpression__Group_3__0 )* ) ;
    public final void rule__TriangleDeclarationExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5712:1: ( ( ( rule__TriangleDeclarationExpression__Group_3__0 )* ) )
            // InternalGeometryCodes.g:5713:1: ( ( rule__TriangleDeclarationExpression__Group_3__0 )* )
            {
            // InternalGeometryCodes.g:5713:1: ( ( rule__TriangleDeclarationExpression__Group_3__0 )* )
            // InternalGeometryCodes.g:5714:2: ( rule__TriangleDeclarationExpression__Group_3__0 )*
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getGroup_3()); 
            // InternalGeometryCodes.g:5715:2: ( rule__TriangleDeclarationExpression__Group_3__0 )*
            loop40:
            do {
                int alt40=2;
                int LA40_0 = input.LA(1);

                if ( (LA40_0==40) ) {
                    alt40=1;
                }


                switch (alt40) {
            	case 1 :
            	    // InternalGeometryCodes.g:5715:3: rule__TriangleDeclarationExpression__Group_3__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__TriangleDeclarationExpression__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop40;
                }
            } while (true);

             after(grammarAccess.getTriangleDeclarationExpressionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__3__Impl"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__4"
    // InternalGeometryCodes.g:5723:1: rule__TriangleDeclarationExpression__Group__4 : rule__TriangleDeclarationExpression__Group__4__Impl ;
    public final void rule__TriangleDeclarationExpression__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5727:1: ( rule__TriangleDeclarationExpression__Group__4__Impl )
            // InternalGeometryCodes.g:5728:2: rule__TriangleDeclarationExpression__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__4"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group__4__Impl"
    // InternalGeometryCodes.g:5734:1: rule__TriangleDeclarationExpression__Group__4__Impl : ( ')' ) ;
    public final void rule__TriangleDeclarationExpression__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5738:1: ( ( ')' ) )
            // InternalGeometryCodes.g:5739:1: ( ')' )
            {
            // InternalGeometryCodes.g:5739:1: ( ')' )
            // InternalGeometryCodes.g:5740:2: ')'
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getRightParenthesisKeyword_4()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getTriangleDeclarationExpressionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group__4__Impl"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group_3__0"
    // InternalGeometryCodes.g:5750:1: rule__TriangleDeclarationExpression__Group_3__0 : rule__TriangleDeclarationExpression__Group_3__0__Impl rule__TriangleDeclarationExpression__Group_3__1 ;
    public final void rule__TriangleDeclarationExpression__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5754:1: ( rule__TriangleDeclarationExpression__Group_3__0__Impl rule__TriangleDeclarationExpression__Group_3__1 )
            // InternalGeometryCodes.g:5755:2: rule__TriangleDeclarationExpression__Group_3__0__Impl rule__TriangleDeclarationExpression__Group_3__1
            {
            pushFollow(FOLLOW_23);
            rule__TriangleDeclarationExpression__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group_3__0"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group_3__0__Impl"
    // InternalGeometryCodes.g:5762:1: rule__TriangleDeclarationExpression__Group_3__0__Impl : ( ',' ) ;
    public final void rule__TriangleDeclarationExpression__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5766:1: ( ( ',' ) )
            // InternalGeometryCodes.g:5767:1: ( ',' )
            {
            // InternalGeometryCodes.g:5767:1: ( ',' )
            // InternalGeometryCodes.g:5768:2: ','
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getCommaKeyword_3_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getTriangleDeclarationExpressionAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group_3__0__Impl"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group_3__1"
    // InternalGeometryCodes.g:5777:1: rule__TriangleDeclarationExpression__Group_3__1 : rule__TriangleDeclarationExpression__Group_3__1__Impl ;
    public final void rule__TriangleDeclarationExpression__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5781:1: ( rule__TriangleDeclarationExpression__Group_3__1__Impl )
            // InternalGeometryCodes.g:5782:2: rule__TriangleDeclarationExpression__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group_3__1"


    // $ANTLR start "rule__TriangleDeclarationExpression__Group_3__1__Impl"
    // InternalGeometryCodes.g:5788:1: rule__TriangleDeclarationExpression__Group_3__1__Impl : ( ( rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1 ) ) ;
    public final void rule__TriangleDeclarationExpression__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5792:1: ( ( ( rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1 ) ) )
            // InternalGeometryCodes.g:5793:1: ( ( rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1 ) )
            {
            // InternalGeometryCodes.g:5793:1: ( ( rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1 ) )
            // InternalGeometryCodes.g:5794:2: ( rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1 )
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsAssignment_3_1()); 
            // InternalGeometryCodes.g:5795:2: ( rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1 )
            // InternalGeometryCodes.g:5795:3: rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__Group_3__1__Impl"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__0"
    // InternalGeometryCodes.g:5804:1: rule__QuadDeclarationExpression__Group__0 : rule__QuadDeclarationExpression__Group__0__Impl rule__QuadDeclarationExpression__Group__1 ;
    public final void rule__QuadDeclarationExpression__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5808:1: ( rule__QuadDeclarationExpression__Group__0__Impl rule__QuadDeclarationExpression__Group__1 )
            // InternalGeometryCodes.g:5809:2: rule__QuadDeclarationExpression__Group__0__Impl rule__QuadDeclarationExpression__Group__1
            {
            pushFollow(FOLLOW_8);
            rule__QuadDeclarationExpression__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__0"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__0__Impl"
    // InternalGeometryCodes.g:5816:1: rule__QuadDeclarationExpression__Group__0__Impl : ( 'quad' ) ;
    public final void rule__QuadDeclarationExpression__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5820:1: ( ( 'quad' ) )
            // InternalGeometryCodes.g:5821:1: ( 'quad' )
            {
            // InternalGeometryCodes.g:5821:1: ( 'quad' )
            // InternalGeometryCodes.g:5822:2: 'quad'
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getQuadKeyword_0()); 
            match(input,33,FOLLOW_2); 
             after(grammarAccess.getQuadDeclarationExpressionAccess().getQuadKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__0__Impl"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__1"
    // InternalGeometryCodes.g:5831:1: rule__QuadDeclarationExpression__Group__1 : rule__QuadDeclarationExpression__Group__1__Impl rule__QuadDeclarationExpression__Group__2 ;
    public final void rule__QuadDeclarationExpression__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5835:1: ( rule__QuadDeclarationExpression__Group__1__Impl rule__QuadDeclarationExpression__Group__2 )
            // InternalGeometryCodes.g:5836:2: rule__QuadDeclarationExpression__Group__1__Impl rule__QuadDeclarationExpression__Group__2
            {
            pushFollow(FOLLOW_23);
            rule__QuadDeclarationExpression__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__1"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__1__Impl"
    // InternalGeometryCodes.g:5843:1: rule__QuadDeclarationExpression__Group__1__Impl : ( '(' ) ;
    public final void rule__QuadDeclarationExpression__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5847:1: ( ( '(' ) )
            // InternalGeometryCodes.g:5848:1: ( '(' )
            {
            // InternalGeometryCodes.g:5848:1: ( '(' )
            // InternalGeometryCodes.g:5849:2: '('
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getLeftParenthesisKeyword_1()); 
            match(input,36,FOLLOW_2); 
             after(grammarAccess.getQuadDeclarationExpressionAccess().getLeftParenthesisKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__1__Impl"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__2"
    // InternalGeometryCodes.g:5858:1: rule__QuadDeclarationExpression__Group__2 : rule__QuadDeclarationExpression__Group__2__Impl rule__QuadDeclarationExpression__Group__3 ;
    public final void rule__QuadDeclarationExpression__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5862:1: ( rule__QuadDeclarationExpression__Group__2__Impl rule__QuadDeclarationExpression__Group__3 )
            // InternalGeometryCodes.g:5863:2: rule__QuadDeclarationExpression__Group__2__Impl rule__QuadDeclarationExpression__Group__3
            {
            pushFollow(FOLLOW_41);
            rule__QuadDeclarationExpression__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__2"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__2__Impl"
    // InternalGeometryCodes.g:5870:1: rule__QuadDeclarationExpression__Group__2__Impl : ( ( rule__QuadDeclarationExpression__ArgumentsAssignment_2 ) ) ;
    public final void rule__QuadDeclarationExpression__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5874:1: ( ( ( rule__QuadDeclarationExpression__ArgumentsAssignment_2 ) ) )
            // InternalGeometryCodes.g:5875:1: ( ( rule__QuadDeclarationExpression__ArgumentsAssignment_2 ) )
            {
            // InternalGeometryCodes.g:5875:1: ( ( rule__QuadDeclarationExpression__ArgumentsAssignment_2 ) )
            // InternalGeometryCodes.g:5876:2: ( rule__QuadDeclarationExpression__ArgumentsAssignment_2 )
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsAssignment_2()); 
            // InternalGeometryCodes.g:5877:2: ( rule__QuadDeclarationExpression__ArgumentsAssignment_2 )
            // InternalGeometryCodes.g:5877:3: rule__QuadDeclarationExpression__ArgumentsAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__ArgumentsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__2__Impl"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__3"
    // InternalGeometryCodes.g:5885:1: rule__QuadDeclarationExpression__Group__3 : rule__QuadDeclarationExpression__Group__3__Impl rule__QuadDeclarationExpression__Group__4 ;
    public final void rule__QuadDeclarationExpression__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5889:1: ( rule__QuadDeclarationExpression__Group__3__Impl rule__QuadDeclarationExpression__Group__4 )
            // InternalGeometryCodes.g:5890:2: rule__QuadDeclarationExpression__Group__3__Impl rule__QuadDeclarationExpression__Group__4
            {
            pushFollow(FOLLOW_41);
            rule__QuadDeclarationExpression__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__3"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__3__Impl"
    // InternalGeometryCodes.g:5897:1: rule__QuadDeclarationExpression__Group__3__Impl : ( ( rule__QuadDeclarationExpression__Group_3__0 )* ) ;
    public final void rule__QuadDeclarationExpression__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5901:1: ( ( ( rule__QuadDeclarationExpression__Group_3__0 )* ) )
            // InternalGeometryCodes.g:5902:1: ( ( rule__QuadDeclarationExpression__Group_3__0 )* )
            {
            // InternalGeometryCodes.g:5902:1: ( ( rule__QuadDeclarationExpression__Group_3__0 )* )
            // InternalGeometryCodes.g:5903:2: ( rule__QuadDeclarationExpression__Group_3__0 )*
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getGroup_3()); 
            // InternalGeometryCodes.g:5904:2: ( rule__QuadDeclarationExpression__Group_3__0 )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==40) ) {
                    alt41=1;
                }


                switch (alt41) {
            	case 1 :
            	    // InternalGeometryCodes.g:5904:3: rule__QuadDeclarationExpression__Group_3__0
            	    {
            	    pushFollow(FOLLOW_13);
            	    rule__QuadDeclarationExpression__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);

             after(grammarAccess.getQuadDeclarationExpressionAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__3__Impl"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__4"
    // InternalGeometryCodes.g:5912:1: rule__QuadDeclarationExpression__Group__4 : rule__QuadDeclarationExpression__Group__4__Impl ;
    public final void rule__QuadDeclarationExpression__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5916:1: ( rule__QuadDeclarationExpression__Group__4__Impl )
            // InternalGeometryCodes.g:5917:2: rule__QuadDeclarationExpression__Group__4__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__4"


    // $ANTLR start "rule__QuadDeclarationExpression__Group__4__Impl"
    // InternalGeometryCodes.g:5923:1: rule__QuadDeclarationExpression__Group__4__Impl : ( ')' ) ;
    public final void rule__QuadDeclarationExpression__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5927:1: ( ( ')' ) )
            // InternalGeometryCodes.g:5928:1: ( ')' )
            {
            // InternalGeometryCodes.g:5928:1: ( ')' )
            // InternalGeometryCodes.g:5929:2: ')'
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getRightParenthesisKeyword_4()); 
            match(input,37,FOLLOW_2); 
             after(grammarAccess.getQuadDeclarationExpressionAccess().getRightParenthesisKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group__4__Impl"


    // $ANTLR start "rule__QuadDeclarationExpression__Group_3__0"
    // InternalGeometryCodes.g:5939:1: rule__QuadDeclarationExpression__Group_3__0 : rule__QuadDeclarationExpression__Group_3__0__Impl rule__QuadDeclarationExpression__Group_3__1 ;
    public final void rule__QuadDeclarationExpression__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5943:1: ( rule__QuadDeclarationExpression__Group_3__0__Impl rule__QuadDeclarationExpression__Group_3__1 )
            // InternalGeometryCodes.g:5944:2: rule__QuadDeclarationExpression__Group_3__0__Impl rule__QuadDeclarationExpression__Group_3__1
            {
            pushFollow(FOLLOW_23);
            rule__QuadDeclarationExpression__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group_3__0"


    // $ANTLR start "rule__QuadDeclarationExpression__Group_3__0__Impl"
    // InternalGeometryCodes.g:5951:1: rule__QuadDeclarationExpression__Group_3__0__Impl : ( ',' ) ;
    public final void rule__QuadDeclarationExpression__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5955:1: ( ( ',' ) )
            // InternalGeometryCodes.g:5956:1: ( ',' )
            {
            // InternalGeometryCodes.g:5956:1: ( ',' )
            // InternalGeometryCodes.g:5957:2: ','
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getCommaKeyword_3_0()); 
            match(input,40,FOLLOW_2); 
             after(grammarAccess.getQuadDeclarationExpressionAccess().getCommaKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group_3__0__Impl"


    // $ANTLR start "rule__QuadDeclarationExpression__Group_3__1"
    // InternalGeometryCodes.g:5966:1: rule__QuadDeclarationExpression__Group_3__1 : rule__QuadDeclarationExpression__Group_3__1__Impl ;
    public final void rule__QuadDeclarationExpression__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5970:1: ( rule__QuadDeclarationExpression__Group_3__1__Impl )
            // InternalGeometryCodes.g:5971:2: rule__QuadDeclarationExpression__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group_3__1"


    // $ANTLR start "rule__QuadDeclarationExpression__Group_3__1__Impl"
    // InternalGeometryCodes.g:5977:1: rule__QuadDeclarationExpression__Group_3__1__Impl : ( ( rule__QuadDeclarationExpression__ArgumentsAssignment_3_1 ) ) ;
    public final void rule__QuadDeclarationExpression__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5981:1: ( ( ( rule__QuadDeclarationExpression__ArgumentsAssignment_3_1 ) ) )
            // InternalGeometryCodes.g:5982:1: ( ( rule__QuadDeclarationExpression__ArgumentsAssignment_3_1 ) )
            {
            // InternalGeometryCodes.g:5982:1: ( ( rule__QuadDeclarationExpression__ArgumentsAssignment_3_1 ) )
            // InternalGeometryCodes.g:5983:2: ( rule__QuadDeclarationExpression__ArgumentsAssignment_3_1 )
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsAssignment_3_1()); 
            // InternalGeometryCodes.g:5984:2: ( rule__QuadDeclarationExpression__ArgumentsAssignment_3_1 )
            // InternalGeometryCodes.g:5984:3: rule__QuadDeclarationExpression__ArgumentsAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__QuadDeclarationExpression__ArgumentsAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__Group_3__1__Impl"


    // $ANTLR start "rule__Literal__Group_3__0"
    // InternalGeometryCodes.g:5993:1: rule__Literal__Group_3__0 : rule__Literal__Group_3__0__Impl rule__Literal__Group_3__1 ;
    public final void rule__Literal__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:5997:1: ( rule__Literal__Group_3__0__Impl rule__Literal__Group_3__1 )
            // InternalGeometryCodes.g:5998:2: rule__Literal__Group_3__0__Impl rule__Literal__Group_3__1
            {
            pushFollow(FOLLOW_42);
            rule__Literal__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Literal__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_3__0"


    // $ANTLR start "rule__Literal__Group_3__0__Impl"
    // InternalGeometryCodes.g:6005:1: rule__Literal__Group_3__0__Impl : ( () ) ;
    public final void rule__Literal__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6009:1: ( ( () ) )
            // InternalGeometryCodes.g:6010:1: ( () )
            {
            // InternalGeometryCodes.g:6010:1: ( () )
            // InternalGeometryCodes.g:6011:2: ()
            {
             before(grammarAccess.getLiteralAccess().getEmptyLiteralAction_3_0()); 
            // InternalGeometryCodes.g:6012:2: ()
            // InternalGeometryCodes.g:6012:3: 
            {
            }

             after(grammarAccess.getLiteralAccess().getEmptyLiteralAction_3_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_3__0__Impl"


    // $ANTLR start "rule__Literal__Group_3__1"
    // InternalGeometryCodes.g:6020:1: rule__Literal__Group_3__1 : rule__Literal__Group_3__1__Impl ;
    public final void rule__Literal__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6024:1: ( rule__Literal__Group_3__1__Impl )
            // InternalGeometryCodes.g:6025:2: rule__Literal__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Literal__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_3__1"


    // $ANTLR start "rule__Literal__Group_3__1__Impl"
    // InternalGeometryCodes.g:6031:1: rule__Literal__Group_3__1__Impl : ( 'empty' ) ;
    public final void rule__Literal__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6035:1: ( ( 'empty' ) )
            // InternalGeometryCodes.g:6036:1: ( 'empty' )
            {
            // InternalGeometryCodes.g:6036:1: ( 'empty' )
            // InternalGeometryCodes.g:6037:2: 'empty'
            {
             before(grammarAccess.getLiteralAccess().getEmptyKeyword_3_1()); 
            match(input,56,FOLLOW_2); 
             after(grammarAccess.getLiteralAccess().getEmptyKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group_3__1__Impl"


    // $ANTLR start "rule__BooleanLiteral__Group_1__0"
    // InternalGeometryCodes.g:6047:1: rule__BooleanLiteral__Group_1__0 : rule__BooleanLiteral__Group_1__0__Impl rule__BooleanLiteral__Group_1__1 ;
    public final void rule__BooleanLiteral__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6051:1: ( rule__BooleanLiteral__Group_1__0__Impl rule__BooleanLiteral__Group_1__1 )
            // InternalGeometryCodes.g:6052:2: rule__BooleanLiteral__Group_1__0__Impl rule__BooleanLiteral__Group_1__1
            {
            pushFollow(FOLLOW_43);
            rule__BooleanLiteral__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group_1__0"


    // $ANTLR start "rule__BooleanLiteral__Group_1__0__Impl"
    // InternalGeometryCodes.g:6059:1: rule__BooleanLiteral__Group_1__0__Impl : ( () ) ;
    public final void rule__BooleanLiteral__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6063:1: ( ( () ) )
            // InternalGeometryCodes.g:6064:1: ( () )
            {
            // InternalGeometryCodes.g:6064:1: ( () )
            // InternalGeometryCodes.g:6065:2: ()
            {
             before(grammarAccess.getBooleanLiteralAccess().getBooleanLiteralAction_1_0()); 
            // InternalGeometryCodes.g:6066:2: ()
            // InternalGeometryCodes.g:6066:3: 
            {
            }

             after(grammarAccess.getBooleanLiteralAccess().getBooleanLiteralAction_1_0()); 

            }


            }

        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group_1__0__Impl"


    // $ANTLR start "rule__BooleanLiteral__Group_1__1"
    // InternalGeometryCodes.g:6074:1: rule__BooleanLiteral__Group_1__1 : rule__BooleanLiteral__Group_1__1__Impl ;
    public final void rule__BooleanLiteral__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6078:1: ( rule__BooleanLiteral__Group_1__1__Impl )
            // InternalGeometryCodes.g:6079:2: rule__BooleanLiteral__Group_1__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__BooleanLiteral__Group_1__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group_1__1"


    // $ANTLR start "rule__BooleanLiteral__Group_1__1__Impl"
    // InternalGeometryCodes.g:6085:1: rule__BooleanLiteral__Group_1__1__Impl : ( 'false' ) ;
    public final void rule__BooleanLiteral__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6089:1: ( ( 'false' ) )
            // InternalGeometryCodes.g:6090:1: ( 'false' )
            {
            // InternalGeometryCodes.g:6090:1: ( 'false' )
            // InternalGeometryCodes.g:6091:2: 'false'
            {
             before(grammarAccess.getBooleanLiteralAccess().getFalseKeyword_1_1()); 
            match(input,57,FOLLOW_2); 
             after(grammarAccess.getBooleanLiteralAccess().getFalseKeyword_1_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__Group_1__1__Impl"


    // $ANTLR start "rule__File__ImportsAssignment_0"
    // InternalGeometryCodes.g:6101:1: rule__File__ImportsAssignment_0 : ( ruleImportStatement ) ;
    public final void rule__File__ImportsAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6105:1: ( ( ruleImportStatement ) )
            // InternalGeometryCodes.g:6106:2: ( ruleImportStatement )
            {
            // InternalGeometryCodes.g:6106:2: ( ruleImportStatement )
            // InternalGeometryCodes.g:6107:3: ruleImportStatement
            {
             before(grammarAccess.getFileAccess().getImportsImportStatementParserRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleImportStatement();

            state._fsp--;

             after(grammarAccess.getFileAccess().getImportsImportStatementParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__ImportsAssignment_0"


    // $ANTLR start "rule__File__FunctionsAssignment_1_0"
    // InternalGeometryCodes.g:6116:1: rule__File__FunctionsAssignment_1_0 : ( ruleFunctionDeclaration ) ;
    public final void rule__File__FunctionsAssignment_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6120:1: ( ( ruleFunctionDeclaration ) )
            // InternalGeometryCodes.g:6121:2: ( ruleFunctionDeclaration )
            {
            // InternalGeometryCodes.g:6121:2: ( ruleFunctionDeclaration )
            // InternalGeometryCodes.g:6122:3: ruleFunctionDeclaration
            {
             before(grammarAccess.getFileAccess().getFunctionsFunctionDeclarationParserRuleCall_1_0_0()); 
            pushFollow(FOLLOW_2);
            ruleFunctionDeclaration();

            state._fsp--;

             after(grammarAccess.getFileAccess().getFunctionsFunctionDeclarationParserRuleCall_1_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__FunctionsAssignment_1_0"


    // $ANTLR start "rule__File__IteratorsAssignment_1_1"
    // InternalGeometryCodes.g:6131:1: rule__File__IteratorsAssignment_1_1 : ( ruleIteratorDeclaration ) ;
    public final void rule__File__IteratorsAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6135:1: ( ( ruleIteratorDeclaration ) )
            // InternalGeometryCodes.g:6136:2: ( ruleIteratorDeclaration )
            {
            // InternalGeometryCodes.g:6136:2: ( ruleIteratorDeclaration )
            // InternalGeometryCodes.g:6137:3: ruleIteratorDeclaration
            {
             before(grammarAccess.getFileAccess().getIteratorsIteratorDeclarationParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleIteratorDeclaration();

            state._fsp--;

             after(grammarAccess.getFileAccess().getIteratorsIteratorDeclarationParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__IteratorsAssignment_1_1"


    // $ANTLR start "rule__File__MaterialsAssignment_1_2"
    // InternalGeometryCodes.g:6146:1: rule__File__MaterialsAssignment_1_2 : ( ruleMaterialDeclaration ) ;
    public final void rule__File__MaterialsAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6150:1: ( ( ruleMaterialDeclaration ) )
            // InternalGeometryCodes.g:6151:2: ( ruleMaterialDeclaration )
            {
            // InternalGeometryCodes.g:6151:2: ( ruleMaterialDeclaration )
            // InternalGeometryCodes.g:6152:3: ruleMaterialDeclaration
            {
             before(grammarAccess.getFileAccess().getMaterialsMaterialDeclarationParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleMaterialDeclaration();

            state._fsp--;

             after(grammarAccess.getFileAccess().getMaterialsMaterialDeclarationParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__MaterialsAssignment_1_2"


    // $ANTLR start "rule__File__StatementsAssignment_1_3"
    // InternalGeometryCodes.g:6161:1: rule__File__StatementsAssignment_1_3 : ( ruleStatement ) ;
    public final void rule__File__StatementsAssignment_1_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6165:1: ( ( ruleStatement ) )
            // InternalGeometryCodes.g:6166:2: ( ruleStatement )
            {
            // InternalGeometryCodes.g:6166:2: ( ruleStatement )
            // InternalGeometryCodes.g:6167:3: ruleStatement
            {
             before(grammarAccess.getFileAccess().getStatementsStatementParserRuleCall_1_3_0()); 
            pushFollow(FOLLOW_2);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getFileAccess().getStatementsStatementParserRuleCall_1_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__File__StatementsAssignment_1_3"


    // $ANTLR start "rule__ImportStatement__ImportURIAssignment_1"
    // InternalGeometryCodes.g:6176:1: rule__ImportStatement__ImportURIAssignment_1 : ( RULE_STRING ) ;
    public final void rule__ImportStatement__ImportURIAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6180:1: ( ( RULE_STRING ) )
            // InternalGeometryCodes.g:6181:2: ( RULE_STRING )
            {
            // InternalGeometryCodes.g:6181:2: ( RULE_STRING )
            // InternalGeometryCodes.g:6182:3: RULE_STRING
            {
             before(grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ImportStatement__ImportURIAssignment_1"


    // $ANTLR start "rule__FunctionDeclaration__TypeAssignment_0"
    // InternalGeometryCodes.g:6191:1: rule__FunctionDeclaration__TypeAssignment_0 : ( ruleType ) ;
    public final void rule__FunctionDeclaration__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6195:1: ( ( ruleType ) )
            // InternalGeometryCodes.g:6196:2: ( ruleType )
            {
            // InternalGeometryCodes.g:6196:2: ( ruleType )
            // InternalGeometryCodes.g:6197:3: ruleType
            {
             before(grammarAccess.getFunctionDeclarationAccess().getTypeTypeEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getFunctionDeclarationAccess().getTypeTypeEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__TypeAssignment_0"


    // $ANTLR start "rule__FunctionDeclaration__NameAssignment_1"
    // InternalGeometryCodes.g:6206:1: rule__FunctionDeclaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__FunctionDeclaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6210:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6211:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6211:2: ( RULE_ID )
            // InternalGeometryCodes.g:6212:3: RULE_ID
            {
             before(grammarAccess.getFunctionDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__NameAssignment_1"


    // $ANTLR start "rule__FunctionDeclaration__ParametersAssignment_3_0"
    // InternalGeometryCodes.g:6221:1: rule__FunctionDeclaration__ParametersAssignment_3_0 : ( ruleParameter ) ;
    public final void rule__FunctionDeclaration__ParametersAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6225:1: ( ( ruleParameter ) )
            // InternalGeometryCodes.g:6226:2: ( ruleParameter )
            {
            // InternalGeometryCodes.g:6226:2: ( ruleParameter )
            // InternalGeometryCodes.g:6227:3: ruleParameter
            {
             before(grammarAccess.getFunctionDeclarationAccess().getParametersParameterParserRuleCall_3_0_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getFunctionDeclarationAccess().getParametersParameterParserRuleCall_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__ParametersAssignment_3_0"


    // $ANTLR start "rule__FunctionDeclaration__ParametersAssignment_3_1_1"
    // InternalGeometryCodes.g:6236:1: rule__FunctionDeclaration__ParametersAssignment_3_1_1 : ( ruleParameter ) ;
    public final void rule__FunctionDeclaration__ParametersAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6240:1: ( ( ruleParameter ) )
            // InternalGeometryCodes.g:6241:2: ( ruleParameter )
            {
            // InternalGeometryCodes.g:6241:2: ( ruleParameter )
            // InternalGeometryCodes.g:6242:3: ruleParameter
            {
             before(grammarAccess.getFunctionDeclarationAccess().getParametersParameterParserRuleCall_3_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getFunctionDeclarationAccess().getParametersParameterParserRuleCall_3_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__ParametersAssignment_3_1_1"


    // $ANTLR start "rule__FunctionDeclaration__StatementsAssignment_6"
    // InternalGeometryCodes.g:6251:1: rule__FunctionDeclaration__StatementsAssignment_6 : ( ruleStatement ) ;
    public final void rule__FunctionDeclaration__StatementsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6255:1: ( ( ruleStatement ) )
            // InternalGeometryCodes.g:6256:2: ( ruleStatement )
            {
            // InternalGeometryCodes.g:6256:2: ( ruleStatement )
            // InternalGeometryCodes.g:6257:3: ruleStatement
            {
             before(grammarAccess.getFunctionDeclarationAccess().getStatementsStatementParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getFunctionDeclarationAccess().getStatementsStatementParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionDeclaration__StatementsAssignment_6"


    // $ANTLR start "rule__IteratorDeclaration__NameAssignment_1"
    // InternalGeometryCodes.g:6266:1: rule__IteratorDeclaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__IteratorDeclaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6270:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6271:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6271:2: ( RULE_ID )
            // InternalGeometryCodes.g:6272:3: RULE_ID
            {
             before(grammarAccess.getIteratorDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getIteratorDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__NameAssignment_1"


    // $ANTLR start "rule__IteratorDeclaration__ParametersAssignment_3_0"
    // InternalGeometryCodes.g:6281:1: rule__IteratorDeclaration__ParametersAssignment_3_0 : ( ruleParameter ) ;
    public final void rule__IteratorDeclaration__ParametersAssignment_3_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6285:1: ( ( ruleParameter ) )
            // InternalGeometryCodes.g:6286:2: ( ruleParameter )
            {
            // InternalGeometryCodes.g:6286:2: ( ruleParameter )
            // InternalGeometryCodes.g:6287:3: ruleParameter
            {
             before(grammarAccess.getIteratorDeclarationAccess().getParametersParameterParserRuleCall_3_0_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getIteratorDeclarationAccess().getParametersParameterParserRuleCall_3_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__ParametersAssignment_3_0"


    // $ANTLR start "rule__IteratorDeclaration__ParametersAssignment_3_1_1"
    // InternalGeometryCodes.g:6296:1: rule__IteratorDeclaration__ParametersAssignment_3_1_1 : ( ruleParameter ) ;
    public final void rule__IteratorDeclaration__ParametersAssignment_3_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6300:1: ( ( ruleParameter ) )
            // InternalGeometryCodes.g:6301:2: ( ruleParameter )
            {
            // InternalGeometryCodes.g:6301:2: ( ruleParameter )
            // InternalGeometryCodes.g:6302:3: ruleParameter
            {
             before(grammarAccess.getIteratorDeclarationAccess().getParametersParameterParserRuleCall_3_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getIteratorDeclarationAccess().getParametersParameterParserRuleCall_3_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__ParametersAssignment_3_1_1"


    // $ANTLR start "rule__IteratorDeclaration__IteratedParameterAssignment_7"
    // InternalGeometryCodes.g:6311:1: rule__IteratorDeclaration__IteratedParameterAssignment_7 : ( ruleParameter ) ;
    public final void rule__IteratorDeclaration__IteratedParameterAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6315:1: ( ( ruleParameter ) )
            // InternalGeometryCodes.g:6316:2: ( ruleParameter )
            {
            // InternalGeometryCodes.g:6316:2: ( ruleParameter )
            // InternalGeometryCodes.g:6317:3: ruleParameter
            {
             before(grammarAccess.getIteratorDeclarationAccess().getIteratedParameterParameterParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleParameter();

            state._fsp--;

             after(grammarAccess.getIteratorDeclarationAccess().getIteratedParameterParameterParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__IteratedParameterAssignment_7"


    // $ANTLR start "rule__IteratorDeclaration__StatementsAssignment_9"
    // InternalGeometryCodes.g:6326:1: rule__IteratorDeclaration__StatementsAssignment_9 : ( ruleStatement ) ;
    public final void rule__IteratorDeclaration__StatementsAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6330:1: ( ( ruleStatement ) )
            // InternalGeometryCodes.g:6331:2: ( ruleStatement )
            {
            // InternalGeometryCodes.g:6331:2: ( ruleStatement )
            // InternalGeometryCodes.g:6332:3: ruleStatement
            {
             before(grammarAccess.getIteratorDeclarationAccess().getStatementsStatementParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getIteratorDeclarationAccess().getStatementsStatementParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IteratorDeclaration__StatementsAssignment_9"


    // $ANTLR start "rule__Parameter__TypeAssignment_0"
    // InternalGeometryCodes.g:6341:1: rule__Parameter__TypeAssignment_0 : ( ruleType ) ;
    public final void rule__Parameter__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6345:1: ( ( ruleType ) )
            // InternalGeometryCodes.g:6346:2: ( ruleType )
            {
            // InternalGeometryCodes.g:6346:2: ( ruleType )
            // InternalGeometryCodes.g:6347:3: ruleType
            {
             before(grammarAccess.getParameterAccess().getTypeTypeEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getParameterAccess().getTypeTypeEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__TypeAssignment_0"


    // $ANTLR start "rule__Parameter__NameAssignment_1"
    // InternalGeometryCodes.g:6356:1: rule__Parameter__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Parameter__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6360:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6361:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6361:2: ( RULE_ID )
            // InternalGeometryCodes.g:6362:3: RULE_ID
            {
             before(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Parameter__NameAssignment_1"


    // $ANTLR start "rule__MaterialDeclaration__NameAssignment_1"
    // InternalGeometryCodes.g:6371:1: rule__MaterialDeclaration__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__MaterialDeclaration__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6375:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6376:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6376:2: ( RULE_ID )
            // InternalGeometryCodes.g:6377:3: RULE_ID
            {
             before(grammarAccess.getMaterialDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMaterialDeclarationAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__NameAssignment_1"


    // $ANTLR start "rule__MaterialDeclaration__PropertiesAssignment_3"
    // InternalGeometryCodes.g:6386:1: rule__MaterialDeclaration__PropertiesAssignment_3 : ( ruleMaterialProperty ) ;
    public final void rule__MaterialDeclaration__PropertiesAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6390:1: ( ( ruleMaterialProperty ) )
            // InternalGeometryCodes.g:6391:2: ( ruleMaterialProperty )
            {
            // InternalGeometryCodes.g:6391:2: ( ruleMaterialProperty )
            // InternalGeometryCodes.g:6392:3: ruleMaterialProperty
            {
             before(grammarAccess.getMaterialDeclarationAccess().getPropertiesMaterialPropertyParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleMaterialProperty();

            state._fsp--;

             after(grammarAccess.getMaterialDeclarationAccess().getPropertiesMaterialPropertyParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialDeclaration__PropertiesAssignment_3"


    // $ANTLR start "rule__MaterialStringProperty__NameAssignment_0"
    // InternalGeometryCodes.g:6401:1: rule__MaterialStringProperty__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__MaterialStringProperty__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6405:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6406:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6406:2: ( RULE_ID )
            // InternalGeometryCodes.g:6407:3: RULE_ID
            {
             before(grammarAccess.getMaterialStringPropertyAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMaterialStringPropertyAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialStringProperty__NameAssignment_0"


    // $ANTLR start "rule__MaterialStringProperty__ValueAssignment_2"
    // InternalGeometryCodes.g:6416:1: rule__MaterialStringProperty__ValueAssignment_2 : ( RULE_STRING ) ;
    public final void rule__MaterialStringProperty__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6420:1: ( ( RULE_STRING ) )
            // InternalGeometryCodes.g:6421:2: ( RULE_STRING )
            {
            // InternalGeometryCodes.g:6421:2: ( RULE_STRING )
            // InternalGeometryCodes.g:6422:3: RULE_STRING
            {
             before(grammarAccess.getMaterialStringPropertyAccess().getValueSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getMaterialStringPropertyAccess().getValueSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialStringProperty__ValueAssignment_2"


    // $ANTLR start "rule__MaterialFloatProperty__NameAssignment_0"
    // InternalGeometryCodes.g:6431:1: rule__MaterialFloatProperty__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__MaterialFloatProperty__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6435:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6436:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6436:2: ( RULE_ID )
            // InternalGeometryCodes.g:6437:3: RULE_ID
            {
             before(grammarAccess.getMaterialFloatPropertyAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMaterialFloatPropertyAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialFloatProperty__NameAssignment_0"


    // $ANTLR start "rule__MaterialFloatProperty__ValueAssignment_2"
    // InternalGeometryCodes.g:6446:1: rule__MaterialFloatProperty__ValueAssignment_2 : ( RULE_FLOAT ) ;
    public final void rule__MaterialFloatProperty__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6450:1: ( ( RULE_FLOAT ) )
            // InternalGeometryCodes.g:6451:2: ( RULE_FLOAT )
            {
            // InternalGeometryCodes.g:6451:2: ( RULE_FLOAT )
            // InternalGeometryCodes.g:6452:3: RULE_FLOAT
            {
             before(grammarAccess.getMaterialFloatPropertyAccess().getValueFLOATTerminalRuleCall_2_0()); 
            match(input,RULE_FLOAT,FOLLOW_2); 
             after(grammarAccess.getMaterialFloatPropertyAccess().getValueFLOATTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialFloatProperty__ValueAssignment_2"


    // $ANTLR start "rule__MaterialBooleanProperty__NameAssignment_0"
    // InternalGeometryCodes.g:6461:1: rule__MaterialBooleanProperty__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__MaterialBooleanProperty__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6465:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6466:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6466:2: ( RULE_ID )
            // InternalGeometryCodes.g:6467:3: RULE_ID
            {
             before(grammarAccess.getMaterialBooleanPropertyAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getMaterialBooleanPropertyAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__NameAssignment_0"


    // $ANTLR start "rule__MaterialBooleanProperty__ValueAssignment_2_0"
    // InternalGeometryCodes.g:6476:1: rule__MaterialBooleanProperty__ValueAssignment_2_0 : ( ( 'yes' ) ) ;
    public final void rule__MaterialBooleanProperty__ValueAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6480:1: ( ( ( 'yes' ) ) )
            // InternalGeometryCodes.g:6481:2: ( ( 'yes' ) )
            {
            // InternalGeometryCodes.g:6481:2: ( ( 'yes' ) )
            // InternalGeometryCodes.g:6482:3: ( 'yes' )
            {
             before(grammarAccess.getMaterialBooleanPropertyAccess().getValueYesKeyword_2_0_0()); 
            // InternalGeometryCodes.g:6483:3: ( 'yes' )
            // InternalGeometryCodes.g:6484:4: 'yes'
            {
             before(grammarAccess.getMaterialBooleanPropertyAccess().getValueYesKeyword_2_0_0()); 
            match(input,58,FOLLOW_2); 
             after(grammarAccess.getMaterialBooleanPropertyAccess().getValueYesKeyword_2_0_0()); 

            }

             after(grammarAccess.getMaterialBooleanPropertyAccess().getValueYesKeyword_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MaterialBooleanProperty__ValueAssignment_2_0"


    // $ANTLR start "rule__DeclarationStatement__TypeAssignment_0"
    // InternalGeometryCodes.g:6495:1: rule__DeclarationStatement__TypeAssignment_0 : ( ruleType ) ;
    public final void rule__DeclarationStatement__TypeAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6499:1: ( ( ruleType ) )
            // InternalGeometryCodes.g:6500:2: ( ruleType )
            {
            // InternalGeometryCodes.g:6500:2: ( ruleType )
            // InternalGeometryCodes.g:6501:3: ruleType
            {
             before(grammarAccess.getDeclarationStatementAccess().getTypeTypeEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getDeclarationStatementAccess().getTypeTypeEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__TypeAssignment_0"


    // $ANTLR start "rule__DeclarationStatement__NameAssignment_1"
    // InternalGeometryCodes.g:6510:1: rule__DeclarationStatement__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__DeclarationStatement__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6514:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6515:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6515:2: ( RULE_ID )
            // InternalGeometryCodes.g:6516:3: RULE_ID
            {
             before(grammarAccess.getDeclarationStatementAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getDeclarationStatementAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__NameAssignment_1"


    // $ANTLR start "rule__DeclarationStatement__ValueAssignment_3"
    // InternalGeometryCodes.g:6525:1: rule__DeclarationStatement__ValueAssignment_3 : ( ruleExpression ) ;
    public final void rule__DeclarationStatement__ValueAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6529:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6530:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6530:2: ( ruleExpression )
            // InternalGeometryCodes.g:6531:3: ruleExpression
            {
             before(grammarAccess.getDeclarationStatementAccess().getValueExpressionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getDeclarationStatementAccess().getValueExpressionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__DeclarationStatement__ValueAssignment_3"


    // $ANTLR start "rule__AssignmentStatement__TargetAssignment_0"
    // InternalGeometryCodes.g:6540:1: rule__AssignmentStatement__TargetAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__AssignmentStatement__TargetAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6544:1: ( ( ( RULE_ID ) ) )
            // InternalGeometryCodes.g:6545:2: ( ( RULE_ID ) )
            {
            // InternalGeometryCodes.g:6545:2: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6546:3: ( RULE_ID )
            {
             before(grammarAccess.getAssignmentStatementAccess().getTargetReferenceableElementCrossReference_0_0()); 
            // InternalGeometryCodes.g:6547:3: ( RULE_ID )
            // InternalGeometryCodes.g:6548:4: RULE_ID
            {
             before(grammarAccess.getAssignmentStatementAccess().getTargetReferenceableElementIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getAssignmentStatementAccess().getTargetReferenceableElementIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getAssignmentStatementAccess().getTargetReferenceableElementCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__TargetAssignment_0"


    // $ANTLR start "rule__AssignmentStatement__OperatorAssignment_1"
    // InternalGeometryCodes.g:6559:1: rule__AssignmentStatement__OperatorAssignment_1 : ( ruleAssignmentOperator ) ;
    public final void rule__AssignmentStatement__OperatorAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6563:1: ( ( ruleAssignmentOperator ) )
            // InternalGeometryCodes.g:6564:2: ( ruleAssignmentOperator )
            {
            // InternalGeometryCodes.g:6564:2: ( ruleAssignmentOperator )
            // InternalGeometryCodes.g:6565:3: ruleAssignmentOperator
            {
             before(grammarAccess.getAssignmentStatementAccess().getOperatorAssignmentOperatorEnumRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAssignmentOperator();

            state._fsp--;

             after(grammarAccess.getAssignmentStatementAccess().getOperatorAssignmentOperatorEnumRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__OperatorAssignment_1"


    // $ANTLR start "rule__AssignmentStatement__ValueAssignment_2"
    // InternalGeometryCodes.g:6574:1: rule__AssignmentStatement__ValueAssignment_2 : ( ruleExpression ) ;
    public final void rule__AssignmentStatement__ValueAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6578:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6579:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6579:2: ( ruleExpression )
            // InternalGeometryCodes.g:6580:3: ruleExpression
            {
             before(grammarAccess.getAssignmentStatementAccess().getValueExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getAssignmentStatementAccess().getValueExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AssignmentStatement__ValueAssignment_2"


    // $ANTLR start "rule__ReferenceStatement__ReferenceAssignment"
    // InternalGeometryCodes.g:6589:1: rule__ReferenceStatement__ReferenceAssignment : ( ruleReferenceExpression ) ;
    public final void rule__ReferenceStatement__ReferenceAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6593:1: ( ( ruleReferenceExpression ) )
            // InternalGeometryCodes.g:6594:2: ( ruleReferenceExpression )
            {
            // InternalGeometryCodes.g:6594:2: ( ruleReferenceExpression )
            // InternalGeometryCodes.g:6595:3: ruleReferenceExpression
            {
             before(grammarAccess.getReferenceStatementAccess().getReferenceReferenceExpressionParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleReferenceExpression();

            state._fsp--;

             after(grammarAccess.getReferenceStatementAccess().getReferenceReferenceExpressionParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceStatement__ReferenceAssignment"


    // $ANTLR start "rule__ReturnStatement__ValueAssignment_2_0"
    // InternalGeometryCodes.g:6604:1: rule__ReturnStatement__ValueAssignment_2_0 : ( ruleExpression ) ;
    public final void rule__ReturnStatement__ValueAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6608:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6609:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6609:2: ( ruleExpression )
            // InternalGeometryCodes.g:6610:3: ruleExpression
            {
             before(grammarAccess.getReturnStatementAccess().getValueExpressionParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getReturnStatementAccess().getValueExpressionParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReturnStatement__ValueAssignment_2_0"


    // $ANTLR start "rule__IfStatement__ConditionAssignment_2"
    // InternalGeometryCodes.g:6619:1: rule__IfStatement__ConditionAssignment_2 : ( ruleExpression ) ;
    public final void rule__IfStatement__ConditionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6623:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6624:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6624:2: ( ruleExpression )
            // InternalGeometryCodes.g:6625:3: ruleExpression
            {
             before(grammarAccess.getIfStatementAccess().getConditionExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getIfStatementAccess().getConditionExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__ConditionAssignment_2"


    // $ANTLR start "rule__IfStatement__StatementsAssignment_5"
    // InternalGeometryCodes.g:6634:1: rule__IfStatement__StatementsAssignment_5 : ( ruleStatement ) ;
    public final void rule__IfStatement__StatementsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6638:1: ( ( ruleStatement ) )
            // InternalGeometryCodes.g:6639:2: ( ruleStatement )
            {
            // InternalGeometryCodes.g:6639:2: ( ruleStatement )
            // InternalGeometryCodes.g:6640:3: ruleStatement
            {
             before(grammarAccess.getIfStatementAccess().getStatementsStatementParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getIfStatementAccess().getStatementsStatementParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__IfStatement__StatementsAssignment_5"


    // $ANTLR start "rule__ForLoopStatement__TypeAssignment_2"
    // InternalGeometryCodes.g:6649:1: rule__ForLoopStatement__TypeAssignment_2 : ( ruleType ) ;
    public final void rule__ForLoopStatement__TypeAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6653:1: ( ( ruleType ) )
            // InternalGeometryCodes.g:6654:2: ( ruleType )
            {
            // InternalGeometryCodes.g:6654:2: ( ruleType )
            // InternalGeometryCodes.g:6655:3: ruleType
            {
             before(grammarAccess.getForLoopStatementAccess().getTypeTypeEnumRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getForLoopStatementAccess().getTypeTypeEnumRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__TypeAssignment_2"


    // $ANTLR start "rule__ForLoopStatement__NameAssignment_3"
    // InternalGeometryCodes.g:6664:1: rule__ForLoopStatement__NameAssignment_3 : ( RULE_ID ) ;
    public final void rule__ForLoopStatement__NameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6668:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6669:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6669:2: ( RULE_ID )
            // InternalGeometryCodes.g:6670:3: RULE_ID
            {
             before(grammarAccess.getForLoopStatementAccess().getNameIDTerminalRuleCall_3_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getForLoopStatementAccess().getNameIDTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__NameAssignment_3"


    // $ANTLR start "rule__ForLoopStatement__FromValueAssignment_5"
    // InternalGeometryCodes.g:6679:1: rule__ForLoopStatement__FromValueAssignment_5 : ( ruleExpression ) ;
    public final void rule__ForLoopStatement__FromValueAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6683:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6684:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6684:2: ( ruleExpression )
            // InternalGeometryCodes.g:6685:3: ruleExpression
            {
             before(grammarAccess.getForLoopStatementAccess().getFromValueExpressionParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getForLoopStatementAccess().getFromValueExpressionParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__FromValueAssignment_5"


    // $ANTLR start "rule__ForLoopStatement__ToValueAssignment_7"
    // InternalGeometryCodes.g:6694:1: rule__ForLoopStatement__ToValueAssignment_7 : ( ruleExpression ) ;
    public final void rule__ForLoopStatement__ToValueAssignment_7() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6698:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6699:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6699:2: ( ruleExpression )
            // InternalGeometryCodes.g:6700:3: ruleExpression
            {
             before(grammarAccess.getForLoopStatementAccess().getToValueExpressionParserRuleCall_7_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getForLoopStatementAccess().getToValueExpressionParserRuleCall_7_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__ToValueAssignment_7"


    // $ANTLR start "rule__ForLoopStatement__StatementsAssignment_10"
    // InternalGeometryCodes.g:6709:1: rule__ForLoopStatement__StatementsAssignment_10 : ( ruleStatement ) ;
    public final void rule__ForLoopStatement__StatementsAssignment_10() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6713:1: ( ( ruleStatement ) )
            // InternalGeometryCodes.g:6714:2: ( ruleStatement )
            {
            // InternalGeometryCodes.g:6714:2: ( ruleStatement )
            // InternalGeometryCodes.g:6715:3: ruleStatement
            {
             before(grammarAccess.getForLoopStatementAccess().getStatementsStatementParserRuleCall_10_0()); 
            pushFollow(FOLLOW_2);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getForLoopStatementAccess().getStatementsStatementParserRuleCall_10_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForLoopStatement__StatementsAssignment_10"


    // $ANTLR start "rule__ForEachLoopStatement__TypeAssignment_3"
    // InternalGeometryCodes.g:6724:1: rule__ForEachLoopStatement__TypeAssignment_3 : ( ruleType ) ;
    public final void rule__ForEachLoopStatement__TypeAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6728:1: ( ( ruleType ) )
            // InternalGeometryCodes.g:6729:2: ( ruleType )
            {
            // InternalGeometryCodes.g:6729:2: ( ruleType )
            // InternalGeometryCodes.g:6730:3: ruleType
            {
             before(grammarAccess.getForEachLoopStatementAccess().getTypeTypeEnumRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleType();

            state._fsp--;

             after(grammarAccess.getForEachLoopStatementAccess().getTypeTypeEnumRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__TypeAssignment_3"


    // $ANTLR start "rule__ForEachLoopStatement__NameAssignment_4"
    // InternalGeometryCodes.g:6739:1: rule__ForEachLoopStatement__NameAssignment_4 : ( RULE_ID ) ;
    public final void rule__ForEachLoopStatement__NameAssignment_4() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6743:1: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:6744:2: ( RULE_ID )
            {
            // InternalGeometryCodes.g:6744:2: ( RULE_ID )
            // InternalGeometryCodes.g:6745:3: RULE_ID
            {
             before(grammarAccess.getForEachLoopStatementAccess().getNameIDTerminalRuleCall_4_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getForEachLoopStatementAccess().getNameIDTerminalRuleCall_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__NameAssignment_4"


    // $ANTLR start "rule__ForEachLoopStatement__ValueAssignment_6"
    // InternalGeometryCodes.g:6754:1: rule__ForEachLoopStatement__ValueAssignment_6 : ( ruleExpression ) ;
    public final void rule__ForEachLoopStatement__ValueAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6758:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6759:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6759:2: ( ruleExpression )
            // InternalGeometryCodes.g:6760:3: ruleExpression
            {
             before(grammarAccess.getForEachLoopStatementAccess().getValueExpressionParserRuleCall_6_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getForEachLoopStatementAccess().getValueExpressionParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__ValueAssignment_6"


    // $ANTLR start "rule__ForEachLoopStatement__StatementsAssignment_9"
    // InternalGeometryCodes.g:6769:1: rule__ForEachLoopStatement__StatementsAssignment_9 : ( ruleStatement ) ;
    public final void rule__ForEachLoopStatement__StatementsAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6773:1: ( ( ruleStatement ) )
            // InternalGeometryCodes.g:6774:2: ( ruleStatement )
            {
            // InternalGeometryCodes.g:6774:2: ( ruleStatement )
            // InternalGeometryCodes.g:6775:3: ruleStatement
            {
             before(grammarAccess.getForEachLoopStatementAccess().getStatementsStatementParserRuleCall_9_0()); 
            pushFollow(FOLLOW_2);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getForEachLoopStatementAccess().getStatementsStatementParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ForEachLoopStatement__StatementsAssignment_9"


    // $ANTLR start "rule__WhileLoopStatement__ConditionAssignment_2"
    // InternalGeometryCodes.g:6784:1: rule__WhileLoopStatement__ConditionAssignment_2 : ( ruleExpression ) ;
    public final void rule__WhileLoopStatement__ConditionAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6788:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6789:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6789:2: ( ruleExpression )
            // InternalGeometryCodes.g:6790:3: ruleExpression
            {
             before(grammarAccess.getWhileLoopStatementAccess().getConditionExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getWhileLoopStatementAccess().getConditionExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__ConditionAssignment_2"


    // $ANTLR start "rule__WhileLoopStatement__StatementsAssignment_5"
    // InternalGeometryCodes.g:6799:1: rule__WhileLoopStatement__StatementsAssignment_5 : ( ruleStatement ) ;
    public final void rule__WhileLoopStatement__StatementsAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6803:1: ( ( ruleStatement ) )
            // InternalGeometryCodes.g:6804:2: ( ruleStatement )
            {
            // InternalGeometryCodes.g:6804:2: ( ruleStatement )
            // InternalGeometryCodes.g:6805:3: ruleStatement
            {
             before(grammarAccess.getWhileLoopStatementAccess().getStatementsStatementParserRuleCall_5_0()); 
            pushFollow(FOLLOW_2);
            ruleStatement();

            state._fsp--;

             after(grammarAccess.getWhileLoopStatementAccess().getStatementsStatementParserRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__WhileLoopStatement__StatementsAssignment_5"


    // $ANTLR start "rule__OutputStatement__ValueAssignment_1"
    // InternalGeometryCodes.g:6814:1: rule__OutputStatement__ValueAssignment_1 : ( ruleExpression ) ;
    public final void rule__OutputStatement__ValueAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6818:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:6819:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:6819:2: ( ruleExpression )
            // InternalGeometryCodes.g:6820:3: ruleExpression
            {
             before(grammarAccess.getOutputStatementAccess().getValueExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getOutputStatementAccess().getValueExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OutputStatement__ValueAssignment_1"


    // $ANTLR start "rule__LogicalExpression__OperatorAssignment_1_1"
    // InternalGeometryCodes.g:6829:1: rule__LogicalExpression__OperatorAssignment_1_1 : ( ruleLogicalOperator ) ;
    public final void rule__LogicalExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6833:1: ( ( ruleLogicalOperator ) )
            // InternalGeometryCodes.g:6834:2: ( ruleLogicalOperator )
            {
            // InternalGeometryCodes.g:6834:2: ( ruleLogicalOperator )
            // InternalGeometryCodes.g:6835:3: ruleLogicalOperator
            {
             before(grammarAccess.getLogicalExpressionAccess().getOperatorLogicalOperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLogicalOperator();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionAccess().getOperatorLogicalOperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__LogicalExpression__RightChildAssignment_1_2"
    // InternalGeometryCodes.g:6844:1: rule__LogicalExpression__RightChildAssignment_1_2 : ( ruleConditionalExpression ) ;
    public final void rule__LogicalExpression__RightChildAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6848:1: ( ( ruleConditionalExpression ) )
            // InternalGeometryCodes.g:6849:2: ( ruleConditionalExpression )
            {
            // InternalGeometryCodes.g:6849:2: ( ruleConditionalExpression )
            // InternalGeometryCodes.g:6850:3: ruleConditionalExpression
            {
             before(grammarAccess.getLogicalExpressionAccess().getRightChildConditionalExpressionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleConditionalExpression();

            state._fsp--;

             after(grammarAccess.getLogicalExpressionAccess().getRightChildConditionalExpressionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__LogicalExpression__RightChildAssignment_1_2"


    // $ANTLR start "rule__ConditionalExpression__OperatorAssignment_1_1"
    // InternalGeometryCodes.g:6859:1: rule__ConditionalExpression__OperatorAssignment_1_1 : ( ruleComparisonOperator ) ;
    public final void rule__ConditionalExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6863:1: ( ( ruleComparisonOperator ) )
            // InternalGeometryCodes.g:6864:2: ( ruleComparisonOperator )
            {
            // InternalGeometryCodes.g:6864:2: ( ruleComparisonOperator )
            // InternalGeometryCodes.g:6865:3: ruleComparisonOperator
            {
             before(grammarAccess.getConditionalExpressionAccess().getOperatorComparisonOperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleComparisonOperator();

            state._fsp--;

             after(grammarAccess.getConditionalExpressionAccess().getOperatorComparisonOperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__ConditionalExpression__RightChildAssignment_1_2"
    // InternalGeometryCodes.g:6874:1: rule__ConditionalExpression__RightChildAssignment_1_2 : ( ruleAdditiveExpression ) ;
    public final void rule__ConditionalExpression__RightChildAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6878:1: ( ( ruleAdditiveExpression ) )
            // InternalGeometryCodes.g:6879:2: ( ruleAdditiveExpression )
            {
            // InternalGeometryCodes.g:6879:2: ( ruleAdditiveExpression )
            // InternalGeometryCodes.g:6880:3: ruleAdditiveExpression
            {
             before(grammarAccess.getConditionalExpressionAccess().getRightChildAdditiveExpressionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleAdditiveExpression();

            state._fsp--;

             after(grammarAccess.getConditionalExpressionAccess().getRightChildAdditiveExpressionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ConditionalExpression__RightChildAssignment_1_2"


    // $ANTLR start "rule__AdditiveExpression__OperatorAssignment_1_1"
    // InternalGeometryCodes.g:6889:1: rule__AdditiveExpression__OperatorAssignment_1_1 : ( ruleAdditiveOperator ) ;
    public final void rule__AdditiveExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6893:1: ( ( ruleAdditiveOperator ) )
            // InternalGeometryCodes.g:6894:2: ( ruleAdditiveOperator )
            {
            // InternalGeometryCodes.g:6894:2: ( ruleAdditiveOperator )
            // InternalGeometryCodes.g:6895:3: ruleAdditiveOperator
            {
             before(grammarAccess.getAdditiveExpressionAccess().getOperatorAdditiveOperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleAdditiveOperator();

            state._fsp--;

             after(grammarAccess.getAdditiveExpressionAccess().getOperatorAdditiveOperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__AdditiveExpression__RightChildAssignment_1_2"
    // InternalGeometryCodes.g:6904:1: rule__AdditiveExpression__RightChildAssignment_1_2 : ( ruleMultiplicativeExpression ) ;
    public final void rule__AdditiveExpression__RightChildAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6908:1: ( ( ruleMultiplicativeExpression ) )
            // InternalGeometryCodes.g:6909:2: ( ruleMultiplicativeExpression )
            {
            // InternalGeometryCodes.g:6909:2: ( ruleMultiplicativeExpression )
            // InternalGeometryCodes.g:6910:3: ruleMultiplicativeExpression
            {
             before(grammarAccess.getAdditiveExpressionAccess().getRightChildMultiplicativeExpressionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleMultiplicativeExpression();

            state._fsp--;

             after(grammarAccess.getAdditiveExpressionAccess().getRightChildMultiplicativeExpressionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__AdditiveExpression__RightChildAssignment_1_2"


    // $ANTLR start "rule__MultiplicativeExpression__OperatorAssignment_1_1"
    // InternalGeometryCodes.g:6919:1: rule__MultiplicativeExpression__OperatorAssignment_1_1 : ( ruleMultiplicativeOperator ) ;
    public final void rule__MultiplicativeExpression__OperatorAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6923:1: ( ( ruleMultiplicativeOperator ) )
            // InternalGeometryCodes.g:6924:2: ( ruleMultiplicativeOperator )
            {
            // InternalGeometryCodes.g:6924:2: ( ruleMultiplicativeOperator )
            // InternalGeometryCodes.g:6925:3: ruleMultiplicativeOperator
            {
             before(grammarAccess.getMultiplicativeExpressionAccess().getOperatorMultiplicativeOperatorEnumRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleMultiplicativeOperator();

            state._fsp--;

             after(grammarAccess.getMultiplicativeExpressionAccess().getOperatorMultiplicativeOperatorEnumRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__OperatorAssignment_1_1"


    // $ANTLR start "rule__MultiplicativeExpression__RightChildAssignment_1_2"
    // InternalGeometryCodes.g:6934:1: rule__MultiplicativeExpression__RightChildAssignment_1_2 : ( ruleLiteralExpression ) ;
    public final void rule__MultiplicativeExpression__RightChildAssignment_1_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6938:1: ( ( ruleLiteralExpression ) )
            // InternalGeometryCodes.g:6939:2: ( ruleLiteralExpression )
            {
            // InternalGeometryCodes.g:6939:2: ( ruleLiteralExpression )
            // InternalGeometryCodes.g:6940:3: ruleLiteralExpression
            {
             before(grammarAccess.getMultiplicativeExpressionAccess().getRightChildLiteralExpressionParserRuleCall_1_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLiteralExpression();

            state._fsp--;

             after(grammarAccess.getMultiplicativeExpressionAccess().getRightChildLiteralExpressionParserRuleCall_1_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__MultiplicativeExpression__RightChildAssignment_1_2"


    // $ANTLR start "rule__UnaryExpression__OperatorAssignment_0"
    // InternalGeometryCodes.g:6949:1: rule__UnaryExpression__OperatorAssignment_0 : ( ruleUnaryOperator ) ;
    public final void rule__UnaryExpression__OperatorAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6953:1: ( ( ruleUnaryOperator ) )
            // InternalGeometryCodes.g:6954:2: ( ruleUnaryOperator )
            {
            // InternalGeometryCodes.g:6954:2: ( ruleUnaryOperator )
            // InternalGeometryCodes.g:6955:3: ruleUnaryOperator
            {
             before(grammarAccess.getUnaryExpressionAccess().getOperatorUnaryOperatorEnumRuleCall_0_0()); 
            pushFollow(FOLLOW_2);
            ruleUnaryOperator();

            state._fsp--;

             after(grammarAccess.getUnaryExpressionAccess().getOperatorUnaryOperatorEnumRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryExpression__OperatorAssignment_0"


    // $ANTLR start "rule__UnaryExpression__ChildAssignment_1"
    // InternalGeometryCodes.g:6964:1: rule__UnaryExpression__ChildAssignment_1 : ( ruleLiteralExpression ) ;
    public final void rule__UnaryExpression__ChildAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6968:1: ( ( ruleLiteralExpression ) )
            // InternalGeometryCodes.g:6969:2: ( ruleLiteralExpression )
            {
            // InternalGeometryCodes.g:6969:2: ( ruleLiteralExpression )
            // InternalGeometryCodes.g:6970:3: ruleLiteralExpression
            {
             before(grammarAccess.getUnaryExpressionAccess().getChildLiteralExpressionParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLiteralExpression();

            state._fsp--;

             after(grammarAccess.getUnaryExpressionAccess().getChildLiteralExpressionParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UnaryExpression__ChildAssignment_1"


    // $ANTLR start "rule__ReferenceExpression__NextAssignment_1_1"
    // InternalGeometryCodes.g:6979:1: rule__ReferenceExpression__NextAssignment_1_1 : ( ruleReferenceExpression ) ;
    public final void rule__ReferenceExpression__NextAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6983:1: ( ( ruleReferenceExpression ) )
            // InternalGeometryCodes.g:6984:2: ( ruleReferenceExpression )
            {
            // InternalGeometryCodes.g:6984:2: ( ruleReferenceExpression )
            // InternalGeometryCodes.g:6985:3: ruleReferenceExpression
            {
             before(grammarAccess.getReferenceExpressionAccess().getNextReferenceExpressionParserRuleCall_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleReferenceExpression();

            state._fsp--;

             after(grammarAccess.getReferenceExpressionAccess().getNextReferenceExpressionParserRuleCall_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferenceExpression__NextAssignment_1_1"


    // $ANTLR start "rule__VariableReferenceExpression__TargetAssignment"
    // InternalGeometryCodes.g:6994:1: rule__VariableReferenceExpression__TargetAssignment : ( ( RULE_ID ) ) ;
    public final void rule__VariableReferenceExpression__TargetAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:6998:1: ( ( ( RULE_ID ) ) )
            // InternalGeometryCodes.g:6999:2: ( ( RULE_ID ) )
            {
            // InternalGeometryCodes.g:6999:2: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:7000:3: ( RULE_ID )
            {
             before(grammarAccess.getVariableReferenceExpressionAccess().getTargetReferenceableElementCrossReference_0()); 
            // InternalGeometryCodes.g:7001:3: ( RULE_ID )
            // InternalGeometryCodes.g:7002:4: RULE_ID
            {
             before(grammarAccess.getVariableReferenceExpressionAccess().getTargetReferenceableElementIDTerminalRuleCall_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getVariableReferenceExpressionAccess().getTargetReferenceableElementIDTerminalRuleCall_0_1()); 

            }

             after(grammarAccess.getVariableReferenceExpressionAccess().getTargetReferenceableElementCrossReference_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VariableReferenceExpression__TargetAssignment"


    // $ANTLR start "rule__FunctionCallExpression__TargetAssignment_0"
    // InternalGeometryCodes.g:7013:1: rule__FunctionCallExpression__TargetAssignment_0 : ( ( RULE_ID ) ) ;
    public final void rule__FunctionCallExpression__TargetAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7017:1: ( ( ( RULE_ID ) ) )
            // InternalGeometryCodes.g:7018:2: ( ( RULE_ID ) )
            {
            // InternalGeometryCodes.g:7018:2: ( ( RULE_ID ) )
            // InternalGeometryCodes.g:7019:3: ( RULE_ID )
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getTargetReferenceableElementCrossReference_0_0()); 
            // InternalGeometryCodes.g:7020:3: ( RULE_ID )
            // InternalGeometryCodes.g:7021:4: RULE_ID
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getTargetReferenceableElementIDTerminalRuleCall_0_0_1()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFunctionCallExpressionAccess().getTargetReferenceableElementIDTerminalRuleCall_0_0_1()); 

            }

             after(grammarAccess.getFunctionCallExpressionAccess().getTargetReferenceableElementCrossReference_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__TargetAssignment_0"


    // $ANTLR start "rule__FunctionCallExpression__ArgumentsAssignment_2_0"
    // InternalGeometryCodes.g:7032:1: rule__FunctionCallExpression__ArgumentsAssignment_2_0 : ( ruleExpression ) ;
    public final void rule__FunctionCallExpression__ArgumentsAssignment_2_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7036:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7037:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7037:2: ( ruleExpression )
            // InternalGeometryCodes.g:7038:3: ruleExpression
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getArgumentsExpressionParserRuleCall_2_0_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getFunctionCallExpressionAccess().getArgumentsExpressionParserRuleCall_2_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__ArgumentsAssignment_2_0"


    // $ANTLR start "rule__FunctionCallExpression__ArgumentsAssignment_2_1_1"
    // InternalGeometryCodes.g:7047:1: rule__FunctionCallExpression__ArgumentsAssignment_2_1_1 : ( ruleExpression ) ;
    public final void rule__FunctionCallExpression__ArgumentsAssignment_2_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7051:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7052:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7052:2: ( ruleExpression )
            // InternalGeometryCodes.g:7053:3: ruleExpression
            {
             before(grammarAccess.getFunctionCallExpressionAccess().getArgumentsExpressionParserRuleCall_2_1_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getFunctionCallExpressionAccess().getArgumentsExpressionParserRuleCall_2_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FunctionCallExpression__ArgumentsAssignment_2_1_1"


    // $ANTLR start "rule__VectorDeclarationExpression__ArgumentsAssignment_2"
    // InternalGeometryCodes.g:7062:1: rule__VectorDeclarationExpression__ArgumentsAssignment_2 : ( ruleExpression ) ;
    public final void rule__VectorDeclarationExpression__ArgumentsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7066:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7067:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7067:2: ( ruleExpression )
            // InternalGeometryCodes.g:7068:3: ruleExpression
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__ArgumentsAssignment_2"


    // $ANTLR start "rule__VectorDeclarationExpression__ArgumentsAssignment_3_1"
    // InternalGeometryCodes.g:7077:1: rule__VectorDeclarationExpression__ArgumentsAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__VectorDeclarationExpression__ArgumentsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7081:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7082:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7082:2: ( ruleExpression )
            // InternalGeometryCodes.g:7083:3: ruleExpression
            {
             before(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VectorDeclarationExpression__ArgumentsAssignment_3_1"


    // $ANTLR start "rule__VertexDeclarationExpression__ArgumentsAssignment_2"
    // InternalGeometryCodes.g:7092:1: rule__VertexDeclarationExpression__ArgumentsAssignment_2 : ( ruleExpression ) ;
    public final void rule__VertexDeclarationExpression__ArgumentsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7096:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7097:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7097:2: ( ruleExpression )
            // InternalGeometryCodes.g:7098:3: ruleExpression
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__ArgumentsAssignment_2"


    // $ANTLR start "rule__VertexDeclarationExpression__ArgumentsAssignment_3_1"
    // InternalGeometryCodes.g:7107:1: rule__VertexDeclarationExpression__ArgumentsAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__VertexDeclarationExpression__ArgumentsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7111:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7112:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7112:2: ( ruleExpression )
            // InternalGeometryCodes.g:7113:3: ruleExpression
            {
             before(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VertexDeclarationExpression__ArgumentsAssignment_3_1"


    // $ANTLR start "rule__TriangleDeclarationExpression__ArgumentsAssignment_2"
    // InternalGeometryCodes.g:7122:1: rule__TriangleDeclarationExpression__ArgumentsAssignment_2 : ( ruleExpression ) ;
    public final void rule__TriangleDeclarationExpression__ArgumentsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7126:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7127:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7127:2: ( ruleExpression )
            // InternalGeometryCodes.g:7128:3: ruleExpression
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__ArgumentsAssignment_2"


    // $ANTLR start "rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1"
    // InternalGeometryCodes.g:7137:1: rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7141:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7142:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7142:2: ( ruleExpression )
            // InternalGeometryCodes.g:7143:3: ruleExpression
            {
             before(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TriangleDeclarationExpression__ArgumentsAssignment_3_1"


    // $ANTLR start "rule__QuadDeclarationExpression__ArgumentsAssignment_2"
    // InternalGeometryCodes.g:7152:1: rule__QuadDeclarationExpression__ArgumentsAssignment_2 : ( ruleExpression ) ;
    public final void rule__QuadDeclarationExpression__ArgumentsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7156:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7157:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7157:2: ( ruleExpression )
            // InternalGeometryCodes.g:7158:3: ruleExpression
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__ArgumentsAssignment_2"


    // $ANTLR start "rule__QuadDeclarationExpression__ArgumentsAssignment_3_1"
    // InternalGeometryCodes.g:7167:1: rule__QuadDeclarationExpression__ArgumentsAssignment_3_1 : ( ruleExpression ) ;
    public final void rule__QuadDeclarationExpression__ArgumentsAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7171:1: ( ( ruleExpression ) )
            // InternalGeometryCodes.g:7172:2: ( ruleExpression )
            {
            // InternalGeometryCodes.g:7172:2: ( ruleExpression )
            // InternalGeometryCodes.g:7173:3: ruleExpression
            {
             before(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleExpression();

            state._fsp--;

             after(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__QuadDeclarationExpression__ArgumentsAssignment_3_1"


    // $ANTLR start "rule__FloatLiteral__ValueAssignment"
    // InternalGeometryCodes.g:7182:1: rule__FloatLiteral__ValueAssignment : ( RULE_FLOAT ) ;
    public final void rule__FloatLiteral__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7186:1: ( ( RULE_FLOAT ) )
            // InternalGeometryCodes.g:7187:2: ( RULE_FLOAT )
            {
            // InternalGeometryCodes.g:7187:2: ( RULE_FLOAT )
            // InternalGeometryCodes.g:7188:3: RULE_FLOAT
            {
             before(grammarAccess.getFloatLiteralAccess().getValueFLOATTerminalRuleCall_0()); 
            match(input,RULE_FLOAT,FOLLOW_2); 
             after(grammarAccess.getFloatLiteralAccess().getValueFLOATTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FloatLiteral__ValueAssignment"


    // $ANTLR start "rule__StringLiteral__ValueAssignment"
    // InternalGeometryCodes.g:7197:1: rule__StringLiteral__ValueAssignment : ( RULE_STRING ) ;
    public final void rule__StringLiteral__ValueAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7201:1: ( ( RULE_STRING ) )
            // InternalGeometryCodes.g:7202:2: ( RULE_STRING )
            {
            // InternalGeometryCodes.g:7202:2: ( RULE_STRING )
            // InternalGeometryCodes.g:7203:3: RULE_STRING
            {
             before(grammarAccess.getStringLiteralAccess().getValueSTRINGTerminalRuleCall_0()); 
            match(input,RULE_STRING,FOLLOW_2); 
             after(grammarAccess.getStringLiteralAccess().getValueSTRINGTerminalRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__StringLiteral__ValueAssignment"


    // $ANTLR start "rule__BooleanLiteral__ValueAssignment_0"
    // InternalGeometryCodes.g:7212:1: rule__BooleanLiteral__ValueAssignment_0 : ( ( 'true' ) ) ;
    public final void rule__BooleanLiteral__ValueAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalGeometryCodes.g:7216:1: ( ( ( 'true' ) ) )
            // InternalGeometryCodes.g:7217:2: ( ( 'true' ) )
            {
            // InternalGeometryCodes.g:7217:2: ( ( 'true' ) )
            // InternalGeometryCodes.g:7218:3: ( 'true' )
            {
             before(grammarAccess.getBooleanLiteralAccess().getValueTrueKeyword_0_0()); 
            // InternalGeometryCodes.g:7219:3: ( 'true' )
            // InternalGeometryCodes.g:7220:4: 'true'
            {
             before(grammarAccess.getBooleanLiteralAccess().getValueTrueKeyword_0_0()); 
            match(input,59,FOLLOW_2); 
             after(grammarAccess.getBooleanLiteralAccess().getValueTrueKeyword_0_0()); 

            }

             after(grammarAccess.getBooleanLiteralAccess().getValueTrueKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__BooleanLiteral__ValueAssignment_0"

    // Delegated rules


    protected DFA1 dfa1 = new DFA1(this);
    protected DFA4 dfa4 = new DFA4(this);
    static final String dfa_1s = "\14\uffff";
    static final String dfa_2s = "\7\5\3\uffff\1\16\1\uffff";
    static final String dfa_3s = "\1\65\6\5\3\uffff\1\44\1\uffff";
    static final String dfa_4s = "\7\uffff\1\2\1\3\1\4\1\uffff\1\1";
    static final String dfa_5s = "\14\uffff}>";
    static final String[] dfa_6s = {
            "\1\11\27\uffff\1\1\1\2\1\3\1\4\1\5\1\6\6\uffff\1\7\2\uffff\1\10\1\uffff\3\11\3\uffff\2\11",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "",
            "",
            "",
            "\1\11\25\uffff\1\13",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA1 extends DFA {

        public DFA1(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 1;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "1139:1: rule__File__Alternatives_1 : ( ( ( rule__File__FunctionsAssignment_1_0 ) ) | ( ( rule__File__IteratorsAssignment_1_1 ) ) | ( ( rule__File__MaterialsAssignment_1_2 ) ) | ( ( rule__File__StatementsAssignment_1_3 ) ) );";
        }
    }
    static final String dfa_7s = "\2\uffff\1\11\11\uffff";
    static final String dfa_8s = "\1\5\1\uffff\1\5\2\uffff\1\44\6\uffff";
    static final String dfa_9s = "\1\65\1\uffff\1\66\2\uffff\1\53\6\uffff";
    static final String dfa_10s = "\1\uffff\1\1\1\uffff\1\4\1\5\1\uffff\1\10\1\11\1\2\1\3\1\7\1\6";
    static final String[] dfa_11s = {
            "\1\2\27\uffff\6\1\13\uffff\1\3\1\4\1\5\3\uffff\1\6\1\7",
            "",
            "\1\11\10\uffff\2\10\15\uffff\6\11\1\uffff\1\11\2\uffff\1\11\1\uffff\1\11\2\uffff\1\11\1\uffff\3\11\3\uffff\3\11",
            "",
            "",
            "\1\13\6\uffff\1\12",
            "",
            "",
            "",
            "",
            "",
            ""
    };
    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final short[] dfa_10 = DFA.unpackEncodedString(dfa_10s);
    static final short[][] dfa_11 = unpackEncodedStringArray(dfa_11s);

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = dfa_1;
            this.eof = dfa_7;
            this.min = dfa_8;
            this.max = dfa_9;
            this.accept = dfa_10;
            this.special = dfa_5;
            this.transition = dfa_11;
        }
        public String getDescription() {
            return "1220:1: rule__Statement__Alternatives : ( ( ruleDeclarationStatement ) | ( ruleAssignmentStatement ) | ( ruleReferenceStatement ) | ( ruleReturnStatement ) | ( ruleIfStatement ) | ( ruleForLoopStatement ) | ( ruleForEachLoopStatement ) | ( ruleWhileLoopStatement ) | ( ruleOutputStatement ) );";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0031D207E0000020L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0031D207E0000022L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000001000000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x00000027E0000000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0031D287E0000020L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000010000000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000010000000002L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x00000007E0000000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000040000000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000008000000020L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000000022L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000200000000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0400000000001000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0B80001380030070L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0B80001380032070L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000002000000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0002000000000000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0004000000000000L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0008000000000000L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000018000000L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000000018000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000007E00000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000007E00002L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000000060000L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000000060002L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000000000180000L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000000000180002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0040000000000000L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0B80003380030070L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000012000000000L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0B00000000000050L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0A00000000000000L});

}