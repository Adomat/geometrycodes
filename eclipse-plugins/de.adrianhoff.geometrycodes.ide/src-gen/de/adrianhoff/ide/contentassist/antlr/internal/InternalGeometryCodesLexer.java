package de.adrianhoff.ide.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGeometryCodesLexer extends Lexer {
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int RULE_FLOAT=6;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators

    public InternalGeometryCodesLexer() {;} 
    public InternalGeometryCodesLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalGeometryCodesLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalGeometryCodes.g"; }

    // $ANTLR start "T__12"
    public final void mT__12() throws RecognitionException {
        try {
            int _type = T__12;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:11:7: ( 'no' )
            // InternalGeometryCodes.g:11:9: 'no'
            {
            match("no"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__12"

    // $ANTLR start "T__13"
    public final void mT__13() throws RecognitionException {
        try {
            int _type = T__13;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:12:7: ( 'nothing' )
            // InternalGeometryCodes.g:12:9: 'nothing'
            {
            match("nothing"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__13"

    // $ANTLR start "T__14"
    public final void mT__14() throws RecognitionException {
        try {
            int _type = T__14;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:13:7: ( '=' )
            // InternalGeometryCodes.g:13:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__14"

    // $ANTLR start "T__15"
    public final void mT__15() throws RecognitionException {
        try {
            int _type = T__15;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:14:7: ( '+=' )
            // InternalGeometryCodes.g:14:9: '+='
            {
            match("+="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__15"

    // $ANTLR start "T__16"
    public final void mT__16() throws RecognitionException {
        try {
            int _type = T__16;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:15:7: ( '!' )
            // InternalGeometryCodes.g:15:9: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__16"

    // $ANTLR start "T__17"
    public final void mT__17() throws RecognitionException {
        try {
            int _type = T__17;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:16:7: ( '-' )
            // InternalGeometryCodes.g:16:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__17"

    // $ANTLR start "T__18"
    public final void mT__18() throws RecognitionException {
        try {
            int _type = T__18;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:17:7: ( '+' )
            // InternalGeometryCodes.g:17:9: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__18"

    // $ANTLR start "T__19"
    public final void mT__19() throws RecognitionException {
        try {
            int _type = T__19;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:18:7: ( '*' )
            // InternalGeometryCodes.g:18:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__19"

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:19:7: ( '/' )
            // InternalGeometryCodes.g:19:9: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:20:7: ( '==' )
            // InternalGeometryCodes.g:20:9: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:21:7: ( '!=' )
            // InternalGeometryCodes.g:21:9: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:22:7: ( '>' )
            // InternalGeometryCodes.g:22:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:23:7: ( '>=' )
            // InternalGeometryCodes.g:23:9: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:24:7: ( '<' )
            // InternalGeometryCodes.g:24:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:25:7: ( '<=' )
            // InternalGeometryCodes.g:25:9: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:26:7: ( '&&' )
            // InternalGeometryCodes.g:26:9: '&&'
            {
            match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:27:7: ( '||' )
            // InternalGeometryCodes.g:27:9: '||'
            {
            match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:28:7: ( 'void' )
            // InternalGeometryCodes.g:28:9: 'void'
            {
            match("void"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:29:7: ( 'float' )
            // InternalGeometryCodes.g:29:9: 'float'
            {
            match("float"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:30:7: ( 'vector' )
            // InternalGeometryCodes.g:30:9: 'vector'
            {
            match("vector"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:31:7: ( 'triangle' )
            // InternalGeometryCodes.g:31:9: 'triangle'
            {
            match("triangle"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:32:7: ( 'quad' )
            // InternalGeometryCodes.g:32:9: 'quad'
            {
            match("quad"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:33:7: ( 'mesh' )
            // InternalGeometryCodes.g:33:9: 'mesh'
            {
            match("mesh"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:34:7: ( 'import' )
            // InternalGeometryCodes.g:34:9: 'import'
            {
            match("import"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:35:7: ( '(' )
            // InternalGeometryCodes.g:35:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:36:7: ( ')' )
            // InternalGeometryCodes.g:36:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:37:7: ( '{' )
            // InternalGeometryCodes.g:37:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:38:7: ( '}' )
            // InternalGeometryCodes.g:38:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:39:7: ( ',' )
            // InternalGeometryCodes.g:39:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:40:7: ( 'iterator' )
            // InternalGeometryCodes.g:40:9: 'iterator'
            {
            match("iterator"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:41:7: ( 'over' )
            // InternalGeometryCodes.g:41:9: 'over'
            {
            match("over"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:42:7: ( 'each' )
            // InternalGeometryCodes.g:42:9: 'each'
            {
            match("each"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:43:7: ( 'material' )
            // InternalGeometryCodes.g:43:9: 'material'
            {
            match("material"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:44:7: ( ':' )
            // InternalGeometryCodes.g:44:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:45:7: ( 'return' )
            // InternalGeometryCodes.g:45:9: 'return'
            {
            match("return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:46:7: ( 'if' )
            // InternalGeometryCodes.g:46:9: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:47:7: ( 'for' )
            // InternalGeometryCodes.g:47:9: 'for'
            {
            match("for"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:48:7: ( 'from' )
            // InternalGeometryCodes.g:48:9: 'from'
            {
            match("from"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:49:7: ( 'to' )
            // InternalGeometryCodes.g:49:9: 'to'
            {
            match("to"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:50:7: ( 'in' )
            // InternalGeometryCodes.g:50:9: 'in'
            {
            match("in"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:51:7: ( 'while' )
            // InternalGeometryCodes.g:51:9: 'while'
            {
            match("while"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:52:7: ( 'output' )
            // InternalGeometryCodes.g:52:9: 'output'
            {
            match("output"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:53:7: ( '.' )
            // InternalGeometryCodes.g:53:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:54:7: ( 'vertex' )
            // InternalGeometryCodes.g:54:9: 'vertex'
            {
            match("vertex"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "T__56"
    public final void mT__56() throws RecognitionException {
        try {
            int _type = T__56;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:55:7: ( 'empty' )
            // InternalGeometryCodes.g:55:9: 'empty'
            {
            match("empty"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__56"

    // $ANTLR start "T__57"
    public final void mT__57() throws RecognitionException {
        try {
            int _type = T__57;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:56:7: ( 'false' )
            // InternalGeometryCodes.g:56:9: 'false'
            {
            match("false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__57"

    // $ANTLR start "T__58"
    public final void mT__58() throws RecognitionException {
        try {
            int _type = T__58;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:57:7: ( 'yes' )
            // InternalGeometryCodes.g:57:9: 'yes'
            {
            match("yes"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__58"

    // $ANTLR start "T__59"
    public final void mT__59() throws RecognitionException {
        try {
            int _type = T__59;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:58:7: ( 'true' )
            // InternalGeometryCodes.g:58:9: 'true'
            {
            match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__59"

    // $ANTLR start "RULE_FLOAT"
    public final void mRULE_FLOAT() throws RecognitionException {
        try {
            int _type = RULE_FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:7231:12: ( RULE_INT ( '.' RULE_INT )? )
            // InternalGeometryCodes.g:7231:14: RULE_INT ( '.' RULE_INT )?
            {
            mRULE_INT(); 
            // InternalGeometryCodes.g:7231:23: ( '.' RULE_INT )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='.') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalGeometryCodes.g:7231:24: '.' RULE_INT
                    {
                    match('.'); 
                    mRULE_INT(); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FLOAT"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:7233:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalGeometryCodes.g:7233:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalGeometryCodes.g:7233:11: ( '^' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='^') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalGeometryCodes.g:7233:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalGeometryCodes.g:7233:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')||(LA3_0>='A' && LA3_0<='Z')||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalGeometryCodes.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            // InternalGeometryCodes.g:7235:19: ( ( '0' .. '9' )+ )
            // InternalGeometryCodes.g:7235:21: ( '0' .. '9' )+
            {
            // InternalGeometryCodes.g:7235:21: ( '0' .. '9' )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='0' && LA4_0<='9')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalGeometryCodes.g:7235:22: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:7237:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalGeometryCodes.g:7237:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalGeometryCodes.g:7237:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='\"') ) {
                alt7=1;
            }
            else if ( (LA7_0=='\'') ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalGeometryCodes.g:7237:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalGeometryCodes.g:7237:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop5:
                    do {
                        int alt5=3;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0=='\\') ) {
                            alt5=1;
                        }
                        else if ( ((LA5_0>='\u0000' && LA5_0<='!')||(LA5_0>='#' && LA5_0<='[')||(LA5_0>=']' && LA5_0<='\uFFFF')) ) {
                            alt5=2;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalGeometryCodes.g:7237:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalGeometryCodes.g:7237:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:7237:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalGeometryCodes.g:7237:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0=='\\') ) {
                            alt6=1;
                        }
                        else if ( ((LA6_0>='\u0000' && LA6_0<='&')||(LA6_0>='(' && LA6_0<='[')||(LA6_0>=']' && LA6_0<='\uFFFF')) ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalGeometryCodes.g:7237:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalGeometryCodes.g:7237:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:7239:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalGeometryCodes.g:7239:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalGeometryCodes.g:7239:24: ( options {greedy=false; } : . )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0=='*') ) {
                    int LA8_1 = input.LA(2);

                    if ( (LA8_1=='/') ) {
                        alt8=2;
                    }
                    else if ( ((LA8_1>='\u0000' && LA8_1<='.')||(LA8_1>='0' && LA8_1<='\uFFFF')) ) {
                        alt8=1;
                    }


                }
                else if ( ((LA8_0>='\u0000' && LA8_0<=')')||(LA8_0>='+' && LA8_0<='\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalGeometryCodes.g:7239:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:7241:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalGeometryCodes.g:7241:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalGeometryCodes.g:7241:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='\u0000' && LA9_0<='\t')||(LA9_0>='\u000B' && LA9_0<='\f')||(LA9_0>='\u000E' && LA9_0<='\uFFFF')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalGeometryCodes.g:7241:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            // InternalGeometryCodes.g:7241:40: ( ( '\\r' )? '\\n' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0=='\n'||LA11_0=='\r') ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalGeometryCodes.g:7241:41: ( '\\r' )? '\\n'
                    {
                    // InternalGeometryCodes.g:7241:41: ( '\\r' )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0=='\r') ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalGeometryCodes.g:7241:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:7243:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalGeometryCodes.g:7243:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalGeometryCodes.g:7243:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( ((LA12_0>='\t' && LA12_0<='\n')||LA12_0=='\r'||LA12_0==' ') ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalGeometryCodes.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalGeometryCodes.g:7245:16: ( . )
            // InternalGeometryCodes.g:7245:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalGeometryCodes.g:1:8: ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | RULE_FLOAT | RULE_ID | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt13=55;
        alt13 = dfa13.predict(input);
        switch (alt13) {
            case 1 :
                // InternalGeometryCodes.g:1:10: T__12
                {
                mT__12(); 

                }
                break;
            case 2 :
                // InternalGeometryCodes.g:1:16: T__13
                {
                mT__13(); 

                }
                break;
            case 3 :
                // InternalGeometryCodes.g:1:22: T__14
                {
                mT__14(); 

                }
                break;
            case 4 :
                // InternalGeometryCodes.g:1:28: T__15
                {
                mT__15(); 

                }
                break;
            case 5 :
                // InternalGeometryCodes.g:1:34: T__16
                {
                mT__16(); 

                }
                break;
            case 6 :
                // InternalGeometryCodes.g:1:40: T__17
                {
                mT__17(); 

                }
                break;
            case 7 :
                // InternalGeometryCodes.g:1:46: T__18
                {
                mT__18(); 

                }
                break;
            case 8 :
                // InternalGeometryCodes.g:1:52: T__19
                {
                mT__19(); 

                }
                break;
            case 9 :
                // InternalGeometryCodes.g:1:58: T__20
                {
                mT__20(); 

                }
                break;
            case 10 :
                // InternalGeometryCodes.g:1:64: T__21
                {
                mT__21(); 

                }
                break;
            case 11 :
                // InternalGeometryCodes.g:1:70: T__22
                {
                mT__22(); 

                }
                break;
            case 12 :
                // InternalGeometryCodes.g:1:76: T__23
                {
                mT__23(); 

                }
                break;
            case 13 :
                // InternalGeometryCodes.g:1:82: T__24
                {
                mT__24(); 

                }
                break;
            case 14 :
                // InternalGeometryCodes.g:1:88: T__25
                {
                mT__25(); 

                }
                break;
            case 15 :
                // InternalGeometryCodes.g:1:94: T__26
                {
                mT__26(); 

                }
                break;
            case 16 :
                // InternalGeometryCodes.g:1:100: T__27
                {
                mT__27(); 

                }
                break;
            case 17 :
                // InternalGeometryCodes.g:1:106: T__28
                {
                mT__28(); 

                }
                break;
            case 18 :
                // InternalGeometryCodes.g:1:112: T__29
                {
                mT__29(); 

                }
                break;
            case 19 :
                // InternalGeometryCodes.g:1:118: T__30
                {
                mT__30(); 

                }
                break;
            case 20 :
                // InternalGeometryCodes.g:1:124: T__31
                {
                mT__31(); 

                }
                break;
            case 21 :
                // InternalGeometryCodes.g:1:130: T__32
                {
                mT__32(); 

                }
                break;
            case 22 :
                // InternalGeometryCodes.g:1:136: T__33
                {
                mT__33(); 

                }
                break;
            case 23 :
                // InternalGeometryCodes.g:1:142: T__34
                {
                mT__34(); 

                }
                break;
            case 24 :
                // InternalGeometryCodes.g:1:148: T__35
                {
                mT__35(); 

                }
                break;
            case 25 :
                // InternalGeometryCodes.g:1:154: T__36
                {
                mT__36(); 

                }
                break;
            case 26 :
                // InternalGeometryCodes.g:1:160: T__37
                {
                mT__37(); 

                }
                break;
            case 27 :
                // InternalGeometryCodes.g:1:166: T__38
                {
                mT__38(); 

                }
                break;
            case 28 :
                // InternalGeometryCodes.g:1:172: T__39
                {
                mT__39(); 

                }
                break;
            case 29 :
                // InternalGeometryCodes.g:1:178: T__40
                {
                mT__40(); 

                }
                break;
            case 30 :
                // InternalGeometryCodes.g:1:184: T__41
                {
                mT__41(); 

                }
                break;
            case 31 :
                // InternalGeometryCodes.g:1:190: T__42
                {
                mT__42(); 

                }
                break;
            case 32 :
                // InternalGeometryCodes.g:1:196: T__43
                {
                mT__43(); 

                }
                break;
            case 33 :
                // InternalGeometryCodes.g:1:202: T__44
                {
                mT__44(); 

                }
                break;
            case 34 :
                // InternalGeometryCodes.g:1:208: T__45
                {
                mT__45(); 

                }
                break;
            case 35 :
                // InternalGeometryCodes.g:1:214: T__46
                {
                mT__46(); 

                }
                break;
            case 36 :
                // InternalGeometryCodes.g:1:220: T__47
                {
                mT__47(); 

                }
                break;
            case 37 :
                // InternalGeometryCodes.g:1:226: T__48
                {
                mT__48(); 

                }
                break;
            case 38 :
                // InternalGeometryCodes.g:1:232: T__49
                {
                mT__49(); 

                }
                break;
            case 39 :
                // InternalGeometryCodes.g:1:238: T__50
                {
                mT__50(); 

                }
                break;
            case 40 :
                // InternalGeometryCodes.g:1:244: T__51
                {
                mT__51(); 

                }
                break;
            case 41 :
                // InternalGeometryCodes.g:1:250: T__52
                {
                mT__52(); 

                }
                break;
            case 42 :
                // InternalGeometryCodes.g:1:256: T__53
                {
                mT__53(); 

                }
                break;
            case 43 :
                // InternalGeometryCodes.g:1:262: T__54
                {
                mT__54(); 

                }
                break;
            case 44 :
                // InternalGeometryCodes.g:1:268: T__55
                {
                mT__55(); 

                }
                break;
            case 45 :
                // InternalGeometryCodes.g:1:274: T__56
                {
                mT__56(); 

                }
                break;
            case 46 :
                // InternalGeometryCodes.g:1:280: T__57
                {
                mT__57(); 

                }
                break;
            case 47 :
                // InternalGeometryCodes.g:1:286: T__58
                {
                mT__58(); 

                }
                break;
            case 48 :
                // InternalGeometryCodes.g:1:292: T__59
                {
                mT__59(); 

                }
                break;
            case 49 :
                // InternalGeometryCodes.g:1:298: RULE_FLOAT
                {
                mRULE_FLOAT(); 

                }
                break;
            case 50 :
                // InternalGeometryCodes.g:1:309: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 51 :
                // InternalGeometryCodes.g:1:317: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 52 :
                // InternalGeometryCodes.g:1:329: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 53 :
                // InternalGeometryCodes.g:1:345: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 54 :
                // InternalGeometryCodes.g:1:361: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 55 :
                // InternalGeometryCodes.g:1:369: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA13 dfa13 = new DFA13(this);
    static final String DFA13_eotS =
        "\1\uffff\1\46\1\50\1\52\1\54\2\uffff\1\61\1\63\1\65\2\44\6\46\5\uffff\2\46\1\uffff\2\46\1\uffff\1\46\1\uffff\1\44\1\uffff\2\44\2\uffff\1\131\22\uffff\7\46\1\143\5\46\1\151\1\152\5\uffff\4\46\1\uffff\2\46\1\uffff\1\46\3\uffff\1\46\1\uffff\4\46\1\167\4\46\1\uffff\5\46\2\uffff\6\46\1\u0087\1\46\1\u0089\3\46\1\uffff\1\u008d\2\46\1\u0090\1\u0091\1\u0092\3\46\1\u0096\1\46\1\u0098\3\46\1\uffff\1\46\1\uffff\2\46\1\u009f\1\uffff\1\u00a0\1\46\3\uffff\3\46\1\uffff\1\46\1\uffff\1\u00a6\1\46\1\u00a8\1\46\1\u00aa\1\u00ab\2\uffff\2\46\1\u00ae\1\46\1\u00b0\1\uffff\1\u00b1\1\uffff\1\u00b2\2\uffff\2\46\1\uffff\1\46\3\uffff\1\u00b6\1\u00b7\1\u00b8\3\uffff";
    static final String DFA13_eofS =
        "\u00b9\uffff";
    static final String DFA13_minS =
        "\1\0\1\157\3\75\2\uffff\1\52\2\75\1\46\1\174\1\145\1\141\1\157\1\165\1\141\1\146\5\uffff\1\165\1\141\1\uffff\1\145\1\150\1\uffff\1\145\1\uffff\1\101\1\uffff\2\0\2\uffff\1\60\22\uffff\1\151\1\143\1\157\1\162\1\157\1\154\1\151\1\60\1\141\1\163\1\164\1\160\1\145\2\60\5\uffff\1\145\1\164\1\143\1\160\1\uffff\1\164\1\151\1\uffff\1\163\3\uffff\1\150\1\uffff\1\144\2\164\1\141\1\60\1\155\1\163\1\141\1\145\1\uffff\1\144\1\150\1\145\1\157\1\162\2\uffff\1\162\1\160\1\150\1\164\1\165\1\154\1\60\1\151\1\60\1\157\1\145\1\164\1\uffff\1\60\1\145\1\156\3\60\2\162\1\141\1\60\1\165\1\60\1\171\1\162\1\145\1\uffff\1\156\1\uffff\1\162\1\170\1\60\1\uffff\1\60\1\147\3\uffff\1\151\2\164\1\uffff\1\164\1\uffff\1\60\1\156\1\60\1\147\2\60\2\uffff\1\154\1\141\1\60\1\157\1\60\1\uffff\1\60\1\uffff\1\60\2\uffff\1\145\1\154\1\uffff\1\162\3\uffff\3\60\3\uffff";
    static final String DFA13_maxS =
        "\1\uffff\1\157\3\75\2\uffff\1\57\2\75\1\46\1\174\1\157\2\162\1\165\1\145\1\164\5\uffff\1\166\1\155\1\uffff\1\145\1\150\1\uffff\1\145\1\uffff\1\172\1\uffff\2\uffff\2\uffff\1\172\22\uffff\1\151\1\162\1\157\1\162\1\157\1\154\1\165\1\172\1\141\1\163\1\164\1\160\1\145\2\172\5\uffff\1\145\1\164\1\143\1\160\1\uffff\1\164\1\151\1\uffff\1\163\3\uffff\1\150\1\uffff\1\144\2\164\1\141\1\172\1\155\1\163\1\141\1\145\1\uffff\1\144\1\150\1\145\1\157\1\162\2\uffff\1\162\1\160\1\150\1\164\1\165\1\154\1\172\1\151\1\172\1\157\1\145\1\164\1\uffff\1\172\1\145\1\156\3\172\2\162\1\141\1\172\1\165\1\172\1\171\1\162\1\145\1\uffff\1\156\1\uffff\1\162\1\170\1\172\1\uffff\1\172\1\147\3\uffff\1\151\2\164\1\uffff\1\164\1\uffff\1\172\1\156\1\172\1\147\2\172\2\uffff\1\154\1\141\1\172\1\157\1\172\1\uffff\1\172\1\uffff\1\172\2\uffff\1\145\1\154\1\uffff\1\162\3\uffff\3\172\3\uffff";
    static final String DFA13_acceptS =
        "\5\uffff\1\6\1\10\13\uffff\1\31\1\32\1\33\1\34\1\35\2\uffff\1\42\2\uffff\1\53\1\uffff\1\61\1\uffff\1\62\2\uffff\1\66\1\67\1\uffff\1\62\1\12\1\3\1\4\1\7\1\13\1\5\1\6\1\10\1\64\1\65\1\11\1\15\1\14\1\17\1\16\1\20\1\21\17\uffff\1\31\1\32\1\33\1\34\1\35\4\uffff\1\42\2\uffff\1\53\1\uffff\1\61\1\63\1\66\1\uffff\1\1\11\uffff\1\47\5\uffff\1\44\1\50\14\uffff\1\45\17\uffff\1\57\1\uffff\1\22\3\uffff\1\46\2\uffff\1\60\1\26\1\27\3\uffff\1\37\1\uffff\1\40\6\uffff\1\23\1\56\5\uffff\1\55\1\uffff\1\51\1\uffff\1\24\1\54\2\uffff\1\30\1\uffff\1\52\1\43\1\2\3\uffff\1\25\1\41\1\36";
    static final String DFA13_specialS =
        "\1\2\40\uffff\1\1\1\0\u0096\uffff}>";
    static final String[] DFA13_transitionS = {
            "\11\44\2\43\2\44\1\43\22\44\1\43\1\4\1\41\3\44\1\12\1\42\1\22\1\23\1\6\1\3\1\26\1\5\1\34\1\7\12\36\1\31\1\44\1\11\1\2\1\10\2\44\32\40\3\44\1\37\1\40\1\44\4\40\1\30\1\15\2\40\1\21\3\40\1\20\1\1\1\27\1\40\1\17\1\32\1\40\1\16\1\40\1\14\1\33\1\40\1\35\1\40\1\24\1\13\1\25\uff82\44",
            "\1\45",
            "\1\47",
            "\1\51",
            "\1\53",
            "",
            "",
            "\1\57\4\uffff\1\60",
            "\1\62",
            "\1\64",
            "\1\66",
            "\1\67",
            "\1\71\11\uffff\1\70",
            "\1\75\12\uffff\1\72\2\uffff\1\73\2\uffff\1\74",
            "\1\77\2\uffff\1\76",
            "\1\100",
            "\1\102\3\uffff\1\101",
            "\1\105\6\uffff\1\103\1\106\5\uffff\1\104",
            "",
            "",
            "",
            "",
            "",
            "\1\115\1\114",
            "\1\116\13\uffff\1\117",
            "",
            "\1\121",
            "\1\122",
            "",
            "\1\124",
            "",
            "\32\46\4\uffff\1\46\1\uffff\32\46",
            "",
            "\0\126",
            "\0\126",
            "",
            "",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\23\46\1\130\6\46",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\132",
            "\1\133\16\uffff\1\134",
            "\1\135",
            "\1\136",
            "\1\137",
            "\1\140",
            "\1\141\13\uffff\1\142",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\147",
            "\1\150",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "",
            "",
            "",
            "",
            "",
            "\1\153",
            "\1\154",
            "\1\155",
            "\1\156",
            "",
            "\1\157",
            "\1\160",
            "",
            "\1\161",
            "",
            "",
            "",
            "\1\162",
            "",
            "\1\163",
            "\1\164",
            "\1\165",
            "\1\166",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\170",
            "\1\171",
            "\1\172",
            "\1\173",
            "",
            "\1\174",
            "\1\175",
            "\1\176",
            "\1\177",
            "\1\u0080",
            "",
            "",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u0088",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u008a",
            "\1\u008b",
            "\1\u008c",
            "",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u008e",
            "\1\u008f",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u0097",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u0099",
            "\1\u009a",
            "\1\u009b",
            "",
            "\1\u009c",
            "",
            "\1\u009d",
            "\1\u009e",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u00a1",
            "",
            "",
            "",
            "\1\u00a2",
            "\1\u00a3",
            "\1\u00a4",
            "",
            "\1\u00a5",
            "",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u00a7",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u00a9",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "",
            "",
            "\1\u00ac",
            "\1\u00ad",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\1\u00af",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "",
            "",
            "\1\u00b3",
            "\1\u00b4",
            "",
            "\1\u00b5",
            "",
            "",
            "",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "\12\46\7\uffff\32\46\4\uffff\1\46\1\uffff\32\46",
            "",
            "",
            ""
    };

    static final short[] DFA13_eot = DFA.unpackEncodedString(DFA13_eotS);
    static final short[] DFA13_eof = DFA.unpackEncodedString(DFA13_eofS);
    static final char[] DFA13_min = DFA.unpackEncodedStringToUnsignedChars(DFA13_minS);
    static final char[] DFA13_max = DFA.unpackEncodedStringToUnsignedChars(DFA13_maxS);
    static final short[] DFA13_accept = DFA.unpackEncodedString(DFA13_acceptS);
    static final short[] DFA13_special = DFA.unpackEncodedString(DFA13_specialS);
    static final short[][] DFA13_transition;

    static {
        int numStates = DFA13_transitionS.length;
        DFA13_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA13_transition[i] = DFA.unpackEncodedString(DFA13_transitionS[i]);
        }
    }

    class DFA13 extends DFA {

        public DFA13(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 13;
            this.eot = DFA13_eot;
            this.eof = DFA13_eof;
            this.min = DFA13_min;
            this.max = DFA13_max;
            this.accept = DFA13_accept;
            this.special = DFA13_special;
            this.transition = DFA13_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__12 | T__13 | T__14 | T__15 | T__16 | T__17 | T__18 | T__19 | T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | RULE_FLOAT | RULE_ID | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA13_34 = input.LA(1);

                        s = -1;
                        if ( ((LA13_34>='\u0000' && LA13_34<='\uFFFF')) ) {s = 86;}

                        else s = 36;

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA13_33 = input.LA(1);

                        s = -1;
                        if ( ((LA13_33>='\u0000' && LA13_33<='\uFFFF')) ) {s = 86;}

                        else s = 36;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA13_0 = input.LA(1);

                        s = -1;
                        if ( (LA13_0=='n') ) {s = 1;}

                        else if ( (LA13_0=='=') ) {s = 2;}

                        else if ( (LA13_0=='+') ) {s = 3;}

                        else if ( (LA13_0=='!') ) {s = 4;}

                        else if ( (LA13_0=='-') ) {s = 5;}

                        else if ( (LA13_0=='*') ) {s = 6;}

                        else if ( (LA13_0=='/') ) {s = 7;}

                        else if ( (LA13_0=='>') ) {s = 8;}

                        else if ( (LA13_0=='<') ) {s = 9;}

                        else if ( (LA13_0=='&') ) {s = 10;}

                        else if ( (LA13_0=='|') ) {s = 11;}

                        else if ( (LA13_0=='v') ) {s = 12;}

                        else if ( (LA13_0=='f') ) {s = 13;}

                        else if ( (LA13_0=='t') ) {s = 14;}

                        else if ( (LA13_0=='q') ) {s = 15;}

                        else if ( (LA13_0=='m') ) {s = 16;}

                        else if ( (LA13_0=='i') ) {s = 17;}

                        else if ( (LA13_0=='(') ) {s = 18;}

                        else if ( (LA13_0==')') ) {s = 19;}

                        else if ( (LA13_0=='{') ) {s = 20;}

                        else if ( (LA13_0=='}') ) {s = 21;}

                        else if ( (LA13_0==',') ) {s = 22;}

                        else if ( (LA13_0=='o') ) {s = 23;}

                        else if ( (LA13_0=='e') ) {s = 24;}

                        else if ( (LA13_0==':') ) {s = 25;}

                        else if ( (LA13_0=='r') ) {s = 26;}

                        else if ( (LA13_0=='w') ) {s = 27;}

                        else if ( (LA13_0=='.') ) {s = 28;}

                        else if ( (LA13_0=='y') ) {s = 29;}

                        else if ( ((LA13_0>='0' && LA13_0<='9')) ) {s = 30;}

                        else if ( (LA13_0=='^') ) {s = 31;}

                        else if ( ((LA13_0>='A' && LA13_0<='Z')||LA13_0=='_'||(LA13_0>='a' && LA13_0<='d')||(LA13_0>='g' && LA13_0<='h')||(LA13_0>='j' && LA13_0<='l')||LA13_0=='p'||LA13_0=='s'||LA13_0=='u'||LA13_0=='x'||LA13_0=='z') ) {s = 32;}

                        else if ( (LA13_0=='\"') ) {s = 33;}

                        else if ( (LA13_0=='\'') ) {s = 34;}

                        else if ( ((LA13_0>='\t' && LA13_0<='\n')||LA13_0=='\r'||LA13_0==' ') ) {s = 35;}

                        else if ( ((LA13_0>='\u0000' && LA13_0<='\b')||(LA13_0>='\u000B' && LA13_0<='\f')||(LA13_0>='\u000E' && LA13_0<='\u001F')||(LA13_0>='#' && LA13_0<='%')||LA13_0==';'||(LA13_0>='?' && LA13_0<='@')||(LA13_0>='[' && LA13_0<=']')||LA13_0=='`'||(LA13_0>='~' && LA13_0<='\uFFFF')) ) {s = 36;}

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 13, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}