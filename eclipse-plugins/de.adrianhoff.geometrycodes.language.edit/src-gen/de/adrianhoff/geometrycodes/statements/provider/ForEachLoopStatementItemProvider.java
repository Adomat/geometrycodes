/**
 */
package de.adrianhoff.geometrycodes.statements.provider;


import de.adrianhoff.geometrycodes.core.CorePackage;

import de.adrianhoff.geometrycodes.expressions.ExpressionsFactory;

import de.adrianhoff.geometrycodes.literals.LiteralsFactory;

import de.adrianhoff.geometrycodes.statements.ForEachLoopStatement;
import de.adrianhoff.geometrycodes.statements.StatementsPackage;

import de.adrianhoff.geometrycodes.types.Type;
import de.adrianhoff.geometrycodes.types.TypesPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.adrianhoff.geometrycodes.statements.ForEachLoopStatement} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ForEachLoopStatementItemProvider extends ConditionalStatementItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForEachLoopStatementItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addPredecessorTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_NamedElement_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_NamedElement_name_feature", "_UI_NamedElement_type"),
				 CorePackage.Literals.NAMED_ELEMENT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Predecessor Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPredecessorTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ReferenceableElement_predecessorType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ReferenceableElement_predecessorType_feature", "_UI_ReferenceableElement_type"),
				 CorePackage.Literals.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(TypesPackage.Literals.TYPED_ELEMENT__TYPE);
			childrenFeatures.add(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ForEachLoopStatement.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ForEachLoopStatement"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ForEachLoopStatement)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ForEachLoopStatement_type") :
			getString("_UI_ForEachLoopStatement_type") + " " + label;
	}


	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ForEachLoopStatement.class)) {
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__NAME:
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__TYPE:
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(TypesPackage.Literals.TYPED_ELEMENT__TYPE,
				 Type.VOID));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createUnaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createBinaryExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createLogicalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createConditionalExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createAdditiveExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createMultiplicativeExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createReferenceExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createVariableReferenceExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createFunctionCallExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createVectorDeclarationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createTriangleDeclarationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createQuadDeclarationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createVertexDeclarationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 ExpressionsFactory.eINSTANCE.createLiteralExpression()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 LiteralsFactory.eINSTANCE.createBooleanLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 LiteralsFactory.eINSTANCE.createFloatLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 LiteralsFactory.eINSTANCE.createStringLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT__VALUE,
				 LiteralsFactory.eINSTANCE.createEmptyLiteral()));
	}

}
