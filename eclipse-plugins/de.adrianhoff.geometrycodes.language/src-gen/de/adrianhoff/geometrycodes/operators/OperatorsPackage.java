/**
 */
package de.adrianhoff.geometrycodes.operators;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.adrianhoff.geometrycodes.operators.OperatorsFactory
 * @model kind="package"
 * @generated
 */
public interface OperatorsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "operators";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.adrianhoff.de/GeometryCodes/operators";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "operators";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OperatorsPackage eINSTANCE = de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.operators.UnaryOperator <em>Unary Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.operators.UnaryOperator
	 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getUnaryOperator()
	 * @generated
	 */
	int UNARY_OPERATOR = 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.operators.AdditiveOperator <em>Additive Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.operators.AdditiveOperator
	 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getAdditiveOperator()
	 * @generated
	 */
	int ADDITIVE_OPERATOR = 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.operators.MultiplicativeOperator <em>Multiplicative Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.operators.MultiplicativeOperator
	 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getMultiplicativeOperator()
	 * @generated
	 */
	int MULTIPLICATIVE_OPERATOR = 2;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.operators.ComparisonOperator <em>Comparison Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.operators.ComparisonOperator
	 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getComparisonOperator()
	 * @generated
	 */
	int COMPARISON_OPERATOR = 3;


	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.operators.LogicalOperator <em>Logical Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.operators.LogicalOperator
	 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getLogicalOperator()
	 * @generated
	 */
	int LOGICAL_OPERATOR = 4;


	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.operators.AssignmentOperator <em>Assignment Operator</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.operators.AssignmentOperator
	 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getAssignmentOperator()
	 * @generated
	 */
	int ASSIGNMENT_OPERATOR = 5;


	/**
	 * Returns the meta object for enum '{@link de.adrianhoff.geometrycodes.operators.UnaryOperator <em>Unary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Unary Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.operators.UnaryOperator
	 * @generated
	 */
	EEnum getUnaryOperator();

	/**
	 * Returns the meta object for enum '{@link de.adrianhoff.geometrycodes.operators.AdditiveOperator <em>Additive Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Additive Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.operators.AdditiveOperator
	 * @generated
	 */
	EEnum getAdditiveOperator();

	/**
	 * Returns the meta object for enum '{@link de.adrianhoff.geometrycodes.operators.MultiplicativeOperator <em>Multiplicative Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Multiplicative Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.operators.MultiplicativeOperator
	 * @generated
	 */
	EEnum getMultiplicativeOperator();

	/**
	 * Returns the meta object for enum '{@link de.adrianhoff.geometrycodes.operators.ComparisonOperator <em>Comparison Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Comparison Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.operators.ComparisonOperator
	 * @generated
	 */
	EEnum getComparisonOperator();

	/**
	 * Returns the meta object for enum '{@link de.adrianhoff.geometrycodes.operators.LogicalOperator <em>Logical Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Logical Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.operators.LogicalOperator
	 * @generated
	 */
	EEnum getLogicalOperator();

	/**
	 * Returns the meta object for enum '{@link de.adrianhoff.geometrycodes.operators.AssignmentOperator <em>Assignment Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Assignment Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.operators.AssignmentOperator
	 * @generated
	 */
	EEnum getAssignmentOperator();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OperatorsFactory getOperatorsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.operators.UnaryOperator <em>Unary Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.operators.UnaryOperator
		 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getUnaryOperator()
		 * @generated
		 */
		EEnum UNARY_OPERATOR = eINSTANCE.getUnaryOperator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.operators.AdditiveOperator <em>Additive Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.operators.AdditiveOperator
		 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getAdditiveOperator()
		 * @generated
		 */
		EEnum ADDITIVE_OPERATOR = eINSTANCE.getAdditiveOperator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.operators.MultiplicativeOperator <em>Multiplicative Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.operators.MultiplicativeOperator
		 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getMultiplicativeOperator()
		 * @generated
		 */
		EEnum MULTIPLICATIVE_OPERATOR = eINSTANCE.getMultiplicativeOperator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.operators.ComparisonOperator <em>Comparison Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.operators.ComparisonOperator
		 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getComparisonOperator()
		 * @generated
		 */
		EEnum COMPARISON_OPERATOR = eINSTANCE.getComparisonOperator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.operators.LogicalOperator <em>Logical Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.operators.LogicalOperator
		 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getLogicalOperator()
		 * @generated
		 */
		EEnum LOGICAL_OPERATOR = eINSTANCE.getLogicalOperator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.operators.AssignmentOperator <em>Assignment Operator</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.operators.AssignmentOperator
		 * @see de.adrianhoff.geometrycodes.operators.impl.OperatorsPackageImpl#getAssignmentOperator()
		 * @generated
		 */
		EEnum ASSIGNMENT_OPERATOR = eINSTANCE.getAssignmentOperator();

	}

} //OperatorsPackage
