/**
 */
package de.adrianhoff.geometrycodes.literals;

import de.adrianhoff.geometrycodes.expressions.Expression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.literals.LiteralsPackage#getLiteral()
 * @model abstract="true"
 * @generated
 */
public interface Literal extends Expression {
} // Literal
