/**
 */
package de.adrianhoff.geometrycodes.literals;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Empty Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.literals.LiteralsPackage#getEmptyLiteral()
 * @model
 * @generated
 */
public interface EmptyLiteral extends Literal {
} // EmptyLiteral
