/**
 */
package de.adrianhoff.geometrycodes.literals;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Float Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.literals.FloatLiteral#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.literals.LiteralsPackage#getFloatLiteral()
 * @model
 * @generated
 */
public interface FloatLiteral extends Literal {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(float)
	 * @see de.adrianhoff.geometrycodes.literals.LiteralsPackage#getFloatLiteral_Value()
	 * @model required="true"
	 * @generated
	 */
	float getValue();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.literals.FloatLiteral#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(float value);

} // FloatLiteral
