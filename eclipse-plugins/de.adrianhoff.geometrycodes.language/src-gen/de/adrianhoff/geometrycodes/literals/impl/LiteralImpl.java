/**
 */
package de.adrianhoff.geometrycodes.literals.impl;

import de.adrianhoff.geometrycodes.expressions.impl.ExpressionImpl;

import de.adrianhoff.geometrycodes.literals.Literal;
import de.adrianhoff.geometrycodes.literals.LiteralsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class LiteralImpl extends ExpressionImpl implements Literal {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LiteralImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LiteralsPackage.Literals.LITERAL;
	}

} //LiteralImpl
