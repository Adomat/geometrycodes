/**
 */
package de.adrianhoff.geometrycodes.literals.impl;

import de.adrianhoff.geometrycodes.literals.EmptyLiteral;
import de.adrianhoff.geometrycodes.literals.LiteralsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Empty Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EmptyLiteralImpl extends LiteralImpl implements EmptyLiteral {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EmptyLiteralImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LiteralsPackage.Literals.EMPTY_LITERAL;
	}

} //EmptyLiteralImpl
