/**
 */
package de.adrianhoff.geometrycodes.core;

import de.adrianhoff.geometrycodes.statements.StatementContainer;

import de.adrianhoff.geometrycodes.types.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iterator Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.IteratorDeclaration#getIteratedParameter <em>Iterated Parameter</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getIteratorDeclaration()
 * @model
 * @generated
 */
public interface IteratorDeclaration extends ParameterContainer, ReferenceableElement, StatementContainer, TypedElement {
	/**
	 * Returns the value of the '<em><b>Iterated Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterated Parameter</em>' containment reference.
	 * @see #setIteratedParameter(Parameter)
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getIteratorDeclaration_IteratedParameter()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Parameter getIteratedParameter();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.core.IteratorDeclaration#getIteratedParameter <em>Iterated Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterated Parameter</em>' containment reference.
	 * @see #getIteratedParameter()
	 * @generated
	 */
	void setIteratedParameter(Parameter value);

} // IteratorDeclaration
