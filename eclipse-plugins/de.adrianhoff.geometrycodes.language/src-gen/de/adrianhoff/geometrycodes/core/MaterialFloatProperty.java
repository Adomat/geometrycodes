/**
 */
package de.adrianhoff.geometrycodes.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Float Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.MaterialFloatProperty#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getMaterialFloatProperty()
 * @model
 * @generated
 */
public interface MaterialFloatProperty extends MaterialProperty {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(float)
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getMaterialFloatProperty_Value()
	 * @model required="true"
	 * @generated
	 */
	float getValue();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.core.MaterialFloatProperty#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(float value);

} // MaterialFloatProperty
