/**
 */
package de.adrianhoff.geometrycodes.core;

import de.adrianhoff.geometrycodes.types.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends ReferenceableElement, TypedElement {
} // Parameter
