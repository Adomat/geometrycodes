/**
 */
package de.adrianhoff.geometrycodes.core.impl;

import de.adrianhoff.geometrycodes.core.CorePackage;
import de.adrianhoff.geometrycodes.core.File;
import de.adrianhoff.geometrycodes.core.FunctionDeclaration;
import de.adrianhoff.geometrycodes.core.ImportStatement;
import de.adrianhoff.geometrycodes.core.IteratorDeclaration;
import de.adrianhoff.geometrycodes.core.MaterialDeclaration;

import de.adrianhoff.geometrycodes.statements.OutputStatement;
import de.adrianhoff.geometrycodes.statements.impl.StatementContainerImpl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.FileImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.FileImpl#getMaterials <em>Materials</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.FileImpl#getFunctions <em>Functions</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.FileImpl#getIterators <em>Iterators</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FileImpl extends StatementContainerImpl implements File {
	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<ImportStatement> imports;

	/**
	 * The cached value of the '{@link #getMaterials() <em>Materials</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaterials()
	 * @generated
	 * @ordered
	 */
	protected EList<MaterialDeclaration> materials;

	/**
	 * The cached value of the '{@link #getFunctions() <em>Functions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFunctions()
	 * @generated
	 * @ordered
	 */
	protected EList<FunctionDeclaration> functions;

	/**
	 * The cached value of the '{@link #getIterators() <em>Iterators</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIterators()
	 * @generated
	 * @ordered
	 */
	protected EList<IteratorDeclaration> iterators;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FileImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.FILE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<ImportStatement> getImports() {
		if (imports == null) {
			imports = new EObjectContainmentEList<ImportStatement>(ImportStatement.class, this, CorePackage.FILE__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<MaterialDeclaration> getMaterials() {
		if (materials == null) {
			materials = new EObjectContainmentEList<MaterialDeclaration>(MaterialDeclaration.class, this, CorePackage.FILE__MATERIALS);
		}
		return materials;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<FunctionDeclaration> getFunctions() {
		if (functions == null) {
			functions = new EObjectContainmentEList<FunctionDeclaration>(FunctionDeclaration.class, this, CorePackage.FILE__FUNCTIONS);
		}
		return functions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<IteratorDeclaration> getIterators() {
		if (iterators == null) {
			iterators = new EObjectContainmentEList<IteratorDeclaration>(IteratorDeclaration.class, this, CorePackage.FILE__ITERATORS);
		}
		return iterators;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.FILE__IMPORTS:
				return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
			case CorePackage.FILE__MATERIALS:
				return ((InternalEList<?>)getMaterials()).basicRemove(otherEnd, msgs);
			case CorePackage.FILE__FUNCTIONS:
				return ((InternalEList<?>)getFunctions()).basicRemove(otherEnd, msgs);
			case CorePackage.FILE__ITERATORS:
				return ((InternalEList<?>)getIterators()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.FILE__IMPORTS:
				return getImports();
			case CorePackage.FILE__MATERIALS:
				return getMaterials();
			case CorePackage.FILE__FUNCTIONS:
				return getFunctions();
			case CorePackage.FILE__ITERATORS:
				return getIterators();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.FILE__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends ImportStatement>)newValue);
				return;
			case CorePackage.FILE__MATERIALS:
				getMaterials().clear();
				getMaterials().addAll((Collection<? extends MaterialDeclaration>)newValue);
				return;
			case CorePackage.FILE__FUNCTIONS:
				getFunctions().clear();
				getFunctions().addAll((Collection<? extends FunctionDeclaration>)newValue);
				return;
			case CorePackage.FILE__ITERATORS:
				getIterators().clear();
				getIterators().addAll((Collection<? extends IteratorDeclaration>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.FILE__IMPORTS:
				getImports().clear();
				return;
			case CorePackage.FILE__MATERIALS:
				getMaterials().clear();
				return;
			case CorePackage.FILE__FUNCTIONS:
				getFunctions().clear();
				return;
			case CorePackage.FILE__ITERATORS:
				getIterators().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.FILE__IMPORTS:
				return imports != null && !imports.isEmpty();
			case CorePackage.FILE__MATERIALS:
				return materials != null && !materials.isEmpty();
			case CorePackage.FILE__FUNCTIONS:
				return functions != null && !functions.isEmpty();
			case CorePackage.FILE__ITERATORS:
				return iterators != null && !iterators.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FileImpl
