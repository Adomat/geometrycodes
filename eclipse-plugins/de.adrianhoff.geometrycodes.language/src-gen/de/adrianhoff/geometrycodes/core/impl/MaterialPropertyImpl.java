/**
 */
package de.adrianhoff.geometrycodes.core.impl;

import de.adrianhoff.geometrycodes.core.CorePackage;
import de.adrianhoff.geometrycodes.core.MaterialProperty;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Material Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class MaterialPropertyImpl extends NamedElementImpl implements MaterialProperty {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaterialPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.MATERIAL_PROPERTY;
	}

} //MaterialPropertyImpl
