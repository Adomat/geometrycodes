/**
 */
package de.adrianhoff.geometrycodes.core.impl;

import de.adrianhoff.geometrycodes.core.CorePackage;
import de.adrianhoff.geometrycodes.core.ReferenceableElement;

import de.adrianhoff.geometrycodes.types.Type;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Referenceable Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.ReferenceableElementImpl#getPredecessorType <em>Predecessor Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ReferenceableElementImpl extends NamedElementImpl implements ReferenceableElement {
	/**
	 * The default value of the '{@link #getPredecessorType() <em>Predecessor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessorType()
	 * @generated
	 * @ordered
	 */
	protected static final Type PREDECESSOR_TYPE_EDEFAULT = Type.VOID;

	/**
	 * The cached value of the '{@link #getPredecessorType() <em>Predecessor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessorType()
	 * @generated
	 * @ordered
	 */
	protected Type predecessorType = PREDECESSOR_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ReferenceableElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.REFERENCEABLE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Type getPredecessorType() {
		return predecessorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPredecessorType(Type newPredecessorType) {
		Type oldPredecessorType = predecessorType;
		predecessorType = newPredecessorType == null ? PREDECESSOR_TYPE_EDEFAULT : newPredecessorType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE, oldPredecessorType, predecessorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE:
				return getPredecessorType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE:
				setPredecessorType((Type)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE:
				setPredecessorType(PREDECESSOR_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE:
				return predecessorType != PREDECESSOR_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (predecessorType: ");
		result.append(predecessorType);
		result.append(')');
		return result.toString();
	}

} //ReferenceableElementImpl
