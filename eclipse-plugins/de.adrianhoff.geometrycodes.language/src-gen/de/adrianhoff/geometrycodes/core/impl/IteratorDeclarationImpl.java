/**
 */
package de.adrianhoff.geometrycodes.core.impl;

import de.adrianhoff.geometrycodes.core.CorePackage;
import de.adrianhoff.geometrycodes.core.IteratorDeclaration;
import de.adrianhoff.geometrycodes.core.NamedElement;
import de.adrianhoff.geometrycodes.core.Parameter;
import de.adrianhoff.geometrycodes.core.ReferenceableElement;

import de.adrianhoff.geometrycodes.statements.Statement;
import de.adrianhoff.geometrycodes.statements.StatementContainer;
import de.adrianhoff.geometrycodes.statements.StatementsPackage;

import de.adrianhoff.geometrycodes.types.Type;
import de.adrianhoff.geometrycodes.types.TypedElement;
import de.adrianhoff.geometrycodes.types.TypesPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Iterator Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl#getPredecessorType <em>Predecessor Type</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl#getStatements <em>Statements</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl#getIteratedParameter <em>Iterated Parameter</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IteratorDeclarationImpl extends ParameterContainerImpl implements IteratorDeclaration {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPredecessorType() <em>Predecessor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessorType()
	 * @generated
	 * @ordered
	 */
	protected static final Type PREDECESSOR_TYPE_EDEFAULT = Type.VOID;

	/**
	 * The cached value of the '{@link #getPredecessorType() <em>Predecessor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessorType()
	 * @generated
	 * @ordered
	 */
	protected Type predecessorType = PREDECESSOR_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStatements() <em>Statements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatements()
	 * @generated
	 * @ordered
	 */
	protected EList<Statement> statements;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final Type TYPE_EDEFAULT = Type.VOID;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getIteratedParameter() <em>Iterated Parameter</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIteratedParameter()
	 * @generated
	 * @ordered
	 */
	protected Parameter iteratedParameter;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IteratorDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.ITERATOR_DECLARATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ITERATOR_DECLARATION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Type getPredecessorType() {
		return predecessorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPredecessorType(Type newPredecessorType) {
		Type oldPredecessorType = predecessorType;
		predecessorType = newPredecessorType == null ? PREDECESSOR_TYPE_EDEFAULT : newPredecessorType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ITERATOR_DECLARATION__PREDECESSOR_TYPE, oldPredecessorType, predecessorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Statement> getStatements() {
		if (statements == null) {
			statements = new EObjectContainmentEList<Statement>(Statement.class, this, CorePackage.ITERATOR_DECLARATION__STATEMENTS);
		}
		return statements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Type getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(Type newType) {
		Type oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ITERATOR_DECLARATION__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Parameter getIteratedParameter() {
		return iteratedParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIteratedParameter(Parameter newIteratedParameter, NotificationChain msgs) {
		Parameter oldIteratedParameter = iteratedParameter;
		iteratedParameter = newIteratedParameter;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER, oldIteratedParameter, newIteratedParameter);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setIteratedParameter(Parameter newIteratedParameter) {
		if (newIteratedParameter != iteratedParameter) {
			NotificationChain msgs = null;
			if (iteratedParameter != null)
				msgs = ((InternalEObject)iteratedParameter).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER, null, msgs);
			if (newIteratedParameter != null)
				msgs = ((InternalEObject)newIteratedParameter).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER, null, msgs);
			msgs = basicSetIteratedParameter(newIteratedParameter, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER, newIteratedParameter, newIteratedParameter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case CorePackage.ITERATOR_DECLARATION__STATEMENTS:
				return ((InternalEList<?>)getStatements()).basicRemove(otherEnd, msgs);
			case CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER:
				return basicSetIteratedParameter(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case CorePackage.ITERATOR_DECLARATION__NAME:
				return getName();
			case CorePackage.ITERATOR_DECLARATION__PREDECESSOR_TYPE:
				return getPredecessorType();
			case CorePackage.ITERATOR_DECLARATION__STATEMENTS:
				return getStatements();
			case CorePackage.ITERATOR_DECLARATION__TYPE:
				return getType();
			case CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER:
				return getIteratedParameter();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case CorePackage.ITERATOR_DECLARATION__NAME:
				setName((String)newValue);
				return;
			case CorePackage.ITERATOR_DECLARATION__PREDECESSOR_TYPE:
				setPredecessorType((Type)newValue);
				return;
			case CorePackage.ITERATOR_DECLARATION__STATEMENTS:
				getStatements().clear();
				getStatements().addAll((Collection<? extends Statement>)newValue);
				return;
			case CorePackage.ITERATOR_DECLARATION__TYPE:
				setType((Type)newValue);
				return;
			case CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER:
				setIteratedParameter((Parameter)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case CorePackage.ITERATOR_DECLARATION__NAME:
				setName(NAME_EDEFAULT);
				return;
			case CorePackage.ITERATOR_DECLARATION__PREDECESSOR_TYPE:
				setPredecessorType(PREDECESSOR_TYPE_EDEFAULT);
				return;
			case CorePackage.ITERATOR_DECLARATION__STATEMENTS:
				getStatements().clear();
				return;
			case CorePackage.ITERATOR_DECLARATION__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER:
				setIteratedParameter((Parameter)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case CorePackage.ITERATOR_DECLARATION__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case CorePackage.ITERATOR_DECLARATION__PREDECESSOR_TYPE:
				return predecessorType != PREDECESSOR_TYPE_EDEFAULT;
			case CorePackage.ITERATOR_DECLARATION__STATEMENTS:
				return statements != null && !statements.isEmpty();
			case CorePackage.ITERATOR_DECLARATION__TYPE:
				return type != TYPE_EDEFAULT;
			case CorePackage.ITERATOR_DECLARATION__ITERATED_PARAMETER:
				return iteratedParameter != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case CorePackage.ITERATOR_DECLARATION__NAME: return CorePackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == ReferenceableElement.class) {
			switch (derivedFeatureID) {
				case CorePackage.ITERATOR_DECLARATION__PREDECESSOR_TYPE: return CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE;
				default: return -1;
			}
		}
		if (baseClass == StatementContainer.class) {
			switch (derivedFeatureID) {
				case CorePackage.ITERATOR_DECLARATION__STATEMENTS: return StatementsPackage.STATEMENT_CONTAINER__STATEMENTS;
				default: return -1;
			}
		}
		if (baseClass == TypedElement.class) {
			switch (derivedFeatureID) {
				case CorePackage.ITERATOR_DECLARATION__TYPE: return TypesPackage.TYPED_ELEMENT__TYPE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case CorePackage.NAMED_ELEMENT__NAME: return CorePackage.ITERATOR_DECLARATION__NAME;
				default: return -1;
			}
		}
		if (baseClass == ReferenceableElement.class) {
			switch (baseFeatureID) {
				case CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE: return CorePackage.ITERATOR_DECLARATION__PREDECESSOR_TYPE;
				default: return -1;
			}
		}
		if (baseClass == StatementContainer.class) {
			switch (baseFeatureID) {
				case StatementsPackage.STATEMENT_CONTAINER__STATEMENTS: return CorePackage.ITERATOR_DECLARATION__STATEMENTS;
				default: return -1;
			}
		}
		if (baseClass == TypedElement.class) {
			switch (baseFeatureID) {
				case TypesPackage.TYPED_ELEMENT__TYPE: return CorePackage.ITERATOR_DECLARATION__TYPE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", predecessorType: ");
		result.append(predecessorType);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //IteratorDeclarationImpl
