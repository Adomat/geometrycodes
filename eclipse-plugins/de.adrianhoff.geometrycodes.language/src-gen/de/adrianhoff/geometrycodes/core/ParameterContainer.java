/**
 */
package de.adrianhoff.geometrycodes.core;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.ParameterContainer#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getParameterContainer()
 * @model abstract="true"
 * @generated
 */
public interface ParameterContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link de.adrianhoff.geometrycodes.core.Parameter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getParameterContainer_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Parameter> getParameters();

} // ParameterContainer
