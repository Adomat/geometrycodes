/**
 */
package de.adrianhoff.geometrycodes.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Boolean Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.MaterialBooleanProperty#isValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getMaterialBooleanProperty()
 * @model
 * @generated
 */
public interface MaterialBooleanProperty extends MaterialProperty {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(boolean)
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getMaterialBooleanProperty_Value()
	 * @model required="true"
	 * @generated
	 */
	boolean isValue();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.core.MaterialBooleanProperty#isValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isValue()
	 * @generated
	 */
	void setValue(boolean value);

} // MaterialBooleanProperty
