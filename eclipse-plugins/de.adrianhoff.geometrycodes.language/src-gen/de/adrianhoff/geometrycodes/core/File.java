/**
 */
package de.adrianhoff.geometrycodes.core;

import de.adrianhoff.geometrycodes.statements.OutputStatement;
import de.adrianhoff.geometrycodes.statements.StatementContainer;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>File</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.File#getImports <em>Imports</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.File#getMaterials <em>Materials</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.File#getFunctions <em>Functions</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.core.File#getIterators <em>Iterators</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getFile()
 * @model
 * @generated
 */
public interface File extends StatementContainer {
	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
	 * The list contents are of type {@link de.adrianhoff.geometrycodes.core.ImportStatement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference list.
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getFile_Imports()
	 * @model containment="true"
	 * @generated
	 */
	EList<ImportStatement> getImports();

	/**
	 * Returns the value of the '<em><b>Materials</b></em>' containment reference list.
	 * The list contents are of type {@link de.adrianhoff.geometrycodes.core.MaterialDeclaration}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Materials</em>' containment reference list.
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getFile_Materials()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialDeclaration> getMaterials();

	/**
	 * Returns the value of the '<em><b>Functions</b></em>' containment reference list.
	 * The list contents are of type {@link de.adrianhoff.geometrycodes.core.FunctionDeclaration}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functions</em>' containment reference list.
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getFile_Functions()
	 * @model containment="true"
	 * @generated
	 */
	EList<FunctionDeclaration> getFunctions();

	/**
	 * Returns the value of the '<em><b>Iterators</b></em>' containment reference list.
	 * The list contents are of type {@link de.adrianhoff.geometrycodes.core.IteratorDeclaration}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterators</em>' containment reference list.
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getFile_Iterators()
	 * @model containment="true"
	 * @generated
	 */
	EList<IteratorDeclaration> getIterators();

} // File
