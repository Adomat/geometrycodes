/**
 */
package de.adrianhoff.geometrycodes.core;

import de.adrianhoff.geometrycodes.types.Type;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referenceable Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.ReferenceableElement#getPredecessorType <em>Predecessor Type</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getReferenceableElement()
 * @model abstract="true"
 * @generated
 */
public interface ReferenceableElement extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Predecessor Type</b></em>' attribute.
	 * The default value is <code>"VOID"</code>.
	 * The literals are from the enumeration {@link de.adrianhoff.geometrycodes.types.Type}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Predecessor Type</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.types.Type
	 * @see #setPredecessorType(Type)
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getReferenceableElement_PredecessorType()
	 * @model default="VOID" required="true"
	 * @generated
	 */
	Type getPredecessorType();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.core.ReferenceableElement#getPredecessorType <em>Predecessor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Predecessor Type</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.types.Type
	 * @see #getPredecessorType()
	 * @generated
	 */
	void setPredecessorType(Type value);

} // ReferenceableElement
