/**
 */
package de.adrianhoff.geometrycodes.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Property</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getMaterialProperty()
 * @model abstract="true"
 * @generated
 */
public interface MaterialProperty extends NamedElement {
} // MaterialProperty
