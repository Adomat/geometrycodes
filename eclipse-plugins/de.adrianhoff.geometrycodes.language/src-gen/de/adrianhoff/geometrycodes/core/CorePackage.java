/**
 */
package de.adrianhoff.geometrycodes.core;

import de.adrianhoff.geometrycodes.statements.StatementsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.adrianhoff.geometrycodes.core.CoreFactory
 * @model kind="package"
 * @generated
 */
public interface CorePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "core";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.adrianhoff.de/GeometryCodes/core";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "core";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CorePackage eINSTANCE = de.adrianhoff.geometrycodes.core.impl.CorePackageImpl.init();

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.NamedElementImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.ReferenceableElementImpl <em>Referenceable Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.ReferenceableElementImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getReferenceableElement()
	 * @generated
	 */
	int REFERENCEABLE_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCEABLE_ELEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Predecessor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Referenceable Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCEABLE_ELEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.FileImpl <em>File</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.FileImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getFile()
	 * @generated
	 */
	int FILE = 2;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__STATEMENTS = StatementsPackage.STATEMENT_CONTAINER__STATEMENTS;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__IMPORTS = StatementsPackage.STATEMENT_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Materials</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__MATERIALS = StatementsPackage.STATEMENT_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__FUNCTIONS = StatementsPackage.STATEMENT_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Iterators</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE__ITERATORS = StatementsPackage.STATEMENT_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>File</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FILE_FEATURE_COUNT = StatementsPackage.STATEMENT_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.ImportStatementImpl <em>Import Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.ImportStatementImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getImportStatement()
	 * @generated
	 */
	int IMPORT_STATEMENT = 3;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_STATEMENT__IMPORT_URI = 0;

	/**
	 * The number of structural features of the '<em>Import Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPORT_STATEMENT_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.ParameterContainerImpl <em>Parameter Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.ParameterContainerImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getParameterContainer()
	 * @generated
	 */
	int PARAMETER_CONTAINER = 11;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_CONTAINER__PARAMETERS = 0;

	/**
	 * The number of structural features of the '<em>Parameter Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.FunctionDeclarationImpl <em>Function Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.FunctionDeclarationImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getFunctionDeclaration()
	 * @generated
	 */
	int FUNCTION_DECLARATION = 4;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_DECLARATION__PARAMETERS = PARAMETER_CONTAINER__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_DECLARATION__NAME = PARAMETER_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Predecessor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_DECLARATION__PREDECESSOR_TYPE = PARAMETER_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_DECLARATION__STATEMENTS = PARAMETER_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_DECLARATION__TYPE = PARAMETER_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Function Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_DECLARATION_FEATURE_COUNT = PARAMETER_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl <em>Iterator Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getIteratorDeclaration()
	 * @generated
	 */
	int ITERATOR_DECLARATION = 5;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATOR_DECLARATION__PARAMETERS = PARAMETER_CONTAINER__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATOR_DECLARATION__NAME = PARAMETER_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Predecessor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATOR_DECLARATION__PREDECESSOR_TYPE = PARAMETER_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATOR_DECLARATION__STATEMENTS = PARAMETER_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATOR_DECLARATION__TYPE = PARAMETER_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Iterated Parameter</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATOR_DECLARATION__ITERATED_PARAMETER = PARAMETER_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Iterator Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ITERATOR_DECLARATION_FEATURE_COUNT = PARAMETER_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialDeclarationImpl <em>Material Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.MaterialDeclarationImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialDeclaration()
	 * @generated
	 */
	int MATERIAL_DECLARATION = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DECLARATION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DECLARATION__PROPERTIES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_DECLARATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialPropertyImpl <em>Material Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.MaterialPropertyImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialProperty()
	 * @generated
	 */
	int MATERIAL_PROPERTY = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_PROPERTY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Material Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_PROPERTY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialStringPropertyImpl <em>Material String Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.MaterialStringPropertyImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialStringProperty()
	 * @generated
	 */
	int MATERIAL_STRING_PROPERTY = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_STRING_PROPERTY__NAME = MATERIAL_PROPERTY__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_STRING_PROPERTY__VALUE = MATERIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material String Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_STRING_PROPERTY_FEATURE_COUNT = MATERIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialFloatPropertyImpl <em>Material Float Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.MaterialFloatPropertyImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialFloatProperty()
	 * @generated
	 */
	int MATERIAL_FLOAT_PROPERTY = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_FLOAT_PROPERTY__NAME = MATERIAL_PROPERTY__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_FLOAT_PROPERTY__VALUE = MATERIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material Float Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_FLOAT_PROPERTY_FEATURE_COUNT = MATERIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialBooleanPropertyImpl <em>Material Boolean Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.MaterialBooleanPropertyImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialBooleanProperty()
	 * @generated
	 */
	int MATERIAL_BOOLEAN_PROPERTY = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_BOOLEAN_PROPERTY__NAME = MATERIAL_PROPERTY__NAME;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_BOOLEAN_PROPERTY__VALUE = MATERIAL_PROPERTY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Material Boolean Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MATERIAL_BOOLEAN_PROPERTY_FEATURE_COUNT = MATERIAL_PROPERTY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.core.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.core.impl.ParameterImpl
	 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = REFERENCEABLE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Predecessor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__PREDECESSOR_TYPE = REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__TYPE = REFERENCEABLE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = REFERENCEABLE_ELEMENT_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see de.adrianhoff.geometrycodes.core.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.core.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.adrianhoff.geometrycodes.core.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.ReferenceableElement <em>Referenceable Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Referenceable Element</em>'.
	 * @see de.adrianhoff.geometrycodes.core.ReferenceableElement
	 * @generated
	 */
	EClass getReferenceableElement();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.core.ReferenceableElement#getPredecessorType <em>Predecessor Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Predecessor Type</em>'.
	 * @see de.adrianhoff.geometrycodes.core.ReferenceableElement#getPredecessorType()
	 * @see #getReferenceableElement()
	 * @generated
	 */
	EAttribute getReferenceableElement_PredecessorType();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.File <em>File</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>File</em>'.
	 * @see de.adrianhoff.geometrycodes.core.File
	 * @generated
	 */
	EClass getFile();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.core.File#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see de.adrianhoff.geometrycodes.core.File#getImports()
	 * @see #getFile()
	 * @generated
	 */
	EReference getFile_Imports();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.core.File#getMaterials <em>Materials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Materials</em>'.
	 * @see de.adrianhoff.geometrycodes.core.File#getMaterials()
	 * @see #getFile()
	 * @generated
	 */
	EReference getFile_Materials();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.core.File#getFunctions <em>Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Functions</em>'.
	 * @see de.adrianhoff.geometrycodes.core.File#getFunctions()
	 * @see #getFile()
	 * @generated
	 */
	EReference getFile_Functions();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.core.File#getIterators <em>Iterators</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Iterators</em>'.
	 * @see de.adrianhoff.geometrycodes.core.File#getIterators()
	 * @see #getFile()
	 * @generated
	 */
	EReference getFile_Iterators();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.ImportStatement <em>Import Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Import Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.core.ImportStatement
	 * @generated
	 */
	EClass getImportStatement();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.core.ImportStatement#getImportURI <em>Import URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import URI</em>'.
	 * @see de.adrianhoff.geometrycodes.core.ImportStatement#getImportURI()
	 * @see #getImportStatement()
	 * @generated
	 */
	EAttribute getImportStatement_ImportURI();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.FunctionDeclaration <em>Function Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Declaration</em>'.
	 * @see de.adrianhoff.geometrycodes.core.FunctionDeclaration
	 * @generated
	 */
	EClass getFunctionDeclaration();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.IteratorDeclaration <em>Iterator Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Iterator Declaration</em>'.
	 * @see de.adrianhoff.geometrycodes.core.IteratorDeclaration
	 * @generated
	 */
	EClass getIteratorDeclaration();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.core.IteratorDeclaration#getIteratedParameter <em>Iterated Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Iterated Parameter</em>'.
	 * @see de.adrianhoff.geometrycodes.core.IteratorDeclaration#getIteratedParameter()
	 * @see #getIteratorDeclaration()
	 * @generated
	 */
	EReference getIteratorDeclaration_IteratedParameter();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.MaterialDeclaration <em>Material Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Declaration</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialDeclaration
	 * @generated
	 */
	EClass getMaterialDeclaration();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.core.MaterialDeclaration#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Properties</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialDeclaration#getProperties()
	 * @see #getMaterialDeclaration()
	 * @generated
	 */
	EReference getMaterialDeclaration_Properties();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.MaterialProperty <em>Material Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Property</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialProperty
	 * @generated
	 */
	EClass getMaterialProperty();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.MaterialStringProperty <em>Material String Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material String Property</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialStringProperty
	 * @generated
	 */
	EClass getMaterialStringProperty();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.core.MaterialStringProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialStringProperty#getValue()
	 * @see #getMaterialStringProperty()
	 * @generated
	 */
	EAttribute getMaterialStringProperty_Value();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.MaterialFloatProperty <em>Material Float Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Float Property</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialFloatProperty
	 * @generated
	 */
	EClass getMaterialFloatProperty();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.core.MaterialFloatProperty#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialFloatProperty#getValue()
	 * @see #getMaterialFloatProperty()
	 * @generated
	 */
	EAttribute getMaterialFloatProperty_Value();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.MaterialBooleanProperty <em>Material Boolean Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Material Boolean Property</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialBooleanProperty
	 * @generated
	 */
	EClass getMaterialBooleanProperty();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.core.MaterialBooleanProperty#isValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see de.adrianhoff.geometrycodes.core.MaterialBooleanProperty#isValue()
	 * @see #getMaterialBooleanProperty()
	 * @generated
	 */
	EAttribute getMaterialBooleanProperty_Value();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.ParameterContainer <em>Parameter Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter Container</em>'.
	 * @see de.adrianhoff.geometrycodes.core.ParameterContainer
	 * @generated
	 */
	EClass getParameterContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.core.ParameterContainer#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see de.adrianhoff.geometrycodes.core.ParameterContainer#getParameters()
	 * @see #getParameterContainer()
	 * @generated
	 */
	EReference getParameterContainer_Parameters();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.core.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see de.adrianhoff.geometrycodes.core.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoreFactory getCoreFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.NamedElementImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.ReferenceableElementImpl <em>Referenceable Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.ReferenceableElementImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getReferenceableElement()
		 * @generated
		 */
		EClass REFERENCEABLE_ELEMENT = eINSTANCE.getReferenceableElement();

		/**
		 * The meta object literal for the '<em><b>Predecessor Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE = eINSTANCE.getReferenceableElement_PredecessorType();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.FileImpl <em>File</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.FileImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getFile()
		 * @generated
		 */
		EClass FILE = eINSTANCE.getFile();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILE__IMPORTS = eINSTANCE.getFile_Imports();

		/**
		 * The meta object literal for the '<em><b>Materials</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILE__MATERIALS = eINSTANCE.getFile_Materials();

		/**
		 * The meta object literal for the '<em><b>Functions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILE__FUNCTIONS = eINSTANCE.getFile_Functions();

		/**
		 * The meta object literal for the '<em><b>Iterators</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FILE__ITERATORS = eINSTANCE.getFile_Iterators();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.ImportStatementImpl <em>Import Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.ImportStatementImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getImportStatement()
		 * @generated
		 */
		EClass IMPORT_STATEMENT = eINSTANCE.getImportStatement();

		/**
		 * The meta object literal for the '<em><b>Import URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMPORT_STATEMENT__IMPORT_URI = eINSTANCE.getImportStatement_ImportURI();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.FunctionDeclarationImpl <em>Function Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.FunctionDeclarationImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getFunctionDeclaration()
		 * @generated
		 */
		EClass FUNCTION_DECLARATION = eINSTANCE.getFunctionDeclaration();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl <em>Iterator Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.IteratorDeclarationImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getIteratorDeclaration()
		 * @generated
		 */
		EClass ITERATOR_DECLARATION = eINSTANCE.getIteratorDeclaration();

		/**
		 * The meta object literal for the '<em><b>Iterated Parameter</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ITERATOR_DECLARATION__ITERATED_PARAMETER = eINSTANCE.getIteratorDeclaration_IteratedParameter();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialDeclarationImpl <em>Material Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.MaterialDeclarationImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialDeclaration()
		 * @generated
		 */
		EClass MATERIAL_DECLARATION = eINSTANCE.getMaterialDeclaration();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MATERIAL_DECLARATION__PROPERTIES = eINSTANCE.getMaterialDeclaration_Properties();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialPropertyImpl <em>Material Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.MaterialPropertyImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialProperty()
		 * @generated
		 */
		EClass MATERIAL_PROPERTY = eINSTANCE.getMaterialProperty();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialStringPropertyImpl <em>Material String Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.MaterialStringPropertyImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialStringProperty()
		 * @generated
		 */
		EClass MATERIAL_STRING_PROPERTY = eINSTANCE.getMaterialStringProperty();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_STRING_PROPERTY__VALUE = eINSTANCE.getMaterialStringProperty_Value();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialFloatPropertyImpl <em>Material Float Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.MaterialFloatPropertyImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialFloatProperty()
		 * @generated
		 */
		EClass MATERIAL_FLOAT_PROPERTY = eINSTANCE.getMaterialFloatProperty();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_FLOAT_PROPERTY__VALUE = eINSTANCE.getMaterialFloatProperty_Value();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.MaterialBooleanPropertyImpl <em>Material Boolean Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.MaterialBooleanPropertyImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getMaterialBooleanProperty()
		 * @generated
		 */
		EClass MATERIAL_BOOLEAN_PROPERTY = eINSTANCE.getMaterialBooleanProperty();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MATERIAL_BOOLEAN_PROPERTY__VALUE = eINSTANCE.getMaterialBooleanProperty_Value();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.ParameterContainerImpl <em>Parameter Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.ParameterContainerImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getParameterContainer()
		 * @generated
		 */
		EClass PARAMETER_CONTAINER = eINSTANCE.getParameterContainer();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARAMETER_CONTAINER__PARAMETERS = eINSTANCE.getParameterContainer_Parameters();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.core.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.core.impl.ParameterImpl
		 * @see de.adrianhoff.geometrycodes.core.impl.CorePackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

	}

} //CorePackage
