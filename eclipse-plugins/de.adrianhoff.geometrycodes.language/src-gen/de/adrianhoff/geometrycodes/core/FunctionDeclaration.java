/**
 */
package de.adrianhoff.geometrycodes.core;

import de.adrianhoff.geometrycodes.statements.StatementContainer;

import de.adrianhoff.geometrycodes.types.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getFunctionDeclaration()
 * @model
 * @generated
 */
public interface FunctionDeclaration extends ParameterContainer, ReferenceableElement, StatementContainer, TypedElement {
} // FunctionDeclaration
