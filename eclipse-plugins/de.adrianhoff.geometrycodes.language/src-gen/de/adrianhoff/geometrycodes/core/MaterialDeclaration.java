/**
 */
package de.adrianhoff.geometrycodes.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Material Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.core.MaterialDeclaration#getProperties <em>Properties</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.core.CorePackage#getMaterialDeclaration()
 * @model
 * @generated
 */
public interface MaterialDeclaration extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Properties</b></em>' containment reference list.
	 * The list contents are of type {@link de.adrianhoff.geometrycodes.core.MaterialProperty}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Properties</em>' containment reference list.
	 * @see de.adrianhoff.geometrycodes.core.CorePackage#getMaterialDeclaration_Properties()
	 * @model containment="true"
	 * @generated
	 */
	EList<MaterialProperty> getProperties();

} // MaterialDeclaration
