/**
 */
package de.adrianhoff.geometrycodes.types;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Typed Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.types.TypedElement#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.types.TypesPackage#getTypedElement()
 * @model abstract="true"
 * @generated
 */
public interface TypedElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link de.adrianhoff.geometrycodes.types.Type}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.types.Type
	 * @see #setType(Type)
	 * @see de.adrianhoff.geometrycodes.types.TypesPackage#getTypedElement_Type()
	 * @model required="true"
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.types.TypedElement#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.types.Type
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

} // TypedElement
