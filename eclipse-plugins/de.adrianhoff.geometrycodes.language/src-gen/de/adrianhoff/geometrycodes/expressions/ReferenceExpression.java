/**
 */
package de.adrianhoff.geometrycodes.expressions;

import de.adrianhoff.geometrycodes.core.ReferenceableElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.ReferenceExpression#getTarget <em>Target</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.ReferenceExpression#getNext <em>Next</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getReferenceExpression()
 * @model
 * @generated
 */
public interface ReferenceExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ReferenceableElement)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getReferenceExpression_Target()
	 * @model required="true"
	 * @generated
	 */
	ReferenceableElement getTarget();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.ReferenceExpression#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ReferenceableElement value);

	/**
	 * Returns the value of the '<em><b>Next</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next</em>' containment reference.
	 * @see #setNext(ReferenceExpression)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getReferenceExpression_Next()
	 * @model containment="true"
	 * @generated
	 */
	ReferenceExpression getNext();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.ReferenceExpression#getNext <em>Next</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next</em>' containment reference.
	 * @see #getNext()
	 * @generated
	 */
	void setNext(ReferenceExpression value);

} // ReferenceExpression
