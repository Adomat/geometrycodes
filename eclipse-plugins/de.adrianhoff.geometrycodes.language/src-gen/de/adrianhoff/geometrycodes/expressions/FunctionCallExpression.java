/**
 */
package de.adrianhoff.geometrycodes.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Function Call Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getFunctionCallExpression()
 * @model
 * @generated
 */
public interface FunctionCallExpression extends ReferenceExpression, ArgumentContainer {
} // FunctionCallExpression
