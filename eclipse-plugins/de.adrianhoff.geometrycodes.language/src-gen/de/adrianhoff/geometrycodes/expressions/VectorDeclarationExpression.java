/**
 */
package de.adrianhoff.geometrycodes.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vector Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getVectorDeclarationExpression()
 * @model
 * @generated
 */
public interface VectorDeclarationExpression extends DeclarationExpression {
} // VectorDeclarationExpression
