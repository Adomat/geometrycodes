/**
 */
package de.adrianhoff.geometrycodes.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Triangle Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getTriangleDeclarationExpression()
 * @model
 * @generated
 */
public interface TriangleDeclarationExpression extends DeclarationExpression {
} // TriangleDeclarationExpression
