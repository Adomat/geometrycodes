/**
 */
package de.adrianhoff.geometrycodes.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.BinaryExpression#getLeftChild <em>Left Child</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.BinaryExpression#getRightChild <em>Right Child</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getBinaryExpression()
 * @model
 * @generated
 */
public interface BinaryExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Child</em>' containment reference.
	 * @see #setLeftChild(Expression)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getBinaryExpression_LeftChild()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getLeftChild();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.BinaryExpression#getLeftChild <em>Left Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Child</em>' containment reference.
	 * @see #getLeftChild()
	 * @generated
	 */
	void setLeftChild(Expression value);

	/**
	 * Returns the value of the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Child</em>' containment reference.
	 * @see #setRightChild(Expression)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getBinaryExpression_RightChild()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getRightChild();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.BinaryExpression#getRightChild <em>Right Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Child</em>' containment reference.
	 * @see #getRightChild()
	 * @generated
	 */
	void setRightChild(Expression value);

} // BinaryExpression
