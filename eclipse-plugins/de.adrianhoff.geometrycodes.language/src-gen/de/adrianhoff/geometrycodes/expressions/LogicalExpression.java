/**
 */
package de.adrianhoff.geometrycodes.expressions;

import de.adrianhoff.geometrycodes.operators.LogicalOperator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logical Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.LogicalExpression#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getLogicalExpression()
 * @model
 * @generated
 */
public interface LogicalExpression extends BinaryExpression {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link de.adrianhoff.geometrycodes.operators.LogicalOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.LogicalOperator
	 * @see #setOperator(LogicalOperator)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getLogicalExpression_Operator()
	 * @model required="true"
	 * @generated
	 */
	LogicalOperator getOperator();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.LogicalExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.LogicalOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(LogicalOperator value);

} // LogicalExpression
