/**
 */
package de.adrianhoff.geometrycodes.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Reference Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getVariableReferenceExpression()
 * @model
 * @generated
 */
public interface VariableReferenceExpression extends ReferenceExpression {
} // VariableReferenceExpression
