/**
 */
package de.adrianhoff.geometrycodes.expressions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsFactory
 * @model kind="package"
 * @generated
 */
public interface ExpressionsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "expressions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.adrianhoff.de/GeometryCodes/expressions";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "expressions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExpressionsPackage eINSTANCE = de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.ArgumentContainerImpl <em>Argument Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ArgumentContainerImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getArgumentContainer()
	 * @generated
	 */
	int ARGUMENT_CONTAINER = 0;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_CONTAINER__ARGUMENTS = 0;

	/**
	 * The number of structural features of the '<em>Argument Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ARGUMENT_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.ExpressionImpl <em>Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getExpression()
	 * @generated
	 */
	int EXPRESSION = 1;

	/**
	 * The number of structural features of the '<em>Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXPRESSION_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.UnaryExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getUnaryExpression()
	 * @generated
	 */
	int UNARY_EXPRESSION = 2;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__OPERATOR = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION__CHILD = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Unary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.BinaryExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getBinaryExpression()
	 * @generated
	 */
	int BINARY_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__LEFT_CHILD = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION__RIGHT_CHILD = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.LogicalExpressionImpl <em>Logical Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.LogicalExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getLogicalExpression()
	 * @generated
	 */
	int LOGICAL_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__LEFT_CHILD = BINARY_EXPRESSION__LEFT_CHILD;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__RIGHT_CHILD = BINARY_EXPRESSION__RIGHT_CHILD;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION__OPERATOR = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Logical Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.ConditionalExpressionImpl <em>Conditional Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ConditionalExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getConditionalExpression()
	 * @generated
	 */
	int CONDITIONAL_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__LEFT_CHILD = BINARY_EXPRESSION__LEFT_CHILD;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__RIGHT_CHILD = BINARY_EXPRESSION__RIGHT_CHILD;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION__OPERATOR = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Conditional Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.AdditiveExpressionImpl <em>Additive Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.AdditiveExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getAdditiveExpression()
	 * @generated
	 */
	int ADDITIVE_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDITIVE_EXPRESSION__LEFT_CHILD = BINARY_EXPRESSION__LEFT_CHILD;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDITIVE_EXPRESSION__RIGHT_CHILD = BINARY_EXPRESSION__RIGHT_CHILD;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDITIVE_EXPRESSION__OPERATOR = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Additive Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADDITIVE_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.MultiplicativeExpressionImpl <em>Multiplicative Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.MultiplicativeExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getMultiplicativeExpression()
	 * @generated
	 */
	int MULTIPLICATIVE_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Left Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICATIVE_EXPRESSION__LEFT_CHILD = BINARY_EXPRESSION__LEFT_CHILD;

	/**
	 * The feature id for the '<em><b>Right Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICATIVE_EXPRESSION__RIGHT_CHILD = BINARY_EXPRESSION__RIGHT_CHILD;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICATIVE_EXPRESSION__OPERATOR = BINARY_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Multiplicative Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICATIVE_EXPRESSION_FEATURE_COUNT = BINARY_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.ReferenceExpressionImpl <em>Reference Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ReferenceExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getReferenceExpression()
	 * @generated
	 */
	int REFERENCE_EXPRESSION = 8;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EXPRESSION__TARGET = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Next</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EXPRESSION__NEXT = EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Reference Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.VariableReferenceExpressionImpl <em>Variable Reference Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.VariableReferenceExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getVariableReferenceExpression()
	 * @generated
	 */
	int VARIABLE_REFERENCE_EXPRESSION = 9;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_EXPRESSION__TARGET = REFERENCE_EXPRESSION__TARGET;

	/**
	 * The feature id for the '<em><b>Next</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_EXPRESSION__NEXT = REFERENCE_EXPRESSION__NEXT;

	/**
	 * The number of structural features of the '<em>Variable Reference Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_REFERENCE_EXPRESSION_FEATURE_COUNT = REFERENCE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.FunctionCallExpressionImpl <em>Function Call Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.FunctionCallExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getFunctionCallExpression()
	 * @generated
	 */
	int FUNCTION_CALL_EXPRESSION = 10;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CALL_EXPRESSION__TARGET = REFERENCE_EXPRESSION__TARGET;

	/**
	 * The feature id for the '<em><b>Next</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CALL_EXPRESSION__NEXT = REFERENCE_EXPRESSION__NEXT;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CALL_EXPRESSION__ARGUMENTS = REFERENCE_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Function Call Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_CALL_EXPRESSION_FEATURE_COUNT = REFERENCE_EXPRESSION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.DeclarationExpressionImpl <em>Declaration Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.DeclarationExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getDeclarationExpression()
	 * @generated
	 */
	int DECLARATION_EXPRESSION = 11;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_EXPRESSION__ARGUMENTS = ARGUMENT_CONTAINER__ARGUMENTS;

	/**
	 * The number of structural features of the '<em>Declaration Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_EXPRESSION_FEATURE_COUNT = ARGUMENT_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.VectorDeclarationExpressionImpl <em>Vector Declaration Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.VectorDeclarationExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getVectorDeclarationExpression()
	 * @generated
	 */
	int VECTOR_DECLARATION_EXPRESSION = 12;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VECTOR_DECLARATION_EXPRESSION__ARGUMENTS = DECLARATION_EXPRESSION__ARGUMENTS;

	/**
	 * The number of structural features of the '<em>Vector Declaration Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VECTOR_DECLARATION_EXPRESSION_FEATURE_COUNT = DECLARATION_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.TriangleDeclarationExpressionImpl <em>Triangle Declaration Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.TriangleDeclarationExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getTriangleDeclarationExpression()
	 * @generated
	 */
	int TRIANGLE_DECLARATION_EXPRESSION = 13;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIANGLE_DECLARATION_EXPRESSION__ARGUMENTS = DECLARATION_EXPRESSION__ARGUMENTS;

	/**
	 * The number of structural features of the '<em>Triangle Declaration Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRIANGLE_DECLARATION_EXPRESSION_FEATURE_COUNT = DECLARATION_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.QuadDeclarationExpressionImpl <em>Quad Declaration Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.QuadDeclarationExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getQuadDeclarationExpression()
	 * @generated
	 */
	int QUAD_DECLARATION_EXPRESSION = 14;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUAD_DECLARATION_EXPRESSION__ARGUMENTS = DECLARATION_EXPRESSION__ARGUMENTS;

	/**
	 * The number of structural features of the '<em>Quad Declaration Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUAD_DECLARATION_EXPRESSION_FEATURE_COUNT = DECLARATION_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.VertexDeclarationExpressionImpl <em>Vertex Declaration Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.VertexDeclarationExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getVertexDeclarationExpression()
	 * @generated
	 */
	int VERTEX_DECLARATION_EXPRESSION = 15;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTEX_DECLARATION_EXPRESSION__ARGUMENTS = DECLARATION_EXPRESSION__ARGUMENTS;

	/**
	 * The number of structural features of the '<em>Vertex Declaration Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTEX_DECLARATION_EXPRESSION_FEATURE_COUNT = DECLARATION_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.expressions.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.expressions.impl.LiteralExpressionImpl
	 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getLiteralExpression()
	 * @generated
	 */
	int LITERAL_EXPRESSION = 16;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION__LITERAL = EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Literal Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LITERAL_EXPRESSION_FEATURE_COUNT = EXPRESSION_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.ArgumentContainer <em>Argument Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Argument Container</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.ArgumentContainer
	 * @generated
	 */
	EClass getArgumentContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.expressions.ArgumentContainer#getArguments <em>Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Arguments</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.ArgumentContainer#getArguments()
	 * @see #getArgumentContainer()
	 * @generated
	 */
	EReference getArgumentContainer_Arguments();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.Expression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.Expression
	 * @generated
	 */
	EClass getExpression();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.UnaryExpression <em>Unary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.UnaryExpression
	 * @generated
	 */
	EClass getUnaryExpression();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.expressions.UnaryExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.UnaryExpression#getOperator()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EAttribute getUnaryExpression_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.expressions.UnaryExpression#getChild <em>Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Child</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.UnaryExpression#getChild()
	 * @see #getUnaryExpression()
	 * @generated
	 */
	EReference getUnaryExpression_Child();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.BinaryExpression <em>Binary Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.BinaryExpression
	 * @generated
	 */
	EClass getBinaryExpression();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.expressions.BinaryExpression#getLeftChild <em>Left Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Left Child</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.BinaryExpression#getLeftChild()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_LeftChild();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.expressions.BinaryExpression#getRightChild <em>Right Child</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right Child</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.BinaryExpression#getRightChild()
	 * @see #getBinaryExpression()
	 * @generated
	 */
	EReference getBinaryExpression_RightChild();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.LogicalExpression <em>Logical Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.LogicalExpression
	 * @generated
	 */
	EClass getLogicalExpression();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.expressions.LogicalExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.LogicalExpression#getOperator()
	 * @see #getLogicalExpression()
	 * @generated
	 */
	EAttribute getLogicalExpression_Operator();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.ConditionalExpression <em>Conditional Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.ConditionalExpression
	 * @generated
	 */
	EClass getConditionalExpression();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.expressions.ConditionalExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.ConditionalExpression#getOperator()
	 * @see #getConditionalExpression()
	 * @generated
	 */
	EAttribute getConditionalExpression_Operator();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.AdditiveExpression <em>Additive Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Additive Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.AdditiveExpression
	 * @generated
	 */
	EClass getAdditiveExpression();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.expressions.AdditiveExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.AdditiveExpression#getOperator()
	 * @see #getAdditiveExpression()
	 * @generated
	 */
	EAttribute getAdditiveExpression_Operator();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.MultiplicativeExpression <em>Multiplicative Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplicative Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.MultiplicativeExpression
	 * @generated
	 */
	EClass getMultiplicativeExpression();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.expressions.MultiplicativeExpression#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.MultiplicativeExpression#getOperator()
	 * @see #getMultiplicativeExpression()
	 * @generated
	 */
	EAttribute getMultiplicativeExpression_Operator();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.ReferenceExpression <em>Reference Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.ReferenceExpression
	 * @generated
	 */
	EClass getReferenceExpression();

	/**
	 * Returns the meta object for the reference '{@link de.adrianhoff.geometrycodes.expressions.ReferenceExpression#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.ReferenceExpression#getTarget()
	 * @see #getReferenceExpression()
	 * @generated
	 */
	EReference getReferenceExpression_Target();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.expressions.ReferenceExpression#getNext <em>Next</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Next</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.ReferenceExpression#getNext()
	 * @see #getReferenceExpression()
	 * @generated
	 */
	EReference getReferenceExpression_Next();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.VariableReferenceExpression <em>Variable Reference Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable Reference Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.VariableReferenceExpression
	 * @generated
	 */
	EClass getVariableReferenceExpression();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.FunctionCallExpression <em>Function Call Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function Call Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.FunctionCallExpression
	 * @generated
	 */
	EClass getFunctionCallExpression();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.DeclarationExpression <em>Declaration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Declaration Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.DeclarationExpression
	 * @generated
	 */
	EClass getDeclarationExpression();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.VectorDeclarationExpression <em>Vector Declaration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vector Declaration Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.VectorDeclarationExpression
	 * @generated
	 */
	EClass getVectorDeclarationExpression();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.TriangleDeclarationExpression <em>Triangle Declaration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Triangle Declaration Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.TriangleDeclarationExpression
	 * @generated
	 */
	EClass getTriangleDeclarationExpression();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.QuadDeclarationExpression <em>Quad Declaration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quad Declaration Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.QuadDeclarationExpression
	 * @generated
	 */
	EClass getQuadDeclarationExpression();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.VertexDeclarationExpression <em>Vertex Declaration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertex Declaration Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.VertexDeclarationExpression
	 * @generated
	 */
	EClass getVertexDeclarationExpression();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.expressions.LiteralExpression <em>Literal Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Literal Expression</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.LiteralExpression
	 * @generated
	 */
	EClass getLiteralExpression();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.expressions.LiteralExpression#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see de.adrianhoff.geometrycodes.expressions.LiteralExpression#getLiteral()
	 * @see #getLiteralExpression()
	 * @generated
	 */
	EReference getLiteralExpression_Literal();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExpressionsFactory getExpressionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.ArgumentContainerImpl <em>Argument Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ArgumentContainerImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getArgumentContainer()
		 * @generated
		 */
		EClass ARGUMENT_CONTAINER = eINSTANCE.getArgumentContainer();

		/**
		 * The meta object literal for the '<em><b>Arguments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ARGUMENT_CONTAINER__ARGUMENTS = eINSTANCE.getArgumentContainer_Arguments();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.ExpressionImpl <em>Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getExpression()
		 * @generated
		 */
		EClass EXPRESSION = eINSTANCE.getExpression();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.UnaryExpressionImpl <em>Unary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.UnaryExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getUnaryExpression()
		 * @generated
		 */
		EClass UNARY_EXPRESSION = eINSTANCE.getUnaryExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_EXPRESSION__OPERATOR = eINSTANCE.getUnaryExpression_Operator();

		/**
		 * The meta object literal for the '<em><b>Child</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_EXPRESSION__CHILD = eINSTANCE.getUnaryExpression_Child();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.BinaryExpressionImpl <em>Binary Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.BinaryExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getBinaryExpression()
		 * @generated
		 */
		EClass BINARY_EXPRESSION = eINSTANCE.getBinaryExpression();

		/**
		 * The meta object literal for the '<em><b>Left Child</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__LEFT_CHILD = eINSTANCE.getBinaryExpression_LeftChild();

		/**
		 * The meta object literal for the '<em><b>Right Child</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EXPRESSION__RIGHT_CHILD = eINSTANCE.getBinaryExpression_RightChild();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.LogicalExpressionImpl <em>Logical Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.LogicalExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getLogicalExpression()
		 * @generated
		 */
		EClass LOGICAL_EXPRESSION = eINSTANCE.getLogicalExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGICAL_EXPRESSION__OPERATOR = eINSTANCE.getLogicalExpression_Operator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.ConditionalExpressionImpl <em>Conditional Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ConditionalExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getConditionalExpression()
		 * @generated
		 */
		EClass CONDITIONAL_EXPRESSION = eINSTANCE.getConditionalExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITIONAL_EXPRESSION__OPERATOR = eINSTANCE.getConditionalExpression_Operator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.AdditiveExpressionImpl <em>Additive Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.AdditiveExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getAdditiveExpression()
		 * @generated
		 */
		EClass ADDITIVE_EXPRESSION = eINSTANCE.getAdditiveExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADDITIVE_EXPRESSION__OPERATOR = eINSTANCE.getAdditiveExpression_Operator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.MultiplicativeExpressionImpl <em>Multiplicative Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.MultiplicativeExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getMultiplicativeExpression()
		 * @generated
		 */
		EClass MULTIPLICATIVE_EXPRESSION = eINSTANCE.getMultiplicativeExpression();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTIPLICATIVE_EXPRESSION__OPERATOR = eINSTANCE.getMultiplicativeExpression_Operator();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.ReferenceExpressionImpl <em>Reference Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ReferenceExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getReferenceExpression()
		 * @generated
		 */
		EClass REFERENCE_EXPRESSION = eINSTANCE.getReferenceExpression();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_EXPRESSION__TARGET = eINSTANCE.getReferenceExpression_Target();

		/**
		 * The meta object literal for the '<em><b>Next</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_EXPRESSION__NEXT = eINSTANCE.getReferenceExpression_Next();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.VariableReferenceExpressionImpl <em>Variable Reference Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.VariableReferenceExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getVariableReferenceExpression()
		 * @generated
		 */
		EClass VARIABLE_REFERENCE_EXPRESSION = eINSTANCE.getVariableReferenceExpression();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.FunctionCallExpressionImpl <em>Function Call Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.FunctionCallExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getFunctionCallExpression()
		 * @generated
		 */
		EClass FUNCTION_CALL_EXPRESSION = eINSTANCE.getFunctionCallExpression();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.DeclarationExpressionImpl <em>Declaration Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.DeclarationExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getDeclarationExpression()
		 * @generated
		 */
		EClass DECLARATION_EXPRESSION = eINSTANCE.getDeclarationExpression();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.VectorDeclarationExpressionImpl <em>Vector Declaration Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.VectorDeclarationExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getVectorDeclarationExpression()
		 * @generated
		 */
		EClass VECTOR_DECLARATION_EXPRESSION = eINSTANCE.getVectorDeclarationExpression();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.TriangleDeclarationExpressionImpl <em>Triangle Declaration Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.TriangleDeclarationExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getTriangleDeclarationExpression()
		 * @generated
		 */
		EClass TRIANGLE_DECLARATION_EXPRESSION = eINSTANCE.getTriangleDeclarationExpression();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.QuadDeclarationExpressionImpl <em>Quad Declaration Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.QuadDeclarationExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getQuadDeclarationExpression()
		 * @generated
		 */
		EClass QUAD_DECLARATION_EXPRESSION = eINSTANCE.getQuadDeclarationExpression();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.VertexDeclarationExpressionImpl <em>Vertex Declaration Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.VertexDeclarationExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getVertexDeclarationExpression()
		 * @generated
		 */
		EClass VERTEX_DECLARATION_EXPRESSION = eINSTANCE.getVertexDeclarationExpression();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.expressions.impl.LiteralExpressionImpl <em>Literal Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.expressions.impl.LiteralExpressionImpl
		 * @see de.adrianhoff.geometrycodes.expressions.impl.ExpressionsPackageImpl#getLiteralExpression()
		 * @generated
		 */
		EClass LITERAL_EXPRESSION = eINSTANCE.getLiteralExpression();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LITERAL_EXPRESSION__LITERAL = eINSTANCE.getLiteralExpression_Literal();

	}

} //ExpressionsPackage
