/**
 */
package de.adrianhoff.geometrycodes.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getDeclarationExpression()
 * @model abstract="true"
 * @generated
 */
public interface DeclarationExpression extends ArgumentContainer, Expression {
} // DeclarationExpression
