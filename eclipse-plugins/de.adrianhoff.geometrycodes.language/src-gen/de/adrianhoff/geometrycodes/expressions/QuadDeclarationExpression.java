/**
 */
package de.adrianhoff.geometrycodes.expressions;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quad Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getQuadDeclarationExpression()
 * @model
 * @generated
 */
public interface QuadDeclarationExpression extends DeclarationExpression {
} // QuadDeclarationExpression
