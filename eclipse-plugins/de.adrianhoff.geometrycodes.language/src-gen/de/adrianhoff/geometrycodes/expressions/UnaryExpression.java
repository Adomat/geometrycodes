/**
 */
package de.adrianhoff.geometrycodes.expressions;

import de.adrianhoff.geometrycodes.operators.UnaryOperator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.UnaryExpression#getOperator <em>Operator</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.UnaryExpression#getChild <em>Child</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getUnaryExpression()
 * @model
 * @generated
 */
public interface UnaryExpression extends Expression {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link de.adrianhoff.geometrycodes.operators.UnaryOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.UnaryOperator
	 * @see #setOperator(UnaryOperator)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getUnaryExpression_Operator()
	 * @model required="true"
	 * @generated
	 */
	UnaryOperator getOperator();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.UnaryExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.UnaryOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(UnaryOperator value);

	/**
	 * Returns the value of the '<em><b>Child</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Child</em>' containment reference.
	 * @see #setChild(Expression)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getUnaryExpression_Child()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getChild();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.UnaryExpression#getChild <em>Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Child</em>' containment reference.
	 * @see #getChild()
	 * @generated
	 */
	void setChild(Expression value);

} // UnaryExpression
