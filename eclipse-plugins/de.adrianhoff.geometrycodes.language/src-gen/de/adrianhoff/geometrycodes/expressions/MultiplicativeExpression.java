/**
 */
package de.adrianhoff.geometrycodes.expressions;

import de.adrianhoff.geometrycodes.operators.MultiplicativeOperator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplicative Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.MultiplicativeExpression#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getMultiplicativeExpression()
 * @model
 * @generated
 */
public interface MultiplicativeExpression extends BinaryExpression {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link de.adrianhoff.geometrycodes.operators.MultiplicativeOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.MultiplicativeOperator
	 * @see #setOperator(MultiplicativeOperator)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getMultiplicativeExpression_Operator()
	 * @model required="true"
	 * @generated
	 */
	MultiplicativeOperator getOperator();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.MultiplicativeExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.MultiplicativeOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(MultiplicativeOperator value);

} // MultiplicativeExpression
