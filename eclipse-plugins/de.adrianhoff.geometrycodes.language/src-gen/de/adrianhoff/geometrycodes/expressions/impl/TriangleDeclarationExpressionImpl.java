/**
 */
package de.adrianhoff.geometrycodes.expressions.impl;

import de.adrianhoff.geometrycodes.expressions.ExpressionsPackage;
import de.adrianhoff.geometrycodes.expressions.TriangleDeclarationExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Triangle Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class TriangleDeclarationExpressionImpl extends DeclarationExpressionImpl implements TriangleDeclarationExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TriangleDeclarationExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.TRIANGLE_DECLARATION_EXPRESSION;
	}

} //TriangleDeclarationExpressionImpl
