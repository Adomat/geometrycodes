/**
 */
package de.adrianhoff.geometrycodes.expressions.impl;

import de.adrianhoff.geometrycodes.expressions.DeclarationExpression;
import de.adrianhoff.geometrycodes.expressions.ExpressionsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DeclarationExpressionImpl extends ArgumentContainerImpl implements DeclarationExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeclarationExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.DECLARATION_EXPRESSION;
	}

} //DeclarationExpressionImpl
