/**
 */
package de.adrianhoff.geometrycodes.expressions.impl;

import de.adrianhoff.geometrycodes.expressions.BinaryExpression;
import de.adrianhoff.geometrycodes.expressions.Expression;
import de.adrianhoff.geometrycodes.expressions.ExpressionsPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Binary Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.impl.BinaryExpressionImpl#getLeftChild <em>Left Child</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.impl.BinaryExpressionImpl#getRightChild <em>Right Child</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BinaryExpressionImpl extends ExpressionImpl implements BinaryExpression {
	/**
	 * The cached value of the '{@link #getLeftChild() <em>Left Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftChild()
	 * @generated
	 * @ordered
	 */
	protected Expression leftChild;

	/**
	 * The cached value of the '{@link #getRightChild() <em>Right Child</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightChild()
	 * @generated
	 * @ordered
	 */
	protected Expression rightChild;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.BINARY_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Expression getLeftChild() {
		return leftChild;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLeftChild(Expression newLeftChild, NotificationChain msgs) {
		Expression oldLeftChild = leftChild;
		leftChild = newLeftChild;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD, oldLeftChild, newLeftChild);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setLeftChild(Expression newLeftChild) {
		if (newLeftChild != leftChild) {
			NotificationChain msgs = null;
			if (leftChild != null)
				msgs = ((InternalEObject)leftChild).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD, null, msgs);
			if (newLeftChild != null)
				msgs = ((InternalEObject)newLeftChild).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD, null, msgs);
			msgs = basicSetLeftChild(newLeftChild, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD, newLeftChild, newLeftChild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Expression getRightChild() {
		return rightChild;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRightChild(Expression newRightChild, NotificationChain msgs) {
		Expression oldRightChild = rightChild;
		rightChild = newRightChild;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD, oldRightChild, newRightChild);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setRightChild(Expression newRightChild) {
		if (newRightChild != rightChild) {
			NotificationChain msgs = null;
			if (rightChild != null)
				msgs = ((InternalEObject)rightChild).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD, null, msgs);
			if (newRightChild != null)
				msgs = ((InternalEObject)newRightChild).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD, null, msgs);
			msgs = basicSetRightChild(newRightChild, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD, newRightChild, newRightChild));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD:
				return basicSetLeftChild(null, msgs);
			case ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD:
				return basicSetRightChild(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD:
				return getLeftChild();
			case ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD:
				return getRightChild();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD:
				setLeftChild((Expression)newValue);
				return;
			case ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD:
				setRightChild((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD:
				setLeftChild((Expression)null);
				return;
			case ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD:
				setRightChild((Expression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ExpressionsPackage.BINARY_EXPRESSION__LEFT_CHILD:
				return leftChild != null;
			case ExpressionsPackage.BINARY_EXPRESSION__RIGHT_CHILD:
				return rightChild != null;
		}
		return super.eIsSet(featureID);
	}

} //BinaryExpressionImpl
