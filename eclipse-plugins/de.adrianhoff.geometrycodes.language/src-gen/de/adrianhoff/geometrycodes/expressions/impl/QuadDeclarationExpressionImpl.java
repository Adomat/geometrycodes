/**
 */
package de.adrianhoff.geometrycodes.expressions.impl;

import de.adrianhoff.geometrycodes.expressions.ExpressionsPackage;
import de.adrianhoff.geometrycodes.expressions.QuadDeclarationExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quad Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class QuadDeclarationExpressionImpl extends DeclarationExpressionImpl implements QuadDeclarationExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QuadDeclarationExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.QUAD_DECLARATION_EXPRESSION;
	}

} //QuadDeclarationExpressionImpl
