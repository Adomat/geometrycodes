/**
 */
package de.adrianhoff.geometrycodes.expressions;

import de.adrianhoff.geometrycodes.operators.AdditiveOperator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Additive Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.expressions.AdditiveExpression#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getAdditiveExpression()
 * @model
 * @generated
 */
public interface AdditiveExpression extends BinaryExpression {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link de.adrianhoff.geometrycodes.operators.AdditiveOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.AdditiveOperator
	 * @see #setOperator(AdditiveOperator)
	 * @see de.adrianhoff.geometrycodes.expressions.ExpressionsPackage#getAdditiveExpression_Operator()
	 * @model required="true"
	 * @generated
	 */
	AdditiveOperator getOperator();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.expressions.AdditiveExpression#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.AdditiveOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(AdditiveOperator value);

} // AdditiveExpression
