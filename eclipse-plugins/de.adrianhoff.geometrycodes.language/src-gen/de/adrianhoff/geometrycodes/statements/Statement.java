/**
 */
package de.adrianhoff.geometrycodes.statements;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getStatement()
 * @model abstract="true"
 * @generated
 */
public interface Statement extends EObject {
} // Statement
