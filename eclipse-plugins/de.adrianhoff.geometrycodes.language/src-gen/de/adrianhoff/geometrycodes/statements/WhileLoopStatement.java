/**
 */
package de.adrianhoff.geometrycodes.statements;

import de.adrianhoff.geometrycodes.expressions.Expression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>While Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.WhileLoopStatement#getCondition <em>Condition</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getWhileLoopStatement()
 * @model
 * @generated
 */
public interface WhileLoopStatement extends ConditionalStatement {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(Expression)
	 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getWhileLoopStatement_Condition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getCondition();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.statements.WhileLoopStatement#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(Expression value);

} // WhileLoopStatement
