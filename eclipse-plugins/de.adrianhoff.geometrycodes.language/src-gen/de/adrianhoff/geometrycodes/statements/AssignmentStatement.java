/**
 */
package de.adrianhoff.geometrycodes.statements;

import de.adrianhoff.geometrycodes.core.ReferenceableElement;

import de.adrianhoff.geometrycodes.expressions.Expression;
import de.adrianhoff.geometrycodes.operators.AssignmentOperator;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getTarget <em>Target</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getOperator <em>Operator</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getAssignmentStatement()
 * @model
 * @generated
 */
public interface AssignmentStatement extends Statement {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ReferenceableElement)
	 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getAssignmentStatement_Target()
	 * @model required="true"
	 * @generated
	 */
	ReferenceableElement getTarget();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ReferenceableElement value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link de.adrianhoff.geometrycodes.operators.AssignmentOperator}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.AssignmentOperator
	 * @see #setOperator(AssignmentOperator)
	 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getAssignmentStatement_Operator()
	 * @model required="true"
	 * @generated
	 */
	AssignmentOperator getOperator();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see de.adrianhoff.geometrycodes.operators.AssignmentOperator
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(AssignmentOperator value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Expression)
	 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getAssignmentStatement_Value()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getValue();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Expression value);

} // AssignmentStatement
