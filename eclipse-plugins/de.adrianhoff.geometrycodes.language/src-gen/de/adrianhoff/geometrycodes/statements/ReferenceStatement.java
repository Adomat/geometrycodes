/**
 */
package de.adrianhoff.geometrycodes.statements;

import de.adrianhoff.geometrycodes.expressions.ReferenceExpression;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reference Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.ReferenceStatement#getReference <em>Reference</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getReferenceStatement()
 * @model
 * @generated
 */
public interface ReferenceStatement extends Statement {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' containment reference.
	 * @see #setReference(ReferenceExpression)
	 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getReferenceStatement_Reference()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ReferenceExpression getReference();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.statements.ReferenceStatement#getReference <em>Reference</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' containment reference.
	 * @see #getReference()
	 * @generated
	 */
	void setReference(ReferenceExpression value);

} // ReferenceStatement
