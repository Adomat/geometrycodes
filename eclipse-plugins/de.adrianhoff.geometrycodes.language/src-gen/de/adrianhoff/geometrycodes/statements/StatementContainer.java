/**
 */
package de.adrianhoff.geometrycodes.statements;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.StatementContainer#getStatements <em>Statements</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getStatementContainer()
 * @model abstract="true"
 * @generated
 */
public interface StatementContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Statements</b></em>' containment reference list.
	 * The list contents are of type {@link de.adrianhoff.geometrycodes.statements.Statement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Statements</em>' containment reference list.
	 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getStatementContainer_Statements()
	 * @model containment="true"
	 * @generated
	 */
	EList<Statement> getStatements();

} // StatementContainer
