/**
 */
package de.adrianhoff.geometrycodes.statements;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.adrianhoff.geometrycodes.statements.StatementsFactory
 * @model kind="package"
 * @generated
 */
public interface StatementsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "statements";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.adrianhoff.de/GeometryCodes/statements";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "statements";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	StatementsPackage eINSTANCE = de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.StatementContainerImpl <em>Statement Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementContainerImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getStatementContainer()
	 * @generated
	 */
	int STATEMENT_CONTAINER = 0;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_CONTAINER__STATEMENTS = 0;

	/**
	 * The number of structural features of the '<em>Statement Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.StatementImpl <em>Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getStatement()
	 * @generated
	 */
	int STATEMENT = 1;

	/**
	 * The number of structural features of the '<em>Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.ConditionalStatementImpl <em>Conditional Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.ConditionalStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getConditionalStatement()
	 * @generated
	 */
	int CONDITIONAL_STATEMENT = 2;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_STATEMENT__STATEMENTS = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Conditional Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITIONAL_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.IfStatementImpl <em>If Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.IfStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getIfStatement()
	 * @generated
	 */
	int IF_STATEMENT = 3;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__STATEMENTS = CONDITIONAL_STATEMENT__STATEMENTS;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT__CONDITION = CONDITIONAL_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>If Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_STATEMENT_FEATURE_COUNT = CONDITIONAL_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.WhileLoopStatementImpl <em>While Loop Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.WhileLoopStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getWhileLoopStatement()
	 * @generated
	 */
	int WHILE_LOOP_STATEMENT = 4;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_LOOP_STATEMENT__STATEMENTS = CONDITIONAL_STATEMENT__STATEMENTS;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_LOOP_STATEMENT__CONDITION = CONDITIONAL_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>While Loop Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WHILE_LOOP_STATEMENT_FEATURE_COUNT = CONDITIONAL_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.ForLoopStatementImpl <em>For Loop Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.ForLoopStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getForLoopStatement()
	 * @generated
	 */
	int FOR_LOOP_STATEMENT = 5;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_LOOP_STATEMENT__STATEMENTS = CONDITIONAL_STATEMENT__STATEMENTS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_LOOP_STATEMENT__NAME = CONDITIONAL_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Predecessor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_LOOP_STATEMENT__PREDECESSOR_TYPE = CONDITIONAL_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_LOOP_STATEMENT__TYPE = CONDITIONAL_STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>From Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_LOOP_STATEMENT__FROM_VALUE = CONDITIONAL_STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>To Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_LOOP_STATEMENT__TO_VALUE = CONDITIONAL_STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>For Loop Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_LOOP_STATEMENT_FEATURE_COUNT = CONDITIONAL_STATEMENT_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.ForEachLoopStatementImpl <em>For Each Loop Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.ForEachLoopStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getForEachLoopStatement()
	 * @generated
	 */
	int FOR_EACH_LOOP_STATEMENT = 6;

	/**
	 * The feature id for the '<em><b>Statements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_LOOP_STATEMENT__STATEMENTS = CONDITIONAL_STATEMENT__STATEMENTS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_LOOP_STATEMENT__NAME = CONDITIONAL_STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Predecessor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE = CONDITIONAL_STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_LOOP_STATEMENT__TYPE = CONDITIONAL_STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_LOOP_STATEMENT__VALUE = CONDITIONAL_STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>For Each Loop Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_EACH_LOOP_STATEMENT_FEATURE_COUNT = CONDITIONAL_STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.ReferenceStatementImpl <em>Reference Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.ReferenceStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getReferenceStatement()
	 * @generated
	 */
	int REFERENCE_STATEMENT = 7;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_STATEMENT__REFERENCE = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Reference Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REFERENCE_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.DeclarationStatementImpl <em>Declaration Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.DeclarationStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getDeclarationStatement()
	 * @generated
	 */
	int DECLARATION_STATEMENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_STATEMENT__NAME = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Predecessor Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_STATEMENT__PREDECESSOR_TYPE = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_STATEMENT__TYPE = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_STATEMENT__VALUE = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Declaration Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECLARATION_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 4;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.AssignmentStatementImpl <em>Assignment Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.AssignmentStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getAssignmentStatement()
	 * @generated
	 */
	int ASSIGNMENT_STATEMENT = 9;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_STATEMENT__TARGET = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_STATEMENT__OPERATOR = STATEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_STATEMENT__VALUE = STATEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Assignment Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASSIGNMENT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.ReturnStatementImpl <em>Return Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.ReturnStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getReturnStatement()
	 * @generated
	 */
	int RETURN_STATEMENT = 10;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT__VALUE = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Return Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RETURN_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;


	/**
	 * The meta object id for the '{@link de.adrianhoff.geometrycodes.statements.impl.OutputStatementImpl <em>Output Statement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.adrianhoff.geometrycodes.statements.impl.OutputStatementImpl
	 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getOutputStatement()
	 * @generated
	 */
	int OUTPUT_STATEMENT = 11;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_STATEMENT__VALUE = STATEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Output Statement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OUTPUT_STATEMENT_FEATURE_COUNT = STATEMENT_FEATURE_COUNT + 1;


	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.StatementContainer <em>Statement Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Statement Container</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.StatementContainer
	 * @generated
	 */
	EClass getStatementContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.statements.StatementContainer#getStatements <em>Statements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Statements</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.StatementContainer#getStatements()
	 * @see #getStatementContainer()
	 * @generated
	 */
	EReference getStatementContainer_Statements();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.Statement <em>Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.Statement
	 * @generated
	 */
	EClass getStatement();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.ConditionalStatement <em>Conditional Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conditional Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ConditionalStatement
	 * @generated
	 */
	EClass getConditionalStatement();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.IfStatement <em>If Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.IfStatement
	 * @generated
	 */
	EClass getIfStatement();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.IfStatement#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.IfStatement#getCondition()
	 * @see #getIfStatement()
	 * @generated
	 */
	EReference getIfStatement_Condition();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.WhileLoopStatement <em>While Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>While Loop Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.WhileLoopStatement
	 * @generated
	 */
	EClass getWhileLoopStatement();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.WhileLoopStatement#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.WhileLoopStatement#getCondition()
	 * @see #getWhileLoopStatement()
	 * @generated
	 */
	EReference getWhileLoopStatement_Condition();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.ForLoopStatement <em>For Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Loop Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ForLoopStatement
	 * @generated
	 */
	EClass getForLoopStatement();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.ForLoopStatement#getFromValue <em>From Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>From Value</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ForLoopStatement#getFromValue()
	 * @see #getForLoopStatement()
	 * @generated
	 */
	EReference getForLoopStatement_FromValue();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.ForLoopStatement#getToValue <em>To Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>To Value</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ForLoopStatement#getToValue()
	 * @see #getForLoopStatement()
	 * @generated
	 */
	EReference getForLoopStatement_ToValue();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.ForEachLoopStatement <em>For Each Loop Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Each Loop Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ForEachLoopStatement
	 * @generated
	 */
	EClass getForEachLoopStatement();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.ForEachLoopStatement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ForEachLoopStatement#getValue()
	 * @see #getForEachLoopStatement()
	 * @generated
	 */
	EReference getForEachLoopStatement_Value();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.ReferenceStatement <em>Reference Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reference Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ReferenceStatement
	 * @generated
	 */
	EClass getReferenceStatement();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.ReferenceStatement#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Reference</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ReferenceStatement#getReference()
	 * @see #getReferenceStatement()
	 * @generated
	 */
	EReference getReferenceStatement_Reference();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.DeclarationStatement <em>Declaration Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Declaration Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.DeclarationStatement
	 * @generated
	 */
	EClass getDeclarationStatement();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.DeclarationStatement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.DeclarationStatement#getValue()
	 * @see #getDeclarationStatement()
	 * @generated
	 */
	EReference getDeclarationStatement_Value();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement <em>Assignment Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Assignment Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.AssignmentStatement
	 * @generated
	 */
	EClass getAssignmentStatement();

	/**
	 * Returns the meta object for the reference '{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.AssignmentStatement#getTarget()
	 * @see #getAssignmentStatement()
	 * @generated
	 */
	EReference getAssignmentStatement_Target();

	/**
	 * Returns the meta object for the attribute '{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.AssignmentStatement#getOperator()
	 * @see #getAssignmentStatement()
	 * @generated
	 */
	EAttribute getAssignmentStatement_Operator();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.AssignmentStatement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.AssignmentStatement#getValue()
	 * @see #getAssignmentStatement()
	 * @generated
	 */
	EReference getAssignmentStatement_Value();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.ReturnStatement <em>Return Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Return Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ReturnStatement
	 * @generated
	 */
	EClass getReturnStatement();

	/**
	 * Returns the meta object for the containment reference list '{@link de.adrianhoff.geometrycodes.statements.ReturnStatement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Value</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.ReturnStatement#getValue()
	 * @see #getReturnStatement()
	 * @generated
	 */
	EReference getReturnStatement_Value();

	/**
	 * Returns the meta object for class '{@link de.adrianhoff.geometrycodes.statements.OutputStatement <em>Output Statement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Output Statement</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.OutputStatement
	 * @generated
	 */
	EClass getOutputStatement();

	/**
	 * Returns the meta object for the containment reference '{@link de.adrianhoff.geometrycodes.statements.OutputStatement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see de.adrianhoff.geometrycodes.statements.OutputStatement#getValue()
	 * @see #getOutputStatement()
	 * @generated
	 */
	EReference getOutputStatement_Value();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	StatementsFactory getStatementsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.StatementContainerImpl <em>Statement Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementContainerImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getStatementContainer()
		 * @generated
		 */
		EClass STATEMENT_CONTAINER = eINSTANCE.getStatementContainer();

		/**
		 * The meta object literal for the '<em><b>Statements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STATEMENT_CONTAINER__STATEMENTS = eINSTANCE.getStatementContainer_Statements();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.StatementImpl <em>Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getStatement()
		 * @generated
		 */
		EClass STATEMENT = eINSTANCE.getStatement();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.ConditionalStatementImpl <em>Conditional Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.ConditionalStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getConditionalStatement()
		 * @generated
		 */
		EClass CONDITIONAL_STATEMENT = eINSTANCE.getConditionalStatement();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.IfStatementImpl <em>If Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.IfStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getIfStatement()
		 * @generated
		 */
		EClass IF_STATEMENT = eINSTANCE.getIfStatement();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_STATEMENT__CONDITION = eINSTANCE.getIfStatement_Condition();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.WhileLoopStatementImpl <em>While Loop Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.WhileLoopStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getWhileLoopStatement()
		 * @generated
		 */
		EClass WHILE_LOOP_STATEMENT = eINSTANCE.getWhileLoopStatement();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WHILE_LOOP_STATEMENT__CONDITION = eINSTANCE.getWhileLoopStatement_Condition();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.ForLoopStatementImpl <em>For Loop Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.ForLoopStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getForLoopStatement()
		 * @generated
		 */
		EClass FOR_LOOP_STATEMENT = eINSTANCE.getForLoopStatement();

		/**
		 * The meta object literal for the '<em><b>From Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_LOOP_STATEMENT__FROM_VALUE = eINSTANCE.getForLoopStatement_FromValue();

		/**
		 * The meta object literal for the '<em><b>To Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_LOOP_STATEMENT__TO_VALUE = eINSTANCE.getForLoopStatement_ToValue();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.ForEachLoopStatementImpl <em>For Each Loop Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.ForEachLoopStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getForEachLoopStatement()
		 * @generated
		 */
		EClass FOR_EACH_LOOP_STATEMENT = eINSTANCE.getForEachLoopStatement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_EACH_LOOP_STATEMENT__VALUE = eINSTANCE.getForEachLoopStatement_Value();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.ReferenceStatementImpl <em>Reference Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.ReferenceStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getReferenceStatement()
		 * @generated
		 */
		EClass REFERENCE_STATEMENT = eINSTANCE.getReferenceStatement();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REFERENCE_STATEMENT__REFERENCE = eINSTANCE.getReferenceStatement_Reference();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.DeclarationStatementImpl <em>Declaration Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.DeclarationStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getDeclarationStatement()
		 * @generated
		 */
		EClass DECLARATION_STATEMENT = eINSTANCE.getDeclarationStatement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECLARATION_STATEMENT__VALUE = eINSTANCE.getDeclarationStatement_Value();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.AssignmentStatementImpl <em>Assignment Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.AssignmentStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getAssignmentStatement()
		 * @generated
		 */
		EClass ASSIGNMENT_STATEMENT = eINSTANCE.getAssignmentStatement();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGNMENT_STATEMENT__TARGET = eINSTANCE.getAssignmentStatement_Target();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASSIGNMENT_STATEMENT__OPERATOR = eINSTANCE.getAssignmentStatement_Operator();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASSIGNMENT_STATEMENT__VALUE = eINSTANCE.getAssignmentStatement_Value();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.ReturnStatementImpl <em>Return Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.ReturnStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getReturnStatement()
		 * @generated
		 */
		EClass RETURN_STATEMENT = eINSTANCE.getReturnStatement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RETURN_STATEMENT__VALUE = eINSTANCE.getReturnStatement_Value();

		/**
		 * The meta object literal for the '{@link de.adrianhoff.geometrycodes.statements.impl.OutputStatementImpl <em>Output Statement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.adrianhoff.geometrycodes.statements.impl.OutputStatementImpl
		 * @see de.adrianhoff.geometrycodes.statements.impl.StatementsPackageImpl#getOutputStatement()
		 * @generated
		 */
		EClass OUTPUT_STATEMENT = eINSTANCE.getOutputStatement();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OUTPUT_STATEMENT__VALUE = eINSTANCE.getOutputStatement_Value();

	}

} //StatementsPackage
