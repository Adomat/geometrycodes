/**
 */
package de.adrianhoff.geometrycodes.statements;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conditional Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getConditionalStatement()
 * @model abstract="true"
 * @generated
 */
public interface ConditionalStatement extends Statement, StatementContainer {
} // ConditionalStatement
