/**
 */
package de.adrianhoff.geometrycodes.statements;

import de.adrianhoff.geometrycodes.core.ReferenceableElement;

import de.adrianhoff.geometrycodes.expressions.Expression;

import de.adrianhoff.geometrycodes.types.TypedElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.ForLoopStatement#getFromValue <em>From Value</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.ForLoopStatement#getToValue <em>To Value</em>}</li>
 * </ul>
 *
 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getForLoopStatement()
 * @model
 * @generated
 */
public interface ForLoopStatement extends ConditionalStatement, ReferenceableElement, TypedElement {
	/**
	 * Returns the value of the '<em><b>From Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Value</em>' containment reference.
	 * @see #setFromValue(Expression)
	 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getForLoopStatement_FromValue()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getFromValue();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.statements.ForLoopStatement#getFromValue <em>From Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Value</em>' containment reference.
	 * @see #getFromValue()
	 * @generated
	 */
	void setFromValue(Expression value);

	/**
	 * Returns the value of the '<em><b>To Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Value</em>' containment reference.
	 * @see #setToValue(Expression)
	 * @see de.adrianhoff.geometrycodes.statements.StatementsPackage#getForLoopStatement_ToValue()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Expression getToValue();

	/**
	 * Sets the value of the '{@link de.adrianhoff.geometrycodes.statements.ForLoopStatement#getToValue <em>To Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Value</em>' containment reference.
	 * @see #getToValue()
	 * @generated
	 */
	void setToValue(Expression value);

} // ForLoopStatement
