/**
 */
package de.adrianhoff.geometrycodes.statements.impl;

import de.adrianhoff.geometrycodes.core.CorePackage;
import de.adrianhoff.geometrycodes.core.NamedElement;
import de.adrianhoff.geometrycodes.core.ReferenceableElement;

import de.adrianhoff.geometrycodes.expressions.Expression;

import de.adrianhoff.geometrycodes.statements.ForEachLoopStatement;
import de.adrianhoff.geometrycodes.statements.StatementsPackage;

import de.adrianhoff.geometrycodes.types.Type;
import de.adrianhoff.geometrycodes.types.TypedElement;
import de.adrianhoff.geometrycodes.types.TypesPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Each Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.impl.ForEachLoopStatementImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.impl.ForEachLoopStatementImpl#getPredecessorType <em>Predecessor Type</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.impl.ForEachLoopStatementImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.adrianhoff.geometrycodes.statements.impl.ForEachLoopStatementImpl#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ForEachLoopStatementImpl extends ConditionalStatementImpl implements ForEachLoopStatement {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPredecessorType() <em>Predecessor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessorType()
	 * @generated
	 * @ordered
	 */
	protected static final Type PREDECESSOR_TYPE_EDEFAULT = Type.VOID;

	/**
	 * The cached value of the '{@link #getPredecessorType() <em>Predecessor Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPredecessorType()
	 * @generated
	 * @ordered
	 */
	protected Type predecessorType = PREDECESSOR_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final Type TYPE_EDEFAULT = Type.VOID;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected Expression value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForEachLoopStatementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return StatementsPackage.Literals.FOR_EACH_LOOP_STATEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatementsPackage.FOR_EACH_LOOP_STATEMENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Type getPredecessorType() {
		return predecessorType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPredecessorType(Type newPredecessorType) {
		Type oldPredecessorType = predecessorType;
		predecessorType = newPredecessorType == null ? PREDECESSOR_TYPE_EDEFAULT : newPredecessorType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatementsPackage.FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE, oldPredecessorType, predecessorType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Type getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setType(Type newType) {
		Type oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatementsPackage.FOR_EACH_LOOP_STATEMENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Expression getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(Expression newValue, NotificationChain msgs) {
		Expression oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE, oldValue, newValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setValue(Expression newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject)value).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE, null, msgs);
			if (newValue != null)
				msgs = ((InternalEObject)newValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE, null, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE, newValue, newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE:
				return basicSetValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__NAME:
				return getName();
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE:
				return getPredecessorType();
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__TYPE:
				return getType();
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE:
				return getValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__NAME:
				setName((String)newValue);
				return;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE:
				setPredecessorType((Type)newValue);
				return;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__TYPE:
				setType((Type)newValue);
				return;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE:
				setValue((Expression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE:
				setPredecessorType(PREDECESSOR_TYPE_EDEFAULT);
				return;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE:
				setValue((Expression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE:
				return predecessorType != PREDECESSOR_TYPE_EDEFAULT;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__TYPE:
				return type != TYPE_EDEFAULT;
			case StatementsPackage.FOR_EACH_LOOP_STATEMENT__VALUE:
				return value != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case StatementsPackage.FOR_EACH_LOOP_STATEMENT__NAME: return CorePackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == ReferenceableElement.class) {
			switch (derivedFeatureID) {
				case StatementsPackage.FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE: return CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE;
				default: return -1;
			}
		}
		if (baseClass == TypedElement.class) {
			switch (derivedFeatureID) {
				case StatementsPackage.FOR_EACH_LOOP_STATEMENT__TYPE: return TypesPackage.TYPED_ELEMENT__TYPE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case CorePackage.NAMED_ELEMENT__NAME: return StatementsPackage.FOR_EACH_LOOP_STATEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == ReferenceableElement.class) {
			switch (baseFeatureID) {
				case CorePackage.REFERENCEABLE_ELEMENT__PREDECESSOR_TYPE: return StatementsPackage.FOR_EACH_LOOP_STATEMENT__PREDECESSOR_TYPE;
				default: return -1;
			}
		}
		if (baseClass == TypedElement.class) {
			switch (baseFeatureID) {
				case TypesPackage.TYPED_ELEMENT__TYPE: return StatementsPackage.FOR_EACH_LOOP_STATEMENT__TYPE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", predecessorType: ");
		result.append(predecessorType);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //ForEachLoopStatementImpl
