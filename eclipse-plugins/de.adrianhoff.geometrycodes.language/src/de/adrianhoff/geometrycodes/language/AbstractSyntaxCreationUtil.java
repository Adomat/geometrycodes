package de.adrianhoff.geometrycodes.language;

import de.adrianhoff.geometrycodes.core.CoreFactory;
import de.adrianhoff.geometrycodes.core.FunctionDeclaration;
import de.adrianhoff.geometrycodes.core.IteratorDeclaration;
import de.adrianhoff.geometrycodes.core.Parameter;
import de.adrianhoff.geometrycodes.statements.DeclarationStatement;
import de.adrianhoff.geometrycodes.statements.StatementsFactory;
import de.adrianhoff.geometrycodes.types.Type;

public class AbstractSyntaxCreationUtil {
	
	public static FunctionDeclaration createFunctionDeclaration(Type predecessorType, Type returnType, String name) {
		FunctionDeclaration function = CoreFactory.eINSTANCE.createFunctionDeclaration();
		function.setPredecessorType(predecessorType);
		function.setType(returnType);
		function.setName(name);
		return function;
	}

	public static IteratorDeclaration createIteratorDeclaration(Type predecessorType, Type returnType, String name, Parameter iteratedParameter) {
		IteratorDeclaration iterator = CoreFactory.eINSTANCE.createIteratorDeclaration();
		iterator.setPredecessorType(predecessorType);
		iterator.setType(returnType);
		iterator.setName(name);
		iterator.setIteratedParameter(iteratedParameter);
		return iterator;
	}

	public static DeclarationStatement createDeclarationStatement(Type predecessorType, Type returnType, String name) {
		DeclarationStatement statement = StatementsFactory.eINSTANCE.createDeclarationStatement();
		statement.setPredecessorType(predecessorType);
		statement.setType(returnType);
		statement.setName(name);
		return statement;
	}
	
}
