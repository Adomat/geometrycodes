package de.adrianhoff.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.common.util.Enumerator;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import de.adrianhoff.services.GeometryCodesGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalGeometryCodesParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_FLOAT", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'import'", "'('", "','", "')'", "'{'", "'}'", "'iterator'", "'over'", "'each'", "'material'", "':'", "'yes'", "'no'", "'='", "'return'", "'nothing'", "'if'", "'for'", "'from'", "'to'", "'in'", "'while'", "'output'", "'.'", "'vector'", "'vertex'", "'triangle'", "'quad'", "'empty'", "'true'", "'false'", "'+='", "'!'", "'-'", "'+'", "'*'", "'/'", "'=='", "'!='", "'>'", "'>='", "'<'", "'<='", "'&&'", "'||'", "'void'", "'float'", "'mesh'"
    };
    public static final int T__50=50;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__59=59;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__55=55;
    public static final int T__12=12;
    public static final int T__56=56;
    public static final int T__13=13;
    public static final int T__57=57;
    public static final int T__14=14;
    public static final int T__58=58;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_ID=5;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=7;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=8;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_STRING=4;
    public static final int RULE_SL_COMMENT=9;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=10;
    public static final int RULE_ANY_OTHER=11;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int RULE_FLOAT=6;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalGeometryCodesParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalGeometryCodesParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalGeometryCodesParser.tokenNames; }
    public String getGrammarFileName() { return "InternalGeometryCodes.g"; }



     	private GeometryCodesGrammarAccess grammarAccess;

        public InternalGeometryCodesParser(TokenStream input, GeometryCodesGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "File";
       	}

       	@Override
       	protected GeometryCodesGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFile"
    // InternalGeometryCodes.g:65:1: entryRuleFile returns [EObject current=null] : iv_ruleFile= ruleFile EOF ;
    public final EObject entryRuleFile() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFile = null;


        try {
            // InternalGeometryCodes.g:65:45: (iv_ruleFile= ruleFile EOF )
            // InternalGeometryCodes.g:66:2: iv_ruleFile= ruleFile EOF
            {
             newCompositeNode(grammarAccess.getFileRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFile=ruleFile();

            state._fsp--;

             current =iv_ruleFile; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFile"


    // $ANTLR start "ruleFile"
    // InternalGeometryCodes.g:72:1: ruleFile returns [EObject current=null] : ( ( (lv_imports_0_0= ruleImportStatement ) )* ( ( (lv_functions_1_0= ruleFunctionDeclaration ) ) | ( (lv_iterators_2_0= ruleIteratorDeclaration ) ) | ( (lv_materials_3_0= ruleMaterialDeclaration ) ) | ( (lv_statements_4_0= ruleStatement ) ) )* ) ;
    public final EObject ruleFile() throws RecognitionException {
        EObject current = null;

        EObject lv_imports_0_0 = null;

        EObject lv_functions_1_0 = null;

        EObject lv_iterators_2_0 = null;

        EObject lv_materials_3_0 = null;

        EObject lv_statements_4_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:78:2: ( ( ( (lv_imports_0_0= ruleImportStatement ) )* ( ( (lv_functions_1_0= ruleFunctionDeclaration ) ) | ( (lv_iterators_2_0= ruleIteratorDeclaration ) ) | ( (lv_materials_3_0= ruleMaterialDeclaration ) ) | ( (lv_statements_4_0= ruleStatement ) ) )* ) )
            // InternalGeometryCodes.g:79:2: ( ( (lv_imports_0_0= ruleImportStatement ) )* ( ( (lv_functions_1_0= ruleFunctionDeclaration ) ) | ( (lv_iterators_2_0= ruleIteratorDeclaration ) ) | ( (lv_materials_3_0= ruleMaterialDeclaration ) ) | ( (lv_statements_4_0= ruleStatement ) ) )* )
            {
            // InternalGeometryCodes.g:79:2: ( ( (lv_imports_0_0= ruleImportStatement ) )* ( ( (lv_functions_1_0= ruleFunctionDeclaration ) ) | ( (lv_iterators_2_0= ruleIteratorDeclaration ) ) | ( (lv_materials_3_0= ruleMaterialDeclaration ) ) | ( (lv_statements_4_0= ruleStatement ) ) )* )
            // InternalGeometryCodes.g:80:3: ( (lv_imports_0_0= ruleImportStatement ) )* ( ( (lv_functions_1_0= ruleFunctionDeclaration ) ) | ( (lv_iterators_2_0= ruleIteratorDeclaration ) ) | ( (lv_materials_3_0= ruleMaterialDeclaration ) ) | ( (lv_statements_4_0= ruleStatement ) ) )*
            {
            // InternalGeometryCodes.g:80:3: ( (lv_imports_0_0= ruleImportStatement ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalGeometryCodes.g:81:4: (lv_imports_0_0= ruleImportStatement )
            	    {
            	    // InternalGeometryCodes.g:81:4: (lv_imports_0_0= ruleImportStatement )
            	    // InternalGeometryCodes.g:82:5: lv_imports_0_0= ruleImportStatement
            	    {

            	    					newCompositeNode(grammarAccess.getFileAccess().getImportsImportStatementParserRuleCall_0_0());
            	    				
            	    pushFollow(FOLLOW_3);
            	    lv_imports_0_0=ruleImportStatement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFileRule());
            	    					}
            	    					add(
            	    						current,
            	    						"imports",
            	    						lv_imports_0_0,
            	    						"de.adrianhoff.GeometryCodes.ImportStatement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalGeometryCodes.g:99:3: ( ( (lv_functions_1_0= ruleFunctionDeclaration ) ) | ( (lv_iterators_2_0= ruleIteratorDeclaration ) ) | ( (lv_materials_3_0= ruleMaterialDeclaration ) ) | ( (lv_statements_4_0= ruleStatement ) ) )*
            loop2:
            do {
                int alt2=5;
                alt2 = dfa2.predict(input);
                switch (alt2) {
            	case 1 :
            	    // InternalGeometryCodes.g:100:4: ( (lv_functions_1_0= ruleFunctionDeclaration ) )
            	    {
            	    // InternalGeometryCodes.g:100:4: ( (lv_functions_1_0= ruleFunctionDeclaration ) )
            	    // InternalGeometryCodes.g:101:5: (lv_functions_1_0= ruleFunctionDeclaration )
            	    {
            	    // InternalGeometryCodes.g:101:5: (lv_functions_1_0= ruleFunctionDeclaration )
            	    // InternalGeometryCodes.g:102:6: lv_functions_1_0= ruleFunctionDeclaration
            	    {

            	    						newCompositeNode(grammarAccess.getFileAccess().getFunctionsFunctionDeclarationParserRuleCall_1_0_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_functions_1_0=ruleFunctionDeclaration();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFileRule());
            	    						}
            	    						add(
            	    							current,
            	    							"functions",
            	    							lv_functions_1_0,
            	    							"de.adrianhoff.GeometryCodes.FunctionDeclaration");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 2 :
            	    // InternalGeometryCodes.g:120:4: ( (lv_iterators_2_0= ruleIteratorDeclaration ) )
            	    {
            	    // InternalGeometryCodes.g:120:4: ( (lv_iterators_2_0= ruleIteratorDeclaration ) )
            	    // InternalGeometryCodes.g:121:5: (lv_iterators_2_0= ruleIteratorDeclaration )
            	    {
            	    // InternalGeometryCodes.g:121:5: (lv_iterators_2_0= ruleIteratorDeclaration )
            	    // InternalGeometryCodes.g:122:6: lv_iterators_2_0= ruleIteratorDeclaration
            	    {

            	    						newCompositeNode(grammarAccess.getFileAccess().getIteratorsIteratorDeclarationParserRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_iterators_2_0=ruleIteratorDeclaration();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFileRule());
            	    						}
            	    						add(
            	    							current,
            	    							"iterators",
            	    							lv_iterators_2_0,
            	    							"de.adrianhoff.GeometryCodes.IteratorDeclaration");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 3 :
            	    // InternalGeometryCodes.g:140:4: ( (lv_materials_3_0= ruleMaterialDeclaration ) )
            	    {
            	    // InternalGeometryCodes.g:140:4: ( (lv_materials_3_0= ruleMaterialDeclaration ) )
            	    // InternalGeometryCodes.g:141:5: (lv_materials_3_0= ruleMaterialDeclaration )
            	    {
            	    // InternalGeometryCodes.g:141:5: (lv_materials_3_0= ruleMaterialDeclaration )
            	    // InternalGeometryCodes.g:142:6: lv_materials_3_0= ruleMaterialDeclaration
            	    {

            	    						newCompositeNode(grammarAccess.getFileAccess().getMaterialsMaterialDeclarationParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_materials_3_0=ruleMaterialDeclaration();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFileRule());
            	    						}
            	    						add(
            	    							current,
            	    							"materials",
            	    							lv_materials_3_0,
            	    							"de.adrianhoff.GeometryCodes.MaterialDeclaration");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;
            	case 4 :
            	    // InternalGeometryCodes.g:160:4: ( (lv_statements_4_0= ruleStatement ) )
            	    {
            	    // InternalGeometryCodes.g:160:4: ( (lv_statements_4_0= ruleStatement ) )
            	    // InternalGeometryCodes.g:161:5: (lv_statements_4_0= ruleStatement )
            	    {
            	    // InternalGeometryCodes.g:161:5: (lv_statements_4_0= ruleStatement )
            	    // InternalGeometryCodes.g:162:6: lv_statements_4_0= ruleStatement
            	    {

            	    						newCompositeNode(grammarAccess.getFileAccess().getStatementsStatementParserRuleCall_1_3_0());
            	    					
            	    pushFollow(FOLLOW_4);
            	    lv_statements_4_0=ruleStatement();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFileRule());
            	    						}
            	    						add(
            	    							current,
            	    							"statements",
            	    							lv_statements_4_0,
            	    							"de.adrianhoff.GeometryCodes.Statement");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFile"


    // $ANTLR start "entryRuleImportStatement"
    // InternalGeometryCodes.g:184:1: entryRuleImportStatement returns [EObject current=null] : iv_ruleImportStatement= ruleImportStatement EOF ;
    public final EObject entryRuleImportStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImportStatement = null;


        try {
            // InternalGeometryCodes.g:184:56: (iv_ruleImportStatement= ruleImportStatement EOF )
            // InternalGeometryCodes.g:185:2: iv_ruleImportStatement= ruleImportStatement EOF
            {
             newCompositeNode(grammarAccess.getImportStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleImportStatement=ruleImportStatement();

            state._fsp--;

             current =iv_ruleImportStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImportStatement"


    // $ANTLR start "ruleImportStatement"
    // InternalGeometryCodes.g:191:1: ruleImportStatement returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImportStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_importURI_1_0=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:197:2: ( (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) ) )
            // InternalGeometryCodes.g:198:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            {
            // InternalGeometryCodes.g:198:2: (otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) ) )
            // InternalGeometryCodes.g:199:3: otherlv_0= 'import' ( (lv_importURI_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,12,FOLLOW_5); 

            			newLeafNode(otherlv_0, grammarAccess.getImportStatementAccess().getImportKeyword_0());
            		
            // InternalGeometryCodes.g:203:3: ( (lv_importURI_1_0= RULE_STRING ) )
            // InternalGeometryCodes.g:204:4: (lv_importURI_1_0= RULE_STRING )
            {
            // InternalGeometryCodes.g:204:4: (lv_importURI_1_0= RULE_STRING )
            // InternalGeometryCodes.g:205:5: lv_importURI_1_0= RULE_STRING
            {
            lv_importURI_1_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_importURI_1_0, grammarAccess.getImportStatementAccess().getImportURISTRINGTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getImportStatementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"importURI",
            						lv_importURI_1_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImportStatement"


    // $ANTLR start "entryRuleFunctionDeclaration"
    // InternalGeometryCodes.g:225:1: entryRuleFunctionDeclaration returns [EObject current=null] : iv_ruleFunctionDeclaration= ruleFunctionDeclaration EOF ;
    public final EObject entryRuleFunctionDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionDeclaration = null;


        try {
            // InternalGeometryCodes.g:225:60: (iv_ruleFunctionDeclaration= ruleFunctionDeclaration EOF )
            // InternalGeometryCodes.g:226:2: iv_ruleFunctionDeclaration= ruleFunctionDeclaration EOF
            {
             newCompositeNode(grammarAccess.getFunctionDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionDeclaration=ruleFunctionDeclaration();

            state._fsp--;

             current =iv_ruleFunctionDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionDeclaration"


    // $ANTLR start "ruleFunctionDeclaration"
    // InternalGeometryCodes.g:232:1: ruleFunctionDeclaration returns [EObject current=null] : ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '}' ) ;
    public final EObject ruleFunctionDeclaration() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Enumerator lv_type_0_0 = null;

        EObject lv_parameters_3_0 = null;

        EObject lv_parameters_5_0 = null;

        EObject lv_statements_8_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:238:2: ( ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '}' ) )
            // InternalGeometryCodes.g:239:2: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '}' )
            {
            // InternalGeometryCodes.g:239:2: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '}' )
            // InternalGeometryCodes.g:240:3: ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= '{' ( (lv_statements_8_0= ruleStatement ) )* otherlv_9= '}'
            {
            // InternalGeometryCodes.g:240:3: ( (lv_type_0_0= ruleType ) )
            // InternalGeometryCodes.g:241:4: (lv_type_0_0= ruleType )
            {
            // InternalGeometryCodes.g:241:4: (lv_type_0_0= ruleType )
            // InternalGeometryCodes.g:242:5: lv_type_0_0= ruleType
            {

            					newCompositeNode(grammarAccess.getFunctionDeclarationAccess().getTypeTypeEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_6);
            lv_type_0_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFunctionDeclarationRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_0_0,
            						"de.adrianhoff.GeometryCodes.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:259:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalGeometryCodes.g:260:4: (lv_name_1_0= RULE_ID )
            {
            // InternalGeometryCodes.g:260:4: (lv_name_1_0= RULE_ID )
            // InternalGeometryCodes.g:261:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_1_0, grammarAccess.getFunctionDeclarationAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionDeclarationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_2, grammarAccess.getFunctionDeclarationAccess().getLeftParenthesisKeyword_2());
            		
            // InternalGeometryCodes.g:281:3: ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==36||(LA4_0>=38 && LA4_0<=39)||(LA4_0>=57 && LA4_0<=59)) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // InternalGeometryCodes.g:282:4: ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )*
                    {
                    // InternalGeometryCodes.g:282:4: ( (lv_parameters_3_0= ruleParameter ) )
                    // InternalGeometryCodes.g:283:5: (lv_parameters_3_0= ruleParameter )
                    {
                    // InternalGeometryCodes.g:283:5: (lv_parameters_3_0= ruleParameter )
                    // InternalGeometryCodes.g:284:6: lv_parameters_3_0= ruleParameter
                    {

                    						newCompositeNode(grammarAccess.getFunctionDeclarationAccess().getParametersParameterParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_parameters_3_0=ruleParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFunctionDeclarationRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_3_0,
                    							"de.adrianhoff.GeometryCodes.Parameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalGeometryCodes.g:301:4: (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==14) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // InternalGeometryCodes.g:302:5: otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) )
                    	    {
                    	    otherlv_4=(Token)match(input,14,FOLLOW_10); 

                    	    					newLeafNode(otherlv_4, grammarAccess.getFunctionDeclarationAccess().getCommaKeyword_3_1_0());
                    	    				
                    	    // InternalGeometryCodes.g:306:5: ( (lv_parameters_5_0= ruleParameter ) )
                    	    // InternalGeometryCodes.g:307:6: (lv_parameters_5_0= ruleParameter )
                    	    {
                    	    // InternalGeometryCodes.g:307:6: (lv_parameters_5_0= ruleParameter )
                    	    // InternalGeometryCodes.g:308:7: lv_parameters_5_0= ruleParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getFunctionDeclarationAccess().getParametersParameterParserRuleCall_3_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_parameters_5_0=ruleParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFunctionDeclarationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_5_0,
                    	    								"de.adrianhoff.GeometryCodes.Parameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,15,FOLLOW_11); 

            			newLeafNode(otherlv_6, grammarAccess.getFunctionDeclarationAccess().getRightParenthesisKeyword_4());
            		
            otherlv_7=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_7, grammarAccess.getFunctionDeclarationAccess().getLeftCurlyBracketKeyword_5());
            		
            // InternalGeometryCodes.g:335:3: ( (lv_statements_8_0= ruleStatement ) )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==RULE_ID||LA5_0==26||(LA5_0>=28 && LA5_0<=29)||(LA5_0>=33 && LA5_0<=34)||LA5_0==36||(LA5_0>=38 && LA5_0<=39)||(LA5_0>=57 && LA5_0<=59)) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalGeometryCodes.g:336:4: (lv_statements_8_0= ruleStatement )
            	    {
            	    // InternalGeometryCodes.g:336:4: (lv_statements_8_0= ruleStatement )
            	    // InternalGeometryCodes.g:337:5: lv_statements_8_0= ruleStatement
            	    {

            	    					newCompositeNode(grammarAccess.getFunctionDeclarationAccess().getStatementsStatementParserRuleCall_6_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_statements_8_0=ruleStatement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getFunctionDeclarationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"statements",
            	    						lv_statements_8_0,
            	    						"de.adrianhoff.GeometryCodes.Statement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

            otherlv_9=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_9, grammarAccess.getFunctionDeclarationAccess().getRightCurlyBracketKeyword_7());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionDeclaration"


    // $ANTLR start "entryRuleIteratorDeclaration"
    // InternalGeometryCodes.g:362:1: entryRuleIteratorDeclaration returns [EObject current=null] : iv_ruleIteratorDeclaration= ruleIteratorDeclaration EOF ;
    public final EObject entryRuleIteratorDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIteratorDeclaration = null;


        try {
            // InternalGeometryCodes.g:362:60: (iv_ruleIteratorDeclaration= ruleIteratorDeclaration EOF )
            // InternalGeometryCodes.g:363:2: iv_ruleIteratorDeclaration= ruleIteratorDeclaration EOF
            {
             newCompositeNode(grammarAccess.getIteratorDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIteratorDeclaration=ruleIteratorDeclaration();

            state._fsp--;

             current =iv_ruleIteratorDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIteratorDeclaration"


    // $ANTLR start "ruleIteratorDeclaration"
    // InternalGeometryCodes.g:369:1: ruleIteratorDeclaration returns [EObject current=null] : (otherlv_0= 'iterator' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= 'over' otherlv_8= 'each' ( (lv_iteratedParameter_9_0= ruleParameter ) ) otherlv_10= '{' ( (lv_statements_11_0= ruleStatement ) )* otherlv_12= '}' ) ;
    public final EObject ruleIteratorDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_12=null;
        EObject lv_parameters_3_0 = null;

        EObject lv_parameters_5_0 = null;

        EObject lv_iteratedParameter_9_0 = null;

        EObject lv_statements_11_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:375:2: ( (otherlv_0= 'iterator' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= 'over' otherlv_8= 'each' ( (lv_iteratedParameter_9_0= ruleParameter ) ) otherlv_10= '{' ( (lv_statements_11_0= ruleStatement ) )* otherlv_12= '}' ) )
            // InternalGeometryCodes.g:376:2: (otherlv_0= 'iterator' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= 'over' otherlv_8= 'each' ( (lv_iteratedParameter_9_0= ruleParameter ) ) otherlv_10= '{' ( (lv_statements_11_0= ruleStatement ) )* otherlv_12= '}' )
            {
            // InternalGeometryCodes.g:376:2: (otherlv_0= 'iterator' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= 'over' otherlv_8= 'each' ( (lv_iteratedParameter_9_0= ruleParameter ) ) otherlv_10= '{' ( (lv_statements_11_0= ruleStatement ) )* otherlv_12= '}' )
            // InternalGeometryCodes.g:377:3: otherlv_0= 'iterator' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '(' ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )? otherlv_6= ')' otherlv_7= 'over' otherlv_8= 'each' ( (lv_iteratedParameter_9_0= ruleParameter ) ) otherlv_10= '{' ( (lv_statements_11_0= ruleStatement ) )* otherlv_12= '}'
            {
            otherlv_0=(Token)match(input,18,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getIteratorDeclarationAccess().getIteratorKeyword_0());
            		
            // InternalGeometryCodes.g:381:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalGeometryCodes.g:382:4: (lv_name_1_0= RULE_ID )
            {
            // InternalGeometryCodes.g:382:4: (lv_name_1_0= RULE_ID )
            // InternalGeometryCodes.g:383:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(lv_name_1_0, grammarAccess.getIteratorDeclarationAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getIteratorDeclarationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_8); 

            			newLeafNode(otherlv_2, grammarAccess.getIteratorDeclarationAccess().getLeftParenthesisKeyword_2());
            		
            // InternalGeometryCodes.g:403:3: ( ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )* )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==36||(LA7_0>=38 && LA7_0<=39)||(LA7_0>=57 && LA7_0<=59)) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalGeometryCodes.g:404:4: ( (lv_parameters_3_0= ruleParameter ) ) (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )*
                    {
                    // InternalGeometryCodes.g:404:4: ( (lv_parameters_3_0= ruleParameter ) )
                    // InternalGeometryCodes.g:405:5: (lv_parameters_3_0= ruleParameter )
                    {
                    // InternalGeometryCodes.g:405:5: (lv_parameters_3_0= ruleParameter )
                    // InternalGeometryCodes.g:406:6: lv_parameters_3_0= ruleParameter
                    {

                    						newCompositeNode(grammarAccess.getIteratorDeclarationAccess().getParametersParameterParserRuleCall_3_0_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_parameters_3_0=ruleParameter();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getIteratorDeclarationRule());
                    						}
                    						add(
                    							current,
                    							"parameters",
                    							lv_parameters_3_0,
                    							"de.adrianhoff.GeometryCodes.Parameter");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalGeometryCodes.g:423:4: (otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) ) )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==14) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalGeometryCodes.g:424:5: otherlv_4= ',' ( (lv_parameters_5_0= ruleParameter ) )
                    	    {
                    	    otherlv_4=(Token)match(input,14,FOLLOW_10); 

                    	    					newLeafNode(otherlv_4, grammarAccess.getIteratorDeclarationAccess().getCommaKeyword_3_1_0());
                    	    				
                    	    // InternalGeometryCodes.g:428:5: ( (lv_parameters_5_0= ruleParameter ) )
                    	    // InternalGeometryCodes.g:429:6: (lv_parameters_5_0= ruleParameter )
                    	    {
                    	    // InternalGeometryCodes.g:429:6: (lv_parameters_5_0= ruleParameter )
                    	    // InternalGeometryCodes.g:430:7: lv_parameters_5_0= ruleParameter
                    	    {

                    	    							newCompositeNode(grammarAccess.getIteratorDeclarationAccess().getParametersParameterParserRuleCall_3_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_parameters_5_0=ruleParameter();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getIteratorDeclarationRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"parameters",
                    	    								lv_parameters_5_0,
                    	    								"de.adrianhoff.GeometryCodes.Parameter");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_6=(Token)match(input,15,FOLLOW_13); 

            			newLeafNode(otherlv_6, grammarAccess.getIteratorDeclarationAccess().getRightParenthesisKeyword_4());
            		
            otherlv_7=(Token)match(input,19,FOLLOW_14); 

            			newLeafNode(otherlv_7, grammarAccess.getIteratorDeclarationAccess().getOverKeyword_5());
            		
            otherlv_8=(Token)match(input,20,FOLLOW_10); 

            			newLeafNode(otherlv_8, grammarAccess.getIteratorDeclarationAccess().getEachKeyword_6());
            		
            // InternalGeometryCodes.g:461:3: ( (lv_iteratedParameter_9_0= ruleParameter ) )
            // InternalGeometryCodes.g:462:4: (lv_iteratedParameter_9_0= ruleParameter )
            {
            // InternalGeometryCodes.g:462:4: (lv_iteratedParameter_9_0= ruleParameter )
            // InternalGeometryCodes.g:463:5: lv_iteratedParameter_9_0= ruleParameter
            {

            					newCompositeNode(grammarAccess.getIteratorDeclarationAccess().getIteratedParameterParameterParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_11);
            lv_iteratedParameter_9_0=ruleParameter();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIteratorDeclarationRule());
            					}
            					set(
            						current,
            						"iteratedParameter",
            						lv_iteratedParameter_9_0,
            						"de.adrianhoff.GeometryCodes.Parameter");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_10=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_10, grammarAccess.getIteratorDeclarationAccess().getLeftCurlyBracketKeyword_8());
            		
            // InternalGeometryCodes.g:484:3: ( (lv_statements_11_0= ruleStatement ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_ID||LA8_0==26||(LA8_0>=28 && LA8_0<=29)||(LA8_0>=33 && LA8_0<=34)||LA8_0==36||(LA8_0>=38 && LA8_0<=39)||(LA8_0>=57 && LA8_0<=59)) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalGeometryCodes.g:485:4: (lv_statements_11_0= ruleStatement )
            	    {
            	    // InternalGeometryCodes.g:485:4: (lv_statements_11_0= ruleStatement )
            	    // InternalGeometryCodes.g:486:5: lv_statements_11_0= ruleStatement
            	    {

            	    					newCompositeNode(grammarAccess.getIteratorDeclarationAccess().getStatementsStatementParserRuleCall_9_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_statements_11_0=ruleStatement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getIteratorDeclarationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"statements",
            	    						lv_statements_11_0,
            	    						"de.adrianhoff.GeometryCodes.Statement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_12=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_12, grammarAccess.getIteratorDeclarationAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIteratorDeclaration"


    // $ANTLR start "entryRuleParameter"
    // InternalGeometryCodes.g:511:1: entryRuleParameter returns [EObject current=null] : iv_ruleParameter= ruleParameter EOF ;
    public final EObject entryRuleParameter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParameter = null;


        try {
            // InternalGeometryCodes.g:511:50: (iv_ruleParameter= ruleParameter EOF )
            // InternalGeometryCodes.g:512:2: iv_ruleParameter= ruleParameter EOF
            {
             newCompositeNode(grammarAccess.getParameterRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleParameter=ruleParameter();

            state._fsp--;

             current =iv_ruleParameter; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParameter"


    // $ANTLR start "ruleParameter"
    // InternalGeometryCodes.g:518:1: ruleParameter returns [EObject current=null] : ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleParameter() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Enumerator lv_type_0_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:524:2: ( ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalGeometryCodes.g:525:2: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalGeometryCodes.g:525:2: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) )
            // InternalGeometryCodes.g:526:3: ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) )
            {
            // InternalGeometryCodes.g:526:3: ( (lv_type_0_0= ruleType ) )
            // InternalGeometryCodes.g:527:4: (lv_type_0_0= ruleType )
            {
            // InternalGeometryCodes.g:527:4: (lv_type_0_0= ruleType )
            // InternalGeometryCodes.g:528:5: lv_type_0_0= ruleType
            {

            					newCompositeNode(grammarAccess.getParameterAccess().getTypeTypeEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_6);
            lv_type_0_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getParameterRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_0_0,
            						"de.adrianhoff.GeometryCodes.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:545:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalGeometryCodes.g:546:4: (lv_name_1_0= RULE_ID )
            {
            // InternalGeometryCodes.g:546:4: (lv_name_1_0= RULE_ID )
            // InternalGeometryCodes.g:547:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getParameterAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getParameterRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParameter"


    // $ANTLR start "entryRuleMaterialDeclaration"
    // InternalGeometryCodes.g:567:1: entryRuleMaterialDeclaration returns [EObject current=null] : iv_ruleMaterialDeclaration= ruleMaterialDeclaration EOF ;
    public final EObject entryRuleMaterialDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMaterialDeclaration = null;


        try {
            // InternalGeometryCodes.g:567:60: (iv_ruleMaterialDeclaration= ruleMaterialDeclaration EOF )
            // InternalGeometryCodes.g:568:2: iv_ruleMaterialDeclaration= ruleMaterialDeclaration EOF
            {
             newCompositeNode(grammarAccess.getMaterialDeclarationRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMaterialDeclaration=ruleMaterialDeclaration();

            state._fsp--;

             current =iv_ruleMaterialDeclaration; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMaterialDeclaration"


    // $ANTLR start "ruleMaterialDeclaration"
    // InternalGeometryCodes.g:574:1: ruleMaterialDeclaration returns [EObject current=null] : (otherlv_0= 'material' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= ruleMaterialProperty ) )* otherlv_4= '}' ) ;
    public final EObject ruleMaterialDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_properties_3_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:580:2: ( (otherlv_0= 'material' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= ruleMaterialProperty ) )* otherlv_4= '}' ) )
            // InternalGeometryCodes.g:581:2: (otherlv_0= 'material' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= ruleMaterialProperty ) )* otherlv_4= '}' )
            {
            // InternalGeometryCodes.g:581:2: (otherlv_0= 'material' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= ruleMaterialProperty ) )* otherlv_4= '}' )
            // InternalGeometryCodes.g:582:3: otherlv_0= 'material' ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_properties_3_0= ruleMaterialProperty ) )* otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,21,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getMaterialDeclarationAccess().getMaterialKeyword_0());
            		
            // InternalGeometryCodes.g:586:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalGeometryCodes.g:587:4: (lv_name_1_0= RULE_ID )
            {
            // InternalGeometryCodes.g:587:4: (lv_name_1_0= RULE_ID )
            // InternalGeometryCodes.g:588:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_11); 

            					newLeafNode(lv_name_1_0, grammarAccess.getMaterialDeclarationAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMaterialDeclarationRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,16,FOLLOW_15); 

            			newLeafNode(otherlv_2, grammarAccess.getMaterialDeclarationAccess().getLeftCurlyBracketKeyword_2());
            		
            // InternalGeometryCodes.g:608:3: ( (lv_properties_3_0= ruleMaterialProperty ) )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalGeometryCodes.g:609:4: (lv_properties_3_0= ruleMaterialProperty )
            	    {
            	    // InternalGeometryCodes.g:609:4: (lv_properties_3_0= ruleMaterialProperty )
            	    // InternalGeometryCodes.g:610:5: lv_properties_3_0= ruleMaterialProperty
            	    {

            	    					newCompositeNode(grammarAccess.getMaterialDeclarationAccess().getPropertiesMaterialPropertyParserRuleCall_3_0());
            	    				
            	    pushFollow(FOLLOW_15);
            	    lv_properties_3_0=ruleMaterialProperty();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getMaterialDeclarationRule());
            	    					}
            	    					add(
            	    						current,
            	    						"properties",
            	    						lv_properties_3_0,
            	    						"de.adrianhoff.GeometryCodes.MaterialProperty");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            otherlv_4=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_4, grammarAccess.getMaterialDeclarationAccess().getRightCurlyBracketKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMaterialDeclaration"


    // $ANTLR start "entryRuleMaterialProperty"
    // InternalGeometryCodes.g:635:1: entryRuleMaterialProperty returns [EObject current=null] : iv_ruleMaterialProperty= ruleMaterialProperty EOF ;
    public final EObject entryRuleMaterialProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMaterialProperty = null;


        try {
            // InternalGeometryCodes.g:635:57: (iv_ruleMaterialProperty= ruleMaterialProperty EOF )
            // InternalGeometryCodes.g:636:2: iv_ruleMaterialProperty= ruleMaterialProperty EOF
            {
             newCompositeNode(grammarAccess.getMaterialPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMaterialProperty=ruleMaterialProperty();

            state._fsp--;

             current =iv_ruleMaterialProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMaterialProperty"


    // $ANTLR start "ruleMaterialProperty"
    // InternalGeometryCodes.g:642:1: ruleMaterialProperty returns [EObject current=null] : (this_MaterialStringProperty_0= ruleMaterialStringProperty | this_MaterialFloatProperty_1= ruleMaterialFloatProperty | this_MaterialBooleanProperty_2= ruleMaterialBooleanProperty ) ;
    public final EObject ruleMaterialProperty() throws RecognitionException {
        EObject current = null;

        EObject this_MaterialStringProperty_0 = null;

        EObject this_MaterialFloatProperty_1 = null;

        EObject this_MaterialBooleanProperty_2 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:648:2: ( (this_MaterialStringProperty_0= ruleMaterialStringProperty | this_MaterialFloatProperty_1= ruleMaterialFloatProperty | this_MaterialBooleanProperty_2= ruleMaterialBooleanProperty ) )
            // InternalGeometryCodes.g:649:2: (this_MaterialStringProperty_0= ruleMaterialStringProperty | this_MaterialFloatProperty_1= ruleMaterialFloatProperty | this_MaterialBooleanProperty_2= ruleMaterialBooleanProperty )
            {
            // InternalGeometryCodes.g:649:2: (this_MaterialStringProperty_0= ruleMaterialStringProperty | this_MaterialFloatProperty_1= ruleMaterialFloatProperty | this_MaterialBooleanProperty_2= ruleMaterialBooleanProperty )
            int alt10=3;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==RULE_ID) ) {
                int LA10_1 = input.LA(2);

                if ( (LA10_1==22) ) {
                    switch ( input.LA(3) ) {
                    case RULE_FLOAT:
                        {
                        alt10=2;
                        }
                        break;
                    case 23:
                    case 24:
                        {
                        alt10=3;
                        }
                        break;
                    case RULE_STRING:
                        {
                        alt10=1;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 10, 2, input);

                        throw nvae;
                    }

                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalGeometryCodes.g:650:3: this_MaterialStringProperty_0= ruleMaterialStringProperty
                    {

                    			newCompositeNode(grammarAccess.getMaterialPropertyAccess().getMaterialStringPropertyParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_MaterialStringProperty_0=ruleMaterialStringProperty();

                    state._fsp--;


                    			current = this_MaterialStringProperty_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:659:3: this_MaterialFloatProperty_1= ruleMaterialFloatProperty
                    {

                    			newCompositeNode(grammarAccess.getMaterialPropertyAccess().getMaterialFloatPropertyParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_MaterialFloatProperty_1=ruleMaterialFloatProperty();

                    state._fsp--;


                    			current = this_MaterialFloatProperty_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:668:3: this_MaterialBooleanProperty_2= ruleMaterialBooleanProperty
                    {

                    			newCompositeNode(grammarAccess.getMaterialPropertyAccess().getMaterialBooleanPropertyParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_MaterialBooleanProperty_2=ruleMaterialBooleanProperty();

                    state._fsp--;


                    			current = this_MaterialBooleanProperty_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMaterialProperty"


    // $ANTLR start "entryRuleMaterialStringProperty"
    // InternalGeometryCodes.g:680:1: entryRuleMaterialStringProperty returns [EObject current=null] : iv_ruleMaterialStringProperty= ruleMaterialStringProperty EOF ;
    public final EObject entryRuleMaterialStringProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMaterialStringProperty = null;


        try {
            // InternalGeometryCodes.g:680:63: (iv_ruleMaterialStringProperty= ruleMaterialStringProperty EOF )
            // InternalGeometryCodes.g:681:2: iv_ruleMaterialStringProperty= ruleMaterialStringProperty EOF
            {
             newCompositeNode(grammarAccess.getMaterialStringPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMaterialStringProperty=ruleMaterialStringProperty();

            state._fsp--;

             current =iv_ruleMaterialStringProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMaterialStringProperty"


    // $ANTLR start "ruleMaterialStringProperty"
    // InternalGeometryCodes.g:687:1: ruleMaterialStringProperty returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleMaterialStringProperty() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:693:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_STRING ) ) ) )
            // InternalGeometryCodes.g:694:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_STRING ) ) )
            {
            // InternalGeometryCodes.g:694:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_STRING ) ) )
            // InternalGeometryCodes.g:695:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_STRING ) )
            {
            // InternalGeometryCodes.g:695:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGeometryCodes.g:696:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGeometryCodes.g:696:4: (lv_name_0_0= RULE_ID )
            // InternalGeometryCodes.g:697:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            					newLeafNode(lv_name_0_0, grammarAccess.getMaterialStringPropertyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMaterialStringPropertyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,22,FOLLOW_5); 

            			newLeafNode(otherlv_1, grammarAccess.getMaterialStringPropertyAccess().getColonKeyword_1());
            		
            // InternalGeometryCodes.g:717:3: ( (lv_value_2_0= RULE_STRING ) )
            // InternalGeometryCodes.g:718:4: (lv_value_2_0= RULE_STRING )
            {
            // InternalGeometryCodes.g:718:4: (lv_value_2_0= RULE_STRING )
            // InternalGeometryCodes.g:719:5: lv_value_2_0= RULE_STRING
            {
            lv_value_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            					newLeafNode(lv_value_2_0, grammarAccess.getMaterialStringPropertyAccess().getValueSTRINGTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMaterialStringPropertyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_2_0,
            						"org.eclipse.xtext.common.Terminals.STRING");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMaterialStringProperty"


    // $ANTLR start "entryRuleMaterialFloatProperty"
    // InternalGeometryCodes.g:739:1: entryRuleMaterialFloatProperty returns [EObject current=null] : iv_ruleMaterialFloatProperty= ruleMaterialFloatProperty EOF ;
    public final EObject entryRuleMaterialFloatProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMaterialFloatProperty = null;


        try {
            // InternalGeometryCodes.g:739:62: (iv_ruleMaterialFloatProperty= ruleMaterialFloatProperty EOF )
            // InternalGeometryCodes.g:740:2: iv_ruleMaterialFloatProperty= ruleMaterialFloatProperty EOF
            {
             newCompositeNode(grammarAccess.getMaterialFloatPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMaterialFloatProperty=ruleMaterialFloatProperty();

            state._fsp--;

             current =iv_ruleMaterialFloatProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMaterialFloatProperty"


    // $ANTLR start "ruleMaterialFloatProperty"
    // InternalGeometryCodes.g:746:1: ruleMaterialFloatProperty returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_FLOAT ) ) ) ;
    public final EObject ruleMaterialFloatProperty() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:752:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_FLOAT ) ) ) )
            // InternalGeometryCodes.g:753:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_FLOAT ) ) )
            {
            // InternalGeometryCodes.g:753:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_FLOAT ) ) )
            // InternalGeometryCodes.g:754:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( (lv_value_2_0= RULE_FLOAT ) )
            {
            // InternalGeometryCodes.g:754:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGeometryCodes.g:755:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGeometryCodes.g:755:4: (lv_name_0_0= RULE_ID )
            // InternalGeometryCodes.g:756:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            					newLeafNode(lv_name_0_0, grammarAccess.getMaterialFloatPropertyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMaterialFloatPropertyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,22,FOLLOW_17); 

            			newLeafNode(otherlv_1, grammarAccess.getMaterialFloatPropertyAccess().getColonKeyword_1());
            		
            // InternalGeometryCodes.g:776:3: ( (lv_value_2_0= RULE_FLOAT ) )
            // InternalGeometryCodes.g:777:4: (lv_value_2_0= RULE_FLOAT )
            {
            // InternalGeometryCodes.g:777:4: (lv_value_2_0= RULE_FLOAT )
            // InternalGeometryCodes.g:778:5: lv_value_2_0= RULE_FLOAT
            {
            lv_value_2_0=(Token)match(input,RULE_FLOAT,FOLLOW_2); 

            					newLeafNode(lv_value_2_0, grammarAccess.getMaterialFloatPropertyAccess().getValueFLOATTerminalRuleCall_2_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMaterialFloatPropertyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"value",
            						lv_value_2_0,
            						"de.adrianhoff.GeometryCodes.FLOAT");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMaterialFloatProperty"


    // $ANTLR start "entryRuleMaterialBooleanProperty"
    // InternalGeometryCodes.g:798:1: entryRuleMaterialBooleanProperty returns [EObject current=null] : iv_ruleMaterialBooleanProperty= ruleMaterialBooleanProperty EOF ;
    public final EObject entryRuleMaterialBooleanProperty() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMaterialBooleanProperty = null;


        try {
            // InternalGeometryCodes.g:798:64: (iv_ruleMaterialBooleanProperty= ruleMaterialBooleanProperty EOF )
            // InternalGeometryCodes.g:799:2: iv_ruleMaterialBooleanProperty= ruleMaterialBooleanProperty EOF
            {
             newCompositeNode(grammarAccess.getMaterialBooleanPropertyRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMaterialBooleanProperty=ruleMaterialBooleanProperty();

            state._fsp--;

             current =iv_ruleMaterialBooleanProperty; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMaterialBooleanProperty"


    // $ANTLR start "ruleMaterialBooleanProperty"
    // InternalGeometryCodes.g:805:1: ruleMaterialBooleanProperty returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_value_2_0= 'yes' ) ) | otherlv_3= 'no' ) ) ;
    public final EObject ruleMaterialBooleanProperty() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        Token otherlv_1=null;
        Token lv_value_2_0=null;
        Token otherlv_3=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:811:2: ( ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_value_2_0= 'yes' ) ) | otherlv_3= 'no' ) ) )
            // InternalGeometryCodes.g:812:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_value_2_0= 'yes' ) ) | otherlv_3= 'no' ) )
            {
            // InternalGeometryCodes.g:812:2: ( ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_value_2_0= 'yes' ) ) | otherlv_3= 'no' ) )
            // InternalGeometryCodes.g:813:3: ( (lv_name_0_0= RULE_ID ) ) otherlv_1= ':' ( ( (lv_value_2_0= 'yes' ) ) | otherlv_3= 'no' )
            {
            // InternalGeometryCodes.g:813:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalGeometryCodes.g:814:4: (lv_name_0_0= RULE_ID )
            {
            // InternalGeometryCodes.g:814:4: (lv_name_0_0= RULE_ID )
            // InternalGeometryCodes.g:815:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_16); 

            					newLeafNode(lv_name_0_0, grammarAccess.getMaterialBooleanPropertyAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getMaterialBooleanPropertyRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,22,FOLLOW_18); 

            			newLeafNode(otherlv_1, grammarAccess.getMaterialBooleanPropertyAccess().getColonKeyword_1());
            		
            // InternalGeometryCodes.g:835:3: ( ( (lv_value_2_0= 'yes' ) ) | otherlv_3= 'no' )
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==23) ) {
                alt11=1;
            }
            else if ( (LA11_0==24) ) {
                alt11=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 11, 0, input);

                throw nvae;
            }
            switch (alt11) {
                case 1 :
                    // InternalGeometryCodes.g:836:4: ( (lv_value_2_0= 'yes' ) )
                    {
                    // InternalGeometryCodes.g:836:4: ( (lv_value_2_0= 'yes' ) )
                    // InternalGeometryCodes.g:837:5: (lv_value_2_0= 'yes' )
                    {
                    // InternalGeometryCodes.g:837:5: (lv_value_2_0= 'yes' )
                    // InternalGeometryCodes.g:838:6: lv_value_2_0= 'yes'
                    {
                    lv_value_2_0=(Token)match(input,23,FOLLOW_2); 

                    						newLeafNode(lv_value_2_0, grammarAccess.getMaterialBooleanPropertyAccess().getValueYesKeyword_2_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getMaterialBooleanPropertyRule());
                    						}
                    						setWithLastConsumed(current, "value", lv_value_2_0 != null, "yes");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:851:4: otherlv_3= 'no'
                    {
                    otherlv_3=(Token)match(input,24,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getMaterialBooleanPropertyAccess().getNoKeyword_2_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMaterialBooleanProperty"


    // $ANTLR start "entryRuleStatement"
    // InternalGeometryCodes.g:860:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // InternalGeometryCodes.g:860:50: (iv_ruleStatement= ruleStatement EOF )
            // InternalGeometryCodes.g:861:2: iv_ruleStatement= ruleStatement EOF
            {
             newCompositeNode(grammarAccess.getStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStatement=ruleStatement();

            state._fsp--;

             current =iv_ruleStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // InternalGeometryCodes.g:867:1: ruleStatement returns [EObject current=null] : (this_DeclarationStatement_0= ruleDeclarationStatement | this_AssignmentStatement_1= ruleAssignmentStatement | this_ReferenceStatement_2= ruleReferenceStatement | this_ReturnStatement_3= ruleReturnStatement | this_IfStatement_4= ruleIfStatement | this_ForLoopStatement_5= ruleForLoopStatement | this_ForEachLoopStatement_6= ruleForEachLoopStatement | this_WhileLoopStatement_7= ruleWhileLoopStatement | this_OutputStatement_8= ruleOutputStatement ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        EObject this_DeclarationStatement_0 = null;

        EObject this_AssignmentStatement_1 = null;

        EObject this_ReferenceStatement_2 = null;

        EObject this_ReturnStatement_3 = null;

        EObject this_IfStatement_4 = null;

        EObject this_ForLoopStatement_5 = null;

        EObject this_ForEachLoopStatement_6 = null;

        EObject this_WhileLoopStatement_7 = null;

        EObject this_OutputStatement_8 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:873:2: ( (this_DeclarationStatement_0= ruleDeclarationStatement | this_AssignmentStatement_1= ruleAssignmentStatement | this_ReferenceStatement_2= ruleReferenceStatement | this_ReturnStatement_3= ruleReturnStatement | this_IfStatement_4= ruleIfStatement | this_ForLoopStatement_5= ruleForLoopStatement | this_ForEachLoopStatement_6= ruleForEachLoopStatement | this_WhileLoopStatement_7= ruleWhileLoopStatement | this_OutputStatement_8= ruleOutputStatement ) )
            // InternalGeometryCodes.g:874:2: (this_DeclarationStatement_0= ruleDeclarationStatement | this_AssignmentStatement_1= ruleAssignmentStatement | this_ReferenceStatement_2= ruleReferenceStatement | this_ReturnStatement_3= ruleReturnStatement | this_IfStatement_4= ruleIfStatement | this_ForLoopStatement_5= ruleForLoopStatement | this_ForEachLoopStatement_6= ruleForEachLoopStatement | this_WhileLoopStatement_7= ruleWhileLoopStatement | this_OutputStatement_8= ruleOutputStatement )
            {
            // InternalGeometryCodes.g:874:2: (this_DeclarationStatement_0= ruleDeclarationStatement | this_AssignmentStatement_1= ruleAssignmentStatement | this_ReferenceStatement_2= ruleReferenceStatement | this_ReturnStatement_3= ruleReturnStatement | this_IfStatement_4= ruleIfStatement | this_ForLoopStatement_5= ruleForLoopStatement | this_ForEachLoopStatement_6= ruleForEachLoopStatement | this_WhileLoopStatement_7= ruleWhileLoopStatement | this_OutputStatement_8= ruleOutputStatement )
            int alt12=9;
            alt12 = dfa12.predict(input);
            switch (alt12) {
                case 1 :
                    // InternalGeometryCodes.g:875:3: this_DeclarationStatement_0= ruleDeclarationStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getDeclarationStatementParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_DeclarationStatement_0=ruleDeclarationStatement();

                    state._fsp--;


                    			current = this_DeclarationStatement_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:884:3: this_AssignmentStatement_1= ruleAssignmentStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getAssignmentStatementParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_AssignmentStatement_1=ruleAssignmentStatement();

                    state._fsp--;


                    			current = this_AssignmentStatement_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:893:3: this_ReferenceStatement_2= ruleReferenceStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getReferenceStatementParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_ReferenceStatement_2=ruleReferenceStatement();

                    state._fsp--;


                    			current = this_ReferenceStatement_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:902:3: this_ReturnStatement_3= ruleReturnStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getReturnStatementParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_ReturnStatement_3=ruleReturnStatement();

                    state._fsp--;


                    			current = this_ReturnStatement_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalGeometryCodes.g:911:3: this_IfStatement_4= ruleIfStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getIfStatementParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_IfStatement_4=ruleIfStatement();

                    state._fsp--;


                    			current = this_IfStatement_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 6 :
                    // InternalGeometryCodes.g:920:3: this_ForLoopStatement_5= ruleForLoopStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getForLoopStatementParserRuleCall_5());
                    		
                    pushFollow(FOLLOW_2);
                    this_ForLoopStatement_5=ruleForLoopStatement();

                    state._fsp--;


                    			current = this_ForLoopStatement_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 7 :
                    // InternalGeometryCodes.g:929:3: this_ForEachLoopStatement_6= ruleForEachLoopStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getForEachLoopStatementParserRuleCall_6());
                    		
                    pushFollow(FOLLOW_2);
                    this_ForEachLoopStatement_6=ruleForEachLoopStatement();

                    state._fsp--;


                    			current = this_ForEachLoopStatement_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 8 :
                    // InternalGeometryCodes.g:938:3: this_WhileLoopStatement_7= ruleWhileLoopStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getWhileLoopStatementParserRuleCall_7());
                    		
                    pushFollow(FOLLOW_2);
                    this_WhileLoopStatement_7=ruleWhileLoopStatement();

                    state._fsp--;


                    			current = this_WhileLoopStatement_7;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 9 :
                    // InternalGeometryCodes.g:947:3: this_OutputStatement_8= ruleOutputStatement
                    {

                    			newCompositeNode(grammarAccess.getStatementAccess().getOutputStatementParserRuleCall_8());
                    		
                    pushFollow(FOLLOW_2);
                    this_OutputStatement_8=ruleOutputStatement();

                    state._fsp--;


                    			current = this_OutputStatement_8;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleDeclarationStatement"
    // InternalGeometryCodes.g:959:1: entryRuleDeclarationStatement returns [EObject current=null] : iv_ruleDeclarationStatement= ruleDeclarationStatement EOF ;
    public final EObject entryRuleDeclarationStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclarationStatement = null;


        try {
            // InternalGeometryCodes.g:959:61: (iv_ruleDeclarationStatement= ruleDeclarationStatement EOF )
            // InternalGeometryCodes.g:960:2: iv_ruleDeclarationStatement= ruleDeclarationStatement EOF
            {
             newCompositeNode(grammarAccess.getDeclarationStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeclarationStatement=ruleDeclarationStatement();

            state._fsp--;

             current =iv_ruleDeclarationStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclarationStatement"


    // $ANTLR start "ruleDeclarationStatement"
    // InternalGeometryCodes.g:966:1: ruleDeclarationStatement returns [EObject current=null] : ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleExpression ) ) ) ;
    public final EObject ruleDeclarationStatement() throws RecognitionException {
        EObject current = null;

        Token lv_name_1_0=null;
        Token otherlv_2=null;
        Enumerator lv_type_0_0 = null;

        EObject lv_value_3_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:972:2: ( ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleExpression ) ) ) )
            // InternalGeometryCodes.g:973:2: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleExpression ) ) )
            {
            // InternalGeometryCodes.g:973:2: ( ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleExpression ) ) )
            // InternalGeometryCodes.g:974:3: ( (lv_type_0_0= ruleType ) ) ( (lv_name_1_0= RULE_ID ) ) otherlv_2= '=' ( (lv_value_3_0= ruleExpression ) )
            {
            // InternalGeometryCodes.g:974:3: ( (lv_type_0_0= ruleType ) )
            // InternalGeometryCodes.g:975:4: (lv_type_0_0= ruleType )
            {
            // InternalGeometryCodes.g:975:4: (lv_type_0_0= ruleType )
            // InternalGeometryCodes.g:976:5: lv_type_0_0= ruleType
            {

            					newCompositeNode(grammarAccess.getDeclarationStatementAccess().getTypeTypeEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_6);
            lv_type_0_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDeclarationStatementRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_0_0,
            						"de.adrianhoff.GeometryCodes.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:993:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalGeometryCodes.g:994:4: (lv_name_1_0= RULE_ID )
            {
            // InternalGeometryCodes.g:994:4: (lv_name_1_0= RULE_ID )
            // InternalGeometryCodes.g:995:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_19); 

            					newLeafNode(lv_name_1_0, grammarAccess.getDeclarationStatementAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getDeclarationStatementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_2=(Token)match(input,25,FOLLOW_20); 

            			newLeafNode(otherlv_2, grammarAccess.getDeclarationStatementAccess().getEqualsSignKeyword_2());
            		
            // InternalGeometryCodes.g:1015:3: ( (lv_value_3_0= ruleExpression ) )
            // InternalGeometryCodes.g:1016:4: (lv_value_3_0= ruleExpression )
            {
            // InternalGeometryCodes.g:1016:4: (lv_value_3_0= ruleExpression )
            // InternalGeometryCodes.g:1017:5: lv_value_3_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getDeclarationStatementAccess().getValueExpressionParserRuleCall_3_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_3_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getDeclarationStatementRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_3_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclarationStatement"


    // $ANTLR start "entryRuleAssignmentStatement"
    // InternalGeometryCodes.g:1038:1: entryRuleAssignmentStatement returns [EObject current=null] : iv_ruleAssignmentStatement= ruleAssignmentStatement EOF ;
    public final EObject entryRuleAssignmentStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentStatement = null;


        try {
            // InternalGeometryCodes.g:1038:60: (iv_ruleAssignmentStatement= ruleAssignmentStatement EOF )
            // InternalGeometryCodes.g:1039:2: iv_ruleAssignmentStatement= ruleAssignmentStatement EOF
            {
             newCompositeNode(grammarAccess.getAssignmentStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentStatement=ruleAssignmentStatement();

            state._fsp--;

             current =iv_ruleAssignmentStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentStatement"


    // $ANTLR start "ruleAssignmentStatement"
    // InternalGeometryCodes.g:1045:1: ruleAssignmentStatement returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) ( (lv_operator_1_0= ruleAssignmentOperator ) ) ( (lv_value_2_0= ruleExpression ) ) ) ;
    public final EObject ruleAssignmentStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Enumerator lv_operator_1_0 = null;

        EObject lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1051:2: ( ( ( (otherlv_0= RULE_ID ) ) ( (lv_operator_1_0= ruleAssignmentOperator ) ) ( (lv_value_2_0= ruleExpression ) ) ) )
            // InternalGeometryCodes.g:1052:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_operator_1_0= ruleAssignmentOperator ) ) ( (lv_value_2_0= ruleExpression ) ) )
            {
            // InternalGeometryCodes.g:1052:2: ( ( (otherlv_0= RULE_ID ) ) ( (lv_operator_1_0= ruleAssignmentOperator ) ) ( (lv_value_2_0= ruleExpression ) ) )
            // InternalGeometryCodes.g:1053:3: ( (otherlv_0= RULE_ID ) ) ( (lv_operator_1_0= ruleAssignmentOperator ) ) ( (lv_value_2_0= ruleExpression ) )
            {
            // InternalGeometryCodes.g:1053:3: ( (otherlv_0= RULE_ID ) )
            // InternalGeometryCodes.g:1054:4: (otherlv_0= RULE_ID )
            {
            // InternalGeometryCodes.g:1054:4: (otherlv_0= RULE_ID )
            // InternalGeometryCodes.g:1055:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getAssignmentStatementRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_21); 

            					newLeafNode(otherlv_0, grammarAccess.getAssignmentStatementAccess().getTargetReferenceableElementCrossReference_0_0());
            				

            }


            }

            // InternalGeometryCodes.g:1066:3: ( (lv_operator_1_0= ruleAssignmentOperator ) )
            // InternalGeometryCodes.g:1067:4: (lv_operator_1_0= ruleAssignmentOperator )
            {
            // InternalGeometryCodes.g:1067:4: (lv_operator_1_0= ruleAssignmentOperator )
            // InternalGeometryCodes.g:1068:5: lv_operator_1_0= ruleAssignmentOperator
            {

            					newCompositeNode(grammarAccess.getAssignmentStatementAccess().getOperatorAssignmentOperatorEnumRuleCall_1_0());
            				
            pushFollow(FOLLOW_20);
            lv_operator_1_0=ruleAssignmentOperator();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssignmentStatementRule());
            					}
            					set(
            						current,
            						"operator",
            						lv_operator_1_0,
            						"de.adrianhoff.GeometryCodes.AssignmentOperator");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:1085:3: ( (lv_value_2_0= ruleExpression ) )
            // InternalGeometryCodes.g:1086:4: (lv_value_2_0= ruleExpression )
            {
            // InternalGeometryCodes.g:1086:4: (lv_value_2_0= ruleExpression )
            // InternalGeometryCodes.g:1087:5: lv_value_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getAssignmentStatementAccess().getValueExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getAssignmentStatementRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_2_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentStatement"


    // $ANTLR start "entryRuleReferenceStatement"
    // InternalGeometryCodes.g:1108:1: entryRuleReferenceStatement returns [EObject current=null] : iv_ruleReferenceStatement= ruleReferenceStatement EOF ;
    public final EObject entryRuleReferenceStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceStatement = null;


        try {
            // InternalGeometryCodes.g:1108:59: (iv_ruleReferenceStatement= ruleReferenceStatement EOF )
            // InternalGeometryCodes.g:1109:2: iv_ruleReferenceStatement= ruleReferenceStatement EOF
            {
             newCompositeNode(grammarAccess.getReferenceStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReferenceStatement=ruleReferenceStatement();

            state._fsp--;

             current =iv_ruleReferenceStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceStatement"


    // $ANTLR start "ruleReferenceStatement"
    // InternalGeometryCodes.g:1115:1: ruleReferenceStatement returns [EObject current=null] : ( (lv_reference_0_0= ruleReferenceExpression ) ) ;
    public final EObject ruleReferenceStatement() throws RecognitionException {
        EObject current = null;

        EObject lv_reference_0_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1121:2: ( ( (lv_reference_0_0= ruleReferenceExpression ) ) )
            // InternalGeometryCodes.g:1122:2: ( (lv_reference_0_0= ruleReferenceExpression ) )
            {
            // InternalGeometryCodes.g:1122:2: ( (lv_reference_0_0= ruleReferenceExpression ) )
            // InternalGeometryCodes.g:1123:3: (lv_reference_0_0= ruleReferenceExpression )
            {
            // InternalGeometryCodes.g:1123:3: (lv_reference_0_0= ruleReferenceExpression )
            // InternalGeometryCodes.g:1124:4: lv_reference_0_0= ruleReferenceExpression
            {

            				newCompositeNode(grammarAccess.getReferenceStatementAccess().getReferenceReferenceExpressionParserRuleCall_0());
            			
            pushFollow(FOLLOW_2);
            lv_reference_0_0=ruleReferenceExpression();

            state._fsp--;


            				if (current==null) {
            					current = createModelElementForParent(grammarAccess.getReferenceStatementRule());
            				}
            				set(
            					current,
            					"reference",
            					lv_reference_0_0,
            					"de.adrianhoff.GeometryCodes.ReferenceExpression");
            				afterParserOrEnumRuleCall();
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceStatement"


    // $ANTLR start "entryRuleReturnStatement"
    // InternalGeometryCodes.g:1144:1: entryRuleReturnStatement returns [EObject current=null] : iv_ruleReturnStatement= ruleReturnStatement EOF ;
    public final EObject entryRuleReturnStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReturnStatement = null;


        try {
            // InternalGeometryCodes.g:1144:56: (iv_ruleReturnStatement= ruleReturnStatement EOF )
            // InternalGeometryCodes.g:1145:2: iv_ruleReturnStatement= ruleReturnStatement EOF
            {
             newCompositeNode(grammarAccess.getReturnStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReturnStatement=ruleReturnStatement();

            state._fsp--;

             current =iv_ruleReturnStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturnStatement"


    // $ANTLR start "ruleReturnStatement"
    // InternalGeometryCodes.g:1151:1: ruleReturnStatement returns [EObject current=null] : ( () otherlv_1= 'return' ( ( (lv_value_2_0= ruleExpression ) ) | otherlv_3= 'nothing' ) ) ;
    public final EObject ruleReturnStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_value_2_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1157:2: ( ( () otherlv_1= 'return' ( ( (lv_value_2_0= ruleExpression ) ) | otherlv_3= 'nothing' ) ) )
            // InternalGeometryCodes.g:1158:2: ( () otherlv_1= 'return' ( ( (lv_value_2_0= ruleExpression ) ) | otherlv_3= 'nothing' ) )
            {
            // InternalGeometryCodes.g:1158:2: ( () otherlv_1= 'return' ( ( (lv_value_2_0= ruleExpression ) ) | otherlv_3= 'nothing' ) )
            // InternalGeometryCodes.g:1159:3: () otherlv_1= 'return' ( ( (lv_value_2_0= ruleExpression ) ) | otherlv_3= 'nothing' )
            {
            // InternalGeometryCodes.g:1159:3: ()
            // InternalGeometryCodes.g:1160:4: 
            {

            				current = forceCreateModelElement(
            					grammarAccess.getReturnStatementAccess().getReturnStatementAction_0(),
            					current);
            			

            }

            otherlv_1=(Token)match(input,26,FOLLOW_22); 

            			newLeafNode(otherlv_1, grammarAccess.getReturnStatementAccess().getReturnKeyword_1());
            		
            // InternalGeometryCodes.g:1170:3: ( ( (lv_value_2_0= ruleExpression ) ) | otherlv_3= 'nothing' )
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( ((LA13_0>=RULE_STRING && LA13_0<=RULE_FLOAT)||LA13_0==13||(LA13_0>=36 && LA13_0<=42)||(LA13_0>=44 && LA13_0<=45)) ) {
                alt13=1;
            }
            else if ( (LA13_0==27) ) {
                alt13=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }
            switch (alt13) {
                case 1 :
                    // InternalGeometryCodes.g:1171:4: ( (lv_value_2_0= ruleExpression ) )
                    {
                    // InternalGeometryCodes.g:1171:4: ( (lv_value_2_0= ruleExpression ) )
                    // InternalGeometryCodes.g:1172:5: (lv_value_2_0= ruleExpression )
                    {
                    // InternalGeometryCodes.g:1172:5: (lv_value_2_0= ruleExpression )
                    // InternalGeometryCodes.g:1173:6: lv_value_2_0= ruleExpression
                    {

                    						newCompositeNode(grammarAccess.getReturnStatementAccess().getValueExpressionParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_value_2_0=ruleExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReturnStatementRule());
                    						}
                    						add(
                    							current,
                    							"value",
                    							lv_value_2_0,
                    							"de.adrianhoff.GeometryCodes.Expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:1191:4: otherlv_3= 'nothing'
                    {
                    otherlv_3=(Token)match(input,27,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getReturnStatementAccess().getNothingKeyword_2_1());
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturnStatement"


    // $ANTLR start "entryRuleIfStatement"
    // InternalGeometryCodes.g:1200:1: entryRuleIfStatement returns [EObject current=null] : iv_ruleIfStatement= ruleIfStatement EOF ;
    public final EObject entryRuleIfStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIfStatement = null;


        try {
            // InternalGeometryCodes.g:1200:52: (iv_ruleIfStatement= ruleIfStatement EOF )
            // InternalGeometryCodes.g:1201:2: iv_ruleIfStatement= ruleIfStatement EOF
            {
             newCompositeNode(grammarAccess.getIfStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleIfStatement=ruleIfStatement();

            state._fsp--;

             current =iv_ruleIfStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIfStatement"


    // $ANTLR start "ruleIfStatement"
    // InternalGeometryCodes.g:1207:1: ruleIfStatement returns [EObject current=null] : (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}' ) ;
    public final EObject ruleIfStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_condition_2_0 = null;

        EObject lv_statements_5_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1213:2: ( (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}' ) )
            // InternalGeometryCodes.g:1214:2: (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}' )
            {
            // InternalGeometryCodes.g:1214:2: (otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}' )
            // InternalGeometryCodes.g:1215:3: otherlv_0= 'if' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getIfStatementAccess().getIfKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getIfStatementAccess().getLeftParenthesisKeyword_1());
            		
            // InternalGeometryCodes.g:1223:3: ( (lv_condition_2_0= ruleExpression ) )
            // InternalGeometryCodes.g:1224:4: (lv_condition_2_0= ruleExpression )
            {
            // InternalGeometryCodes.g:1224:4: (lv_condition_2_0= ruleExpression )
            // InternalGeometryCodes.g:1225:5: lv_condition_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getIfStatementAccess().getConditionExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_23);
            lv_condition_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getIfStatementRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_2_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,15,FOLLOW_11); 

            			newLeafNode(otherlv_3, grammarAccess.getIfStatementAccess().getRightParenthesisKeyword_3());
            		
            otherlv_4=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_4, grammarAccess.getIfStatementAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalGeometryCodes.g:1250:3: ( (lv_statements_5_0= ruleStatement ) )*
            loop14:
            do {
                int alt14=2;
                int LA14_0 = input.LA(1);

                if ( (LA14_0==RULE_ID||LA14_0==26||(LA14_0>=28 && LA14_0<=29)||(LA14_0>=33 && LA14_0<=34)||LA14_0==36||(LA14_0>=38 && LA14_0<=39)||(LA14_0>=57 && LA14_0<=59)) ) {
                    alt14=1;
                }


                switch (alt14) {
            	case 1 :
            	    // InternalGeometryCodes.g:1251:4: (lv_statements_5_0= ruleStatement )
            	    {
            	    // InternalGeometryCodes.g:1251:4: (lv_statements_5_0= ruleStatement )
            	    // InternalGeometryCodes.g:1252:5: lv_statements_5_0= ruleStatement
            	    {

            	    					newCompositeNode(grammarAccess.getIfStatementAccess().getStatementsStatementParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_statements_5_0=ruleStatement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getIfStatementRule());
            	    					}
            	    					add(
            	    						current,
            	    						"statements",
            	    						lv_statements_5_0,
            	    						"de.adrianhoff.GeometryCodes.Statement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop14;
                }
            } while (true);

            otherlv_6=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getIfStatementAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIfStatement"


    // $ANTLR start "entryRuleForLoopStatement"
    // InternalGeometryCodes.g:1277:1: entryRuleForLoopStatement returns [EObject current=null] : iv_ruleForLoopStatement= ruleForLoopStatement EOF ;
    public final EObject entryRuleForLoopStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForLoopStatement = null;


        try {
            // InternalGeometryCodes.g:1277:57: (iv_ruleForLoopStatement= ruleForLoopStatement EOF )
            // InternalGeometryCodes.g:1278:2: iv_ruleForLoopStatement= ruleForLoopStatement EOF
            {
             newCompositeNode(grammarAccess.getForLoopStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleForLoopStatement=ruleForLoopStatement();

            state._fsp--;

             current =iv_ruleForLoopStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForLoopStatement"


    // $ANTLR start "ruleForLoopStatement"
    // InternalGeometryCodes.g:1284:1: ruleForLoopStatement returns [EObject current=null] : (otherlv_0= 'for' otherlv_1= '(' ( (lv_type_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= 'from' ( (lv_fromValue_5_0= ruleExpression ) ) otherlv_6= 'to' ( (lv_toValue_7_0= ruleExpression ) ) otherlv_8= ')' otherlv_9= '{' ( (lv_statements_10_0= ruleStatement ) )* otherlv_11= '}' ) ;
    public final EObject ruleForLoopStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token lv_name_3_0=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token otherlv_8=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Enumerator lv_type_2_0 = null;

        EObject lv_fromValue_5_0 = null;

        EObject lv_toValue_7_0 = null;

        EObject lv_statements_10_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1290:2: ( (otherlv_0= 'for' otherlv_1= '(' ( (lv_type_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= 'from' ( (lv_fromValue_5_0= ruleExpression ) ) otherlv_6= 'to' ( (lv_toValue_7_0= ruleExpression ) ) otherlv_8= ')' otherlv_9= '{' ( (lv_statements_10_0= ruleStatement ) )* otherlv_11= '}' ) )
            // InternalGeometryCodes.g:1291:2: (otherlv_0= 'for' otherlv_1= '(' ( (lv_type_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= 'from' ( (lv_fromValue_5_0= ruleExpression ) ) otherlv_6= 'to' ( (lv_toValue_7_0= ruleExpression ) ) otherlv_8= ')' otherlv_9= '{' ( (lv_statements_10_0= ruleStatement ) )* otherlv_11= '}' )
            {
            // InternalGeometryCodes.g:1291:2: (otherlv_0= 'for' otherlv_1= '(' ( (lv_type_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= 'from' ( (lv_fromValue_5_0= ruleExpression ) ) otherlv_6= 'to' ( (lv_toValue_7_0= ruleExpression ) ) otherlv_8= ')' otherlv_9= '{' ( (lv_statements_10_0= ruleStatement ) )* otherlv_11= '}' )
            // InternalGeometryCodes.g:1292:3: otherlv_0= 'for' otherlv_1= '(' ( (lv_type_2_0= ruleType ) ) ( (lv_name_3_0= RULE_ID ) ) otherlv_4= 'from' ( (lv_fromValue_5_0= ruleExpression ) ) otherlv_6= 'to' ( (lv_toValue_7_0= ruleExpression ) ) otherlv_8= ')' otherlv_9= '{' ( (lv_statements_10_0= ruleStatement ) )* otherlv_11= '}'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getForLoopStatementAccess().getForKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_10); 

            			newLeafNode(otherlv_1, grammarAccess.getForLoopStatementAccess().getLeftParenthesisKeyword_1());
            		
            // InternalGeometryCodes.g:1300:3: ( (lv_type_2_0= ruleType ) )
            // InternalGeometryCodes.g:1301:4: (lv_type_2_0= ruleType )
            {
            // InternalGeometryCodes.g:1301:4: (lv_type_2_0= ruleType )
            // InternalGeometryCodes.g:1302:5: lv_type_2_0= ruleType
            {

            					newCompositeNode(grammarAccess.getForLoopStatementAccess().getTypeTypeEnumRuleCall_2_0());
            				
            pushFollow(FOLLOW_6);
            lv_type_2_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getForLoopStatementRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_2_0,
            						"de.adrianhoff.GeometryCodes.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:1319:3: ( (lv_name_3_0= RULE_ID ) )
            // InternalGeometryCodes.g:1320:4: (lv_name_3_0= RULE_ID )
            {
            // InternalGeometryCodes.g:1320:4: (lv_name_3_0= RULE_ID )
            // InternalGeometryCodes.g:1321:5: lv_name_3_0= RULE_ID
            {
            lv_name_3_0=(Token)match(input,RULE_ID,FOLLOW_24); 

            					newLeafNode(lv_name_3_0, grammarAccess.getForLoopStatementAccess().getNameIDTerminalRuleCall_3_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getForLoopStatementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_3_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_4=(Token)match(input,30,FOLLOW_20); 

            			newLeafNode(otherlv_4, grammarAccess.getForLoopStatementAccess().getFromKeyword_4());
            		
            // InternalGeometryCodes.g:1341:3: ( (lv_fromValue_5_0= ruleExpression ) )
            // InternalGeometryCodes.g:1342:4: (lv_fromValue_5_0= ruleExpression )
            {
            // InternalGeometryCodes.g:1342:4: (lv_fromValue_5_0= ruleExpression )
            // InternalGeometryCodes.g:1343:5: lv_fromValue_5_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getForLoopStatementAccess().getFromValueExpressionParserRuleCall_5_0());
            				
            pushFollow(FOLLOW_25);
            lv_fromValue_5_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getForLoopStatementRule());
            					}
            					set(
            						current,
            						"fromValue",
            						lv_fromValue_5_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_6=(Token)match(input,31,FOLLOW_20); 

            			newLeafNode(otherlv_6, grammarAccess.getForLoopStatementAccess().getToKeyword_6());
            		
            // InternalGeometryCodes.g:1364:3: ( (lv_toValue_7_0= ruleExpression ) )
            // InternalGeometryCodes.g:1365:4: (lv_toValue_7_0= ruleExpression )
            {
            // InternalGeometryCodes.g:1365:4: (lv_toValue_7_0= ruleExpression )
            // InternalGeometryCodes.g:1366:5: lv_toValue_7_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getForLoopStatementAccess().getToValueExpressionParserRuleCall_7_0());
            				
            pushFollow(FOLLOW_23);
            lv_toValue_7_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getForLoopStatementRule());
            					}
            					set(
            						current,
            						"toValue",
            						lv_toValue_7_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_8=(Token)match(input,15,FOLLOW_11); 

            			newLeafNode(otherlv_8, grammarAccess.getForLoopStatementAccess().getRightParenthesisKeyword_8());
            		
            otherlv_9=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_9, grammarAccess.getForLoopStatementAccess().getLeftCurlyBracketKeyword_9());
            		
            // InternalGeometryCodes.g:1391:3: ( (lv_statements_10_0= ruleStatement ) )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==RULE_ID||LA15_0==26||(LA15_0>=28 && LA15_0<=29)||(LA15_0>=33 && LA15_0<=34)||LA15_0==36||(LA15_0>=38 && LA15_0<=39)||(LA15_0>=57 && LA15_0<=59)) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // InternalGeometryCodes.g:1392:4: (lv_statements_10_0= ruleStatement )
            	    {
            	    // InternalGeometryCodes.g:1392:4: (lv_statements_10_0= ruleStatement )
            	    // InternalGeometryCodes.g:1393:5: lv_statements_10_0= ruleStatement
            	    {

            	    					newCompositeNode(grammarAccess.getForLoopStatementAccess().getStatementsStatementParserRuleCall_10_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_statements_10_0=ruleStatement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getForLoopStatementRule());
            	    					}
            	    					add(
            	    						current,
            	    						"statements",
            	    						lv_statements_10_0,
            	    						"de.adrianhoff.GeometryCodes.Statement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);

            otherlv_11=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_11, grammarAccess.getForLoopStatementAccess().getRightCurlyBracketKeyword_11());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForLoopStatement"


    // $ANTLR start "entryRuleForEachLoopStatement"
    // InternalGeometryCodes.g:1418:1: entryRuleForEachLoopStatement returns [EObject current=null] : iv_ruleForEachLoopStatement= ruleForEachLoopStatement EOF ;
    public final EObject entryRuleForEachLoopStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleForEachLoopStatement = null;


        try {
            // InternalGeometryCodes.g:1418:61: (iv_ruleForEachLoopStatement= ruleForEachLoopStatement EOF )
            // InternalGeometryCodes.g:1419:2: iv_ruleForEachLoopStatement= ruleForEachLoopStatement EOF
            {
             newCompositeNode(grammarAccess.getForEachLoopStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleForEachLoopStatement=ruleForEachLoopStatement();

            state._fsp--;

             current =iv_ruleForEachLoopStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleForEachLoopStatement"


    // $ANTLR start "ruleForEachLoopStatement"
    // InternalGeometryCodes.g:1425:1: ruleForEachLoopStatement returns [EObject current=null] : (otherlv_0= 'for' otherlv_1= 'each' otherlv_2= '(' ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) otherlv_5= 'in' ( (lv_value_6_0= ruleExpression ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statements_9_0= ruleStatement ) )* otherlv_10= '}' ) ;
    public final EObject ruleForEachLoopStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_2=null;
        Token lv_name_4_0=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Enumerator lv_type_3_0 = null;

        EObject lv_value_6_0 = null;

        EObject lv_statements_9_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1431:2: ( (otherlv_0= 'for' otherlv_1= 'each' otherlv_2= '(' ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) otherlv_5= 'in' ( (lv_value_6_0= ruleExpression ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statements_9_0= ruleStatement ) )* otherlv_10= '}' ) )
            // InternalGeometryCodes.g:1432:2: (otherlv_0= 'for' otherlv_1= 'each' otherlv_2= '(' ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) otherlv_5= 'in' ( (lv_value_6_0= ruleExpression ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statements_9_0= ruleStatement ) )* otherlv_10= '}' )
            {
            // InternalGeometryCodes.g:1432:2: (otherlv_0= 'for' otherlv_1= 'each' otherlv_2= '(' ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) otherlv_5= 'in' ( (lv_value_6_0= ruleExpression ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statements_9_0= ruleStatement ) )* otherlv_10= '}' )
            // InternalGeometryCodes.g:1433:3: otherlv_0= 'for' otherlv_1= 'each' otherlv_2= '(' ( (lv_type_3_0= ruleType ) ) ( (lv_name_4_0= RULE_ID ) ) otherlv_5= 'in' ( (lv_value_6_0= ruleExpression ) ) otherlv_7= ')' otherlv_8= '{' ( (lv_statements_9_0= ruleStatement ) )* otherlv_10= '}'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_14); 

            			newLeafNode(otherlv_0, grammarAccess.getForEachLoopStatementAccess().getForKeyword_0());
            		
            otherlv_1=(Token)match(input,20,FOLLOW_7); 

            			newLeafNode(otherlv_1, grammarAccess.getForEachLoopStatementAccess().getEachKeyword_1());
            		
            otherlv_2=(Token)match(input,13,FOLLOW_10); 

            			newLeafNode(otherlv_2, grammarAccess.getForEachLoopStatementAccess().getLeftParenthesisKeyword_2());
            		
            // InternalGeometryCodes.g:1445:3: ( (lv_type_3_0= ruleType ) )
            // InternalGeometryCodes.g:1446:4: (lv_type_3_0= ruleType )
            {
            // InternalGeometryCodes.g:1446:4: (lv_type_3_0= ruleType )
            // InternalGeometryCodes.g:1447:5: lv_type_3_0= ruleType
            {

            					newCompositeNode(grammarAccess.getForEachLoopStatementAccess().getTypeTypeEnumRuleCall_3_0());
            				
            pushFollow(FOLLOW_6);
            lv_type_3_0=ruleType();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getForEachLoopStatementRule());
            					}
            					set(
            						current,
            						"type",
            						lv_type_3_0,
            						"de.adrianhoff.GeometryCodes.Type");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:1464:3: ( (lv_name_4_0= RULE_ID ) )
            // InternalGeometryCodes.g:1465:4: (lv_name_4_0= RULE_ID )
            {
            // InternalGeometryCodes.g:1465:4: (lv_name_4_0= RULE_ID )
            // InternalGeometryCodes.g:1466:5: lv_name_4_0= RULE_ID
            {
            lv_name_4_0=(Token)match(input,RULE_ID,FOLLOW_26); 

            					newLeafNode(lv_name_4_0, grammarAccess.getForEachLoopStatementAccess().getNameIDTerminalRuleCall_4_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getForEachLoopStatementRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_4_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_5=(Token)match(input,32,FOLLOW_20); 

            			newLeafNode(otherlv_5, grammarAccess.getForEachLoopStatementAccess().getInKeyword_5());
            		
            // InternalGeometryCodes.g:1486:3: ( (lv_value_6_0= ruleExpression ) )
            // InternalGeometryCodes.g:1487:4: (lv_value_6_0= ruleExpression )
            {
            // InternalGeometryCodes.g:1487:4: (lv_value_6_0= ruleExpression )
            // InternalGeometryCodes.g:1488:5: lv_value_6_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getForEachLoopStatementAccess().getValueExpressionParserRuleCall_6_0());
            				
            pushFollow(FOLLOW_23);
            lv_value_6_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getForEachLoopStatementRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_6_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_7=(Token)match(input,15,FOLLOW_11); 

            			newLeafNode(otherlv_7, grammarAccess.getForEachLoopStatementAccess().getRightParenthesisKeyword_7());
            		
            otherlv_8=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_8, grammarAccess.getForEachLoopStatementAccess().getLeftCurlyBracketKeyword_8());
            		
            // InternalGeometryCodes.g:1513:3: ( (lv_statements_9_0= ruleStatement ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==RULE_ID||LA16_0==26||(LA16_0>=28 && LA16_0<=29)||(LA16_0>=33 && LA16_0<=34)||LA16_0==36||(LA16_0>=38 && LA16_0<=39)||(LA16_0>=57 && LA16_0<=59)) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalGeometryCodes.g:1514:4: (lv_statements_9_0= ruleStatement )
            	    {
            	    // InternalGeometryCodes.g:1514:4: (lv_statements_9_0= ruleStatement )
            	    // InternalGeometryCodes.g:1515:5: lv_statements_9_0= ruleStatement
            	    {

            	    					newCompositeNode(grammarAccess.getForEachLoopStatementAccess().getStatementsStatementParserRuleCall_9_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_statements_9_0=ruleStatement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getForEachLoopStatementRule());
            	    					}
            	    					add(
            	    						current,
            	    						"statements",
            	    						lv_statements_9_0,
            	    						"de.adrianhoff.GeometryCodes.Statement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            otherlv_10=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_10, grammarAccess.getForEachLoopStatementAccess().getRightCurlyBracketKeyword_10());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleForEachLoopStatement"


    // $ANTLR start "entryRuleWhileLoopStatement"
    // InternalGeometryCodes.g:1540:1: entryRuleWhileLoopStatement returns [EObject current=null] : iv_ruleWhileLoopStatement= ruleWhileLoopStatement EOF ;
    public final EObject entryRuleWhileLoopStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWhileLoopStatement = null;


        try {
            // InternalGeometryCodes.g:1540:59: (iv_ruleWhileLoopStatement= ruleWhileLoopStatement EOF )
            // InternalGeometryCodes.g:1541:2: iv_ruleWhileLoopStatement= ruleWhileLoopStatement EOF
            {
             newCompositeNode(grammarAccess.getWhileLoopStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleWhileLoopStatement=ruleWhileLoopStatement();

            state._fsp--;

             current =iv_ruleWhileLoopStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWhileLoopStatement"


    // $ANTLR start "ruleWhileLoopStatement"
    // InternalGeometryCodes.g:1547:1: ruleWhileLoopStatement returns [EObject current=null] : (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}' ) ;
    public final EObject ruleWhileLoopStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_condition_2_0 = null;

        EObject lv_statements_5_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1553:2: ( (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}' ) )
            // InternalGeometryCodes.g:1554:2: (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}' )
            {
            // InternalGeometryCodes.g:1554:2: (otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}' )
            // InternalGeometryCodes.g:1555:3: otherlv_0= 'while' otherlv_1= '(' ( (lv_condition_2_0= ruleExpression ) ) otherlv_3= ')' otherlv_4= '{' ( (lv_statements_5_0= ruleStatement ) )* otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,33,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getWhileLoopStatementAccess().getWhileKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getWhileLoopStatementAccess().getLeftParenthesisKeyword_1());
            		
            // InternalGeometryCodes.g:1563:3: ( (lv_condition_2_0= ruleExpression ) )
            // InternalGeometryCodes.g:1564:4: (lv_condition_2_0= ruleExpression )
            {
            // InternalGeometryCodes.g:1564:4: (lv_condition_2_0= ruleExpression )
            // InternalGeometryCodes.g:1565:5: lv_condition_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getWhileLoopStatementAccess().getConditionExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_23);
            lv_condition_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getWhileLoopStatementRule());
            					}
            					set(
            						current,
            						"condition",
            						lv_condition_2_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            otherlv_3=(Token)match(input,15,FOLLOW_11); 

            			newLeafNode(otherlv_3, grammarAccess.getWhileLoopStatementAccess().getRightParenthesisKeyword_3());
            		
            otherlv_4=(Token)match(input,16,FOLLOW_12); 

            			newLeafNode(otherlv_4, grammarAccess.getWhileLoopStatementAccess().getLeftCurlyBracketKeyword_4());
            		
            // InternalGeometryCodes.g:1590:3: ( (lv_statements_5_0= ruleStatement ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==RULE_ID||LA17_0==26||(LA17_0>=28 && LA17_0<=29)||(LA17_0>=33 && LA17_0<=34)||LA17_0==36||(LA17_0>=38 && LA17_0<=39)||(LA17_0>=57 && LA17_0<=59)) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalGeometryCodes.g:1591:4: (lv_statements_5_0= ruleStatement )
            	    {
            	    // InternalGeometryCodes.g:1591:4: (lv_statements_5_0= ruleStatement )
            	    // InternalGeometryCodes.g:1592:5: lv_statements_5_0= ruleStatement
            	    {

            	    					newCompositeNode(grammarAccess.getWhileLoopStatementAccess().getStatementsStatementParserRuleCall_5_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_statements_5_0=ruleStatement();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getWhileLoopStatementRule());
            	    					}
            	    					add(
            	    						current,
            	    						"statements",
            	    						lv_statements_5_0,
            	    						"de.adrianhoff.GeometryCodes.Statement");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            otherlv_6=(Token)match(input,17,FOLLOW_2); 

            			newLeafNode(otherlv_6, grammarAccess.getWhileLoopStatementAccess().getRightCurlyBracketKeyword_6());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWhileLoopStatement"


    // $ANTLR start "entryRuleOutputStatement"
    // InternalGeometryCodes.g:1617:1: entryRuleOutputStatement returns [EObject current=null] : iv_ruleOutputStatement= ruleOutputStatement EOF ;
    public final EObject entryRuleOutputStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputStatement = null;


        try {
            // InternalGeometryCodes.g:1617:56: (iv_ruleOutputStatement= ruleOutputStatement EOF )
            // InternalGeometryCodes.g:1618:2: iv_ruleOutputStatement= ruleOutputStatement EOF
            {
             newCompositeNode(grammarAccess.getOutputStatementRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOutputStatement=ruleOutputStatement();

            state._fsp--;

             current =iv_ruleOutputStatement; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputStatement"


    // $ANTLR start "ruleOutputStatement"
    // InternalGeometryCodes.g:1624:1: ruleOutputStatement returns [EObject current=null] : (otherlv_0= 'output' ( (lv_value_1_0= ruleExpression ) ) ) ;
    public final EObject ruleOutputStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_value_1_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1630:2: ( (otherlv_0= 'output' ( (lv_value_1_0= ruleExpression ) ) ) )
            // InternalGeometryCodes.g:1631:2: (otherlv_0= 'output' ( (lv_value_1_0= ruleExpression ) ) )
            {
            // InternalGeometryCodes.g:1631:2: (otherlv_0= 'output' ( (lv_value_1_0= ruleExpression ) ) )
            // InternalGeometryCodes.g:1632:3: otherlv_0= 'output' ( (lv_value_1_0= ruleExpression ) )
            {
            otherlv_0=(Token)match(input,34,FOLLOW_20); 

            			newLeafNode(otherlv_0, grammarAccess.getOutputStatementAccess().getOutputKeyword_0());
            		
            // InternalGeometryCodes.g:1636:3: ( (lv_value_1_0= ruleExpression ) )
            // InternalGeometryCodes.g:1637:4: (lv_value_1_0= ruleExpression )
            {
            // InternalGeometryCodes.g:1637:4: (lv_value_1_0= ruleExpression )
            // InternalGeometryCodes.g:1638:5: lv_value_1_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getOutputStatementAccess().getValueExpressionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_value_1_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getOutputStatementRule());
            					}
            					set(
            						current,
            						"value",
            						lv_value_1_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputStatement"


    // $ANTLR start "entryRuleExpression"
    // InternalGeometryCodes.g:1659:1: entryRuleExpression returns [EObject current=null] : iv_ruleExpression= ruleExpression EOF ;
    public final EObject entryRuleExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleExpression = null;


        try {
            // InternalGeometryCodes.g:1659:51: (iv_ruleExpression= ruleExpression EOF )
            // InternalGeometryCodes.g:1660:2: iv_ruleExpression= ruleExpression EOF
            {
             newCompositeNode(grammarAccess.getExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleExpression=ruleExpression();

            state._fsp--;

             current =iv_ruleExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExpression"


    // $ANTLR start "ruleExpression"
    // InternalGeometryCodes.g:1666:1: ruleExpression returns [EObject current=null] : this_LogicalExpression_0= ruleLogicalExpression ;
    public final EObject ruleExpression() throws RecognitionException {
        EObject current = null;

        EObject this_LogicalExpression_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1672:2: (this_LogicalExpression_0= ruleLogicalExpression )
            // InternalGeometryCodes.g:1673:2: this_LogicalExpression_0= ruleLogicalExpression
            {

            		newCompositeNode(grammarAccess.getExpressionAccess().getLogicalExpressionParserRuleCall());
            	
            pushFollow(FOLLOW_2);
            this_LogicalExpression_0=ruleLogicalExpression();

            state._fsp--;


            		current = this_LogicalExpression_0;
            		afterParserOrEnumRuleCall();
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExpression"


    // $ANTLR start "entryRuleLogicalExpression"
    // InternalGeometryCodes.g:1684:1: entryRuleLogicalExpression returns [EObject current=null] : iv_ruleLogicalExpression= ruleLogicalExpression EOF ;
    public final EObject entryRuleLogicalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLogicalExpression = null;


        try {
            // InternalGeometryCodes.g:1684:58: (iv_ruleLogicalExpression= ruleLogicalExpression EOF )
            // InternalGeometryCodes.g:1685:2: iv_ruleLogicalExpression= ruleLogicalExpression EOF
            {
             newCompositeNode(grammarAccess.getLogicalExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLogicalExpression=ruleLogicalExpression();

            state._fsp--;

             current =iv_ruleLogicalExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLogicalExpression"


    // $ANTLR start "ruleLogicalExpression"
    // InternalGeometryCodes.g:1691:1: ruleLogicalExpression returns [EObject current=null] : (this_ConditionalExpression_0= ruleConditionalExpression ( () ( (lv_operator_2_0= ruleLogicalOperator ) ) ( (lv_rightChild_3_0= ruleConditionalExpression ) ) )* ) ;
    public final EObject ruleLogicalExpression() throws RecognitionException {
        EObject current = null;

        EObject this_ConditionalExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightChild_3_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1697:2: ( (this_ConditionalExpression_0= ruleConditionalExpression ( () ( (lv_operator_2_0= ruleLogicalOperator ) ) ( (lv_rightChild_3_0= ruleConditionalExpression ) ) )* ) )
            // InternalGeometryCodes.g:1698:2: (this_ConditionalExpression_0= ruleConditionalExpression ( () ( (lv_operator_2_0= ruleLogicalOperator ) ) ( (lv_rightChild_3_0= ruleConditionalExpression ) ) )* )
            {
            // InternalGeometryCodes.g:1698:2: (this_ConditionalExpression_0= ruleConditionalExpression ( () ( (lv_operator_2_0= ruleLogicalOperator ) ) ( (lv_rightChild_3_0= ruleConditionalExpression ) ) )* )
            // InternalGeometryCodes.g:1699:3: this_ConditionalExpression_0= ruleConditionalExpression ( () ( (lv_operator_2_0= ruleLogicalOperator ) ) ( (lv_rightChild_3_0= ruleConditionalExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getLogicalExpressionAccess().getConditionalExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_27);
            this_ConditionalExpression_0=ruleConditionalExpression();

            state._fsp--;


            			current = this_ConditionalExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalGeometryCodes.g:1707:3: ( () ( (lv_operator_2_0= ruleLogicalOperator ) ) ( (lv_rightChild_3_0= ruleConditionalExpression ) ) )*
            loop18:
            do {
                int alt18=2;
                int LA18_0 = input.LA(1);

                if ( ((LA18_0>=55 && LA18_0<=56)) ) {
                    alt18=1;
                }


                switch (alt18) {
            	case 1 :
            	    // InternalGeometryCodes.g:1708:4: () ( (lv_operator_2_0= ruleLogicalOperator ) ) ( (lv_rightChild_3_0= ruleConditionalExpression ) )
            	    {
            	    // InternalGeometryCodes.g:1708:4: ()
            	    // InternalGeometryCodes.g:1709:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getLogicalExpressionAccess().getLogicalExpressionLeftChildAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalGeometryCodes.g:1715:4: ( (lv_operator_2_0= ruleLogicalOperator ) )
            	    // InternalGeometryCodes.g:1716:5: (lv_operator_2_0= ruleLogicalOperator )
            	    {
            	    // InternalGeometryCodes.g:1716:5: (lv_operator_2_0= ruleLogicalOperator )
            	    // InternalGeometryCodes.g:1717:6: lv_operator_2_0= ruleLogicalOperator
            	    {

            	    						newCompositeNode(grammarAccess.getLogicalExpressionAccess().getOperatorLogicalOperatorEnumRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_20);
            	    lv_operator_2_0=ruleLogicalOperator();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLogicalExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"operator",
            	    							lv_operator_2_0,
            	    							"de.adrianhoff.GeometryCodes.LogicalOperator");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalGeometryCodes.g:1734:4: ( (lv_rightChild_3_0= ruleConditionalExpression ) )
            	    // InternalGeometryCodes.g:1735:5: (lv_rightChild_3_0= ruleConditionalExpression )
            	    {
            	    // InternalGeometryCodes.g:1735:5: (lv_rightChild_3_0= ruleConditionalExpression )
            	    // InternalGeometryCodes.g:1736:6: lv_rightChild_3_0= ruleConditionalExpression
            	    {

            	    						newCompositeNode(grammarAccess.getLogicalExpressionAccess().getRightChildConditionalExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_27);
            	    lv_rightChild_3_0=ruleConditionalExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getLogicalExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rightChild",
            	    							lv_rightChild_3_0,
            	    							"de.adrianhoff.GeometryCodes.ConditionalExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop18;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalExpression"


    // $ANTLR start "entryRuleConditionalExpression"
    // InternalGeometryCodes.g:1758:1: entryRuleConditionalExpression returns [EObject current=null] : iv_ruleConditionalExpression= ruleConditionalExpression EOF ;
    public final EObject entryRuleConditionalExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConditionalExpression = null;


        try {
            // InternalGeometryCodes.g:1758:62: (iv_ruleConditionalExpression= ruleConditionalExpression EOF )
            // InternalGeometryCodes.g:1759:2: iv_ruleConditionalExpression= ruleConditionalExpression EOF
            {
             newCompositeNode(grammarAccess.getConditionalExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConditionalExpression=ruleConditionalExpression();

            state._fsp--;

             current =iv_ruleConditionalExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConditionalExpression"


    // $ANTLR start "ruleConditionalExpression"
    // InternalGeometryCodes.g:1765:1: ruleConditionalExpression returns [EObject current=null] : (this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operator_2_0= ruleComparisonOperator ) ) ( (lv_rightChild_3_0= ruleAdditiveExpression ) ) )* ) ;
    public final EObject ruleConditionalExpression() throws RecognitionException {
        EObject current = null;

        EObject this_AdditiveExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightChild_3_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1771:2: ( (this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operator_2_0= ruleComparisonOperator ) ) ( (lv_rightChild_3_0= ruleAdditiveExpression ) ) )* ) )
            // InternalGeometryCodes.g:1772:2: (this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operator_2_0= ruleComparisonOperator ) ) ( (lv_rightChild_3_0= ruleAdditiveExpression ) ) )* )
            {
            // InternalGeometryCodes.g:1772:2: (this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operator_2_0= ruleComparisonOperator ) ) ( (lv_rightChild_3_0= ruleAdditiveExpression ) ) )* )
            // InternalGeometryCodes.g:1773:3: this_AdditiveExpression_0= ruleAdditiveExpression ( () ( (lv_operator_2_0= ruleComparisonOperator ) ) ( (lv_rightChild_3_0= ruleAdditiveExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getConditionalExpressionAccess().getAdditiveExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_28);
            this_AdditiveExpression_0=ruleAdditiveExpression();

            state._fsp--;


            			current = this_AdditiveExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalGeometryCodes.g:1781:3: ( () ( (lv_operator_2_0= ruleComparisonOperator ) ) ( (lv_rightChild_3_0= ruleAdditiveExpression ) ) )*
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>=49 && LA19_0<=54)) ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalGeometryCodes.g:1782:4: () ( (lv_operator_2_0= ruleComparisonOperator ) ) ( (lv_rightChild_3_0= ruleAdditiveExpression ) )
            	    {
            	    // InternalGeometryCodes.g:1782:4: ()
            	    // InternalGeometryCodes.g:1783:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getConditionalExpressionAccess().getConditionalExpressionLeftChildAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalGeometryCodes.g:1789:4: ( (lv_operator_2_0= ruleComparisonOperator ) )
            	    // InternalGeometryCodes.g:1790:5: (lv_operator_2_0= ruleComparisonOperator )
            	    {
            	    // InternalGeometryCodes.g:1790:5: (lv_operator_2_0= ruleComparisonOperator )
            	    // InternalGeometryCodes.g:1791:6: lv_operator_2_0= ruleComparisonOperator
            	    {

            	    						newCompositeNode(grammarAccess.getConditionalExpressionAccess().getOperatorComparisonOperatorEnumRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_20);
            	    lv_operator_2_0=ruleComparisonOperator();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getConditionalExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"operator",
            	    							lv_operator_2_0,
            	    							"de.adrianhoff.GeometryCodes.ComparisonOperator");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalGeometryCodes.g:1808:4: ( (lv_rightChild_3_0= ruleAdditiveExpression ) )
            	    // InternalGeometryCodes.g:1809:5: (lv_rightChild_3_0= ruleAdditiveExpression )
            	    {
            	    // InternalGeometryCodes.g:1809:5: (lv_rightChild_3_0= ruleAdditiveExpression )
            	    // InternalGeometryCodes.g:1810:6: lv_rightChild_3_0= ruleAdditiveExpression
            	    {

            	    						newCompositeNode(grammarAccess.getConditionalExpressionAccess().getRightChildAdditiveExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_28);
            	    lv_rightChild_3_0=ruleAdditiveExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getConditionalExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rightChild",
            	    							lv_rightChild_3_0,
            	    							"de.adrianhoff.GeometryCodes.AdditiveExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop19;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConditionalExpression"


    // $ANTLR start "entryRuleAdditiveExpression"
    // InternalGeometryCodes.g:1832:1: entryRuleAdditiveExpression returns [EObject current=null] : iv_ruleAdditiveExpression= ruleAdditiveExpression EOF ;
    public final EObject entryRuleAdditiveExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAdditiveExpression = null;


        try {
            // InternalGeometryCodes.g:1832:59: (iv_ruleAdditiveExpression= ruleAdditiveExpression EOF )
            // InternalGeometryCodes.g:1833:2: iv_ruleAdditiveExpression= ruleAdditiveExpression EOF
            {
             newCompositeNode(grammarAccess.getAdditiveExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleAdditiveExpression=ruleAdditiveExpression();

            state._fsp--;

             current =iv_ruleAdditiveExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAdditiveExpression"


    // $ANTLR start "ruleAdditiveExpression"
    // InternalGeometryCodes.g:1839:1: ruleAdditiveExpression returns [EObject current=null] : (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ( (lv_rightChild_3_0= ruleMultiplicativeExpression ) ) )* ) ;
    public final EObject ruleAdditiveExpression() throws RecognitionException {
        EObject current = null;

        EObject this_MultiplicativeExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightChild_3_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1845:2: ( (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ( (lv_rightChild_3_0= ruleMultiplicativeExpression ) ) )* ) )
            // InternalGeometryCodes.g:1846:2: (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ( (lv_rightChild_3_0= ruleMultiplicativeExpression ) ) )* )
            {
            // InternalGeometryCodes.g:1846:2: (this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ( (lv_rightChild_3_0= ruleMultiplicativeExpression ) ) )* )
            // InternalGeometryCodes.g:1847:3: this_MultiplicativeExpression_0= ruleMultiplicativeExpression ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ( (lv_rightChild_3_0= ruleMultiplicativeExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getMultiplicativeExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_29);
            this_MultiplicativeExpression_0=ruleMultiplicativeExpression();

            state._fsp--;


            			current = this_MultiplicativeExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalGeometryCodes.g:1855:3: ( () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ( (lv_rightChild_3_0= ruleMultiplicativeExpression ) ) )*
            loop20:
            do {
                int alt20=2;
                int LA20_0 = input.LA(1);

                if ( ((LA20_0>=45 && LA20_0<=46)) ) {
                    alt20=1;
                }


                switch (alt20) {
            	case 1 :
            	    // InternalGeometryCodes.g:1856:4: () ( (lv_operator_2_0= ruleAdditiveOperator ) ) ( (lv_rightChild_3_0= ruleMultiplicativeExpression ) )
            	    {
            	    // InternalGeometryCodes.g:1856:4: ()
            	    // InternalGeometryCodes.g:1857:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getAdditiveExpressionAccess().getAdditiveExpressionLeftChildAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalGeometryCodes.g:1863:4: ( (lv_operator_2_0= ruleAdditiveOperator ) )
            	    // InternalGeometryCodes.g:1864:5: (lv_operator_2_0= ruleAdditiveOperator )
            	    {
            	    // InternalGeometryCodes.g:1864:5: (lv_operator_2_0= ruleAdditiveOperator )
            	    // InternalGeometryCodes.g:1865:6: lv_operator_2_0= ruleAdditiveOperator
            	    {

            	    						newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getOperatorAdditiveOperatorEnumRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_20);
            	    lv_operator_2_0=ruleAdditiveOperator();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAdditiveExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"operator",
            	    							lv_operator_2_0,
            	    							"de.adrianhoff.GeometryCodes.AdditiveOperator");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalGeometryCodes.g:1882:4: ( (lv_rightChild_3_0= ruleMultiplicativeExpression ) )
            	    // InternalGeometryCodes.g:1883:5: (lv_rightChild_3_0= ruleMultiplicativeExpression )
            	    {
            	    // InternalGeometryCodes.g:1883:5: (lv_rightChild_3_0= ruleMultiplicativeExpression )
            	    // InternalGeometryCodes.g:1884:6: lv_rightChild_3_0= ruleMultiplicativeExpression
            	    {

            	    						newCompositeNode(grammarAccess.getAdditiveExpressionAccess().getRightChildMultiplicativeExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_29);
            	    lv_rightChild_3_0=ruleMultiplicativeExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getAdditiveExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rightChild",
            	    							lv_rightChild_3_0,
            	    							"de.adrianhoff.GeometryCodes.MultiplicativeExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop20;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditiveExpression"


    // $ANTLR start "entryRuleMultiplicativeExpression"
    // InternalGeometryCodes.g:1906:1: entryRuleMultiplicativeExpression returns [EObject current=null] : iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF ;
    public final EObject entryRuleMultiplicativeExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMultiplicativeExpression = null;


        try {
            // InternalGeometryCodes.g:1906:65: (iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF )
            // InternalGeometryCodes.g:1907:2: iv_ruleMultiplicativeExpression= ruleMultiplicativeExpression EOF
            {
             newCompositeNode(grammarAccess.getMultiplicativeExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleMultiplicativeExpression=ruleMultiplicativeExpression();

            state._fsp--;

             current =iv_ruleMultiplicativeExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMultiplicativeExpression"


    // $ANTLR start "ruleMultiplicativeExpression"
    // InternalGeometryCodes.g:1913:1: ruleMultiplicativeExpression returns [EObject current=null] : (this_LiteralExpression_0= ruleLiteralExpression ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ( (lv_rightChild_3_0= ruleLiteralExpression ) ) )* ) ;
    public final EObject ruleMultiplicativeExpression() throws RecognitionException {
        EObject current = null;

        EObject this_LiteralExpression_0 = null;

        Enumerator lv_operator_2_0 = null;

        EObject lv_rightChild_3_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1919:2: ( (this_LiteralExpression_0= ruleLiteralExpression ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ( (lv_rightChild_3_0= ruleLiteralExpression ) ) )* ) )
            // InternalGeometryCodes.g:1920:2: (this_LiteralExpression_0= ruleLiteralExpression ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ( (lv_rightChild_3_0= ruleLiteralExpression ) ) )* )
            {
            // InternalGeometryCodes.g:1920:2: (this_LiteralExpression_0= ruleLiteralExpression ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ( (lv_rightChild_3_0= ruleLiteralExpression ) ) )* )
            // InternalGeometryCodes.g:1921:3: this_LiteralExpression_0= ruleLiteralExpression ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ( (lv_rightChild_3_0= ruleLiteralExpression ) ) )*
            {

            			newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getLiteralExpressionParserRuleCall_0());
            		
            pushFollow(FOLLOW_30);
            this_LiteralExpression_0=ruleLiteralExpression();

            state._fsp--;


            			current = this_LiteralExpression_0;
            			afterParserOrEnumRuleCall();
            		
            // InternalGeometryCodes.g:1929:3: ( () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ( (lv_rightChild_3_0= ruleLiteralExpression ) ) )*
            loop21:
            do {
                int alt21=2;
                int LA21_0 = input.LA(1);

                if ( ((LA21_0>=47 && LA21_0<=48)) ) {
                    alt21=1;
                }


                switch (alt21) {
            	case 1 :
            	    // InternalGeometryCodes.g:1930:4: () ( (lv_operator_2_0= ruleMultiplicativeOperator ) ) ( (lv_rightChild_3_0= ruleLiteralExpression ) )
            	    {
            	    // InternalGeometryCodes.g:1930:4: ()
            	    // InternalGeometryCodes.g:1931:5: 
            	    {

            	    					current = forceCreateModelElementAndSet(
            	    						grammarAccess.getMultiplicativeExpressionAccess().getMultiplicativeExpressionLeftChildAction_1_0(),
            	    						current);
            	    				

            	    }

            	    // InternalGeometryCodes.g:1937:4: ( (lv_operator_2_0= ruleMultiplicativeOperator ) )
            	    // InternalGeometryCodes.g:1938:5: (lv_operator_2_0= ruleMultiplicativeOperator )
            	    {
            	    // InternalGeometryCodes.g:1938:5: (lv_operator_2_0= ruleMultiplicativeOperator )
            	    // InternalGeometryCodes.g:1939:6: lv_operator_2_0= ruleMultiplicativeOperator
            	    {

            	    						newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getOperatorMultiplicativeOperatorEnumRuleCall_1_1_0());
            	    					
            	    pushFollow(FOLLOW_20);
            	    lv_operator_2_0=ruleMultiplicativeOperator();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getMultiplicativeExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"operator",
            	    							lv_operator_2_0,
            	    							"de.adrianhoff.GeometryCodes.MultiplicativeOperator");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }

            	    // InternalGeometryCodes.g:1956:4: ( (lv_rightChild_3_0= ruleLiteralExpression ) )
            	    // InternalGeometryCodes.g:1957:5: (lv_rightChild_3_0= ruleLiteralExpression )
            	    {
            	    // InternalGeometryCodes.g:1957:5: (lv_rightChild_3_0= ruleLiteralExpression )
            	    // InternalGeometryCodes.g:1958:6: lv_rightChild_3_0= ruleLiteralExpression
            	    {

            	    						newCompositeNode(grammarAccess.getMultiplicativeExpressionAccess().getRightChildLiteralExpressionParserRuleCall_1_2_0());
            	    					
            	    pushFollow(FOLLOW_30);
            	    lv_rightChild_3_0=ruleLiteralExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getMultiplicativeExpressionRule());
            	    						}
            	    						set(
            	    							current,
            	    							"rightChild",
            	    							lv_rightChild_3_0,
            	    							"de.adrianhoff.GeometryCodes.LiteralExpression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop21;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicativeExpression"


    // $ANTLR start "entryRuleLiteralExpression"
    // InternalGeometryCodes.g:1980:1: entryRuleLiteralExpression returns [EObject current=null] : iv_ruleLiteralExpression= ruleLiteralExpression EOF ;
    public final EObject entryRuleLiteralExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteralExpression = null;


        try {
            // InternalGeometryCodes.g:1980:58: (iv_ruleLiteralExpression= ruleLiteralExpression EOF )
            // InternalGeometryCodes.g:1981:2: iv_ruleLiteralExpression= ruleLiteralExpression EOF
            {
             newCompositeNode(grammarAccess.getLiteralExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLiteralExpression=ruleLiteralExpression();

            state._fsp--;

             current =iv_ruleLiteralExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteralExpression"


    // $ANTLR start "ruleLiteralExpression"
    // InternalGeometryCodes.g:1987:1: ruleLiteralExpression returns [EObject current=null] : (this_Literal_0= ruleLiteral | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) | this_UnaryExpression_4= ruleUnaryExpression | this_ReferenceExpression_5= ruleReferenceExpression | this_DeclarationExpression_6= ruleDeclarationExpression ) ;
    public final EObject ruleLiteralExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject this_Literal_0 = null;

        EObject this_Expression_2 = null;

        EObject this_UnaryExpression_4 = null;

        EObject this_ReferenceExpression_5 = null;

        EObject this_DeclarationExpression_6 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:1993:2: ( (this_Literal_0= ruleLiteral | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) | this_UnaryExpression_4= ruleUnaryExpression | this_ReferenceExpression_5= ruleReferenceExpression | this_DeclarationExpression_6= ruleDeclarationExpression ) )
            // InternalGeometryCodes.g:1994:2: (this_Literal_0= ruleLiteral | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) | this_UnaryExpression_4= ruleUnaryExpression | this_ReferenceExpression_5= ruleReferenceExpression | this_DeclarationExpression_6= ruleDeclarationExpression )
            {
            // InternalGeometryCodes.g:1994:2: (this_Literal_0= ruleLiteral | (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' ) | this_UnaryExpression_4= ruleUnaryExpression | this_ReferenceExpression_5= ruleReferenceExpression | this_DeclarationExpression_6= ruleDeclarationExpression )
            int alt22=5;
            switch ( input.LA(1) ) {
            case RULE_STRING:
            case RULE_FLOAT:
            case 40:
            case 41:
            case 42:
                {
                alt22=1;
                }
                break;
            case 13:
                {
                alt22=2;
                }
                break;
            case 44:
            case 45:
                {
                alt22=3;
                }
                break;
            case RULE_ID:
                {
                alt22=4;
                }
                break;
            case 36:
            case 37:
            case 38:
            case 39:
                {
                alt22=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // InternalGeometryCodes.g:1995:3: this_Literal_0= ruleLiteral
                    {

                    			newCompositeNode(grammarAccess.getLiteralExpressionAccess().getLiteralParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_Literal_0=ruleLiteral();

                    state._fsp--;


                    			current = this_Literal_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2004:3: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    {
                    // InternalGeometryCodes.g:2004:3: (otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')' )
                    // InternalGeometryCodes.g:2005:4: otherlv_1= '(' this_Expression_2= ruleExpression otherlv_3= ')'
                    {
                    otherlv_1=(Token)match(input,13,FOLLOW_20); 

                    				newLeafNode(otherlv_1, grammarAccess.getLiteralExpressionAccess().getLeftParenthesisKeyword_1_0());
                    			

                    				newCompositeNode(grammarAccess.getLiteralExpressionAccess().getExpressionParserRuleCall_1_1());
                    			
                    pushFollow(FOLLOW_23);
                    this_Expression_2=ruleExpression();

                    state._fsp--;


                    				current = this_Expression_2;
                    				afterParserOrEnumRuleCall();
                    			
                    otherlv_3=(Token)match(input,15,FOLLOW_2); 

                    				newLeafNode(otherlv_3, grammarAccess.getLiteralExpressionAccess().getRightParenthesisKeyword_1_2());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:2023:3: this_UnaryExpression_4= ruleUnaryExpression
                    {

                    			newCompositeNode(grammarAccess.getLiteralExpressionAccess().getUnaryExpressionParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_UnaryExpression_4=ruleUnaryExpression();

                    state._fsp--;


                    			current = this_UnaryExpression_4;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:2032:3: this_ReferenceExpression_5= ruleReferenceExpression
                    {

                    			newCompositeNode(grammarAccess.getLiteralExpressionAccess().getReferenceExpressionParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_ReferenceExpression_5=ruleReferenceExpression();

                    state._fsp--;


                    			current = this_ReferenceExpression_5;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 5 :
                    // InternalGeometryCodes.g:2041:3: this_DeclarationExpression_6= ruleDeclarationExpression
                    {

                    			newCompositeNode(grammarAccess.getLiteralExpressionAccess().getDeclarationExpressionParserRuleCall_4());
                    		
                    pushFollow(FOLLOW_2);
                    this_DeclarationExpression_6=ruleDeclarationExpression();

                    state._fsp--;


                    			current = this_DeclarationExpression_6;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteralExpression"


    // $ANTLR start "entryRuleUnaryExpression"
    // InternalGeometryCodes.g:2053:1: entryRuleUnaryExpression returns [EObject current=null] : iv_ruleUnaryExpression= ruleUnaryExpression EOF ;
    public final EObject entryRuleUnaryExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUnaryExpression = null;


        try {
            // InternalGeometryCodes.g:2053:56: (iv_ruleUnaryExpression= ruleUnaryExpression EOF )
            // InternalGeometryCodes.g:2054:2: iv_ruleUnaryExpression= ruleUnaryExpression EOF
            {
             newCompositeNode(grammarAccess.getUnaryExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleUnaryExpression=ruleUnaryExpression();

            state._fsp--;

             current =iv_ruleUnaryExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUnaryExpression"


    // $ANTLR start "ruleUnaryExpression"
    // InternalGeometryCodes.g:2060:1: ruleUnaryExpression returns [EObject current=null] : ( ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_child_1_0= ruleLiteralExpression ) ) ) ;
    public final EObject ruleUnaryExpression() throws RecognitionException {
        EObject current = null;

        Enumerator lv_operator_0_0 = null;

        EObject lv_child_1_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2066:2: ( ( ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_child_1_0= ruleLiteralExpression ) ) ) )
            // InternalGeometryCodes.g:2067:2: ( ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_child_1_0= ruleLiteralExpression ) ) )
            {
            // InternalGeometryCodes.g:2067:2: ( ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_child_1_0= ruleLiteralExpression ) ) )
            // InternalGeometryCodes.g:2068:3: ( (lv_operator_0_0= ruleUnaryOperator ) ) ( (lv_child_1_0= ruleLiteralExpression ) )
            {
            // InternalGeometryCodes.g:2068:3: ( (lv_operator_0_0= ruleUnaryOperator ) )
            // InternalGeometryCodes.g:2069:4: (lv_operator_0_0= ruleUnaryOperator )
            {
            // InternalGeometryCodes.g:2069:4: (lv_operator_0_0= ruleUnaryOperator )
            // InternalGeometryCodes.g:2070:5: lv_operator_0_0= ruleUnaryOperator
            {

            					newCompositeNode(grammarAccess.getUnaryExpressionAccess().getOperatorUnaryOperatorEnumRuleCall_0_0());
            				
            pushFollow(FOLLOW_20);
            lv_operator_0_0=ruleUnaryOperator();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
            					}
            					set(
            						current,
            						"operator",
            						lv_operator_0_0,
            						"de.adrianhoff.GeometryCodes.UnaryOperator");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:2087:3: ( (lv_child_1_0= ruleLiteralExpression ) )
            // InternalGeometryCodes.g:2088:4: (lv_child_1_0= ruleLiteralExpression )
            {
            // InternalGeometryCodes.g:2088:4: (lv_child_1_0= ruleLiteralExpression )
            // InternalGeometryCodes.g:2089:5: lv_child_1_0= ruleLiteralExpression
            {

            					newCompositeNode(grammarAccess.getUnaryExpressionAccess().getChildLiteralExpressionParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_child_1_0=ruleLiteralExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getUnaryExpressionRule());
            					}
            					set(
            						current,
            						"child",
            						lv_child_1_0,
            						"de.adrianhoff.GeometryCodes.LiteralExpression");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryExpression"


    // $ANTLR start "entryRuleReferenceExpression"
    // InternalGeometryCodes.g:2110:1: entryRuleReferenceExpression returns [EObject current=null] : iv_ruleReferenceExpression= ruleReferenceExpression EOF ;
    public final EObject entryRuleReferenceExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferenceExpression = null;


        try {
            // InternalGeometryCodes.g:2110:60: (iv_ruleReferenceExpression= ruleReferenceExpression EOF )
            // InternalGeometryCodes.g:2111:2: iv_ruleReferenceExpression= ruleReferenceExpression EOF
            {
             newCompositeNode(grammarAccess.getReferenceExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleReferenceExpression=ruleReferenceExpression();

            state._fsp--;

             current =iv_ruleReferenceExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferenceExpression"


    // $ANTLR start "ruleReferenceExpression"
    // InternalGeometryCodes.g:2117:1: ruleReferenceExpression returns [EObject current=null] : ( (this_VariableReferenceExpression_0= ruleVariableReferenceExpression | this_FunctionCallExpression_1= ruleFunctionCallExpression ) (otherlv_2= '.' ( (lv_next_3_0= ruleReferenceExpression ) ) )? ) ;
    public final EObject ruleReferenceExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_VariableReferenceExpression_0 = null;

        EObject this_FunctionCallExpression_1 = null;

        EObject lv_next_3_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2123:2: ( ( (this_VariableReferenceExpression_0= ruleVariableReferenceExpression | this_FunctionCallExpression_1= ruleFunctionCallExpression ) (otherlv_2= '.' ( (lv_next_3_0= ruleReferenceExpression ) ) )? ) )
            // InternalGeometryCodes.g:2124:2: ( (this_VariableReferenceExpression_0= ruleVariableReferenceExpression | this_FunctionCallExpression_1= ruleFunctionCallExpression ) (otherlv_2= '.' ( (lv_next_3_0= ruleReferenceExpression ) ) )? )
            {
            // InternalGeometryCodes.g:2124:2: ( (this_VariableReferenceExpression_0= ruleVariableReferenceExpression | this_FunctionCallExpression_1= ruleFunctionCallExpression ) (otherlv_2= '.' ( (lv_next_3_0= ruleReferenceExpression ) ) )? )
            // InternalGeometryCodes.g:2125:3: (this_VariableReferenceExpression_0= ruleVariableReferenceExpression | this_FunctionCallExpression_1= ruleFunctionCallExpression ) (otherlv_2= '.' ( (lv_next_3_0= ruleReferenceExpression ) ) )?
            {
            // InternalGeometryCodes.g:2125:3: (this_VariableReferenceExpression_0= ruleVariableReferenceExpression | this_FunctionCallExpression_1= ruleFunctionCallExpression )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==RULE_ID) ) {
                int LA23_1 = input.LA(2);

                if ( (LA23_1==13) ) {
                    alt23=2;
                }
                else if ( (LA23_1==EOF||LA23_1==RULE_ID||(LA23_1>=14 && LA23_1<=15)||(LA23_1>=17 && LA23_1<=18)||LA23_1==21||LA23_1==26||(LA23_1>=28 && LA23_1<=29)||LA23_1==31||(LA23_1>=33 && LA23_1<=36)||(LA23_1>=38 && LA23_1<=39)||(LA23_1>=45 && LA23_1<=59)) ) {
                    alt23=1;
                }
                else {
                    NoViableAltException nvae =
                        new NoViableAltException("", 23, 1, input);

                    throw nvae;
                }
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalGeometryCodes.g:2126:4: this_VariableReferenceExpression_0= ruleVariableReferenceExpression
                    {

                    				newCompositeNode(grammarAccess.getReferenceExpressionAccess().getVariableReferenceExpressionParserRuleCall_0_0());
                    			
                    pushFollow(FOLLOW_31);
                    this_VariableReferenceExpression_0=ruleVariableReferenceExpression();

                    state._fsp--;


                    				current = this_VariableReferenceExpression_0;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2135:4: this_FunctionCallExpression_1= ruleFunctionCallExpression
                    {

                    				newCompositeNode(grammarAccess.getReferenceExpressionAccess().getFunctionCallExpressionParserRuleCall_0_1());
                    			
                    pushFollow(FOLLOW_31);
                    this_FunctionCallExpression_1=ruleFunctionCallExpression();

                    state._fsp--;


                    				current = this_FunctionCallExpression_1;
                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }

            // InternalGeometryCodes.g:2144:3: (otherlv_2= '.' ( (lv_next_3_0= ruleReferenceExpression ) ) )?
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==35) ) {
                alt24=1;
            }
            switch (alt24) {
                case 1 :
                    // InternalGeometryCodes.g:2145:4: otherlv_2= '.' ( (lv_next_3_0= ruleReferenceExpression ) )
                    {
                    otherlv_2=(Token)match(input,35,FOLLOW_6); 

                    				newLeafNode(otherlv_2, grammarAccess.getReferenceExpressionAccess().getFullStopKeyword_1_0());
                    			
                    // InternalGeometryCodes.g:2149:4: ( (lv_next_3_0= ruleReferenceExpression ) )
                    // InternalGeometryCodes.g:2150:5: (lv_next_3_0= ruleReferenceExpression )
                    {
                    // InternalGeometryCodes.g:2150:5: (lv_next_3_0= ruleReferenceExpression )
                    // InternalGeometryCodes.g:2151:6: lv_next_3_0= ruleReferenceExpression
                    {

                    						newCompositeNode(grammarAccess.getReferenceExpressionAccess().getNextReferenceExpressionParserRuleCall_1_1_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_next_3_0=ruleReferenceExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getReferenceExpressionRule());
                    						}
                    						set(
                    							current,
                    							"next",
                    							lv_next_3_0,
                    							"de.adrianhoff.GeometryCodes.ReferenceExpression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferenceExpression"


    // $ANTLR start "entryRuleVariableReferenceExpression"
    // InternalGeometryCodes.g:2173:1: entryRuleVariableReferenceExpression returns [EObject current=null] : iv_ruleVariableReferenceExpression= ruleVariableReferenceExpression EOF ;
    public final EObject entryRuleVariableReferenceExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariableReferenceExpression = null;


        try {
            // InternalGeometryCodes.g:2173:68: (iv_ruleVariableReferenceExpression= ruleVariableReferenceExpression EOF )
            // InternalGeometryCodes.g:2174:2: iv_ruleVariableReferenceExpression= ruleVariableReferenceExpression EOF
            {
             newCompositeNode(grammarAccess.getVariableReferenceExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVariableReferenceExpression=ruleVariableReferenceExpression();

            state._fsp--;

             current =iv_ruleVariableReferenceExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableReferenceExpression"


    // $ANTLR start "ruleVariableReferenceExpression"
    // InternalGeometryCodes.g:2180:1: ruleVariableReferenceExpression returns [EObject current=null] : ( (otherlv_0= RULE_ID ) ) ;
    public final EObject ruleVariableReferenceExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2186:2: ( ( (otherlv_0= RULE_ID ) ) )
            // InternalGeometryCodes.g:2187:2: ( (otherlv_0= RULE_ID ) )
            {
            // InternalGeometryCodes.g:2187:2: ( (otherlv_0= RULE_ID ) )
            // InternalGeometryCodes.g:2188:3: (otherlv_0= RULE_ID )
            {
            // InternalGeometryCodes.g:2188:3: (otherlv_0= RULE_ID )
            // InternalGeometryCodes.g:2189:4: otherlv_0= RULE_ID
            {

            				if (current==null) {
            					current = createModelElement(grammarAccess.getVariableReferenceExpressionRule());
            				}
            			
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            				newLeafNode(otherlv_0, grammarAccess.getVariableReferenceExpressionAccess().getTargetReferenceableElementCrossReference_0());
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableReferenceExpression"


    // $ANTLR start "entryRuleFunctionCallExpression"
    // InternalGeometryCodes.g:2203:1: entryRuleFunctionCallExpression returns [EObject current=null] : iv_ruleFunctionCallExpression= ruleFunctionCallExpression EOF ;
    public final EObject entryRuleFunctionCallExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunctionCallExpression = null;


        try {
            // InternalGeometryCodes.g:2203:63: (iv_ruleFunctionCallExpression= ruleFunctionCallExpression EOF )
            // InternalGeometryCodes.g:2204:2: iv_ruleFunctionCallExpression= ruleFunctionCallExpression EOF
            {
             newCompositeNode(grammarAccess.getFunctionCallExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFunctionCallExpression=ruleFunctionCallExpression();

            state._fsp--;

             current =iv_ruleFunctionCallExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunctionCallExpression"


    // $ANTLR start "ruleFunctionCallExpression"
    // InternalGeometryCodes.g:2210:1: ruleFunctionCallExpression returns [EObject current=null] : ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* )? otherlv_5= ')' ) ;
    public final EObject ruleFunctionCallExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_arguments_2_0 = null;

        EObject lv_arguments_4_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2216:2: ( ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* )? otherlv_5= ')' ) )
            // InternalGeometryCodes.g:2217:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* )? otherlv_5= ')' )
            {
            // InternalGeometryCodes.g:2217:2: ( ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* )? otherlv_5= ')' )
            // InternalGeometryCodes.g:2218:3: ( (otherlv_0= RULE_ID ) ) otherlv_1= '(' ( ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* )? otherlv_5= ')'
            {
            // InternalGeometryCodes.g:2218:3: ( (otherlv_0= RULE_ID ) )
            // InternalGeometryCodes.g:2219:4: (otherlv_0= RULE_ID )
            {
            // InternalGeometryCodes.g:2219:4: (otherlv_0= RULE_ID )
            // InternalGeometryCodes.g:2220:5: otherlv_0= RULE_ID
            {

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFunctionCallExpressionRule());
            					}
            				
            otherlv_0=(Token)match(input,RULE_ID,FOLLOW_7); 

            					newLeafNode(otherlv_0, grammarAccess.getFunctionCallExpressionAccess().getTargetReferenceableElementCrossReference_0_0());
            				

            }


            }

            otherlv_1=(Token)match(input,13,FOLLOW_32); 

            			newLeafNode(otherlv_1, grammarAccess.getFunctionCallExpressionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalGeometryCodes.g:2235:3: ( ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* )?
            int alt26=2;
            int LA26_0 = input.LA(1);

            if ( ((LA26_0>=RULE_STRING && LA26_0<=RULE_FLOAT)||LA26_0==13||(LA26_0>=36 && LA26_0<=42)||(LA26_0>=44 && LA26_0<=45)) ) {
                alt26=1;
            }
            switch (alt26) {
                case 1 :
                    // InternalGeometryCodes.g:2236:4: ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )*
                    {
                    // InternalGeometryCodes.g:2236:4: ( (lv_arguments_2_0= ruleExpression ) )
                    // InternalGeometryCodes.g:2237:5: (lv_arguments_2_0= ruleExpression )
                    {
                    // InternalGeometryCodes.g:2237:5: (lv_arguments_2_0= ruleExpression )
                    // InternalGeometryCodes.g:2238:6: lv_arguments_2_0= ruleExpression
                    {

                    						newCompositeNode(grammarAccess.getFunctionCallExpressionAccess().getArgumentsExpressionParserRuleCall_2_0_0());
                    					
                    pushFollow(FOLLOW_9);
                    lv_arguments_2_0=ruleExpression();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFunctionCallExpressionRule());
                    						}
                    						add(
                    							current,
                    							"arguments",
                    							lv_arguments_2_0,
                    							"de.adrianhoff.GeometryCodes.Expression");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }

                    // InternalGeometryCodes.g:2255:4: (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )*
                    loop25:
                    do {
                        int alt25=2;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==14) ) {
                            alt25=1;
                        }


                        switch (alt25) {
                    	case 1 :
                    	    // InternalGeometryCodes.g:2256:5: otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) )
                    	    {
                    	    otherlv_3=(Token)match(input,14,FOLLOW_20); 

                    	    					newLeafNode(otherlv_3, grammarAccess.getFunctionCallExpressionAccess().getCommaKeyword_2_1_0());
                    	    				
                    	    // InternalGeometryCodes.g:2260:5: ( (lv_arguments_4_0= ruleExpression ) )
                    	    // InternalGeometryCodes.g:2261:6: (lv_arguments_4_0= ruleExpression )
                    	    {
                    	    // InternalGeometryCodes.g:2261:6: (lv_arguments_4_0= ruleExpression )
                    	    // InternalGeometryCodes.g:2262:7: lv_arguments_4_0= ruleExpression
                    	    {

                    	    							newCompositeNode(grammarAccess.getFunctionCallExpressionAccess().getArgumentsExpressionParserRuleCall_2_1_1_0());
                    	    						
                    	    pushFollow(FOLLOW_9);
                    	    lv_arguments_4_0=ruleExpression();

                    	    state._fsp--;


                    	    							if (current==null) {
                    	    								current = createModelElementForParent(grammarAccess.getFunctionCallExpressionRule());
                    	    							}
                    	    							add(
                    	    								current,
                    	    								"arguments",
                    	    								lv_arguments_4_0,
                    	    								"de.adrianhoff.GeometryCodes.Expression");
                    	    							afterParserOrEnumRuleCall();
                    	    						

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop25;
                        }
                    } while (true);


                    }
                    break;

            }

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getFunctionCallExpressionAccess().getRightParenthesisKeyword_3());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunctionCallExpression"


    // $ANTLR start "entryRuleDeclarationExpression"
    // InternalGeometryCodes.g:2289:1: entryRuleDeclarationExpression returns [EObject current=null] : iv_ruleDeclarationExpression= ruleDeclarationExpression EOF ;
    public final EObject entryRuleDeclarationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclarationExpression = null;


        try {
            // InternalGeometryCodes.g:2289:62: (iv_ruleDeclarationExpression= ruleDeclarationExpression EOF )
            // InternalGeometryCodes.g:2290:2: iv_ruleDeclarationExpression= ruleDeclarationExpression EOF
            {
             newCompositeNode(grammarAccess.getDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleDeclarationExpression=ruleDeclarationExpression();

            state._fsp--;

             current =iv_ruleDeclarationExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclarationExpression"


    // $ANTLR start "ruleDeclarationExpression"
    // InternalGeometryCodes.g:2296:1: ruleDeclarationExpression returns [EObject current=null] : (this_VectorDeclarationExpression_0= ruleVectorDeclarationExpression | this_VertexDeclarationExpression_1= ruleVertexDeclarationExpression | this_TriangleDeclarationExpression_2= ruleTriangleDeclarationExpression | this_QuadDeclarationExpression_3= ruleQuadDeclarationExpression ) ;
    public final EObject ruleDeclarationExpression() throws RecognitionException {
        EObject current = null;

        EObject this_VectorDeclarationExpression_0 = null;

        EObject this_VertexDeclarationExpression_1 = null;

        EObject this_TriangleDeclarationExpression_2 = null;

        EObject this_QuadDeclarationExpression_3 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2302:2: ( (this_VectorDeclarationExpression_0= ruleVectorDeclarationExpression | this_VertexDeclarationExpression_1= ruleVertexDeclarationExpression | this_TriangleDeclarationExpression_2= ruleTriangleDeclarationExpression | this_QuadDeclarationExpression_3= ruleQuadDeclarationExpression ) )
            // InternalGeometryCodes.g:2303:2: (this_VectorDeclarationExpression_0= ruleVectorDeclarationExpression | this_VertexDeclarationExpression_1= ruleVertexDeclarationExpression | this_TriangleDeclarationExpression_2= ruleTriangleDeclarationExpression | this_QuadDeclarationExpression_3= ruleQuadDeclarationExpression )
            {
            // InternalGeometryCodes.g:2303:2: (this_VectorDeclarationExpression_0= ruleVectorDeclarationExpression | this_VertexDeclarationExpression_1= ruleVertexDeclarationExpression | this_TriangleDeclarationExpression_2= ruleTriangleDeclarationExpression | this_QuadDeclarationExpression_3= ruleQuadDeclarationExpression )
            int alt27=4;
            switch ( input.LA(1) ) {
            case 36:
                {
                alt27=1;
                }
                break;
            case 37:
                {
                alt27=2;
                }
                break;
            case 38:
                {
                alt27=3;
                }
                break;
            case 39:
                {
                alt27=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 27, 0, input);

                throw nvae;
            }

            switch (alt27) {
                case 1 :
                    // InternalGeometryCodes.g:2304:3: this_VectorDeclarationExpression_0= ruleVectorDeclarationExpression
                    {

                    			newCompositeNode(grammarAccess.getDeclarationExpressionAccess().getVectorDeclarationExpressionParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_VectorDeclarationExpression_0=ruleVectorDeclarationExpression();

                    state._fsp--;


                    			current = this_VectorDeclarationExpression_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2313:3: this_VertexDeclarationExpression_1= ruleVertexDeclarationExpression
                    {

                    			newCompositeNode(grammarAccess.getDeclarationExpressionAccess().getVertexDeclarationExpressionParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_VertexDeclarationExpression_1=ruleVertexDeclarationExpression();

                    state._fsp--;


                    			current = this_VertexDeclarationExpression_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:2322:3: this_TriangleDeclarationExpression_2= ruleTriangleDeclarationExpression
                    {

                    			newCompositeNode(grammarAccess.getDeclarationExpressionAccess().getTriangleDeclarationExpressionParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_TriangleDeclarationExpression_2=ruleTriangleDeclarationExpression();

                    state._fsp--;


                    			current = this_TriangleDeclarationExpression_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:2331:3: this_QuadDeclarationExpression_3= ruleQuadDeclarationExpression
                    {

                    			newCompositeNode(grammarAccess.getDeclarationExpressionAccess().getQuadDeclarationExpressionParserRuleCall_3());
                    		
                    pushFollow(FOLLOW_2);
                    this_QuadDeclarationExpression_3=ruleQuadDeclarationExpression();

                    state._fsp--;


                    			current = this_QuadDeclarationExpression_3;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclarationExpression"


    // $ANTLR start "entryRuleVectorDeclarationExpression"
    // InternalGeometryCodes.g:2343:1: entryRuleVectorDeclarationExpression returns [EObject current=null] : iv_ruleVectorDeclarationExpression= ruleVectorDeclarationExpression EOF ;
    public final EObject entryRuleVectorDeclarationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVectorDeclarationExpression = null;


        try {
            // InternalGeometryCodes.g:2343:68: (iv_ruleVectorDeclarationExpression= ruleVectorDeclarationExpression EOF )
            // InternalGeometryCodes.g:2344:2: iv_ruleVectorDeclarationExpression= ruleVectorDeclarationExpression EOF
            {
             newCompositeNode(grammarAccess.getVectorDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVectorDeclarationExpression=ruleVectorDeclarationExpression();

            state._fsp--;

             current =iv_ruleVectorDeclarationExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVectorDeclarationExpression"


    // $ANTLR start "ruleVectorDeclarationExpression"
    // InternalGeometryCodes.g:2350:1: ruleVectorDeclarationExpression returns [EObject current=null] : (otherlv_0= 'vector' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleVectorDeclarationExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_arguments_2_0 = null;

        EObject lv_arguments_4_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2356:2: ( (otherlv_0= 'vector' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // InternalGeometryCodes.g:2357:2: (otherlv_0= 'vector' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // InternalGeometryCodes.g:2357:2: (otherlv_0= 'vector' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // InternalGeometryCodes.g:2358:3: otherlv_0= 'vector' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,36,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getVectorDeclarationExpressionAccess().getVectorKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getVectorDeclarationExpressionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalGeometryCodes.g:2366:3: ( (lv_arguments_2_0= ruleExpression ) )
            // InternalGeometryCodes.g:2367:4: (lv_arguments_2_0= ruleExpression )
            {
            // InternalGeometryCodes.g:2367:4: (lv_arguments_2_0= ruleExpression )
            // InternalGeometryCodes.g:2368:5: lv_arguments_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_9);
            lv_arguments_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getVectorDeclarationExpressionRule());
            					}
            					add(
            						current,
            						"arguments",
            						lv_arguments_2_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:2385:3: (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )*
            loop28:
            do {
                int alt28=2;
                int LA28_0 = input.LA(1);

                if ( (LA28_0==14) ) {
                    alt28=1;
                }


                switch (alt28) {
            	case 1 :
            	    // InternalGeometryCodes.g:2386:4: otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,14,FOLLOW_20); 

            	    				newLeafNode(otherlv_3, grammarAccess.getVectorDeclarationExpressionAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalGeometryCodes.g:2390:4: ( (lv_arguments_4_0= ruleExpression ) )
            	    // InternalGeometryCodes.g:2391:5: (lv_arguments_4_0= ruleExpression )
            	    {
            	    // InternalGeometryCodes.g:2391:5: (lv_arguments_4_0= ruleExpression )
            	    // InternalGeometryCodes.g:2392:6: lv_arguments_4_0= ruleExpression
            	    {

            	    						newCompositeNode(grammarAccess.getVectorDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_arguments_4_0=ruleExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getVectorDeclarationExpressionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"arguments",
            	    							lv_arguments_4_0,
            	    							"de.adrianhoff.GeometryCodes.Expression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop28;
                }
            } while (true);

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getVectorDeclarationExpressionAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVectorDeclarationExpression"


    // $ANTLR start "entryRuleVertexDeclarationExpression"
    // InternalGeometryCodes.g:2418:1: entryRuleVertexDeclarationExpression returns [EObject current=null] : iv_ruleVertexDeclarationExpression= ruleVertexDeclarationExpression EOF ;
    public final EObject entryRuleVertexDeclarationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVertexDeclarationExpression = null;


        try {
            // InternalGeometryCodes.g:2418:68: (iv_ruleVertexDeclarationExpression= ruleVertexDeclarationExpression EOF )
            // InternalGeometryCodes.g:2419:2: iv_ruleVertexDeclarationExpression= ruleVertexDeclarationExpression EOF
            {
             newCompositeNode(grammarAccess.getVertexDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleVertexDeclarationExpression=ruleVertexDeclarationExpression();

            state._fsp--;

             current =iv_ruleVertexDeclarationExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVertexDeclarationExpression"


    // $ANTLR start "ruleVertexDeclarationExpression"
    // InternalGeometryCodes.g:2425:1: ruleVertexDeclarationExpression returns [EObject current=null] : (otherlv_0= 'vertex' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleVertexDeclarationExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_arguments_2_0 = null;

        EObject lv_arguments_4_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2431:2: ( (otherlv_0= 'vertex' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // InternalGeometryCodes.g:2432:2: (otherlv_0= 'vertex' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // InternalGeometryCodes.g:2432:2: (otherlv_0= 'vertex' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // InternalGeometryCodes.g:2433:3: otherlv_0= 'vertex' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,37,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getVertexDeclarationExpressionAccess().getVertexKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getVertexDeclarationExpressionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalGeometryCodes.g:2441:3: ( (lv_arguments_2_0= ruleExpression ) )
            // InternalGeometryCodes.g:2442:4: (lv_arguments_2_0= ruleExpression )
            {
            // InternalGeometryCodes.g:2442:4: (lv_arguments_2_0= ruleExpression )
            // InternalGeometryCodes.g:2443:5: lv_arguments_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_9);
            lv_arguments_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getVertexDeclarationExpressionRule());
            					}
            					add(
            						current,
            						"arguments",
            						lv_arguments_2_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:2460:3: (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )*
            loop29:
            do {
                int alt29=2;
                int LA29_0 = input.LA(1);

                if ( (LA29_0==14) ) {
                    alt29=1;
                }


                switch (alt29) {
            	case 1 :
            	    // InternalGeometryCodes.g:2461:4: otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,14,FOLLOW_20); 

            	    				newLeafNode(otherlv_3, grammarAccess.getVertexDeclarationExpressionAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalGeometryCodes.g:2465:4: ( (lv_arguments_4_0= ruleExpression ) )
            	    // InternalGeometryCodes.g:2466:5: (lv_arguments_4_0= ruleExpression )
            	    {
            	    // InternalGeometryCodes.g:2466:5: (lv_arguments_4_0= ruleExpression )
            	    // InternalGeometryCodes.g:2467:6: lv_arguments_4_0= ruleExpression
            	    {

            	    						newCompositeNode(grammarAccess.getVertexDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_arguments_4_0=ruleExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getVertexDeclarationExpressionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"arguments",
            	    							lv_arguments_4_0,
            	    							"de.adrianhoff.GeometryCodes.Expression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop29;
                }
            } while (true);

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getVertexDeclarationExpressionAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVertexDeclarationExpression"


    // $ANTLR start "entryRuleTriangleDeclarationExpression"
    // InternalGeometryCodes.g:2493:1: entryRuleTriangleDeclarationExpression returns [EObject current=null] : iv_ruleTriangleDeclarationExpression= ruleTriangleDeclarationExpression EOF ;
    public final EObject entryRuleTriangleDeclarationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTriangleDeclarationExpression = null;


        try {
            // InternalGeometryCodes.g:2493:70: (iv_ruleTriangleDeclarationExpression= ruleTriangleDeclarationExpression EOF )
            // InternalGeometryCodes.g:2494:2: iv_ruleTriangleDeclarationExpression= ruleTriangleDeclarationExpression EOF
            {
             newCompositeNode(grammarAccess.getTriangleDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleTriangleDeclarationExpression=ruleTriangleDeclarationExpression();

            state._fsp--;

             current =iv_ruleTriangleDeclarationExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTriangleDeclarationExpression"


    // $ANTLR start "ruleTriangleDeclarationExpression"
    // InternalGeometryCodes.g:2500:1: ruleTriangleDeclarationExpression returns [EObject current=null] : (otherlv_0= 'triangle' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleTriangleDeclarationExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_arguments_2_0 = null;

        EObject lv_arguments_4_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2506:2: ( (otherlv_0= 'triangle' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // InternalGeometryCodes.g:2507:2: (otherlv_0= 'triangle' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // InternalGeometryCodes.g:2507:2: (otherlv_0= 'triangle' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // InternalGeometryCodes.g:2508:3: otherlv_0= 'triangle' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,38,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getTriangleDeclarationExpressionAccess().getTriangleKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getTriangleDeclarationExpressionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalGeometryCodes.g:2516:3: ( (lv_arguments_2_0= ruleExpression ) )
            // InternalGeometryCodes.g:2517:4: (lv_arguments_2_0= ruleExpression )
            {
            // InternalGeometryCodes.g:2517:4: (lv_arguments_2_0= ruleExpression )
            // InternalGeometryCodes.g:2518:5: lv_arguments_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_9);
            lv_arguments_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getTriangleDeclarationExpressionRule());
            					}
            					add(
            						current,
            						"arguments",
            						lv_arguments_2_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:2535:3: (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )*
            loop30:
            do {
                int alt30=2;
                int LA30_0 = input.LA(1);

                if ( (LA30_0==14) ) {
                    alt30=1;
                }


                switch (alt30) {
            	case 1 :
            	    // InternalGeometryCodes.g:2536:4: otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,14,FOLLOW_20); 

            	    				newLeafNode(otherlv_3, grammarAccess.getTriangleDeclarationExpressionAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalGeometryCodes.g:2540:4: ( (lv_arguments_4_0= ruleExpression ) )
            	    // InternalGeometryCodes.g:2541:5: (lv_arguments_4_0= ruleExpression )
            	    {
            	    // InternalGeometryCodes.g:2541:5: (lv_arguments_4_0= ruleExpression )
            	    // InternalGeometryCodes.g:2542:6: lv_arguments_4_0= ruleExpression
            	    {

            	    						newCompositeNode(grammarAccess.getTriangleDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_arguments_4_0=ruleExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getTriangleDeclarationExpressionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"arguments",
            	    							lv_arguments_4_0,
            	    							"de.adrianhoff.GeometryCodes.Expression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop30;
                }
            } while (true);

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getTriangleDeclarationExpressionAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTriangleDeclarationExpression"


    // $ANTLR start "entryRuleQuadDeclarationExpression"
    // InternalGeometryCodes.g:2568:1: entryRuleQuadDeclarationExpression returns [EObject current=null] : iv_ruleQuadDeclarationExpression= ruleQuadDeclarationExpression EOF ;
    public final EObject entryRuleQuadDeclarationExpression() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuadDeclarationExpression = null;


        try {
            // InternalGeometryCodes.g:2568:66: (iv_ruleQuadDeclarationExpression= ruleQuadDeclarationExpression EOF )
            // InternalGeometryCodes.g:2569:2: iv_ruleQuadDeclarationExpression= ruleQuadDeclarationExpression EOF
            {
             newCompositeNode(grammarAccess.getQuadDeclarationExpressionRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleQuadDeclarationExpression=ruleQuadDeclarationExpression();

            state._fsp--;

             current =iv_ruleQuadDeclarationExpression; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuadDeclarationExpression"


    // $ANTLR start "ruleQuadDeclarationExpression"
    // InternalGeometryCodes.g:2575:1: ruleQuadDeclarationExpression returns [EObject current=null] : (otherlv_0= 'quad' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' ) ;
    public final EObject ruleQuadDeclarationExpression() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_arguments_2_0 = null;

        EObject lv_arguments_4_0 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2581:2: ( (otherlv_0= 'quad' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' ) )
            // InternalGeometryCodes.g:2582:2: (otherlv_0= 'quad' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            {
            // InternalGeometryCodes.g:2582:2: (otherlv_0= 'quad' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')' )
            // InternalGeometryCodes.g:2583:3: otherlv_0= 'quad' otherlv_1= '(' ( (lv_arguments_2_0= ruleExpression ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )* otherlv_5= ')'
            {
            otherlv_0=(Token)match(input,39,FOLLOW_7); 

            			newLeafNode(otherlv_0, grammarAccess.getQuadDeclarationExpressionAccess().getQuadKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_20); 

            			newLeafNode(otherlv_1, grammarAccess.getQuadDeclarationExpressionAccess().getLeftParenthesisKeyword_1());
            		
            // InternalGeometryCodes.g:2591:3: ( (lv_arguments_2_0= ruleExpression ) )
            // InternalGeometryCodes.g:2592:4: (lv_arguments_2_0= ruleExpression )
            {
            // InternalGeometryCodes.g:2592:4: (lv_arguments_2_0= ruleExpression )
            // InternalGeometryCodes.g:2593:5: lv_arguments_2_0= ruleExpression
            {

            					newCompositeNode(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_9);
            lv_arguments_2_0=ruleExpression();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getQuadDeclarationExpressionRule());
            					}
            					add(
            						current,
            						"arguments",
            						lv_arguments_2_0,
            						"de.adrianhoff.GeometryCodes.Expression");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalGeometryCodes.g:2610:3: (otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) ) )*
            loop31:
            do {
                int alt31=2;
                int LA31_0 = input.LA(1);

                if ( (LA31_0==14) ) {
                    alt31=1;
                }


                switch (alt31) {
            	case 1 :
            	    // InternalGeometryCodes.g:2611:4: otherlv_3= ',' ( (lv_arguments_4_0= ruleExpression ) )
            	    {
            	    otherlv_3=(Token)match(input,14,FOLLOW_20); 

            	    				newLeafNode(otherlv_3, grammarAccess.getQuadDeclarationExpressionAccess().getCommaKeyword_3_0());
            	    			
            	    // InternalGeometryCodes.g:2615:4: ( (lv_arguments_4_0= ruleExpression ) )
            	    // InternalGeometryCodes.g:2616:5: (lv_arguments_4_0= ruleExpression )
            	    {
            	    // InternalGeometryCodes.g:2616:5: (lv_arguments_4_0= ruleExpression )
            	    // InternalGeometryCodes.g:2617:6: lv_arguments_4_0= ruleExpression
            	    {

            	    						newCompositeNode(grammarAccess.getQuadDeclarationExpressionAccess().getArgumentsExpressionParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_9);
            	    lv_arguments_4_0=ruleExpression();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getQuadDeclarationExpressionRule());
            	    						}
            	    						add(
            	    							current,
            	    							"arguments",
            	    							lv_arguments_4_0,
            	    							"de.adrianhoff.GeometryCodes.Expression");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop31;
                }
            } while (true);

            otherlv_5=(Token)match(input,15,FOLLOW_2); 

            			newLeafNode(otherlv_5, grammarAccess.getQuadDeclarationExpressionAccess().getRightParenthesisKeyword_4());
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuadDeclarationExpression"


    // $ANTLR start "entryRuleLiteral"
    // InternalGeometryCodes.g:2643:1: entryRuleLiteral returns [EObject current=null] : iv_ruleLiteral= ruleLiteral EOF ;
    public final EObject entryRuleLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteral = null;


        try {
            // InternalGeometryCodes.g:2643:48: (iv_ruleLiteral= ruleLiteral EOF )
            // InternalGeometryCodes.g:2644:2: iv_ruleLiteral= ruleLiteral EOF
            {
             newCompositeNode(grammarAccess.getLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLiteral=ruleLiteral();

            state._fsp--;

             current =iv_ruleLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // InternalGeometryCodes.g:2650:1: ruleLiteral returns [EObject current=null] : (this_FloatLiteral_0= ruleFloatLiteral | this_StringLiteral_1= ruleStringLiteral | this_BooleanLiteral_2= ruleBooleanLiteral | ( () otherlv_4= 'empty' ) ) ;
    public final EObject ruleLiteral() throws RecognitionException {
        EObject current = null;

        Token otherlv_4=null;
        EObject this_FloatLiteral_0 = null;

        EObject this_StringLiteral_1 = null;

        EObject this_BooleanLiteral_2 = null;



        	enterRule();

        try {
            // InternalGeometryCodes.g:2656:2: ( (this_FloatLiteral_0= ruleFloatLiteral | this_StringLiteral_1= ruleStringLiteral | this_BooleanLiteral_2= ruleBooleanLiteral | ( () otherlv_4= 'empty' ) ) )
            // InternalGeometryCodes.g:2657:2: (this_FloatLiteral_0= ruleFloatLiteral | this_StringLiteral_1= ruleStringLiteral | this_BooleanLiteral_2= ruleBooleanLiteral | ( () otherlv_4= 'empty' ) )
            {
            // InternalGeometryCodes.g:2657:2: (this_FloatLiteral_0= ruleFloatLiteral | this_StringLiteral_1= ruleStringLiteral | this_BooleanLiteral_2= ruleBooleanLiteral | ( () otherlv_4= 'empty' ) )
            int alt32=4;
            switch ( input.LA(1) ) {
            case RULE_FLOAT:
                {
                alt32=1;
                }
                break;
            case RULE_STRING:
                {
                alt32=2;
                }
                break;
            case 41:
            case 42:
                {
                alt32=3;
                }
                break;
            case 40:
                {
                alt32=4;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 32, 0, input);

                throw nvae;
            }

            switch (alt32) {
                case 1 :
                    // InternalGeometryCodes.g:2658:3: this_FloatLiteral_0= ruleFloatLiteral
                    {

                    			newCompositeNode(grammarAccess.getLiteralAccess().getFloatLiteralParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_FloatLiteral_0=ruleFloatLiteral();

                    state._fsp--;


                    			current = this_FloatLiteral_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2667:3: this_StringLiteral_1= ruleStringLiteral
                    {

                    			newCompositeNode(grammarAccess.getLiteralAccess().getStringLiteralParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_StringLiteral_1=ruleStringLiteral();

                    state._fsp--;


                    			current = this_StringLiteral_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:2676:3: this_BooleanLiteral_2= ruleBooleanLiteral
                    {

                    			newCompositeNode(grammarAccess.getLiteralAccess().getBooleanLiteralParserRuleCall_2());
                    		
                    pushFollow(FOLLOW_2);
                    this_BooleanLiteral_2=ruleBooleanLiteral();

                    state._fsp--;


                    			current = this_BooleanLiteral_2;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:2685:3: ( () otherlv_4= 'empty' )
                    {
                    // InternalGeometryCodes.g:2685:3: ( () otherlv_4= 'empty' )
                    // InternalGeometryCodes.g:2686:4: () otherlv_4= 'empty'
                    {
                    // InternalGeometryCodes.g:2686:4: ()
                    // InternalGeometryCodes.g:2687:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getLiteralAccess().getEmptyLiteralAction_3_0(),
                    						current);
                    				

                    }

                    otherlv_4=(Token)match(input,40,FOLLOW_2); 

                    				newLeafNode(otherlv_4, grammarAccess.getLiteralAccess().getEmptyKeyword_3_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "entryRuleFloatLiteral"
    // InternalGeometryCodes.g:2702:1: entryRuleFloatLiteral returns [EObject current=null] : iv_ruleFloatLiteral= ruleFloatLiteral EOF ;
    public final EObject entryRuleFloatLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFloatLiteral = null;


        try {
            // InternalGeometryCodes.g:2702:53: (iv_ruleFloatLiteral= ruleFloatLiteral EOF )
            // InternalGeometryCodes.g:2703:2: iv_ruleFloatLiteral= ruleFloatLiteral EOF
            {
             newCompositeNode(grammarAccess.getFloatLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFloatLiteral=ruleFloatLiteral();

            state._fsp--;

             current =iv_ruleFloatLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFloatLiteral"


    // $ANTLR start "ruleFloatLiteral"
    // InternalGeometryCodes.g:2709:1: ruleFloatLiteral returns [EObject current=null] : ( (lv_value_0_0= RULE_FLOAT ) ) ;
    public final EObject ruleFloatLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2715:2: ( ( (lv_value_0_0= RULE_FLOAT ) ) )
            // InternalGeometryCodes.g:2716:2: ( (lv_value_0_0= RULE_FLOAT ) )
            {
            // InternalGeometryCodes.g:2716:2: ( (lv_value_0_0= RULE_FLOAT ) )
            // InternalGeometryCodes.g:2717:3: (lv_value_0_0= RULE_FLOAT )
            {
            // InternalGeometryCodes.g:2717:3: (lv_value_0_0= RULE_FLOAT )
            // InternalGeometryCodes.g:2718:4: lv_value_0_0= RULE_FLOAT
            {
            lv_value_0_0=(Token)match(input,RULE_FLOAT,FOLLOW_2); 

            				newLeafNode(lv_value_0_0, grammarAccess.getFloatLiteralAccess().getValueFLOATTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getFloatLiteralRule());
            				}
            				setWithLastConsumed(
            					current,
            					"value",
            					lv_value_0_0,
            					"de.adrianhoff.GeometryCodes.FLOAT");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFloatLiteral"


    // $ANTLR start "entryRuleStringLiteral"
    // InternalGeometryCodes.g:2737:1: entryRuleStringLiteral returns [EObject current=null] : iv_ruleStringLiteral= ruleStringLiteral EOF ;
    public final EObject entryRuleStringLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStringLiteral = null;


        try {
            // InternalGeometryCodes.g:2737:54: (iv_ruleStringLiteral= ruleStringLiteral EOF )
            // InternalGeometryCodes.g:2738:2: iv_ruleStringLiteral= ruleStringLiteral EOF
            {
             newCompositeNode(grammarAccess.getStringLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleStringLiteral=ruleStringLiteral();

            state._fsp--;

             current =iv_ruleStringLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringLiteral"


    // $ANTLR start "ruleStringLiteral"
    // InternalGeometryCodes.g:2744:1: ruleStringLiteral returns [EObject current=null] : ( (lv_value_0_0= RULE_STRING ) ) ;
    public final EObject ruleStringLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2750:2: ( ( (lv_value_0_0= RULE_STRING ) ) )
            // InternalGeometryCodes.g:2751:2: ( (lv_value_0_0= RULE_STRING ) )
            {
            // InternalGeometryCodes.g:2751:2: ( (lv_value_0_0= RULE_STRING ) )
            // InternalGeometryCodes.g:2752:3: (lv_value_0_0= RULE_STRING )
            {
            // InternalGeometryCodes.g:2752:3: (lv_value_0_0= RULE_STRING )
            // InternalGeometryCodes.g:2753:4: lv_value_0_0= RULE_STRING
            {
            lv_value_0_0=(Token)match(input,RULE_STRING,FOLLOW_2); 

            				newLeafNode(lv_value_0_0, grammarAccess.getStringLiteralAccess().getValueSTRINGTerminalRuleCall_0());
            			

            				if (current==null) {
            					current = createModelElement(grammarAccess.getStringLiteralRule());
            				}
            				setWithLastConsumed(
            					current,
            					"value",
            					lv_value_0_0,
            					"org.eclipse.xtext.common.Terminals.STRING");
            			

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringLiteral"


    // $ANTLR start "entryRuleBooleanLiteral"
    // InternalGeometryCodes.g:2772:1: entryRuleBooleanLiteral returns [EObject current=null] : iv_ruleBooleanLiteral= ruleBooleanLiteral EOF ;
    public final EObject entryRuleBooleanLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBooleanLiteral = null;


        try {
            // InternalGeometryCodes.g:2772:55: (iv_ruleBooleanLiteral= ruleBooleanLiteral EOF )
            // InternalGeometryCodes.g:2773:2: iv_ruleBooleanLiteral= ruleBooleanLiteral EOF
            {
             newCompositeNode(grammarAccess.getBooleanLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleBooleanLiteral=ruleBooleanLiteral();

            state._fsp--;

             current =iv_ruleBooleanLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBooleanLiteral"


    // $ANTLR start "ruleBooleanLiteral"
    // InternalGeometryCodes.g:2779:1: ruleBooleanLiteral returns [EObject current=null] : ( ( (lv_value_0_0= 'true' ) ) | ( () otherlv_2= 'false' ) ) ;
    public final EObject ruleBooleanLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_value_0_0=null;
        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2785:2: ( ( ( (lv_value_0_0= 'true' ) ) | ( () otherlv_2= 'false' ) ) )
            // InternalGeometryCodes.g:2786:2: ( ( (lv_value_0_0= 'true' ) ) | ( () otherlv_2= 'false' ) )
            {
            // InternalGeometryCodes.g:2786:2: ( ( (lv_value_0_0= 'true' ) ) | ( () otherlv_2= 'false' ) )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==41) ) {
                alt33=1;
            }
            else if ( (LA33_0==42) ) {
                alt33=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalGeometryCodes.g:2787:3: ( (lv_value_0_0= 'true' ) )
                    {
                    // InternalGeometryCodes.g:2787:3: ( (lv_value_0_0= 'true' ) )
                    // InternalGeometryCodes.g:2788:4: (lv_value_0_0= 'true' )
                    {
                    // InternalGeometryCodes.g:2788:4: (lv_value_0_0= 'true' )
                    // InternalGeometryCodes.g:2789:5: lv_value_0_0= 'true'
                    {
                    lv_value_0_0=(Token)match(input,41,FOLLOW_2); 

                    					newLeafNode(lv_value_0_0, grammarAccess.getBooleanLiteralAccess().getValueTrueKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getBooleanLiteralRule());
                    					}
                    					setWithLastConsumed(current, "value", lv_value_0_0 != null, "true");
                    				

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2802:3: ( () otherlv_2= 'false' )
                    {
                    // InternalGeometryCodes.g:2802:3: ( () otherlv_2= 'false' )
                    // InternalGeometryCodes.g:2803:4: () otherlv_2= 'false'
                    {
                    // InternalGeometryCodes.g:2803:4: ()
                    // InternalGeometryCodes.g:2804:5: 
                    {

                    					current = forceCreateModelElement(
                    						grammarAccess.getBooleanLiteralAccess().getBooleanLiteralAction_1_0(),
                    						current);
                    				

                    }

                    otherlv_2=(Token)match(input,42,FOLLOW_2); 

                    				newLeafNode(otherlv_2, grammarAccess.getBooleanLiteralAccess().getFalseKeyword_1_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBooleanLiteral"


    // $ANTLR start "ruleAssignmentOperator"
    // InternalGeometryCodes.g:2819:1: ruleAssignmentOperator returns [Enumerator current=null] : ( (enumLiteral_0= '=' ) | (enumLiteral_1= '+=' ) ) ;
    public final Enumerator ruleAssignmentOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2825:2: ( ( (enumLiteral_0= '=' ) | (enumLiteral_1= '+=' ) ) )
            // InternalGeometryCodes.g:2826:2: ( (enumLiteral_0= '=' ) | (enumLiteral_1= '+=' ) )
            {
            // InternalGeometryCodes.g:2826:2: ( (enumLiteral_0= '=' ) | (enumLiteral_1= '+=' ) )
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==25) ) {
                alt34=1;
            }
            else if ( (LA34_0==43) ) {
                alt34=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 34, 0, input);

                throw nvae;
            }
            switch (alt34) {
                case 1 :
                    // InternalGeometryCodes.g:2827:3: (enumLiteral_0= '=' )
                    {
                    // InternalGeometryCodes.g:2827:3: (enumLiteral_0= '=' )
                    // InternalGeometryCodes.g:2828:4: enumLiteral_0= '='
                    {
                    enumLiteral_0=(Token)match(input,25,FOLLOW_2); 

                    				current = grammarAccess.getAssignmentOperatorAccess().getASSIGNEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getAssignmentOperatorAccess().getASSIGNEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2835:3: (enumLiteral_1= '+=' )
                    {
                    // InternalGeometryCodes.g:2835:3: (enumLiteral_1= '+=' )
                    // InternalGeometryCodes.g:2836:4: enumLiteral_1= '+='
                    {
                    enumLiteral_1=(Token)match(input,43,FOLLOW_2); 

                    				current = grammarAccess.getAssignmentOperatorAccess().getAPPENDEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getAssignmentOperatorAccess().getAPPENDEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentOperator"


    // $ANTLR start "ruleUnaryOperator"
    // InternalGeometryCodes.g:2846:1: ruleUnaryOperator returns [Enumerator current=null] : ( (enumLiteral_0= '!' ) | (enumLiteral_1= '-' ) ) ;
    public final Enumerator ruleUnaryOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2852:2: ( ( (enumLiteral_0= '!' ) | (enumLiteral_1= '-' ) ) )
            // InternalGeometryCodes.g:2853:2: ( (enumLiteral_0= '!' ) | (enumLiteral_1= '-' ) )
            {
            // InternalGeometryCodes.g:2853:2: ( (enumLiteral_0= '!' ) | (enumLiteral_1= '-' ) )
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==44) ) {
                alt35=1;
            }
            else if ( (LA35_0==45) ) {
                alt35=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 35, 0, input);

                throw nvae;
            }
            switch (alt35) {
                case 1 :
                    // InternalGeometryCodes.g:2854:3: (enumLiteral_0= '!' )
                    {
                    // InternalGeometryCodes.g:2854:3: (enumLiteral_0= '!' )
                    // InternalGeometryCodes.g:2855:4: enumLiteral_0= '!'
                    {
                    enumLiteral_0=(Token)match(input,44,FOLLOW_2); 

                    				current = grammarAccess.getUnaryOperatorAccess().getNEGATIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getUnaryOperatorAccess().getNEGATIONEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2862:3: (enumLiteral_1= '-' )
                    {
                    // InternalGeometryCodes.g:2862:3: (enumLiteral_1= '-' )
                    // InternalGeometryCodes.g:2863:4: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,45,FOLLOW_2); 

                    				current = grammarAccess.getUnaryOperatorAccess().getNEGATIONEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getUnaryOperatorAccess().getNEGATIONEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUnaryOperator"


    // $ANTLR start "ruleAdditiveOperator"
    // InternalGeometryCodes.g:2873:1: ruleAdditiveOperator returns [Enumerator current=null] : ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) ;
    public final Enumerator ruleAdditiveOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2879:2: ( ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) ) )
            // InternalGeometryCodes.g:2880:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            {
            // InternalGeometryCodes.g:2880:2: ( (enumLiteral_0= '+' ) | (enumLiteral_1= '-' ) )
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==46) ) {
                alt36=1;
            }
            else if ( (LA36_0==45) ) {
                alt36=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }
            switch (alt36) {
                case 1 :
                    // InternalGeometryCodes.g:2881:3: (enumLiteral_0= '+' )
                    {
                    // InternalGeometryCodes.g:2881:3: (enumLiteral_0= '+' )
                    // InternalGeometryCodes.g:2882:4: enumLiteral_0= '+'
                    {
                    enumLiteral_0=(Token)match(input,46,FOLLOW_2); 

                    				current = grammarAccess.getAdditiveOperatorAccess().getADDITIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getAdditiveOperatorAccess().getADDITIONEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2889:3: (enumLiteral_1= '-' )
                    {
                    // InternalGeometryCodes.g:2889:3: (enumLiteral_1= '-' )
                    // InternalGeometryCodes.g:2890:4: enumLiteral_1= '-'
                    {
                    enumLiteral_1=(Token)match(input,45,FOLLOW_2); 

                    				current = grammarAccess.getAdditiveOperatorAccess().getSUBTRACTIONEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getAdditiveOperatorAccess().getSUBTRACTIONEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAdditiveOperator"


    // $ANTLR start "ruleMultiplicativeOperator"
    // InternalGeometryCodes.g:2900:1: ruleMultiplicativeOperator returns [Enumerator current=null] : ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) ) ;
    public final Enumerator ruleMultiplicativeOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2906:2: ( ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) ) )
            // InternalGeometryCodes.g:2907:2: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) )
            {
            // InternalGeometryCodes.g:2907:2: ( (enumLiteral_0= '*' ) | (enumLiteral_1= '/' ) )
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==47) ) {
                alt37=1;
            }
            else if ( (LA37_0==48) ) {
                alt37=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 37, 0, input);

                throw nvae;
            }
            switch (alt37) {
                case 1 :
                    // InternalGeometryCodes.g:2908:3: (enumLiteral_0= '*' )
                    {
                    // InternalGeometryCodes.g:2908:3: (enumLiteral_0= '*' )
                    // InternalGeometryCodes.g:2909:4: enumLiteral_0= '*'
                    {
                    enumLiteral_0=(Token)match(input,47,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicativeOperatorAccess().getMULTIPLICATIONEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getMultiplicativeOperatorAccess().getMULTIPLICATIONEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2916:3: (enumLiteral_1= '/' )
                    {
                    // InternalGeometryCodes.g:2916:3: (enumLiteral_1= '/' )
                    // InternalGeometryCodes.g:2917:4: enumLiteral_1= '/'
                    {
                    enumLiteral_1=(Token)match(input,48,FOLLOW_2); 

                    				current = grammarAccess.getMultiplicativeOperatorAccess().getDIVISIONEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getMultiplicativeOperatorAccess().getDIVISIONEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMultiplicativeOperator"


    // $ANTLR start "ruleComparisonOperator"
    // InternalGeometryCodes.g:2927:1: ruleComparisonOperator returns [Enumerator current=null] : ( (enumLiteral_0= '==' ) | (enumLiteral_1= '!=' ) | (enumLiteral_2= '>' ) | (enumLiteral_3= '>=' ) | (enumLiteral_4= '<' ) | (enumLiteral_5= '<=' ) ) ;
    public final Enumerator ruleComparisonOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2933:2: ( ( (enumLiteral_0= '==' ) | (enumLiteral_1= '!=' ) | (enumLiteral_2= '>' ) | (enumLiteral_3= '>=' ) | (enumLiteral_4= '<' ) | (enumLiteral_5= '<=' ) ) )
            // InternalGeometryCodes.g:2934:2: ( (enumLiteral_0= '==' ) | (enumLiteral_1= '!=' ) | (enumLiteral_2= '>' ) | (enumLiteral_3= '>=' ) | (enumLiteral_4= '<' ) | (enumLiteral_5= '<=' ) )
            {
            // InternalGeometryCodes.g:2934:2: ( (enumLiteral_0= '==' ) | (enumLiteral_1= '!=' ) | (enumLiteral_2= '>' ) | (enumLiteral_3= '>=' ) | (enumLiteral_4= '<' ) | (enumLiteral_5= '<=' ) )
            int alt38=6;
            switch ( input.LA(1) ) {
            case 49:
                {
                alt38=1;
                }
                break;
            case 50:
                {
                alt38=2;
                }
                break;
            case 51:
                {
                alt38=3;
                }
                break;
            case 52:
                {
                alt38=4;
                }
                break;
            case 53:
                {
                alt38=5;
                }
                break;
            case 54:
                {
                alt38=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 38, 0, input);

                throw nvae;
            }

            switch (alt38) {
                case 1 :
                    // InternalGeometryCodes.g:2935:3: (enumLiteral_0= '==' )
                    {
                    // InternalGeometryCodes.g:2935:3: (enumLiteral_0= '==' )
                    // InternalGeometryCodes.g:2936:4: enumLiteral_0= '=='
                    {
                    enumLiteral_0=(Token)match(input,49,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getEQUALEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getComparisonOperatorAccess().getEQUALEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:2943:3: (enumLiteral_1= '!=' )
                    {
                    // InternalGeometryCodes.g:2943:3: (enumLiteral_1= '!=' )
                    // InternalGeometryCodes.g:2944:4: enumLiteral_1= '!='
                    {
                    enumLiteral_1=(Token)match(input,50,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getNOTEQUALEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getComparisonOperatorAccess().getNOTEQUALEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:2951:3: (enumLiteral_2= '>' )
                    {
                    // InternalGeometryCodes.g:2951:3: (enumLiteral_2= '>' )
                    // InternalGeometryCodes.g:2952:4: enumLiteral_2= '>'
                    {
                    enumLiteral_2=(Token)match(input,51,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getGREATEREnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getComparisonOperatorAccess().getGREATEREnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:2959:3: (enumLiteral_3= '>=' )
                    {
                    // InternalGeometryCodes.g:2959:3: (enumLiteral_3= '>=' )
                    // InternalGeometryCodes.g:2960:4: enumLiteral_3= '>='
                    {
                    enumLiteral_3=(Token)match(input,52,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getGREATEREQUALEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getComparisonOperatorAccess().getGREATEREQUALEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalGeometryCodes.g:2967:3: (enumLiteral_4= '<' )
                    {
                    // InternalGeometryCodes.g:2967:3: (enumLiteral_4= '<' )
                    // InternalGeometryCodes.g:2968:4: enumLiteral_4= '<'
                    {
                    enumLiteral_4=(Token)match(input,53,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getLESSEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getComparisonOperatorAccess().getLESSEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalGeometryCodes.g:2975:3: (enumLiteral_5= '<=' )
                    {
                    // InternalGeometryCodes.g:2975:3: (enumLiteral_5= '<=' )
                    // InternalGeometryCodes.g:2976:4: enumLiteral_5= '<='
                    {
                    enumLiteral_5=(Token)match(input,54,FOLLOW_2); 

                    				current = grammarAccess.getComparisonOperatorAccess().getLESSEQUALEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getComparisonOperatorAccess().getLESSEQUALEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleComparisonOperator"


    // $ANTLR start "ruleLogicalOperator"
    // InternalGeometryCodes.g:2986:1: ruleLogicalOperator returns [Enumerator current=null] : ( (enumLiteral_0= '&&' ) | (enumLiteral_1= '||' ) ) ;
    public final Enumerator ruleLogicalOperator() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:2992:2: ( ( (enumLiteral_0= '&&' ) | (enumLiteral_1= '||' ) ) )
            // InternalGeometryCodes.g:2993:2: ( (enumLiteral_0= '&&' ) | (enumLiteral_1= '||' ) )
            {
            // InternalGeometryCodes.g:2993:2: ( (enumLiteral_0= '&&' ) | (enumLiteral_1= '||' ) )
            int alt39=2;
            int LA39_0 = input.LA(1);

            if ( (LA39_0==55) ) {
                alt39=1;
            }
            else if ( (LA39_0==56) ) {
                alt39=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 39, 0, input);

                throw nvae;
            }
            switch (alt39) {
                case 1 :
                    // InternalGeometryCodes.g:2994:3: (enumLiteral_0= '&&' )
                    {
                    // InternalGeometryCodes.g:2994:3: (enumLiteral_0= '&&' )
                    // InternalGeometryCodes.g:2995:4: enumLiteral_0= '&&'
                    {
                    enumLiteral_0=(Token)match(input,55,FOLLOW_2); 

                    				current = grammarAccess.getLogicalOperatorAccess().getANDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getLogicalOperatorAccess().getANDEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:3002:3: (enumLiteral_1= '||' )
                    {
                    // InternalGeometryCodes.g:3002:3: (enumLiteral_1= '||' )
                    // InternalGeometryCodes.g:3003:4: enumLiteral_1= '||'
                    {
                    enumLiteral_1=(Token)match(input,56,FOLLOW_2); 

                    				current = grammarAccess.getLogicalOperatorAccess().getOREnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getLogicalOperatorAccess().getOREnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLogicalOperator"


    // $ANTLR start "ruleType"
    // InternalGeometryCodes.g:3013:1: ruleType returns [Enumerator current=null] : ( (enumLiteral_0= 'void' ) | (enumLiteral_1= 'float' ) | (enumLiteral_2= 'vector' ) | (enumLiteral_3= 'triangle' ) | (enumLiteral_4= 'quad' ) | (enumLiteral_5= 'mesh' ) ) ;
    public final Enumerator ruleType() throws RecognitionException {
        Enumerator current = null;

        Token enumLiteral_0=null;
        Token enumLiteral_1=null;
        Token enumLiteral_2=null;
        Token enumLiteral_3=null;
        Token enumLiteral_4=null;
        Token enumLiteral_5=null;


        	enterRule();

        try {
            // InternalGeometryCodes.g:3019:2: ( ( (enumLiteral_0= 'void' ) | (enumLiteral_1= 'float' ) | (enumLiteral_2= 'vector' ) | (enumLiteral_3= 'triangle' ) | (enumLiteral_4= 'quad' ) | (enumLiteral_5= 'mesh' ) ) )
            // InternalGeometryCodes.g:3020:2: ( (enumLiteral_0= 'void' ) | (enumLiteral_1= 'float' ) | (enumLiteral_2= 'vector' ) | (enumLiteral_3= 'triangle' ) | (enumLiteral_4= 'quad' ) | (enumLiteral_5= 'mesh' ) )
            {
            // InternalGeometryCodes.g:3020:2: ( (enumLiteral_0= 'void' ) | (enumLiteral_1= 'float' ) | (enumLiteral_2= 'vector' ) | (enumLiteral_3= 'triangle' ) | (enumLiteral_4= 'quad' ) | (enumLiteral_5= 'mesh' ) )
            int alt40=6;
            switch ( input.LA(1) ) {
            case 57:
                {
                alt40=1;
                }
                break;
            case 58:
                {
                alt40=2;
                }
                break;
            case 36:
                {
                alt40=3;
                }
                break;
            case 38:
                {
                alt40=4;
                }
                break;
            case 39:
                {
                alt40=5;
                }
                break;
            case 59:
                {
                alt40=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 40, 0, input);

                throw nvae;
            }

            switch (alt40) {
                case 1 :
                    // InternalGeometryCodes.g:3021:3: (enumLiteral_0= 'void' )
                    {
                    // InternalGeometryCodes.g:3021:3: (enumLiteral_0= 'void' )
                    // InternalGeometryCodes.g:3022:4: enumLiteral_0= 'void'
                    {
                    enumLiteral_0=(Token)match(input,57,FOLLOW_2); 

                    				current = grammarAccess.getTypeAccess().getVOIDEnumLiteralDeclaration_0().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_0, grammarAccess.getTypeAccess().getVOIDEnumLiteralDeclaration_0());
                    			

                    }


                    }
                    break;
                case 2 :
                    // InternalGeometryCodes.g:3029:3: (enumLiteral_1= 'float' )
                    {
                    // InternalGeometryCodes.g:3029:3: (enumLiteral_1= 'float' )
                    // InternalGeometryCodes.g:3030:4: enumLiteral_1= 'float'
                    {
                    enumLiteral_1=(Token)match(input,58,FOLLOW_2); 

                    				current = grammarAccess.getTypeAccess().getFLOATEnumLiteralDeclaration_1().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_1, grammarAccess.getTypeAccess().getFLOATEnumLiteralDeclaration_1());
                    			

                    }


                    }
                    break;
                case 3 :
                    // InternalGeometryCodes.g:3037:3: (enumLiteral_2= 'vector' )
                    {
                    // InternalGeometryCodes.g:3037:3: (enumLiteral_2= 'vector' )
                    // InternalGeometryCodes.g:3038:4: enumLiteral_2= 'vector'
                    {
                    enumLiteral_2=(Token)match(input,36,FOLLOW_2); 

                    				current = grammarAccess.getTypeAccess().getVECTOREnumLiteralDeclaration_2().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_2, grammarAccess.getTypeAccess().getVECTOREnumLiteralDeclaration_2());
                    			

                    }


                    }
                    break;
                case 4 :
                    // InternalGeometryCodes.g:3045:3: (enumLiteral_3= 'triangle' )
                    {
                    // InternalGeometryCodes.g:3045:3: (enumLiteral_3= 'triangle' )
                    // InternalGeometryCodes.g:3046:4: enumLiteral_3= 'triangle'
                    {
                    enumLiteral_3=(Token)match(input,38,FOLLOW_2); 

                    				current = grammarAccess.getTypeAccess().getTRIANGLEEnumLiteralDeclaration_3().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_3, grammarAccess.getTypeAccess().getTRIANGLEEnumLiteralDeclaration_3());
                    			

                    }


                    }
                    break;
                case 5 :
                    // InternalGeometryCodes.g:3053:3: (enumLiteral_4= 'quad' )
                    {
                    // InternalGeometryCodes.g:3053:3: (enumLiteral_4= 'quad' )
                    // InternalGeometryCodes.g:3054:4: enumLiteral_4= 'quad'
                    {
                    enumLiteral_4=(Token)match(input,39,FOLLOW_2); 

                    				current = grammarAccess.getTypeAccess().getQUADEnumLiteralDeclaration_4().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_4, grammarAccess.getTypeAccess().getQUADEnumLiteralDeclaration_4());
                    			

                    }


                    }
                    break;
                case 6 :
                    // InternalGeometryCodes.g:3061:3: (enumLiteral_5= 'mesh' )
                    {
                    // InternalGeometryCodes.g:3061:3: (enumLiteral_5= 'mesh' )
                    // InternalGeometryCodes.g:3062:4: enumLiteral_5= 'mesh'
                    {
                    enumLiteral_5=(Token)match(input,59,FOLLOW_2); 

                    				current = grammarAccess.getTypeAccess().getMESHEnumLiteralDeclaration_5().getEnumLiteral().getInstance();
                    				newLeafNode(enumLiteral_5, grammarAccess.getTypeAccess().getMESHEnumLiteralDeclaration_5());
                    			

                    }


                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleType"

    // Delegated rules


    protected DFA2 dfa2 = new DFA2(this);
    protected DFA12 dfa12 = new DFA12(this);
    static final String dfa_1s = "\15\uffff";
    static final String dfa_2s = "\1\1\14\uffff";
    static final String dfa_3s = "\1\5\1\uffff\6\5\3\uffff\1\15\1\uffff";
    static final String dfa_4s = "\1\73\1\uffff\6\5\3\uffff\1\31\1\uffff";
    static final String dfa_5s = "\1\uffff\1\5\6\uffff\1\2\1\3\1\4\1\uffff\1\1";
    static final String dfa_6s = "\15\uffff}>";
    static final String[] dfa_7s = {
            "\1\12\14\uffff\1\10\2\uffff\1\11\4\uffff\1\12\1\uffff\2\12\3\uffff\2\12\1\uffff\1\4\1\uffff\1\5\1\6\21\uffff\1\2\1\3\1\7",
            "",
            "\1\13",
            "\1\13",
            "\1\13",
            "\1\13",
            "\1\13",
            "\1\13",
            "",
            "",
            "",
            "\1\14\13\uffff\1\12",
            ""
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA2 extends DFA {

        public DFA2(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 2;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "()* loopback of 99:3: ( ( (lv_functions_1_0= ruleFunctionDeclaration ) ) | ( (lv_iterators_2_0= ruleIteratorDeclaration ) ) | ( (lv_materials_3_0= ruleMaterialDeclaration ) ) | ( (lv_statements_4_0= ruleStatement ) ) )*";
        }
    }
    static final String dfa_8s = "\14\uffff";
    static final String dfa_9s = "\2\uffff\1\11\11\uffff";
    static final String dfa_10s = "\1\5\1\uffff\1\5\2\uffff\1\15\6\uffff";
    static final String dfa_11s = "\1\73\1\uffff\1\73\2\uffff\1\24\6\uffff";
    static final String dfa_12s = "\1\uffff\1\1\1\uffff\1\4\1\5\1\uffff\1\10\1\11\1\2\1\3\1\7\1\6";
    static final String dfa_13s = "\14\uffff}>";
    static final String[] dfa_14s = {
            "\1\2\24\uffff\1\3\1\uffff\1\4\1\5\3\uffff\1\6\1\7\1\uffff\1\1\1\uffff\2\1\21\uffff\3\1",
            "",
            "\1\11\7\uffff\1\11\3\uffff\2\11\2\uffff\1\11\3\uffff\1\10\1\11\1\uffff\2\11\3\uffff\4\11\1\uffff\2\11\3\uffff\1\10\15\uffff\3\11",
            "",
            "",
            "\1\13\6\uffff\1\12",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final short[] dfa_9 = DFA.unpackEncodedString(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final char[] dfa_11 = DFA.unpackEncodedStringToUnsignedChars(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[] dfa_13 = DFA.unpackEncodedString(dfa_13s);
    static final short[][] dfa_14 = unpackEncodedStringArray(dfa_14s);

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = dfa_8;
            this.eof = dfa_9;
            this.min = dfa_10;
            this.max = dfa_11;
            this.accept = dfa_12;
            this.special = dfa_13;
            this.transition = dfa_14;
        }
        public String getDescription() {
            return "874:2: (this_DeclarationStatement_0= ruleDeclarationStatement | this_AssignmentStatement_1= ruleAssignmentStatement | this_ReferenceStatement_2= ruleReferenceStatement | this_ReturnStatement_3= ruleReturnStatement | this_IfStatement_4= ruleIfStatement | this_ForLoopStatement_5= ruleForLoopStatement | this_ForEachLoopStatement_6= ruleForEachLoopStatement | this_WhileLoopStatement_7= ruleWhileLoopStatement | this_OutputStatement_8= ruleOutputStatement )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0E0000D634241022L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0E0000D634240022L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0E0000D000008000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000000000C000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0E0000D000000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0E0000D634260020L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000020020L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000001800000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000002000000L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x000037F000002070L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000080002000000L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x000037F008002070L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0180000000000002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x007E000000000002L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000600000000002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0001800000000002L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x000037F00000A070L});

}