grammar de.adrianhoff.GeometryCodes
	with org.eclipse.xtext.common.Terminals

import "http://www.eclipse.org/emf/2002/Ecore" as ecore

import "http://www.adrianhoff.de/GeometryCodes/core"
import "http://www.adrianhoff.de/GeometryCodes/statements"
import "http://www.adrianhoff.de/GeometryCodes/types"
import "http://www.adrianhoff.de/GeometryCodes/expressions"
import "http://www.adrianhoff.de/GeometryCodes/literals"
import "http://www.adrianhoff.de/GeometryCodes/operators"



File:
	(imports += ImportStatement)*
	(functions += FunctionDeclaration
		| iterators += IteratorDeclaration
		| materials += MaterialDeclaration
		| statements += Statement
	)*
;

ImportStatement:
	'import' importURI=STRING;



FunctionDeclaration:
	type=Type name=ID '(' ( parameters += Parameter (',' parameters += Parameter)* )? ')' '{'
		statements += Statement*
	'}'
;

IteratorDeclaration:
	'iterator' name=ID '(' ( parameters += Parameter (',' parameters += Parameter)* )? ')'
	'over' 'each' iteratedParameter=Parameter '{'
		statements += Statement*
	'}'
;

Parameter:
	type=Type name=ID;



MaterialDeclaration:
	'material' name=ID '{'
		properties += MaterialProperty*
	'}'
;

MaterialProperty:
	MaterialStringProperty | MaterialFloatProperty | MaterialBooleanProperty;

MaterialStringProperty:
	name=ID ':' value=STRING;

MaterialFloatProperty:
	name=ID ':' value=FLOAT;

MaterialBooleanProperty:
	name=ID ':' (value?='yes' | 'no');



Statement:
	DeclarationStatement | AssignmentStatement | ReferenceStatement | ReturnStatement
	| IfStatement | ForLoopStatement | ForEachLoopStatement | WhileLoopStatement
	| OutputStatement;

DeclarationStatement:
	type=Type name=ID '=' value=Expression;

AssignmentStatement:
	target=[ReferenceableElement] operator=AssignmentOperator value=Expression;

ReferenceStatement:
	reference = ReferenceExpression;

ReturnStatement:
	{ReturnStatement} 'return' (value += Expression | 'nothing');

IfStatement:
	'if' '(' condition=Expression ')' '{'
		statements += Statement*
	'}';

ForLoopStatement:
	'for' '(' type=Type name=ID 'from' fromValue=Expression 'to' toValue=Expression ')' '{'
		statements += Statement*
	'}';

ForEachLoopStatement:
	'for' 'each' '(' type=Type name=ID 'in' value=Expression ')' '{'
		statements += Statement*
	'}';

WhileLoopStatement:
	'while' '(' condition=Expression ')' '{'
		statements += Statement*
	'}';

OutputStatement:
	'output' value = Expression;



Expression:
	LogicalExpression;

LogicalExpression returns Expression:
	ConditionalExpression ({LogicalExpression.leftChild=current} operator=LogicalOperator rightChild=ConditionalExpression)*;

ConditionalExpression returns Expression:
	AdditiveExpression ({ConditionalExpression.leftChild=current} operator=ComparisonOperator rightChild=AdditiveExpression)*;

AdditiveExpression returns Expression:
	MultiplicativeExpression ({AdditiveExpression.leftChild=current} operator=AdditiveOperator rightChild=MultiplicativeExpression)*;

MultiplicativeExpression returns Expression:
	LiteralExpression ({MultiplicativeExpression.leftChild=current} operator=MultiplicativeOperator rightChild=LiteralExpression)*;

LiteralExpression returns Expression:
	Literal | '(' Expression ')' | UnaryExpression |
	ReferenceExpression | DeclarationExpression;



UnaryExpression:
	operator=UnaryOperator child=LiteralExpression;

ReferenceExpression:
	(VariableReferenceExpression | FunctionCallExpression) ('.' next=ReferenceExpression)?;

VariableReferenceExpression:
	target = [ReferenceableElement];

FunctionCallExpression:
	target = [ReferenceableElement] '(' ( arguments += Expression (',' arguments += Expression)* )? ')';

DeclarationExpression:
	VectorDeclarationExpression | VertexDeclarationExpression | TriangleDeclarationExpression | QuadDeclarationExpression;

VectorDeclarationExpression:
	'vector' '(' arguments += Expression (',' arguments += Expression)* ')';

VertexDeclarationExpression:
	'vertex' '(' arguments += Expression (',' arguments += Expression)* ')';

TriangleDeclarationExpression:
	'triangle'  '(' arguments += Expression (',' arguments += Expression)* ')';

QuadDeclarationExpression:
	'quad' '(' arguments += Expression (',' arguments += Expression)* ')';



Literal returns Expression:
	FloatLiteral | StringLiteral | BooleanLiteral | {EmptyLiteral} 'empty';

FloatLiteral:
	value=FLOAT;

StringLiteral:
	value=STRING;

BooleanLiteral:
	value ?= 'true' | {BooleanLiteral} 'false';



enum AssignmentOperator:
	ASSIGN='=' | APPEND='+=';

enum UnaryOperator:
	NEGATION='!' | NEGATION='-';

enum AdditiveOperator:
	ADDITION='+' | SUBTRACTION='-';

enum MultiplicativeOperator:
	MULTIPLICATION='*' | DIVISION='/';

enum ComparisonOperator:
	EQUAL='==' | NOTEQUAL='!=' | GREATER='>' | GREATEREQUAL='>=' | LESS='<' | LESSEQUAL='<=';

enum LogicalOperator:
	AND='&&' | OR='||';

enum Type:
	VOID='void' | FLOAT='float' | VECTOR='vector' | TRIANGLE='triangle' | QUAD='quad' | MESH='mesh';



terminal FLOAT returns ecore::EFloat: INT ('.' INT)?;


