package de.adrianhoff.scoping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;

import de.adrianhoff.geometrycodes.core.CoreFactory;
import de.adrianhoff.geometrycodes.core.File;
import de.adrianhoff.geometrycodes.core.FunctionDeclaration;
import de.adrianhoff.geometrycodes.core.IteratorDeclaration;
import de.adrianhoff.geometrycodes.core.MaterialDeclaration;
import de.adrianhoff.geometrycodes.language.AbstractSyntaxCreationUtil;
import de.adrianhoff.geometrycodes.statements.Statement;
import de.adrianhoff.geometrycodes.types.Type;

public class GeometryCodeStandardLibrary {
	
	public static final GeometryCodeStandardLibrary INSTANCE = new GeometryCodeStandardLibrary();
	
	
	
	private final URI virtualURI;
	
	public GeometryCodeStandardLibrary() {
		virtualURI = URI.createURI("platform:/resource/de.adrianhoff.geometrycodes/standardlib.geomcode");
	}
	
	
	
	public URI getStandardLibraryResource(final Resource resource) {
		boolean standardResourceAdded = false;
		for(Resource existingResource : resource.getResourceSet().getResources()) {
			if(existingResource.getURI().equals(virtualURI)) {
				standardResourceAdded = true;
				break;
			}
		}
		if(!standardResourceAdded) {
			Resource standardLibResource = resource.getResourceSet().createResource(virtualURI);
			standardLibResource.getContents().add(createStandardLibraryFile());
		}
		return virtualURI;
	}

	private File createStandardLibraryFile() {
		File libFile = CoreFactory.eINSTANCE.createFile();
		libFile.getStatements().addAll(createStandardStatements());
		libFile.getFunctions().addAll(createStandardFunctions());
		libFile.getIterators().addAll(createStandardIterator());
		libFile.getMaterials().addAll(createStandardMaterials());
		return libFile;
	}

	private Collection<? extends Statement> createStandardStatements() {
		List<Statement> standardStatements = new ArrayList<>();
		standardStatements.add(AbstractSyntaxCreationUtil.createDeclarationStatement(Type.VOID, Type.VOID, "math"));
		standardStatements.add(AbstractSyntaxCreationUtil.createDeclarationStatement(Type.VECTOR, Type.FLOAT, "x"));
		standardStatements.add(AbstractSyntaxCreationUtil.createDeclarationStatement(Type.VECTOR, Type.FLOAT, "y"));
		standardStatements.add(AbstractSyntaxCreationUtil.createDeclarationStatement(Type.VECTOR, Type.FLOAT, "z"));
		standardStatements.add(AbstractSyntaxCreationUtil.createDeclarationStatement(Type.TRIANGLE, Type.VECTOR, "normal"));
		return standardStatements;
	}

	private Collection<? extends FunctionDeclaration> createStandardFunctions() {
		List<FunctionDeclaration> standardFunctions = new ArrayList<>();
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.VECTOR, Type.VERTEX, "vertices"));
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.VECTOR, Type.VOID, "normalize"));
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.VECTOR, Type.VECTOR, "normalized"));
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.VECTOR, Type.FLOAT, "angleTo"));
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.VOID, Type.VECTOR, "randomVector"));
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.VOID, Type.FLOAT, "randomFloat"));
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.MESH, Type.VOID, "scale"));
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.MESH, Type.VOID, "rotate"));
		standardFunctions.add(AbstractSyntaxCreationUtil.createFunctionDeclaration(Type.MESH, Type.VOID, "move"));
		return standardFunctions;
	}

	private Collection<? extends IteratorDeclaration> createStandardIterator() {
		List<IteratorDeclaration> standardIterators = new ArrayList<>();
		return standardIterators;
	}

	private Collection<? extends MaterialDeclaration> createStandardMaterials() {
		List<MaterialDeclaration> standardMaterials = new ArrayList<>();
		return standardMaterials;
	}
	
}
