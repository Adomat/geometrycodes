package de.adrianhoff.scoping;

import java.util.LinkedHashSet;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.scoping.impl.ImportUriGlobalScopeProvider;

public class GeometryCodesImportUriGlobalScopeProvider extends ImportUriGlobalScopeProvider {
	
	@Override
	protected LinkedHashSet<URI> getImportedUris(final Resource resource) {
		LinkedHashSet<URI> importedUris = super.getImportedUris(resource);
		importedUris.add(GeometryCodeStandardLibrary.INSTANCE.getStandardLibraryResource(resource));
		return importedUris;
	}
	
}
