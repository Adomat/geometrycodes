/**
 */
package de.adrianhoff.geometrycodes.statements.tests;

import de.adrianhoff.geometrycodes.statements.ConditionalStatement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Conditional Statement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ConditionalStatementTest extends StatementTest {

	/**
	 * Constructs a new Conditional Statement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConditionalStatementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Conditional Statement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ConditionalStatement getFixture() {
		return (ConditionalStatement)fixture;
	}

} //ConditionalStatementTest
