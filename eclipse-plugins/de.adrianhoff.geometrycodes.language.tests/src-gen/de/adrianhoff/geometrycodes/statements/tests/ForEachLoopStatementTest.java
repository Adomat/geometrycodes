/**
 */
package de.adrianhoff.geometrycodes.statements.tests;

import de.adrianhoff.geometrycodes.statements.ForEachLoopStatement;
import de.adrianhoff.geometrycodes.statements.StatementsFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>For Each Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ForEachLoopStatementTest extends ConditionalStatementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ForEachLoopStatementTest.class);
	}

	/**
	 * Constructs a new For Each Loop Statement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForEachLoopStatementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this For Each Loop Statement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ForEachLoopStatement getFixture() {
		return (ForEachLoopStatement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatementsFactory.eINSTANCE.createForEachLoopStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ForEachLoopStatementTest
