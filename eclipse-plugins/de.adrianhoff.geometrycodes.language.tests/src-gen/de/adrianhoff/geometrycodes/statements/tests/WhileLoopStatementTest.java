/**
 */
package de.adrianhoff.geometrycodes.statements.tests;

import de.adrianhoff.geometrycodes.statements.StatementsFactory;
import de.adrianhoff.geometrycodes.statements.WhileLoopStatement;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>While Loop Statement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class WhileLoopStatementTest extends ConditionalStatementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(WhileLoopStatementTest.class);
	}

	/**
	 * Constructs a new While Loop Statement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WhileLoopStatementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this While Loop Statement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected WhileLoopStatement getFixture() {
		return (WhileLoopStatement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatementsFactory.eINSTANCE.createWhileLoopStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //WhileLoopStatementTest
