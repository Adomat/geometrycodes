/**
 */
package de.adrianhoff.geometrycodes.statements.tests;

import de.adrianhoff.geometrycodes.statements.StatementContainer;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Statement Container</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class StatementContainerTest extends TestCase {

	/**
	 * The fixture for this Statement Container test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatementContainer fixture = null;

	/**
	 * Constructs a new Statement Container test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatementContainerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Statement Container test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(StatementContainer fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Statement Container test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StatementContainer getFixture() {
		return fixture;
	}

} //StatementContainerTest
