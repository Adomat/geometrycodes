/**
 */
package de.adrianhoff.geometrycodes.statements.tests;

import de.adrianhoff.geometrycodes.statements.DeclarationStatement;
import de.adrianhoff.geometrycodes.statements.StatementsFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Declaration Statement</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeclarationStatementTest extends StatementTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(DeclarationStatementTest.class);
	}

	/**
	 * Constructs a new Declaration Statement test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationStatementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Declaration Statement test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DeclarationStatement getFixture() {
		return (DeclarationStatement)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(StatementsFactory.eINSTANCE.createDeclarationStatement());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //DeclarationStatementTest
