/**
 */
package de.adrianhoff.geometrycodes.core.tests;

import de.adrianhoff.geometrycodes.core.CoreFactory;
import de.adrianhoff.geometrycodes.core.MaterialBooleanProperty;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Material Boolean Property</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MaterialBooleanPropertyTest extends MaterialPropertyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MaterialBooleanPropertyTest.class);
	}

	/**
	 * Constructs a new Material Boolean Property test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialBooleanPropertyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Material Boolean Property test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected MaterialBooleanProperty getFixture() {
		return (MaterialBooleanProperty)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createMaterialBooleanProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MaterialBooleanPropertyTest
