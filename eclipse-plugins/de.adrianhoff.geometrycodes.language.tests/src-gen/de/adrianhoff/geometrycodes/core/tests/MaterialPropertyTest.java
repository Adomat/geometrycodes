/**
 */
package de.adrianhoff.geometrycodes.core.tests;

import de.adrianhoff.geometrycodes.core.MaterialProperty;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Material Property</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class MaterialPropertyTest extends NamedElementTest {

	/**
	 * Constructs a new Material Property test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialPropertyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Material Property test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected MaterialProperty getFixture() {
		return (MaterialProperty)fixture;
	}

} //MaterialPropertyTest
