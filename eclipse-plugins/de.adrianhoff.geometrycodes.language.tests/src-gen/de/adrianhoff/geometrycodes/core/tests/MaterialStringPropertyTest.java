/**
 */
package de.adrianhoff.geometrycodes.core.tests;

import de.adrianhoff.geometrycodes.core.CoreFactory;
import de.adrianhoff.geometrycodes.core.MaterialStringProperty;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Material String Property</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MaterialStringPropertyTest extends MaterialPropertyTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MaterialStringPropertyTest.class);
	}

	/**
	 * Constructs a new Material String Property test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaterialStringPropertyTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Material String Property test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected MaterialStringProperty getFixture() {
		return (MaterialStringProperty)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(CoreFactory.eINSTANCE.createMaterialStringProperty());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MaterialStringPropertyTest
