/**
 */
package de.adrianhoff.geometrycodes.core.tests;

import de.adrianhoff.geometrycodes.core.ReferenceableElement;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Referenceable Element</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ReferenceableElementTest extends NamedElementTest {

	/**
	 * Constructs a new Referenceable Element test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReferenceableElementTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Referenceable Element test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected ReferenceableElement getFixture() {
		return (ReferenceableElement)fixture;
	}

} //ReferenceableElementTest
