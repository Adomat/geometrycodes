/**
 */
package de.adrianhoff.geometrycodes.literals.tests;

import de.adrianhoff.geometrycodes.literals.EmptyLiteral;
import de.adrianhoff.geometrycodes.literals.LiteralsFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Empty Literal</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class EmptyLiteralTest extends LiteralTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(EmptyLiteralTest.class);
	}

	/**
	 * Constructs a new Empty Literal test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmptyLiteralTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Empty Literal test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EmptyLiteral getFixture() {
		return (EmptyLiteral)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(LiteralsFactory.eINSTANCE.createEmptyLiteral());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //EmptyLiteralTest
