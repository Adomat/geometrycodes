/**
 */
package de.adrianhoff.geometrycodes.literals.tests;

import de.adrianhoff.geometrycodes.expressions.tests.ExpressionTest;

import de.adrianhoff.geometrycodes.literals.Literal;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Literal</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class LiteralTest extends ExpressionTest {

	/**
	 * Constructs a new Literal test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LiteralTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Literal test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Literal getFixture() {
		return (Literal)fixture;
	}

} //LiteralTest
