/**
 */
package de.adrianhoff.geometrycodes.expressions.tests;

import de.adrianhoff.geometrycodes.expressions.ExpressionsFactory;
import de.adrianhoff.geometrycodes.expressions.MultiplicativeExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Multiplicative Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class MultiplicativeExpressionTest extends BinaryExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MultiplicativeExpressionTest.class);
	}

	/**
	 * Constructs a new Multiplicative Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicativeExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Multiplicative Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected MultiplicativeExpression getFixture() {
		return (MultiplicativeExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExpressionsFactory.eINSTANCE.createMultiplicativeExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //MultiplicativeExpressionTest
