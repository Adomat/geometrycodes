/**
 */
package de.adrianhoff.geometrycodes.expressions.tests;

import de.adrianhoff.geometrycodes.expressions.ArgumentContainer;

import junit.framework.TestCase;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Argument Container</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class ArgumentContainerTest extends TestCase {

	/**
	 * The fixture for this Argument Container test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentContainer fixture = null;

	/**
	 * Constructs a new Argument Container test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ArgumentContainerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Argument Container test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(ArgumentContainer fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Argument Container test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ArgumentContainer getFixture() {
		return fixture;
	}

} //ArgumentContainerTest
