/**
 */
package de.adrianhoff.geometrycodes.expressions.tests;

import de.adrianhoff.geometrycodes.expressions.ExpressionsFactory;
import de.adrianhoff.geometrycodes.expressions.VariableReferenceExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Variable Reference Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class VariableReferenceExpressionTest extends ReferenceExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(VariableReferenceExpressionTest.class);
	}

	/**
	 * Constructs a new Variable Reference Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariableReferenceExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Variable Reference Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected VariableReferenceExpression getFixture() {
		return (VariableReferenceExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExpressionsFactory.eINSTANCE.createVariableReferenceExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //VariableReferenceExpressionTest
