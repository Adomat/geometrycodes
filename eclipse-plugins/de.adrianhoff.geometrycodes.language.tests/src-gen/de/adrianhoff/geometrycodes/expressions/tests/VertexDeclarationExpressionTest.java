/**
 */
package de.adrianhoff.geometrycodes.expressions.tests;

import de.adrianhoff.geometrycodes.expressions.ExpressionsFactory;
import de.adrianhoff.geometrycodes.expressions.VertexDeclarationExpression;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Vertex Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class VertexDeclarationExpressionTest extends DeclarationExpressionTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(VertexDeclarationExpressionTest.class);
	}

	/**
	 * Constructs a new Vertex Declaration Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VertexDeclarationExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Vertex Declaration Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected VertexDeclarationExpression getFixture() {
		return (VertexDeclarationExpression)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ExpressionsFactory.eINSTANCE.createVertexDeclarationExpression());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //VertexDeclarationExpressionTest
