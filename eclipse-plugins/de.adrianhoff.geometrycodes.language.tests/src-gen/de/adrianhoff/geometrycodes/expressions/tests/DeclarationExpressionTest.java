/**
 */
package de.adrianhoff.geometrycodes.expressions.tests;

import de.adrianhoff.geometrycodes.expressions.DeclarationExpression;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Declaration Expression</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public abstract class DeclarationExpressionTest extends ArgumentContainerTest {

	/**
	 * Constructs a new Declaration Expression test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeclarationExpressionTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Declaration Expression test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected DeclarationExpression getFixture() {
		return (DeclarationExpression)fixture;
	}

} //DeclarationExpressionTest
