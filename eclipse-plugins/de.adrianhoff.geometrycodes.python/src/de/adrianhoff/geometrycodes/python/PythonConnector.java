package de.adrianhoff.geometrycodes.python;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.IStreamListener;
import org.eclipse.debug.core.Launch;
import org.eclipse.debug.core.model.IStreamMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.launching.IVMInstall;
import org.eclipse.jdt.launching.IVMRunner;
import org.eclipse.jdt.launching.JavaRuntime;
import org.eclipse.jdt.launching.VMRunnerConfiguration;

import py4j.reflection.ReflectionUtil;
import py4j.reflection.RootClassLoadingStrategy;

public class PythonConnector {

	private ILaunch jvmLaunch;
	
	public PythonConnector() {
		// Required for letting Python instantiate the listener in Eclipse Plug-In setup.
		RootClassLoadingStrategy rmmClassLoader = new RootClassLoadingStrategy();
	    ReflectionUtil.setClassLoadingStrategy(rmmClassLoader);
	}

	public void run() {
		List<IProject> projects = getGeometryCodesProjects();
		IProject project = projects.get(0);
		IJavaProject javaProject = JavaCore.create(project);

		try {
			IVMInstall vm = JavaRuntime.getVMInstall(javaProject);
			if (vm == null)
				vm = JavaRuntime.getDefaultVMInstall();
			IVMRunner vmr = vm.getVMRunner(ILaunchManager.RUN_MODE);
			String[] cp = JavaRuntime.computeDefaultRuntimeClassPath(javaProject);
			VMRunnerConfiguration config = new VMRunnerConfiguration("Main", cp);
			jvmLaunch = new Launch(null, ILaunchManager.RUN_MODE, null);
			vmr.run(config, jvmLaunch, null);

			jvmLaunch.getProcesses()[0].getStreamsProxy().getOutputStreamMonitor().addListener(new IStreamListener() {
				@Override
				public void streamAppended(String text, IStreamMonitor monitor) {
					System.out.print("[Geometry Codes: Out]\t" + text);
				}
			});
			jvmLaunch.getProcesses()[0].getStreamsProxy().getErrorStreamMonitor().addListener(new IStreamListener() {
				@Override
				public void streamAppended(String text, IStreamMonitor monitor) {
					System.out.print("[Geometry Codes: Error]\t" + text);
				}
			});
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}
	
	private List<IProject> getGeometryCodesProjects() {
		List<IProject> projects = new LinkedList<>();
		for(IProject project : ResourcesPlugin.getWorkspace().getRoot().getProjects()) {
			try {
				if(true || project.hasNature("GeometryCodes")) {
					projects.add(project);
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}
		return projects;
	}
	
	private boolean isCurrentlyExecuting() {
		if(jvmLaunch == null)
			return false;
		return !jvmLaunch.isTerminated();
	}

}