package de.adrianhoff.geometrycodes.python;

import org.eclipse.ui.IStartup;
import org.osgi.framework.BundleContext;

import py4j.GatewayServer;

public class Startup extends org.eclipse.ui.plugin.AbstractUIPlugin implements IStartup {

	private GatewayServer server;

	@Override
	public void earlyStartup() {
		System.out.println("Starting...");
		server = new GatewayServer(new PythonConnector());
		server.start(false); // false = single-threaded
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		System.out.println("Stopping...");
		if(server != null)
			server.shutdown(true);
		super.stop(context);
	}

}
