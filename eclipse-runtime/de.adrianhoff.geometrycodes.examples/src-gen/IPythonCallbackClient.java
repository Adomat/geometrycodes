
public interface IPythonCallbackClient {
	public String callback(byte[] vertices, byte[] faces);
}
