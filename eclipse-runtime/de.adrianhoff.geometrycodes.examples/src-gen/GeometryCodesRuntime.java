import java.nio.ByteBuffer;

import py4j.GatewayServer;

public class GeometryCodesRuntime {
	
	public static void callbackToBlender() {
		GatewayServer callbackServer = null;
        try {
        	int javaPort = 25340;
        	int pythonPort = 25341;
        	callbackServer = new GatewayServer(null, javaPort, pythonPort,
        			GatewayServer.DEFAULT_CONNECT_TIMEOUT, GatewayServer.DEFAULT_READ_TIMEOUT,
        			null);
            callbackServer.start();
            
            IPythonCallbackClient hello = (IPythonCallbackClient) callbackServer.getPythonServerEntryPoint(new Class[] { IPythonCallbackClient.class });
            hello.callback(convertVerticesToBytes(getVertices()), convertFacesToBytes(getFaces()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
        	if(callbackServer != null) {
        		callbackServer.shutdown(true);
        	}
        }
	}
	
	
	
	/**
	 * Create byte array of face vertex indices.
	 * convert each face (array of integers) into a sequence of bytes,
	 * where each sequence is structured as:<br>
	 * - 1 byte : facePolygonCount (number of indices for this face)<br>
	 * - [facePolygonCount] bytes : indices (in the array of vertices)
	 */
	private static byte[] convertFacesToBytes(int[][] faces) {
		// size of final array byte unknown
		// as faces can have arbitrarily many vertices.
		// -> calculate it
		int numberOfIndices = 0;
		for(int[] face : faces)
			numberOfIndices += face.length;
		
		ByteBuffer buffer = ByteBuffer.allocate(4 * numberOfIndices + 4 * faces.length);
		for(int[] face : faces) {
			// TODO: performance boost opportunity: don't use an entire int here...
			buffer.putInt(face.length);
			for(int index : face) {
				buffer.putInt(index);
			}
		}
		return buffer.array();
	}
	
	private static byte[] convertVerticesToBytes(float[][] vertices) {
		// per vertex: 3 float à 4 byte
		ByteBuffer buffer = ByteBuffer.allocate(4 * 3 * vertices.length);
		for(float[] vertex : vertices) {
			buffer.putFloat(vertex[0]);
			buffer.putFloat(vertex[1]);
			buffer.putFloat(vertex[2]);
		}
		return buffer.array();
	}
	
	
	
	private static int[][] getFaces() {
		return new int[][] {
				new int[] {0, 1, 2},
				new int[] {1, 3, 2}};
	}
	
	private static float[][] getVertices() {
		return new float[][] {
				new float[] { -1, -1, randomlyOffset(0, 0.2f) },
				new float[] { 1, -1, randomlyOffset(0, 0.2f) },
				new float[] { -1, 1, randomlyOffset(0, 0.2f) },
				new float[] { 1, 1, randomlyOffset(0, 0.2f) }};
	}
	
	private static float randomlyOffset(float in, float variance) {
		return in + (float) (Math.random() * 2 * variance) - variance;
	}
	
	
	
//	private static int[] getFaces() {
//		return new int[] {
//				2, // how many faces?
//				0, 1, 2,
//				1, 3, 2};
//	}
//	
//	private static float[] getVertices() {
//		return new float[] {
//				-1, -1, 0,
//				1, -1, 0,
//				-1, 1, 0,
//				1, 1, 5};
//	}
	
}
