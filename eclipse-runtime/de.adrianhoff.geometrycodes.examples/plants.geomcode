import "basics/shapes.geomcode" //as shapes
import "materials.geomcode" //as materials



float plantDensity = 100
float plantBaseSize = 0.2
float plantIncrementHeight = 0.1

float zero = 0.0
vector rotate = vector(zero, zero, 45)

mesh soil = createCube(vector(zero, zero, 0))
soil.scale(vector(2, 1, 0.2))
soil.rotate(rotate)
soil.move(vector(0, 0, 1 - 0.1))

mesh plants = empty
soil.spawnPlants(0.3, 1)

output plants



iterator spawnPlants(float minHeight, float maxHeight) over each triangle t {
	if(t.normal.angleTo(vector(0, 0, 1)) > 45) {
		return nothing
	}
	// faces upwards
	
	for (float i from 0 to plantDensity) {
		vector randPos = sampleRandomPosition(t)
		spawnSinglePlant(randPos, minHeight + math.randomFloat(maxHeight - minHeight))
	}
}

void spawnSinglePlant(vector position, float height) {
	vector baseDir = math.randomVector(1, 1, 0).normalized * (1 + math.randomFloat()) * plantBaseSize / 4
	vector currentPos1 = position + baseDir
	vector currentPos2 = position - baseDir
	
	while(currentPos1.z < (position.z + height)) {
		float currentHeight = currentPos1.z - position.z + plantIncrementHeight
		
		vector nextPos1 = currentPos1 + vector(0, 0, 1) * currentHeight + (height-currentHeight) / height * baseDir
		vector nextPos2 = currentPos2 + vector(0, 0, 1) * currentHeight - (height-currentHeight) / height * baseDir
		
		mesh newQuad = createQuad(currentPos1, currentPos2, nextPos1, nextPos2)
//		newQuad.material = leaf
		plants += newQuad
		
		currentPos1 = nextPos1
		currentPos2 = nextPos2
	}
}

vector sampleRandomPosition(triangle t) {
	vector v0 = t.vertices(0)
	vector v1 = t.vertices(1)
	vector v2 = t.vertices(2)
	
	vector dir1 = v1 - v0
	vector dir2 = v2 - v0
	
	float rand1 = math.randomFloat()
	float rand2 = math.randomFloat()
	
	return v0 + rand1 * dir1 + rand2 * dir2
}
