@echo off

SET ADDON_NAME=geometry_codes
SET CONFIG_FILE=blender_install_addons_path.txt

if exist "%CONFIG_FILE%" (
	rem file does exist, all good
) else (
	echo.
	echo No configuration file found at %~dp0%CONFIG_FILE% ...
	echo Will create one and open default editor.
    echo C:\Users\USERNAME\AppData\Roaming\Blender Foundation\Blender\4.0\scripts\addons>%~dp0%CONFIG_FILE%
	start %~dp0%CONFIG_FILE%
	
	echo.
	echo Insert the correct path in your text editor, then continue...
	
	echo.
	pause
)

set /p BLENDER_INSTALL_ADDONS_FOLDER_PATH=<%CONFIG_FILE%

if exist "%BLENDER_INSTALL_ADDONS_FOLDER_PATH%" (
	rem path does exist, all good
	rem copy over Python scripts
	xcopy /y "%BLENDER_INSTALL_ADDONS_FOLDER_PATH%\%ADDON_NAME%\__init__.py" "%ADDON_NAME%"
	
	rem create the zipped addon
	tar.exe -a -c -f %ADDON_NAME%.zip %ADDON_NAME%
) else (
	echo.
	echo Registered Blender addons folder path not valid: %BLENDER_INSTALL_ADDONS_FOLDER_PATH%
	
	echo.
	pause
)