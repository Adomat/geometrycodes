import gc
import os
import sys
import traceback
import struct
import time
import threading

import bpy
from bl_ui.generic_ui_list import draw_ui_list

from .py4j.java_gateway import JavaGateway, CallbackServerParameters, JavaClass
from .py4j.protocol import (Py4JNetworkError, register_input_converter, ARRAY_TYPE)
from .py4j.clientserver import ClientServer, JavaParameters, PythonParameters

# from . import geometrycodes
# from .mesh_generation import generate_object


os.environ['PYSPARK_PYTHON'] = sys.executable
os.environ['PYSPARK_DRIVER_PYTHON'] = sys.executable

bl_info = {
    'name': 'Geometry Codes',
    'author': 'Adrian Hoff',
    'version': (0, 1),
    'blender': (4, 0, 0),
    'location': '3d View > Tool shelf',
    'description': 'TODO: description',
    'warning': '',
    'wiki_url': 'todo',
    'doc_url': 'todo',
    'tracker_url': '',
    'category': 'Mesh'
}


# class ListConverter(object):
#     def can_convert(self, object):
#         return hasattr2(object, "__iter__") and not isbytearray(object) and\
#             not ispython3bytestr(object) and not isinstance(object, basestring)
#
#     def convert(self, object, gateway_client):
#         ArrayList = JavaClass("java.util.ArrayList", gateway_client)
#         java_list = ArrayList()
#         for element in object:
#             java_list.add(element)
#         return java_list
#
#
# register_input_converter(ListConverter())


def register():
    class_register()
    bpy.types.Scene.my_list = bpy.props.CollectionProperty(type=TestPropertyGroup)
    bpy.types.Scene.my_list_active_index = bpy.props.IntProperty()


def unregister():
    class_unregister()
    del bpy.types.Scene.my_list
    del bpy.types.Scene.my_list_active_index


class TestPropertyGroup(bpy.types.PropertyGroup):
    test: bpy.props.FloatProperty()
    name: bpy.props.StringProperty()


class GeometryCodesPanel(bpy.types.Panel):
    """Creates a Panel in the scene context of the properties editor"""
    bl_label = "Geometry Codes"
    bl_idname = "SCENE_PT_GEOMETRY_CODES"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "scene"
    bl_icon = 'WORLD_DATA'

    def draw(self, context):
        layout = self.layout

        row = layout.row()
        row.label(text="Geometry Codes", icon='WORLD_DATA')

        draw_ui_list(
            layout,
            context,
            list_path="scene.my_list",
            active_index_path="scene.my_list_active_index",
            unique_id="my_list_id",
        )

        row = layout.row(align=True)
        row.scale_y = 2.0

        sub = row.row()
        sub.scale_x = 1.5
        sub.operator("mesh.geometry_codes_generate")

        row.operator("mesh.geometry_codes_disconnect_servers")
        row.enabled = not GeometryCodesGenerateOperator.operation_currently_executing

        disconnect_callback_gateway()


class MESH_UL_mylist(bpy.types.UIList):
    # Constants (flags)
    # Be careful not to shadow FILTER_ITEM (i.e. UIList().bitflag_filter_item)!
    # E.g. VGROUP_EMPTY = 1 << 0

    # Custom properties, saved with .blend file. E.g.
    # use_filter_empty: bpy.props.BoolProperty(
    #     name="Filter Empty", default=False, options=set(),
    #     description="Whether to filter empty vertex groups",
    # )

    # Called for each drawn item.
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index, flt_flag):
        # 'DEFAULT' and 'COMPACT' layout types should usually use the same draw code.
        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            pass
        # 'GRID' layout type should be as compact as possible (typically a single icon!).
        elif self.layout_type == 'GRID':
            pass

    # Called once to draw filtering/reordering options.
    def draw_filter(self, context, layout):
        # Nothing much to say here, it's usual UI code...
        pass

    # Called once to filter/reorder items.
    def filter_items(self, context, data, propname):
        # This function gets the collection property (as the usual tuple (data, propname)), and must return two lists:
        # * The first one is for filtering, it must contain 32bit integers were self.bitflag_filter_item marks the
        #   matching item as filtered (i.e. to be shown), and 31 other bits are free for custom needs. Here we use the
        #   first one to mark VGROUP_EMPTY.
        # * The second one is for reordering, it must return a list containing the new indices of the items (which
        #   gives us a mapping org_idx -> new_idx).
        # Please note that the default UI_UL_list defines helper functions for common tasks (see its doc for more info).
        # If you do not make filtering and/or ordering, return empty list(s) (this will be more efficient than
        # returning full lists doing nothing!).

        # Default return values.
        flt_flags = []
        flt_neworder = []

        # Do filtering/reordering here...

        return flt_flags, flt_neworder


class GeometryCodesDisconnectServerOperator(bpy.types.Operator):
    """
        Actively disconnect from the Geometry Codes Eclipse instance
    """                                                     # Use this as a tooltip for menu items and buttons.
    bl_idname = "mesh.geometry_codes_disconnect_servers"    # Unique identifier for buttons and menu items to reference.
    bl_label = "Disconnect"                                 # Display name in the interface.
    bl_icon = 'WORLD_DATA'
    bl_options = {'REGISTER', 'UNDO'}                      # Enable undo for the operator.

    def execute(self, context):
        return disconnect_callback_gateway()


def disconnect_callback_gateway():
    if GeometryCodesGenerateOperator.callback_gateway is None:
        return {'CANCELLED'}
    if GeometryCodesGenerateOperator.operation_currently_executing:
        return {'CANCELLED'}

    print("Resetting callback client")
    cleanup_gateway(GeometryCodesGenerateOperator.callback_gateway, False)
    GeometryCodesGenerateOperator.callback_gateway = None
    gc.collect()
    return {'FINISHED'}


class GeometryCodesGenerateOperator(bpy.types.Operator):
    """
        Generate Geometry from Geometry Codes Scripts.
        Connects to Eclipse, runs generation, then populates the Blender scene with results.
    """                                         # Use this as a tooltip for menu items and buttons.
    bl_idname = "mesh.geometry_codes_generate"  # Unique identifier for buttons and menu items to reference.
    bl_label = "Generate Geometry"              # Display name in the interface.
    bl_icon = 'WORLD_DATA'
    bl_options = {'REGISTER', 'UNDO'}           # Enable undo for the operator.

    callback_gateway = None
    operation_currently_executing = False

    def execute(self, context):
        if GeometryCodesGenerateOperator.operation_currently_executing:
            print("Cannot execute operation because it is already running...")
            return {'CANCELLED'}
        GeometryCodesGenerateOperator.operation_currently_executing = True

        try:
            print("Calling Eclipse...")
            gateway_to_python = JavaGateway()
            gateway_to_python.entry_point.run()
            cleanup_gateway(gateway_to_python, True)

            entry_point = CallbackClient()
            if GeometryCodesGenerateOperator.callback_gateway is None:
                GeometryCodesGenerateOperator.callback_gateway = ClientServer(
                    java_parameters=JavaParameters(port=25340),         # Java client config
                    python_parameters=PythonParameters(port=25341),     # Python server config
                    python_server_entry_point=entry_point)

            return {'FINISHED'}
        except Py4JNetworkError:
            print(traceback.format_exc())
            GeometryCodesGenerateOperator.operation_currently_executing = False
            return {'CANCELLED'}


def cleanup_gateway(gateway: JavaGateway, keep_callback_server=False):
    gateway.close(keep_callback_server, True)
    gateway.detach(gateway.entry_point)
    gc.collect()


class CallbackClient(object):

    def callback(self, vertices=None, faces=None):
        # print('')
        # print('Vertices:')
        # print(vertices)
        # print(type(vertices))
        # print(len(vertices))
        # print('Faces:')
        # print(faces)
        # print(type(faces))
        # print(len(faces))
        # print('')

        generate_stuff(vertices, faces)

        # After a short delay, make the operation available again
        time.sleep(1)
        GeometryCodesGenerateOperator.operation_currently_executing = False

        print("Returning...")
        return "SUCCESS"

    class Java:
        implements = ["IPythonCallbackClient"]


def generate_stuff(vertices_as_bytes, faces_as_bytes):
    python_vertices = convert_vertices_as_bytes_to_float_multi_list(vertices_as_bytes)
    python_faces = convert_faces_as_bytes_to_float_multi_list(faces_as_bytes)

    # print('Vertices:')
    # for vertex in python_vertices:
    #     print(vertex)
    # print('Faces:')
    # for face in python_faces:
    #     print(face)
    # print('')

    print("Fetched {} vertices and {} faces.\n".format(len(python_vertices), len(python_faces)))

    if len(python_vertices) == 0:
        print("Error: Empty mesh")
        return {'CANCELLED'}

    collection = get_collection('Geometry Codes')
    generated_object = generate_object("Generated Object", "Mesh", False,
                                       python_vertices, [], python_faces)
    collection.objects.link(generated_object)


def convert_vertices_as_bytes_to_float_multi_list(vertices_as_bytes):
    vertices_as_multi_float_list = []
    for i in range(0, len(vertices_as_bytes), 3*4):
        vertices_as_multi_float_list.append(struct.unpack('>3f', vertices_as_bytes[i:i+3*4]))
    return vertices_as_multi_float_list


def convert_faces_as_bytes_to_float_multi_list(faces_as_bytes):
    faces_as_multi_float_list = []
    i = 0
    while i < len(faces_as_bytes):
        number_of_indices = struct.unpack('>I', faces_as_bytes[i+0:i+4])[0]
        new_face = struct.unpack('>{}I'.format(number_of_indices), faces_as_bytes[4+i:4+i+number_of_indices*4])
        faces_as_multi_float_list.append(new_face)
        i += 4 + number_of_indices * 4

    return faces_as_multi_float_list


def generate_object(object_name, mesh_name, shade_smooth, vertices, edges, faces):
    clear_old_data(object_name, mesh_name)

    mesh = bpy.data.meshes.new(mesh_name)
    mesh.from_pydata(vertices, edges, faces)
    if shade_smooth:
        mesh.shade_smooth()
    mesh.update()

    # make object from mesh
    return bpy.data.objects.new(object_name, mesh)


def get_collection(name):
    collection = None
    # search in existing collections
    for existing_collection in bpy.data.collections:
        if existing_collection.name == name:
            collection = existing_collection

    # if no collection exists, create one
    if collection is None:
        collection = bpy.data.collections.new(name)
        bpy.context.scene.collection.children.link(collection)

    return collection


def clear_old_data(object_name, mesh_name):
    # clear old objects
    for existing_object in bpy.data.objects:
        if existing_object.name == object_name:
            bpy.data.objects.remove(existing_object)
            break
    # clear old meshes
    # TODO: not sure if this is necessary - or even bad??
    for existing_mesh in bpy.data.meshes:
        if existing_mesh.name == mesh_name:
            bpy.data.meshes.remove(existing_mesh)
            break


class_register, class_unregister = bpy.utils.register_classes_factory([
    GeometryCodesGenerateOperator,
    GeometryCodesDisconnectServerOperator,
    GeometryCodesPanel,
    MESH_UL_mylist,
    TestPropertyGroup
])